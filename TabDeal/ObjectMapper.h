//
//  ObjectMapper.h
//  TabDeal
//
//  Created by Sreeraj VR on 25/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "UserBasketData.h"
#import "AboutTabDealData.h"
#import "UserFavouriteData.h"
#import "UserPreviousOrder.h"
#import "OrderDetailsList.h"
#import "Userinformation.h"
#import "BankInformation.h"
#import "SearchResultData.h"
#import "SubmittedData.h"
#import "userNotificationListData.h"
#import "UserSellInformation.h"

@interface ObjectMapper : NSObject

-(userNotificationListData *)getUserNotificationListFromDictionary:(NSMutableDictionary *)userPreOrderDictionary;

-(BankInformation *)getBankinformationFromDictionary:(NSMutableDictionary *)userDictionary;

-(Userinformation *)getUserinformationFromDictionary:(NSMutableDictionary *)userDictionary;

-(User *)getUserObjectFromDictionary:(NSMutableDictionary *)userDictionary;

-(UserBasketData *)getUserBasketFromDictionary:(NSMutableDictionary *)userDictionary;

-(AboutTabDealData *) getAboutUsPageDetailFromDictionary:(NSMutableDictionary *)pageDetailDictionary;

-(UserFavouriteData *)getUserFavouriteFromDictionary:(NSMutableDictionary *)userFavouriteDictionary;

-(UserPreviousOrder *)getUserPreviousOrderFromDictionary:(NSMutableDictionary *)userPreOrderDictionary;

-(OrderDetailsList *)getUserOrderListDetailsFromDictionary:(NSMutableDictionary *)userOrderListDetailsDictionary;

-(SearchResultData *)getSearchResultsFromDictionary:(NSMutableDictionary *)searchResultsDictionary;

-(SubmittedData *)getSubmittedResultsFromDictionary:(NSMutableDictionary *)submittedDictionary;

-(UserSellInformation *)getSellInformationFromDictionary:(NSMutableDictionary *)submittedDictionary;

@end
