//
//  SearchResultViewController.m
//  TabDeal
//
//  Created by satheesh on 10/14/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "SearchResultViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SearchResultData.h"

#define OFFSETHEIGHT 10
#define OFFSETWIDTH 10

@interface SearchResultViewController (){
    NSArray *sortMenuListNameArray;
    int sortlistselectednum;
    SearchResultCell * CellView;
}

@end

@implementation SearchResultViewController
@synthesize searchResultArray;

-(void)viewWillAppear:(BOOL)animated{
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:1];
    if (ApplicationDelegate.innerViewCount.intValue==0) {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else{
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    NSLog(@"%@",self.searchDictionary);
    [self loadScroll];
}

-(void)viewDidAppear:(BOOL)animated{
    self.headingLbl.text = localize(@"SEARCH RESULTS :");
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xibLoading];
    
    [self hideSortMenu];
    
    self.sortMenuTable.scrollEnabled = NO;
    sortlistselectednum = 0;
    sortMenuListNameArray = [[NSArray alloc] initWithObjects:localize(@"Price (High to Low)"),localize(@"Price (Low to High)"),localize(@"New to Old"),localize(@"Old to New"), nil];

}

-(NSMutableDictionary *)getUserLanguageData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    NSLog(@"%@",[prefs objectForKey:@"User Id"]);
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]]) {
        [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
        //[postDic setObject:kENGLISH forKey:@"lang_key"];
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [postDic setObject:kARABIC forKey:@"lang_key"];
        }
        else{
            [postDic setObject:kENGLISH forKey:@"lang_key"];
        }
    }
    return postDic;
}


-(void)loadScroll {
    
    if (searchResultArray.count == 0) {
        self.warningBackView.hidden = NO;
        self.warningLbl.font = [UIFont fontWithName:@"Ebrima" size:15];
        self.warningLbl.hidden = NO;
    }
    for (UIView *sub in self.mainScrollView.subviews) {
        [sub removeFromSuperview];
    }
    
    [self.mainScrollView setContentOffset:CGPointZero];
    
    float setYvalue = 0.0f;
    float scrollHeight = 0.0f;
    NSLog(@"%@",searchResultArray);
    for (int i = 0; i<searchResultArray.count; i++) {
        CellView = [[SearchResultCell alloc]init];
        
        CellView.delegateSearchResultItem = self;
        CellView.btnView.tag = i;
        
        SearchResultData *searchObj = [ApplicationDelegate.mapper getSearchResultsFromDictionary:[searchResultArray objectAtIndex:i]];

        CellView.frame = CGRectMake(OFFSETWIDTH, setYvalue + OFFSETHEIGHT , CellView.frame.size.width, CellView.frame.size.height);
        
        setYvalue = CellView.frame.origin.y + CellView.frame.size.height;
        scrollHeight = CellView.frame.origin.y + CellView.frame.size.height;
        NSURL * sampurl = [[NSURL alloc]init];
        sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:searchObj.Image]];
        
        [CellView.iconView sd_setImageWithURL:sampurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
        CellView.brandName.text=searchObj.Brandname;
        CellView.itemName.text=searchObj.ItemName;
        
        if ([searchObj.isOnSale isEqualToNumber:[NSNumber numberWithInt:1]]) {
            CellView.onSaleBadge.hidden = NO;
        }
        
        NewDealsData *newDealsItem = [[NewDealsData alloc]init];
        //newDealsItem.Description = searchObj
        //newDealsItem.Icon = [[self.DealsMainAry objectAtIndex:i]objectForKey:@"Icon"];
        newDealsItem.Id = searchObj.DealId;
        //newDealsItem.Image = [[self.DealsMainAry objectAtIndex:i]objectForKey:@"Image"];
        //newDealsItem.Name = [[self.DealsMainAry objectAtIndex:i]objectForKey:@"Name"];
        NSLog(@"%@",newDealsItem);
        NSLog(@"%@",newDealsItem.Icon);
        CellView.dealsNewObj=newDealsItem;
        
        
         [ApplicationDelegate loadMarqueeLabelWithText:CellView.itemName.text Font:CellView.itemName.font InPlaceOfLabel:CellView.itemName];
         [ApplicationDelegate loadMarqueeLabelWithText:CellView.brandName.text Font:CellView.brandName.font InPlaceOfLabel:CellView.brandName];
        CellView.iconView.layer.cornerRadius = 3.0;
        CellView.iconView.layer.masksToBounds = YES;
        [self.mainScrollView addSubview:CellView];
    }
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width,  (scrollHeight + OFFSETHEIGHT));
    
}

-(void) searchResultItemDidClicked:(SearchResultCell *)searchResultCategoryItem
{
    NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
    newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
    
    
    newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%ld",(long)searchResultCategoryItem.btnView.tag];
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:searchResultCategoryItem.dealsNewObj.Id forKey:@"TrackId"];
    [actionTracking synchronize];
    newDealsDetailVC.productId = searchResultCategoryItem.dealsNewObj.Id;
    newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%ld",(long)searchResultCategoryItem.btnView.tag];
    
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return sortMenuListNameArray.count;
    //return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 42;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        // Configure the cell...
        
        cell.selectionStyle = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        //    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"more_divider.png"]];
        
        cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"more_divider.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        
    }
    
    UIImageView *iconImgView;
    UILabel *nameLabel;
    
    UIImageView * radioButton;
    
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, (tableView.frame.size.width-58), 25)];
        radioButton = [[UIImageView alloc]initWithFrame:CGRectMake(nameLabel.frame.origin.x+nameLabel.frame.size.width + 5, nameLabel.frame.origin.y, 25, 25)];
    }
    else{
        radioButton = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 25, 25)];
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(radioButton.frame.origin.x+radioButton.frame.size.width + 5, radioButton.frame.origin.y, (tableView.frame.size.width-58), 25)];
    }
    
    [cell addSubview:radioButton];
    if (sortlistselectednum == indexPath.row) {
        radioButton.image = [UIImage imageNamed:@"radio_selected"];
    } else {
        radioButton.image = [UIImage imageNamed:@"radio_unselected"];
    }
    nameLabel.textColor = [UIColor colorWithHex:kUNIVERSAL_THEME_COLOR];
    iconImgView.backgroundColor = [UIColor clearColor];
    iconImgView.tag=50;
    nameLabel.tag=60;
    [cell addSubview:iconImgView];
    [cell addSubview:nameLabel];
    
    UILabel* cellLabel = (UILabel*)[cell viewWithTag: 60];
    [cellLabel setBackgroundColor:[UIColor clearColor]];
    cellLabel.text = [self getSortTabItemTitle:indexPath.row];
    [cellLabel setFont:EBRI_NORMAL_FONT(18)];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (sortlistselectednum == indexPath.row) {
        [self hideSortMenu];
    } else {
        sortlistselectednum = indexPath.row;
        NSLog(@"%d",sortlistselectednum);
        [self.sortMenuTable reloadData];
        [self hideSortMenu];
        if (indexPath.row == 0) {
            [self.searchDictionary setObject:@"HTL" forKey:@"SortType"];
        } else if (indexPath.row == 1){
            [self.searchDictionary setObject:@"LTH" forKey:@"SortType"];
        }else if (indexPath.row == 2){
            [self.searchDictionary setObject:@"NTO" forKey:@"SortType"];
        }else if (indexPath.row == 3){
            [self.searchDictionary setObject:@"OTN" forKey:@"SortType"];
        }
        self.sortBtn.selected = !self.sortBtn.selected;
        [self loadSearchScroll];
    }
    
//    moreItemSelectedIndex=indexPath.row;
//    moreMenuItemSelectionFlag = YES;
//    
//    [self selectMoreMenuItem:indexPath.row];
//    [self.moreMenuTable reloadData];
}

- (IBAction)sortBtnAction:(UIButton *)sender {
    self.sortBtn.selected = !self.sortBtn.selected;
    if ([self.sortByView superview])
    {
        [self hideSortMenu];
    }
    else
    {
        [self showSortMenu];
    }
}

-(NSString *)getSortTabItemTitle:(NSUInteger)itemIndex
{
    NSString *titleString=@"";
    titleString = [sortMenuListNameArray objectAtIndex:itemIndex];
    
    return titleString;
}

-(void)showSortMenu
{
    //[self.homeTabContainerView setUserInteractionEnabled:NO];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        float xFact;
        
        float xPos = - self.sortByView.frame.size.width;
        
        NSLog(@"%f",xPos);
        
        xPos = self.view.frame.origin.x - self.sortByView.frame.size.width;
        
        NSLog(@"%f",xPos);
        
        self.sortByView.frame = CGRectMake(xPos, self.lineSeperatorView.frame.origin.y + self.lineSeperatorView.frame.size.height, self.sortByView.frame.size.width, self.sortByView.frame.size.height);
        
        
        xFact = 0;
        
        //xFact=self.view.frame.size.width-self.sortByView.frame.size.width;
        
        
        self.sortMenuTable.delegate=self;
        self.sortMenuTable.dataSource=self;
        
        [self.view insertSubview:self.sortByView atIndex:10];
        
        
        [UIView beginAnimations:@"animateView" context:nil];
        [UIView setAnimationDuration:0.5];
        CGRect viewFrame = [self.sortByView frame];
        viewFrame.origin.x  = xFact;
        self.sortByView.frame = viewFrame;
        
        [UIView commitAnimations];
        [self.sortMenuTable reloadData];
    }
    else{
        float xFact;
        
        float xPos = - self.sortByView.frame.size.width;
        
        NSLog(@"%f",xPos);
        
        xPos = self.view.frame.size.width;
        
        NSLog(@"%f",xPos);
        
        self.sortByView.frame = CGRectMake(xPos, self.lineSeperatorView.frame.origin.y + self.lineSeperatorView.frame.size.height, self.sortByView.frame.size.width, self.sortByView.frame.size.height);
        
        
        xFact = 0;
        
        xFact=self.view.frame.size.width-self.sortByView.frame.size.width;
        
        
        self.sortMenuTable.delegate=self;
        self.sortMenuTable.dataSource=self;
        
        [self.view insertSubview:self.sortByView atIndex:10];
        
        
        [UIView beginAnimations:@"animateView" context:nil];
        [UIView setAnimationDuration:0.5];
        CGRect viewFrame = [self.sortByView frame];
        viewFrame.origin.x  = xFact;
        self.sortByView.frame = viewFrame;
        
        [UIView commitAnimations];
        [self.sortMenuTable reloadData];
    }
}
-(void)hideSortMenu
{
    //[self.homeTabContainerView setUserInteractionEnabled:YES];
    
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [UIView beginAnimations:@"animateView" context:nil];
        CGRect viewFrame = [self.sortByView frame];
        
        viewFrame.origin.x = - self.sortByView.frame.size.width;
        
//        viewFrame.origin.x  = self.view.frame.size.width;
        
        viewFrame.origin.x  = - self.sortByView.frame.size.width;
        
        [UIView setAnimationDuration:0.5];
        self.sortByView.frame = viewFrame;
        [UIView commitAnimations];
        [self performSelector:@selector(removeSortMenu) withObject:nil afterDelay:0.5];
    }
    else{
        [UIView beginAnimations:@"animateView" context:nil];
        CGRect viewFrame = [self.sortByView frame];
        
        viewFrame.origin.x = - self.sortByView.frame.size.width;
        
        viewFrame.origin.x  = self.view.frame.size.width;
        
        [UIView setAnimationDuration:0.5];
        self.sortByView.frame = viewFrame;
        [UIView commitAnimations];
        [self performSelector:@selector(removeSortMenu) withObject:nil afterDelay:0.5];
    }
}
-(void)removeSortMenu
{
    [self.sortByView removeFromSuperview];
}

-(void)loadSearchScroll
{
    //NSDictionary *requestDictionary = @{@"Gender":self.getGenderArray,@"Type": self.getItemArray,@"Brands": self.getBrandsArray,@"Status": self.getStatusArray,@"SortType": @"HTL",@"lang_key": kENGLISH};
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine search:self.searchDictionary CompletionHandler:^(NSMutableArray *responseArray) {
        
        self.searchResultArray = responseArray;
        NSLog(@"%@",searchResultArray);
        [self loadScroll];
        
        [ApplicationDelegate removeProgressHUD];
        
        
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
    }
     ];
}

-(void)xibLoading
{
    NSString *nibName = @"SearchResultViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
    
    
}

@end
