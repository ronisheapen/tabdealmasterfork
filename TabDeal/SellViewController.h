//
//  SellViewController.h
//  TabDeal
//
//  Created by satheesh on 9/22/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface SellViewController : UIViewController<UINavigationControllerDelegate ,UIActionSheetDelegate, UIImagePickerControllerDelegate,ListSelectionProtocol,UITextFieldDelegate>
{
    CGRect uploadImageBtnFrame, uploadImageFrame;
    NSMutableArray * uploadImageArray;
    NSMutableArray * uploadImageViewArray;
    NSMutableArray * uploadImageBtnArray;
    UIImage * certificateImage;
    UIImage * ReceiptImage;
    UIImageView * certificateImageView;
    UIImageView * ReceiptImageView;
    int uploadButtonTag;
    int uploadPhotoCount;
    int dropDownTag;
    
    NSMutableArray * brandNameArray;
    NSMutableArray * brandCodeArray;
    NSString * brandId;
    
    NSMutableArray * countryNameArray;
    NSMutableArray * countryCodeArray;
    
    NSMutableArray * yearArray;
    NSMutableArray * itemNameArray;
    NSMutableArray * currencyNameArray;
    
    NSString * civilId;
    
    NSUserDefaults * actionStatus;
    
    NSUserDefaults * actionTracking;
}

@property (strong, nonatomic)NSString * contextString;
@property (strong, nonatomic)NSString * refferenceString;
@property (strong, nonatomic)NSString * contextStringforSubmission;
@property (strong, nonatomic)NSMutableArray * brandCodeAndName;
@property (strong, nonatomic)NSMutableArray * countryCodeAndName;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *sellBackView;
@property (weak, nonatomic) IBOutlet UIView *uploadBackView;
@property (weak, nonatomic) IBOutlet UIImageView *uploadWhiteBackView;
@property (weak, nonatomic) IBOutlet UIButton *uploadfrmGallCamBtn;
- (IBAction)uploadfrmGallCamBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *uploadImageScrollView;
@property (strong, nonatomic) IBOutlet UIView *uploadImageDetailBackView;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImageDetailImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)deleteBtnActn:(UIButton *)sender;
- (IBAction)closeBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *uploadCertificateBtn;
- (IBAction)uploadCertificateBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *uploadReceiptBtn;
@property (weak, nonatomic) IBOutlet UIButton *uploadCertificateImgBtn;
- (IBAction)uploadCertificateImgBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *uploadReceiptImgBtn;
- (IBAction)uploadReceiptImgBtnActn:(UIButton *)sender;

- (IBAction)uploadReceiptBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
- (IBAction)confirmBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIImageView *uparrowBrand;
@property (weak, nonatomic) IBOutlet UIImageView *downarrowBrand;
@property (weak, nonatomic) IBOutlet UIButton *dropDwnBrandBtn;
- (IBAction)dropDwnBrandBtnActn:(UIButton *)sender;
@property (strong, nonatomic) DropDownView *brandDropDownObj;
@property (weak, nonatomic) IBOutlet UITextField *brandTextView;

@property (weak, nonatomic) IBOutlet UIImageView *uparrowItem;
@property (weak, nonatomic) IBOutlet UIImageView *downarrowItem;
@property (weak, nonatomic) IBOutlet UIButton *dropDwnItemBtn;
- (IBAction)dropDwnItemBtnActn:(UIButton *)sender;
@property (strong, nonatomic) DropDownView *itemDropDownObj;
@property (weak, nonatomic) IBOutlet UITextField *itemTextView;

@property (weak, nonatomic) IBOutlet UIImageView *uparrowYear;
@property (weak, nonatomic) IBOutlet UIImageView *downarrowYear;
@property (weak, nonatomic) IBOutlet UIButton *dropDwnYearBtn;
- (IBAction)dropDwnYearBtnActn:(UIButton *)sender;
@property (strong, nonatomic) DropDownView *YearDropDownObj;
@property (weak, nonatomic) IBOutlet UITextField *yearTextView;

@property (weak, nonatomic) IBOutlet UIImageView *uparrowCurrency;
@property (weak, nonatomic) IBOutlet UIImageView *downarrowCurrency;
@property (weak, nonatomic) IBOutlet UIButton *dropDwnCurrencyBtn;
- (IBAction)dropDwnCurrencyBtnActn:(UIButton *)sender;
@property (strong, nonatomic) DropDownView *currencyDropDownObj;
@property (weak, nonatomic) IBOutlet UITextField *currencyTextView;

@property (weak, nonatomic) IBOutlet UIImageView *uparrowCountry;
@property (weak, nonatomic) IBOutlet UIImageView *downarrowCountry;
@property (weak, nonatomic) IBOutlet UIButton *dropDwnCountryBtn;
- (IBAction)dropDwnCountryBtnActn:(UIButton *)sender;
@property (strong, nonatomic) DropDownView *countryDropDownObj;
@property (weak, nonatomic) IBOutlet UITextField *countryTextView;
- (IBAction)statusBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *statusNewBtn;
@property (weak, nonatomic) IBOutlet UIButton *statusUsedBtn;

@property(strong, nonatomic)NSMutableArray * ProductImageArray;
@property (weak, nonatomic) IBOutlet UITextField *itemPriceTextView;
@property (weak, nonatomic) IBOutlet UILabel *headdingLbl;
@property (weak, nonatomic) IBOutlet UILabel *selectItemStsLbl;
@property (weak, nonatomic) IBOutlet UILabel *lblNew;
@property (weak, nonatomic) IBOutlet UILabel *usedLbl;
@property (weak, nonatomic) IBOutlet UILabel *uploadimageHeaddingLbl;
@property (weak, nonatomic) IBOutlet UILabel *uploadCertificateLbl;
@property (weak, nonatomic) IBOutlet UILabel *uploadReceiptLbl;
@property (weak, nonatomic) IBOutlet UILabel *warningmainLbl;

@end
