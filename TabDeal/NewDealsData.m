//
//  NewDealsData.m
//  TabDeal
//
//  Created by satheesh on 9/29/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "NewDealsData.h"

@implementation NewDealsData
@synthesize Description;
@synthesize Icon;
@synthesize Id;
@synthesize Image;
@synthesize Name;
@synthesize isSoldOut;
@synthesize isOnSale;
@end
