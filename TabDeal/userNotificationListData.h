//
//  userNotificationListData.h
//  TabDeal
//
//  Created by satheesh on 11/27/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface userNotificationListData : NSObject

@property(nonatomic,retain)NSString *image;
@property(nonatomic,retain)NSString *message;
@property(nonatomic,retain)NSString *date;
@property(nonatomic,retain)NSString *status;
@property(nonatomic,retain)NSString *referenceId;
@property(nonatomic,retain)NSString *typeId;
@property(nonatomic,retain)NSString *messageId;

@end
