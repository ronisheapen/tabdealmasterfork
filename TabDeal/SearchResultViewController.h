//
//  SearchResultViewController.h
//  TabDeal
//
//  Created by satheesh on 10/14/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchResultCell.h"
#import "NewDealsDetailsViewController.h"

@interface SearchResultViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SearchResultItemDelegate>{
    NSUserDefaults * actionTracking;
}
@property (strong,nonatomic)NSMutableArray * resultMainAry;
@property (strong,nonatomic)NSMutableArray * searchResultArray;
@property (strong, nonatomic) IBOutlet UIView *sortByView;
- (IBAction)sortBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *sortBtn;
@property (weak, nonatomic) IBOutlet UITableView *sortMenuTable;
@property (weak, nonatomic) IBOutlet UIImageView *lineSeperatorView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (strong,nonatomic)NSMutableDictionary * searchDictionary;
@property (weak, nonatomic) IBOutlet UILabel *headingLbl;
@property (weak, nonatomic) IBOutlet UIImageView *warningBackView;
@property (weak, nonatomic) IBOutlet UILabel *warningLbl;
@end
