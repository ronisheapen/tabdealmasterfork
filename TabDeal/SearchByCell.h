//
//  SearchByCell.h
//  TabDeal
//
//  Created by satheesh on 10/13/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchByCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Name;
@property (weak, nonatomic) IBOutlet UIImageView *tickImage;

@end
