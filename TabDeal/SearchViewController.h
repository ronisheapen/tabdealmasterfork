//
//  SearchViewController.h
//  TabDeal
//
//  Created by satheesh on 9/22/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    CGRect genderFrame,itemTypeFrame,branchFrame,statusFrame,submitFrame;
    CGRect genderCngFrame,itemTypeCngFrame,branchCngFrame,statusCngFrame,submitCngFrame;
    NSMutableArray * genderContentArray, *itemTypeContentArray,* brandsContentArray, * statusContentArray;
    NSMutableArray * brandNameArray;
    NSMutableArray * brandCodeArray;
    NSUserDefaults * actionTracking;
}
@property(strong, nonatomic)NSMutableArray * genderContentArray;
@property(strong, nonatomic)NSMutableArray * itemTypeContentArray;
@property(strong, nonatomic)NSMutableArray * brandsContentArray;
@property(strong, nonatomic)NSMutableArray * brandNameArray;
@property(strong, nonatomic)NSMutableArray * brandCodeArray;
@property(strong, nonatomic)NSMutableArray * statusContentArray;

@property(strong, nonatomic)NSMutableArray * genderTickArray;
@property(strong, nonatomic)NSMutableArray * itemTypeTickArray;
@property(strong, nonatomic)NSMutableArray * brandsTickArray;
@property(strong, nonatomic)NSMutableArray * statusTickArray;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIButton *genderBtn;
- (IBAction)genderBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *itemTypeBtn;
- (IBAction)itemTypeBtnActn:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UIButton *statusBtn;
- (IBAction)statusBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtnActn;
@property (weak, nonatomic) IBOutlet UITableView *genderTableView;
@property (weak, nonatomic) IBOutlet UITableView *itemTableView;
@property (weak, nonatomic) IBOutlet UITableView *statusTableView;
@property (weak, nonatomic) IBOutlet UITableView *brandTableView;
@property (weak, nonatomic) IBOutlet UIButton *brandBtn;
- (IBAction)brandBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *genderLbl;
@property (weak, nonatomic) IBOutlet UILabel *itemTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *brandsLbl;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;

@property (weak, nonatomic) IBOutlet UILabel *genderStsLbl;
@property (weak, nonatomic) IBOutlet UILabel *itemTypeStsLbl;
@property (weak, nonatomic) IBOutlet UILabel *brandsStsLbl;
@property (weak, nonatomic) IBOutlet UILabel *statusStsLbl;

@property (weak, nonatomic) IBOutlet UILabel *searchByHeading;
@end
