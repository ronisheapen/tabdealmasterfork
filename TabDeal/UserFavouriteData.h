//
//  UserFavouriteData.h
//  TabDeal
//
//  Created by Sreeraj VR on 20/10/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserFavouriteData : NSObject

@property(nonatomic,retain)NSString *brand;
@property(nonatomic,retain)NSString *image;
@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSString *Dealid;
@property(nonatomic,retain)NSString *size;

@end
