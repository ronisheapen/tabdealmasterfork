//
//  CreateNewAccountViewController.h
//  TabDeal
//
//  Created by satheesh on 9/17/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

@interface CreateNewAccountViewController : UIViewController<UITextFieldDelegate>{
    NSMutableURLRequest * theRequest;
    NSURL * url;
    UIAlertView * alrtView;
    CGRect scrollFrame;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UILabel *createNewAccountHeadingLbl;
@property (weak, nonatomic) IBOutlet UIImageView *createNewBackView;
@property (weak, nonatomic) IBOutlet UIImageView *firstBackView;
@property (weak, nonatomic) IBOutlet UIImageView *lastBackView;
@property (weak, nonatomic) IBOutlet UIImageView *emailBackView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordBackView;
@property (weak, nonatomic) IBOutlet UIImageView *confirmPasswordBackView;
@property (weak, nonatomic) IBOutlet UIImageView *mobileNumBackView;
@property (weak, nonatomic) IBOutlet UIButton *SignUpBttn;
- (IBAction)signUpBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *backBttn;
- (IBAction)backBttnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextView;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextView;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassTextView;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTextView;
@property (weak, nonatomic) IBOutlet UITextField *codeTextView;

@property (strong , nonatomic)NSURLConnection * connection;
@property( strong , nonatomic)NSMutableData * receivedData;
- (IBAction)textViewDidSelActn:(UITextField *)sender;
- (IBAction)languageSelection:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *signUpLbl;
@property (weak, nonatomic) IBOutlet UIButton *languageBtn;
@end
