//
//  FavouriteItemsViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewDealsDetailsViewController.h"
#import "MyFavoriteCell.h"


@interface FavouriteItemsViewController : UIViewController<FavoriteItemDelegate>{
    NSUserDefaults * actionTracking;
}

@property (strong, nonatomic) IBOutlet UIScrollView *FavouriteScroll;

@property (strong, nonatomic) NSMutableArray * FavouriteDataArray;

@property (weak, nonatomic) IBOutlet UIImageView *warningBackView;
@property (weak, nonatomic) IBOutlet UILabel *warningLbl;
@property (weak, nonatomic) IBOutlet UILabel *headingLbl;

@end
