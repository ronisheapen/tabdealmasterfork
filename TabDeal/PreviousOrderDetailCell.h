//
//  PreviousOrderDetailCell.h
//  TabDeal
//
//  Created by satheesh on 10/8/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviousOrderDetailCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *brandName;

@property (strong, nonatomic) IBOutlet UILabel *itemName;
@property (strong, nonatomic) IBOutlet UILabel *quantity;

@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UILabel *quantityLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@end
