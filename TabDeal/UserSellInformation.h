//
//  UserSellInformation.h
//  TabDeal
//
//  Created by satheesh on 11/17/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserSellInformation : NSObject
@property(nonatomic,retain)NSString *authImages;
@property(nonatomic,retain)NSString *brandId;
@property(nonatomic,retain)NSString *country;
@property(nonatomic,retain)NSString *currency_code;
@property(nonatomic,retain)NSString *idProof;
@property(nonatomic,retain)NSMutableArray *productImageCode;
@property(nonatomic,retain)NSString *rate;
@property(nonatomic,retain)NSString *reciept;
@property(nonatomic,retain)NSString *status;
@property(nonatomic,retain)NSString *type;
@property(nonatomic,retain)NSString *userId;
@property(nonatomic,retain)NSString *yearOfPurchase;
@property(nonatomic,retain)NSString *lang_key;
@end
