//
//  NewDealsCell.h
//  TabDeal
//
//  Created by Sreeraj VR on 29/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewDealsData.h"

@protocol NewDealItemDelegate;

@interface NewDealsCell : UIView

@property (strong, nonatomic)NewDealsData *dealsNewObj;
@property(weak,nonatomic)id<NewDealItemDelegate> delegateNewDealsItem;
@property (weak, nonatomic) IBOutlet UIButton *btnView;
- (IBAction)Buttonclicked:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIImageView *frameView;
@property (weak, nonatomic) IBOutlet UIImageView *onSaleBadge;
@property (weak, nonatomic) IBOutlet UIView *soldOutLblBackView;
@property (weak, nonatomic) IBOutlet UILabel *soldoutLbl;

@end

@protocol NewDealItemDelegate <NSObject>

- (void) newDealsItemDidClicked:(NewDealsCell *) newDealsCategoryItem;

@end