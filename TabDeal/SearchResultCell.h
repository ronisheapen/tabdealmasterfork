//
//  SearchResultCell.h
//  TabDeal
//
//  Created by satheesh on 10/27/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewDealsData.h"

@protocol SearchResultItemDelegate;

@interface SearchResultCell : UIView
@property (strong, nonatomic)NewDealsData *dealsNewObj;
@property(weak,nonatomic)id<SearchResultItemDelegate> delegateSearchResultItem;
@property (weak, nonatomic) IBOutlet UIButton *btnView;
@property (strong, nonatomic) IBOutlet UILabel *brandName;
@property (strong, nonatomic) IBOutlet UILabel *itemName;
@property (strong, nonatomic) IBOutlet UIView *bottomView;


- (IBAction)Buttonclicked:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIImageView *onSaleBadge;

@end


@protocol SearchResultItemDelegate <NSObject>

- (void) searchResultItemDidClicked:(SearchResultCell *) searchResultCategoryItem;

@end