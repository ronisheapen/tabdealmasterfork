//
//  PaymentMethodViewController.h
//  TabDeal
//
//  Created by satheesh on 10/7/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentMethodViewController : UIViewController
{
    NSUserDefaults * actionTracking;
}
@property (weak, nonatomic) IBOutlet UIButton *proceedBtn;
- (IBAction)proceedBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *knetBtn;
@property (weak, nonatomic) IBOutlet UIButton *visaBtn;
@property (weak, nonatomic) IBOutlet UIButton *masterBtn;
@property (weak, nonatomic) IBOutlet UIButton *americanBtn;
@property (weak, nonatomic) IBOutlet UIButton *cashOnDelBtn;

@property (strong, nonatomic) IBOutlet UILabel *SubTotal;
@property (strong, nonatomic) IBOutlet UILabel *DeliveryCharge;
@property (strong, nonatomic) IBOutlet UILabel *TotalCharge;
@property (strong, nonatomic) NSString * orderId;

@property (strong, nonatomic) NSString * paymentMethod;
@property (strong, nonatomic) IBOutlet UIView *responseBackView;
@property (weak, nonatomic) IBOutlet UIWebView *responseWebView;
@property (weak, nonatomic) IBOutlet UIButton *backToHomeBtn;
- (IBAction)backToHomeBtnActn:(UIButton *)sender;

- (IBAction)selectPaymentMethodActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *topBackView;
@property (weak, nonatomic) IBOutlet UIView *downBackView;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;

@property (weak, nonatomic) NSString *cashOnDelFlag;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLbl;
@property (weak, nonatomic) IBOutlet UILabel *deliveryChargesLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalLbl;
@property (weak, nonatomic) IBOutlet UILabel *headdingLbl;
@property (weak, nonatomic) IBOutlet UILabel *conditionLbl;
@property (weak, nonatomic) IBOutlet UILabel *cashOnDeliveryLbl;

@end
