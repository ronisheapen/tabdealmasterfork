//
//  DeliveryScreenViewController.m
//  TabDeal
//
//  Created by satheesh on 10/7/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "DeliveryScreenViewController.h"
#import "PaymentMethodViewController.h"
#import "HomeViewController.h"
#import "Userinformation.h"

#import <QuartzCore/QuartzCore.h>

@interface DeliveryScreenViewController ()

@end

@implementation DeliveryScreenViewController


-(void) viewWillAppear:(BOOL)animated
{
    [self xibLoading];
    
    [self fontIntilization];
    
    dropDownTag = 0;
    
    actionTracking = [NSUserDefaults standardUserDefaults];
    self.SubTotal.text = [actionTracking objectForKey:@"SubTotal"];
    self.DeliveryCharge.text = [actionTracking objectForKey:@"DeliveryCharge"];
    self.TotalCharge.text = [actionTracking objectForKey:@"TotalCharge"];
    [actionTracking synchronize];
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, (self.backWhiteView.frame.origin.y * 2) + self.backWhiteView.frame.size.height);
    
    self.nameTxtField.delegate=self;
    self.addressLine1TxtField.delegate=self;
    self.addressLine2TxtField.delegate=self;
    self.cityTxtField.delegate=self;
    self.postalCodeTxtField.delegate=self;
    self.mobileNumTxtField.delegate=self;
    self.codeTxtField.delegate=self;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.mobileNumTxtField.inputAccessoryView = numberToolbar;
    self.mobileDelEditTxt.inputAccessoryView = numberToolbar;
    
    UIToolbar* numberToolbarCode = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarCode.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarCode.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadCode)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadCode)]];
    [numberToolbarCode sizeToFit];
    self.codeTxtField.inputAccessoryView = numberToolbarCode;
    self.codeDelEditTxt.inputAccessoryView = numberToolbarCode;
    
    
    UIToolbar* PostalCodeToolbarCode = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    PostalCodeToolbarCode.barStyle = UIBarStyleBlackTranslucent;
    PostalCodeToolbarCode.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPostalCode)],
                                    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                    [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithPostalCode)]];
    [PostalCodeToolbarCode sizeToFit];
    self.postalCodeTxtField.inputAccessoryView = PostalCodeToolbarCode;
    self.postalCodeEditTxt.inputAccessoryView = PostalCodeToolbarCode;
    
    self.enterDeliveryAddressTxt.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.enterDeliveryAddressTxt.layer.borderWidth= 1.0f;
    self.nameTxtField.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.nameTxtField.layer.borderWidth= 1.0f;
    self.addressLine1TxtField.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.addressLine1TxtField.layer.borderWidth= 1.0f;
    self.addressLine2TxtField.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.addressLine2TxtField.layer.borderWidth= 1.0f;
    self.cityTxtField.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.cityTxtField.layer.borderWidth= 1.0f;
    self.countryTextView.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.countryTextView.layer.borderWidth= 1.0f;
    self.postalCodeTxtField.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.postalCodeTxtField.layer.borderWidth= 1.0f;
    self.numberBackTxt.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.numberBackTxt.layer.borderWidth= 1.0f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileNumTxtField];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.codeTxtField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.postalCodeTxtField];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileDelEditTxt];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.codeDelEditTxt];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.postalCodeEditTxt];
    
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:([ApplicationDelegate.innerViewCount intValue]+1)];
    if (ApplicationDelegate.innerViewCount.intValue==0)
    {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else
    {
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]])
    {
        [self fetchCountryDataformSever];
        [self fetchUserDataformSever];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    self.subTotalLbl.text = localize(@"Sub Total");
    self.deliveryChargesLbl.text = localize(@"Delivery Charges");
    self.totalLbl.text = localize(@"Total");
    self.enterDeliveryAddressTxt.text = localize(@"Enter Delivery Address:");
    self.nameTxtField.placeholder = localize(@"Receiver full name");
    self.addressLine1TxtField.placeholder = localize(@"Address line 1");
    self.addressLine2TxtField.placeholder = localize(@"Address line 2");
    self.cityTxtField.placeholder = localize(@"City");
    self.countryTextView.placeholder = localize(@"Country");
    self.postalCodeTxtField.placeholder = localize(@"Postal Code (only for international users)");
    self.codeTxtField.placeholder = localize(@"(Code)");
    self.mobileNumTxtField.placeholder = localize(@"(Mobile Number)");
    
    self.deliveryAddressHeading.text = localize(@"DELIVERY ADDRESS");
    [self.saveBtn setTitle:localize(@"SAVE") forState:UIControlStateNormal];
    self.nameEditTxtField.placeholder = localize(@"Receiver full name");
    self.addressLine1EditTxt.placeholder = localize(@"Address line 1");
    self.addressLine2EditTxt.placeholder = localize(@"Address line 2");
    self.cityEditTxt.placeholder = localize(@"City");
    self.countryEditTxt.placeholder = localize(@"Country");
    self.postalCodeEditTxt.placeholder = localize(@"Postal Code (only for international users)");
    self.codeDelEditTxt.placeholder = localize(@"(Code)");
    self.mobileDelEditTxt.placeholder = localize(@"(Mobile Number)");
    
    [self.proceedBtn setTitle:localize(@"PROCEED") forState:UIControlStateNormal];
    self.commentline1.text = localize(@"Kuwait only (delivery charges KD 3.5)");
    self.commentline2.text = localize(@"International (delivery charges KD 30)");
    
    self.editScroll.contentSize = CGSizeMake(self.editScroll.frame.size.width, self.editScroll.frame.size.height + 110);
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    cashOnDeliveryFlag = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)fetchCountryDataformSever
{
    NSMutableDictionary *countryData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserCountryPostData]];
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getCountryDetailsWithDataDictionary:countryData CompletionHandler:^(NSMutableArray *responseArray)
     {
         if ([ApplicationDelegate isValid:responseArray])
         {
             if (responseArray.count>0)
             {
                 self.countryCodeAndName = [[NSMutableArray alloc]init];
                 countryNameArray = [[NSMutableArray alloc]init];
                 countryCodeArray = [[NSMutableArray alloc]init];
                 for (int i=0; i<responseArray.count; i++)
                 {
                     [self.countryCodeAndName addObject:[responseArray objectAtIndex:i]];
                     [countryNameArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"CountryName"]];
                     [countryCodeArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"CountryCode"]];
                 }
             }
             [ApplicationDelegate removeProgressHUD];
         }
         
     } errorHandler:^(NSError *error)
    {
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}

-(void)fetchUserDataformSever
{
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getUserDetailsWithDataDictionary:userData CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         if ([ApplicationDelegate isValid:responseDictionary])
         {
             if (responseDictionary.count>0)
             {
                 self.topBackView.hidden = NO;
                 self.seperatorView.hidden = NO;
                 self.downBackView.hidden = NO;
                 
                 Userinformation *userObj = [ApplicationDelegate.mapper getUserinformationFromDictionary:responseDictionary];
                 
                 self.nameTxtField.text = userObj.recieverName;
                 self.addressLine1TxtField.text = userObj.addressLine1;
                 self.addressLine2TxtField.text = userObj.addressLine2;
                 self.cityTxtField.text = userObj.city;
                 self.countryTextView.text = userObj.country;
                 self.postalCodeTxtField.text = userObj.postalCode;
                 self.codeTxtField.text = userObj.mobCode;
                 self.mobileNumTxtField.text = userObj.mobileNumber;
                 
                 self.nameEditTxtField.text = userObj.recieverName;
                 self.addressLine1EditTxt.text = userObj.addressLine1;
                 self.addressLine2EditTxt.text = userObj.addressLine2;
                 self.cityEditTxt.text = userObj.city;
                 self.countryEditTxt.text = userObj.country;
                 self.postalCodeEditTxt.text = userObj.postalCode;
                 deliveryPostalCodeBuffer = [[NSString alloc]initWithString:userObj.postalCode];
                 self.codeDelEditTxt.text = userObj.mobCode;
                 deliveryCodeBuffer = [[NSString alloc]initWithString:userObj.mobCode];
                 self.mobileDelEditTxt.text = userObj.mobileNumber;
                 deliveryMobileBuffer = [[NSString alloc]initWithString:userObj.mobileNumber];
                 
                 if ((self.nameTxtField.text.length == 0) && (self.addressLine1TxtField.text.length == 0) && (self.cityTxtField.text.length == 0) && (self.mobileNumTxtField.text.length == 0) &&(self.codeTxtField.text.length == 0))
                 {
                     self.editAddressBtn.enabled = NO;
                     self.editAddressBtn.userInteractionEnabled = NO;
                     self.editAddressBtn.alpha = .5;
                     
                     self.editAddressBtn.selected = YES;
                     
                     self.nameTxtField.enabled = YES;
                     self.addressLine1TxtField.enabled = YES;
                     self.addressLine2TxtField.enabled = YES;
                     self.cityTxtField.enabled = YES;
                     self.dropDownDwnArrow.hidden = NO;
                     self.dropDownUpArrow.hidden = YES;
                     self.dropBtn.enabled = YES;
                     self.postalCodeTxtField.enabled = YES;
                     self.codeTxtField.enabled = YES;
                     self.mobileNumTxtField.enabled = YES;
                 }
                 
                 if ([self.countryTextView.text isEqualToString:@""])
                 {
                     
                 }
                 else if ([self.countryTextView.text isEqualToString:@"Kuwait"])
                 {
                     cashOnDeliveryFlag = 1;
                     self.DeliveryCharge.text = localize(@"KD 3.5");
                     NSString * tempTotal = self.SubTotal.text;
                     tempTotal = [tempTotal stringByReplacingOccurrencesOfString:@" " withString:@""];
                     tempTotal = [tempTotal stringByReplacingOccurrencesOfString:@"KD" withString:@""];
                     float value = [tempTotal floatValue];
                     value = value + 3.5;
                     self.TotalCharge.text = [NSString stringWithFormat:localize(@"KD %.2f"),value];
                 }
                 else
                 {
                     cashOnDeliveryFlag = 0;
                     self.DeliveryCharge.text = localize(@"KD 30");
                     NSString * tempTotal = self.SubTotal.text;
                     tempTotal = [tempTotal stringByReplacingOccurrencesOfString:@" " withString:@""];
                     tempTotal = [tempTotal stringByReplacingOccurrencesOfString:@"KD" withString:@""];
                     float value = [tempTotal floatValue];
                     value = value + 30;
                     self.TotalCharge.text = [NSString stringWithFormat:localize(@"KD %.2f"),value];
                 }
             }
         }
         
         [ApplicationDelegate removeProgressHUD];
     } errorHandler:^(NSError *error)
    {
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}

-(NSMutableDictionary *)getUserPostData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else
    {
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(NSMutableDictionary *)getUserCountryPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else
    {
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

- (IBAction)proceedBtnAction:(UIButton *)sender
{
    if ((self.nameTxtField.text.length == 0) && (self.addressLine1TxtField.text.length == 0) && (self.cityTxtField.text.length == 0) && (self.countryTextView.text.length == 0) && (self.mobileNumTxtField.text.length == 0) &&(self.codeTxtField.text.length == 0))
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address details") title:localize(@"Sorry..!")];
    }
    else if(self.nameTxtField.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your Name") title:localize(@"Sorry..!")];
    }
    else if(self.addressLine1TxtField.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address") title:localize(@"Sorry..!")];
    }
    else if(self.countryTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country") title:localize(@"Sorry..!")];
    }
    else if(self.codeTxtField.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter country code") title:localize(@"Sorry..!")];
    }
    else if(self.mobileNumTxtField.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
    }
    else
    {
        if (self.editAddressBtn.selected)
        {
            [self postAddressDetails];
        }
        else
        {
            if(!([self.countryEditTxt.text isEqualToString:@"Kuwait"]||[self.countryEditTxt.text isEqualToString:@"الكويت"]))
            {
                if(self.postalCodeEditTxt.text.length == 0)
                {
                    [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your postal code") title:localize(@"Sorry..!")];
                }
                else
                {
                    PaymentMethodViewController * paymentScreenVC = [[PaymentMethodViewController alloc]initWithNibName:@"PaymentMethodViewController" bundle:nil];
                    paymentScreenVC.view.backgroundColor = [UIColor clearColor];
                    actionTracking = [NSUserDefaults standardUserDefaults];
                    [actionTracking setObject:self.SubTotal.text forKey:@"SubTotal"];
                    [actionTracking setObject:self.DeliveryCharge.text forKey:@"DeliveryCharge"];
                    [actionTracking setObject:self.TotalCharge.text forKey:@"TotalCharge"];
                    [actionTracking setObject:self.orderId forKey:@"orderId"];
                    [actionTracking setObject:[NSString stringWithFormat:@"%d",cashOnDeliveryFlag] forKey:@"cashOnDeliveryFlag"];
                    [actionTracking synchronize];
                    paymentScreenVC.SubTotal.text=self.SubTotal.text;
                    paymentScreenVC.DeliveryCharge.text=self.DeliveryCharge.text;
                    paymentScreenVC.TotalCharge.text=self.TotalCharge.text;
                    paymentScreenVC.orderId = self.orderId;
                    paymentScreenVC.cashOnDelFlag = [NSString stringWithFormat:@"%d",cashOnDeliveryFlag];
                    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PaymentMethodViewController class]])
                    {
                        [ApplicationDelegate.homeTabNav pushViewController:paymentScreenVC animated:NO];
                    }
                }
            }
            else
            {
                PaymentMethodViewController * paymentScreenVC = [[PaymentMethodViewController alloc]initWithNibName:@"PaymentMethodViewController" bundle:nil];
                paymentScreenVC.view.backgroundColor = [UIColor clearColor];
                actionTracking = [NSUserDefaults standardUserDefaults];
                [actionTracking setObject:self.SubTotal.text forKey:@"SubTotal"];
                [actionTracking setObject:self.DeliveryCharge.text forKey:@"DeliveryCharge"];
                [actionTracking setObject:self.TotalCharge.text forKey:@"TotalCharge"];
                [actionTracking setObject:self.orderId forKey:@"orderId"];
                [actionTracking setObject:[NSString stringWithFormat:@"%d",cashOnDeliveryFlag] forKey:@"cashOnDeliveryFlag"];
                [actionTracking synchronize];
                paymentScreenVC.SubTotal.text=self.SubTotal.text;
                paymentScreenVC.DeliveryCharge.text=self.DeliveryCharge.text;
                paymentScreenVC.TotalCharge.text=self.TotalCharge.text;
                paymentScreenVC.orderId = self.orderId;
                paymentScreenVC.cashOnDelFlag = [NSString stringWithFormat:@"%d",cashOnDeliveryFlag];
                if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PaymentMethodViewController class]])
                {
                    [ApplicationDelegate.homeTabNav pushViewController:paymentScreenVC animated:NO];
                }
            }
        }
    }
}

- (IBAction)dropDownActn:(UIButton *)sender
{
    self.dropBtn.selected = !self.dropBtn.selected;
    if (self.dropBtn.selected)
    {
        self.dropDownDwnArrow.hidden = YES;
        self.dropDownUpArrow.hidden = NO;
        if (self.countryDropDownObj.view.superview)
        {
            [self.countryDropDownObj.view removeFromSuperview];
        }
        self.countryDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        self.countryDropDownObj.cellHeight = 40.0f;
        self.countryDropDownObj.view.frame = CGRectMake((self.countryTextView.frame.origin.x),(self.countryTextView.frame.origin.y+self.countryTextView.frame.size.height), (self.countryTextView.frame.size.width), 0);
        
        self.countryDropDownObj.dropDownDelegate = self;
        self.countryDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:countryNameArray];
        
        self.countryDropDownObj.view.layer.borderWidth = 0.1;
        self.countryDropDownObj.view.layer.shadowOpacity = 1.0;
        self.countryDropDownObj.view.layer.shadowRadius = 5.0;
        self.countryDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        self.countryDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.countryDropDownObj.textLabelColor = [UIColor blackColor];
        self.countryDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (countryNameArray.count>0)
        {
            [self.dropBtn.superview addSubview:self.countryDropDownObj.view];
        }
        else
        {
            self.dropBtn.selected=NO;
        }
        
        CGFloat tableHT = 120.0f;
        
        if ((self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.countryDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.countryDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x+2,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.dropDownUpArrow.hidden = YES;
        self.dropDownDwnArrow.hidden = NO;
        self.countryDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.countryDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        for (UIView *vw in self.view.subviews)
        {
            [vw setUserInteractionEnabled:YES];
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
    }
}

- (IBAction)editAddressBtnActn:(UIButton *)sender
{
    self.nameEditTxtField.text = self.nameTxtField.text;
    self.addressLine1EditTxt.text = self.addressLine1TxtField.text;
    self.addressLine2EditTxt.text = self.addressLine2TxtField.text;
    self.cityEditTxt.text = self.cityTxtField.text;
    self.countryEditTxt.text = self.countryTextView.text;
    
    self.postalCodeEditTxt.text = deliveryPostalCodeBuffer;
    self.codeDelEditTxt.text = deliveryCodeBuffer;
    self.mobileDelEditTxt.text = deliveryMobileBuffer;
    
    [self.view addSubview:self.editDeliveryAddressBackView];
    [self.editDeliveryAddressBackView setFrame:self.editDeliveryAddressBackView.superview.bounds];
}

-(void)postAddressDetails
{
    NSMutableDictionary *addressData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserAddressPostData]];
    if (addressData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine EditUserAddressWithDataDictionary:addressData CompletionHandler:^(NSMutableDictionary *responseDic)
         
         {
             if ([ApplicationDelegate isValid:responseDic])
             {
                 if (responseDic.count>0)
                 {
                     if ([[responseDic objectForKey:@"Success"] integerValue])
                     {
                         PaymentMethodViewController * paymentScreenVC = [[PaymentMethodViewController alloc]initWithNibName:@"PaymentMethodViewController" bundle:nil];
                         paymentScreenVC.view.backgroundColor = [UIColor clearColor];
                         actionTracking = [NSUserDefaults standardUserDefaults];
                         [actionTracking setObject:self.SubTotal.text forKey:@"SubTotal"];
                         [actionTracking setObject:self.DeliveryCharge.text forKey:@"DeliveryCharge"];
                         [actionTracking setObject:self.TotalCharge.text forKey:@"TotalCharge"];
                         [actionTracking setObject:self.orderId forKey:@"orderId"];
                         [actionTracking synchronize];
                         paymentScreenVC.SubTotal.text=self.SubTotal.text;
                         paymentScreenVC.DeliveryCharge.text=self.DeliveryCharge.text;
                         paymentScreenVC.TotalCharge.text=self.TotalCharge.text;
                         paymentScreenVC.orderId = self.orderId;
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PaymentMethodViewController class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:paymentScreenVC animated:NO];
                         }
                         [ApplicationDelegate removeProgressHUD];
                     }
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                         
                         [ApplicationDelegate removeProgressHUD];
                     }
                 }
             }
             
         } errorHandler:^(NSError *error)
        {
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
}

-(void)postEditedAddressDetails
{
    NSMutableDictionary *addressData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserEditedAddressPostData]];
    if (addressData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine EditUserAddressWithDataDictionary:addressData CompletionHandler:^(NSMutableDictionary *responseDic)
         {
             if ([ApplicationDelegate isValid:responseDic])
             {
                 if (responseDic.count>0)
                 {
                     NSLog(@"%@",responseDic);
                     if ([[responseDic objectForKey:@"Success"] integerValue])
                     {
                         [self viewWillAppear:YES];
                         [self viewDidLoad];
                     }
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                         [ApplicationDelegate removeProgressHUD];
                     }
                 }
             }
         } errorHandler:^(NSError *error)
         {
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
}

-(NSMutableDictionary *)getUserEditedAddressPostData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.nameEditTxtField.text forKey:@"RecieverName"];
    [postDic setObject:self.addressLine1EditTxt.text forKey:@"AddressLine1"];
    [postDic setObject:self.addressLine2EditTxt.text forKey:@"AddressLine2"];
    [postDic setObject:self.cityEditTxt.text forKey:@"City"];
    [postDic setObject:self.countryEditTxt.text forKey:@"Country"];
    [postDic setObject:self.codeDelEditTxt.text forKey:@"MobCode"];
    [postDic setObject:self.mobileDelEditTxt.text forKey:@"MobileNumber"];
    [postDic setObject:self.postalCodeEditTxt.text forKey:@"PostalCode"];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    return postDic;
}


-(NSMutableDictionary *)getUserAddressPostData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.nameTxtField.text forKey:@"RecieverName"];
    [postDic setObject:self.addressLine1TxtField.text forKey:@"AddressLine1"];
    [postDic setObject:self.addressLine2TxtField.text forKey:@"AddressLine2"];
    [postDic setObject:self.cityTxtField.text forKey:@"City"];
    [postDic setObject:self.countryTextView.text forKey:@"Country"];
    [postDic setObject:self.codeTxtField.text forKey:@"MobCode"];
    [postDic setObject:self.mobileNumTxtField.text forKey:@"MobileNumber"];
    [postDic setObject:self.postalCodeTxtField.text forKey:@"PostalCode"];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    return postDic;
}

-(void)selectList:(int)selectedIndex
{
    if (dropDownTag == 0)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropBtn.selected = NO;
        self.dropDownUpArrow.hidden = YES;
        self.dropDownDwnArrow.hidden = NO;
        self.countryTextView.text =[countryNameArray objectAtIndex:selectedIndex];
        if ([[countryCodeArray objectAtIndex:selectedIndex]isEqualToString:@"KW"])
        {
            cashOnDeliveryFlag = 1;
            self.DeliveryCharge.text = localize(@"KD 3.5");
            NSString * tempTotal = self.SubTotal.text;
            tempTotal = [tempTotal stringByReplacingOccurrencesOfString:@" " withString:@""];
            tempTotal = [tempTotal stringByReplacingOccurrencesOfString:@"KD" withString:@""];
            float value = [tempTotal floatValue];
            value = value + 3.5;
            self.TotalCharge.text = [NSString stringWithFormat:localize(@"KD %.2f"),value];
        }
        else
        {
            cashOnDeliveryFlag = 0;
            self.DeliveryCharge.text = localize(@"KD 30");
            NSString * tempTotal = self.SubTotal.text;
            tempTotal = [tempTotal stringByReplacingOccurrencesOfString:@" " withString:@""];
            tempTotal = [tempTotal stringByReplacingOccurrencesOfString:@"KD" withString:@""];
            float value = [tempTotal floatValue];
            value = value + 30;
            self.TotalCharge.text = [NSString stringWithFormat:localize(@"KD %.2f"),value];
        }
    }
    else
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
            if (finished)
            {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
        
        self.dropBtnEdit.selected = NO;
        self.dropDownUpArrowEdit.hidden = YES;
        self.dropDownDwnArrowEdit.hidden = NO;
        self.countryEditTxt.text =[countryNameArray objectAtIndex:selectedIndex];
    }
}


- (void)limitTextField:(NSNotification *)note
{
    int limit = 10;
    int codeLimit = 3;
    int postcodeLimit=6;
    
    if(self.postalCodeTxtField.text.length> postcodeLimit)
    {
      [self.postalCodeTxtField setText:[self.postalCodeTxtField.text substringToIndex:postcodeLimit]];
    }
    else if (self.mobileNumTxtField.text.length > limit)
    {
        [self.mobileNumTxtField setText:[self.mobileNumTxtField.text substringToIndex:limit]];
    }
    else if(self.codeTxtField.text.length > codeLimit)
    {
        [self.codeTxtField setText:[self.codeTxtField.text substringToIndex:codeLimit]];
    }
    if(self.postalCodeEditTxt.text.length> postcodeLimit)
    {
        [self.postalCodeEditTxt setText:[self.postalCodeEditTxt.text substringToIndex:postcodeLimit]];
    }
    else if (self.mobileDelEditTxt.text.length > limit)
    {
        [self.mobileDelEditTxt setText:[self.mobileDelEditTxt.text substringToIndex:limit]];
    }
    else if(self.codeDelEditTxt.text.length > codeLimit)
    {
        [self.codeDelEditTxt setText:[self.codeDelEditTxt.text substringToIndex:codeLimit]];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)cancelNumberPad
{
    [ self.mobileNumTxtField resignFirstResponder];
    [ self.mobileDelEditTxt resignFirstResponder];
    self.mobileDelEditTxt.text = @"";
}

-(void)doneWithNumberPad
{
    [self.mobileNumTxtField resignFirstResponder];
    [self.mobileDelEditTxt resignFirstResponder];
}

-(void)cancelNumberPadCode
{
    [ self.codeTxtField resignFirstResponder];
    [ self.codeDelEditTxt resignFirstResponder];
    self.codeDelEditTxt.text = @"";
}

-(void)doneWithNumberPadCode
{
    [self.codeTxtField resignFirstResponder];
    [self.codeDelEditTxt resignFirstResponder];
}

-(void)cancelPostalCode
{
    [ self.postalCodeTxtField resignFirstResponder];
    [ self.postalCodeEditTxt resignFirstResponder];
    self.postalCodeEditTxt.text = @"";
}

-(void)doneWithPostalCode
{
    [self.postalCodeTxtField resignFirstResponder];
    [self.postalCodeEditTxt resignFirstResponder];
}

-(void)xibLoading
{
    NSString *nibName = @"DeliveryScreenViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

- (IBAction)dropDownEditActn:(UIButton *)sender
{
    dropDownTag = 1;
    self.dropBtnEdit.selected = !self.dropBtnEdit.selected;
    if (self.dropBtnEdit.selected)
    {
        self.dropDownUpArrowEdit.hidden = NO;
        self.dropDownDwnArrowEdit.hidden = YES;
        if (self.countryDropDownObj.view.superview)
        {
            [self.countryDropDownObj.view removeFromSuperview];
        }
        self.countryDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        self.countryDropDownObj.cellHeight = 40.0f;
        self.countryDropDownObj.view.frame = CGRectMake((self.countryEditTxt.frame.origin.x),(self.countryEditTxt.frame.origin.y+self.countryEditTxt.frame.size.height), (self.countryEditTxt.frame.size.width), 0);
        self.countryDropDownObj.dropDownDelegate = self;
        self.countryDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:countryNameArray];
        self.countryDropDownObj.view.layer.borderWidth = 0.1;
        self.countryDropDownObj.view.layer.shadowOpacity = 1.0;
        self.countryDropDownObj.view.layer.shadowRadius = 5.0;
        self.countryDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        self.countryDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.countryDropDownObj.textLabelColor = [UIColor blackColor];
        self.countryDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (countryNameArray.count>0)
        {
            [self.dropBtnEdit.superview addSubview:self.countryDropDownObj.view];
        }
        else
        {
            self.dropBtnEdit.selected=NO;
        }
        
        CGFloat tableHT = 120.0f;
        
        if ((self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.countryDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.countryDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x+2,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.dropDownUpArrowEdit.hidden = YES;
        self.dropDownDwnArrowEdit.hidden = NO;
        self.countryDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.countryDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        for (UIView *vw in self.view.subviews)
        {
            [vw setUserInteractionEnabled:YES];
        }
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
    }
}

- (IBAction)closeBtnActn:(UIButton *)sender
{
    [self.editDeliveryAddressBackView removeFromSuperview];
}

- (IBAction)saveBtnActn:(UIButton *)sender
{
    if ((self.nameEditTxtField.text.length == 0) && (self.addressLine1EditTxt.text.length == 0) && (self.cityEditTxt.text.length == 0) && (self.countryEditTxt.text.length == 0) && (self.mobileDelEditTxt.text.length == 0) &&(self.codeDelEditTxt.text.length == 0))
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address details") title:localize(@"Sorry..!")];
    } else if(self.nameEditTxtField.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your Name") title:localize(@"Sorry..!")];
    }
    else if(self.addressLine1EditTxt.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address") title:localize(@"Sorry..!")];
    }
    else if(self.countryEditTxt.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country") title:localize(@"Sorry..!")];
    }
    else if(self.codeDelEditTxt.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter country code") title:localize(@"Sorry..!")];
    }
    else if(self.mobileDelEditTxt.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
    }
    else
    {
        if(!([self.countryEditTxt.text isEqualToString:@"Kuwait"]||[self.countryEditTxt.text isEqualToString:@"الكويت"]))
        {
            if(self.postalCodeEditTxt.text.length == 0)
            {
                [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your postal code") title:localize(@"Sorry..!")];
            }
            else
            {
                [self postEditedAddressDetails];
            }
        }
        else
        {
            [self postEditedAddressDetails];
        }
    }
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.editScroll.contentInset = contentInsets;
    self.editScroll.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.editBackView.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.nameEditTxtField.frame.origin) )
    {
        [self.editScroll scrollRectToVisible:self.nameEditTxtField.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.addressLine1EditTxt.frame.origin) )
    {
        [self.editScroll scrollRectToVisible:self.addressLine1EditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.addressLine2EditTxt.frame.origin) )
    {
        [self.editScroll scrollRectToVisible:self.addressLine2EditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.cityEditTxt.frame.origin) )
    {
        [self.editScroll scrollRectToVisible:self.cityEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.postalCodeEditTxt.frame.origin) )
    {
        [self.editScroll scrollRectToVisible:self.postalCodeEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.codeDelEditTxt.frame.origin) )
    {
        [self.editScroll scrollRectToVisible:self.codeDelEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.mobileDelEditTxt.frame.origin) )
    {
        [self.editScroll scrollRectToVisible:self.mobileDelEditTxt.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.editScroll.contentSize = CGSizeMake(self.editScroll.frame.size.width, self.editBackView.frame.size.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.editScroll.contentInset = contentInsets;
    self.editScroll.scrollIndicatorInsets = contentInsets;
}

-(void) fontIntilization
{
    [self.totalLbl setFont:EBRI_BOLD_FONT(17)];
    
    [self.TotalCharge setFont:EBRI_BOLD_FONT(17)];
    
    [self.subTotalLbl setFont:EBRI_NORMAL_FONT(16)];
    
    [self.SubTotal setFont:EBRI_NORMAL_FONT(16)];
    
    [self.deliveryChargesLbl setFont:EBRI_NORMAL_FONT(16)];
    
    [self.DeliveryCharge setFont:EBRI_NORMAL_FONT(16)];
}


@end
