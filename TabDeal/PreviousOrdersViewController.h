//
//  PreviousOrdersViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviousOrdersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSUserDefaults * actionTracking;
}
@property (weak, nonatomic) IBOutlet UITableView *previousOrderTable;
@property (strong, nonatomic) NSMutableArray * PreviousOrderDataArray;
@property (weak, nonatomic) IBOutlet UIImageView *warningBackView;
@property (weak, nonatomic) IBOutlet UILabel *warningLbl;
@property (weak, nonatomic) IBOutlet UILabel *headingLbl;


@end
