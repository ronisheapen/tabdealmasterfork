//
//  NotificationCell.h
//  TabDeal
//
//  Created by satheesh on 10/8/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
//@property (weak, nonatomic) IBOutlet UILabel *desctiptionTxtView;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIWebView *desWebView;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTxtView;
@end
