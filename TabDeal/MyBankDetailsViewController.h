//
//  MyBankDetailsViewController.h
//  TabDeal
//
//  Created by satheesh on 10/8/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface MyBankDetailsViewController : UIViewController<ListSelectionProtocol,UITextFieldDelegate>{
    NSMutableArray * countryNameArray;
    NSMutableArray * countryCodeArray;
}
@property (weak, nonatomic) IBOutlet UITextField *fullNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *bankNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *bankAddressTxt;
@property (weak, nonatomic) IBOutlet UITextField *countryTxt;
@property (weak, nonatomic) IBOutlet UITextField *swiftCodeTxt;
@property (weak, nonatomic) IBOutlet UIButton *saveChangesBtn;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownDwnArrow;
@property (strong, nonatomic) DropDownView *countryDropDownObj;
@property (weak, nonatomic) IBOutlet UIButton *dropBtn;
@property (strong, nonatomic)NSMutableArray * countryCodeAndName;
@property (weak, nonatomic) IBOutlet UITextField *ibanTxt;
@property (weak, nonatomic) IBOutlet UIButton *editBillingAddressBtn;
- (IBAction)editBankBtnActn:(UIButton *)sender;
- (IBAction)dropDownActn:(UIButton *)sender;
- (IBAction)saveChangesBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIImageView *whiteBackView;
@property (weak, nonatomic) IBOutlet UILabel *headdingLbl;
@property (strong, nonatomic) IBOutlet UIView *editBankDetailsBackView;
- (IBAction)closeBtnAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITextField *fullnameEditTxt;
@property (strong, nonatomic) IBOutlet UITextField *bankNameEditTxt;
@property (strong, nonatomic) IBOutlet UITextField *bankAddressEditTxt;
@property (strong, nonatomic) IBOutlet UITextField *countryEditTxt;
@property (strong, nonatomic) IBOutlet UITextField *ibanEditTxt;
@property (strong, nonatomic) IBOutlet UITextField *swiftCodeEditTxt;
@property (strong, nonatomic) IBOutlet UIButton *AddBankButton;
@property (weak, nonatomic) IBOutlet UILabel *myBankDetailsHeading;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@end
