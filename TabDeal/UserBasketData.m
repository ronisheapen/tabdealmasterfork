//
//  UserBasketData.m
//  TabDeal
//
//  Created by satheesh on 10/14/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "UserBasketData.h"

@implementation UserBasketData
@synthesize brand;
@synthesize dealId;
@synthesize image;
@synthesize name;
@synthesize quantity;
@synthesize rate;
@synthesize size;
@synthesize stock;
@end
