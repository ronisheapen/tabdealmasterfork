//
//  NewDealsDetailsViewController.h
//  TabDeal
//
//  Created by satheesh on 10/1/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewDealsDetailsViewController : UIViewController<UIScrollViewDelegate>
{
    int zoomCount;
    CGRect size35Frame,size36Frame,size37Frame,size38Frame,size39Frame,size40Frame,size41Frame,size42Frame;
    NSMutableArray * sizeBttnArray;
    int globalShoeSizeVariable;
    NSUserDefaults * actionTracking;
}
@property (weak, nonatomic) IBOutlet UILabel *Lbl;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *innerScroll;
@property (weak, nonatomic) IBOutlet UIImageView *topView;
@property (strong, nonatomic) IBOutlet UIImageView *bottomView;
@property (weak, nonatomic) IBOutlet UITextView *testText;
@property (weak, nonatomic) IBOutlet UIImageView *testImage;
@property (weak, nonatomic) IBOutlet UIImageView *testTop;
@property (weak, nonatomic) IBOutlet UIImageView *testBottom;
@property (weak, nonatomic) IBOutlet UITextView *testTextView;

@property (strong, nonatomic)NSString * productId;
@property (strong, nonatomic)NSMutableDictionary * dealsDetailDictionary;
@property (strong, nonatomic)NSMutableDictionary * currencyConvertionDictionary;
@property (strong, nonatomic)NSMutableArray * imageArry;

@property (strong, nonatomic)NSMutableArray * globalSizeArray;

@property (weak, nonatomic) IBOutlet UILabel *brandLbl;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *frameBackView;
@property (weak, nonatomic) IBOutlet UIImageView *bannerView;
@property (weak, nonatomic) IBOutlet UIView *productStatusBar;
@property (weak, nonatomic) IBOutlet UIImageView *genderCriteria;
@property (weak, nonatomic) IBOutlet UIImageView *productCategory;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UIButton *leftArrowBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightArrowBtn;





@property (weak, nonatomic) IBOutlet UIScrollView *middleScrollView;
@property (weak, nonatomic) IBOutlet UIView *middleScrollTopView;
@property (weak, nonatomic) IBOutlet UIView *middleScrollDownView;
@property (weak, nonatomic) IBOutlet UIView *downScrollBackView;
@property (weak, nonatomic) IBOutlet UILabel *youMightBeLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *downScrollView;
@property (weak, nonatomic) IBOutlet UIView *middleScrollShoeSizeView;
@property (weak, nonatomic) IBOutlet UIImageView *downScrollDummyFrame;


@property (weak, nonatomic) IBOutlet UILabel *itemName;
@property (weak, nonatomic) IBOutlet UIButton *moreInfoBttn;
@property (weak, nonatomic) IBOutlet UILabel *priceInKd;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UILabel *showInCurrenciesLbl;
@property (weak, nonatomic) IBOutlet UIButton *addToBasketBtn;
@property (weak, nonatomic) IBOutlet UIButton *addToFavoriteBtn;
@property (weak, nonatomic) IBOutlet UIButton *poundBtn;
@property (weak, nonatomic) IBOutlet UIButton *dolarBtn;
@property (weak, nonatomic) IBOutlet UIButton *euroBtn;
@property (weak, nonatomic) IBOutlet UIButton *rupeeBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceBasedOnCurrency;
@property (weak, nonatomic) IBOutlet UILabel *crossedPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *offerPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *crossLbl;
@property (weak, nonatomic) IBOutlet UIButton *zoomBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *zoomScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *zoomImageView;
@property (strong, nonatomic) IBOutlet UIView *zoomBackView;
@property (weak, nonatomic) IBOutlet UIImageView *onSaleBadge;
@property (weak, nonatomic) IBOutlet UIScrollView *chooseSizeScrollView;
@property (weak, nonatomic) IBOutlet UILabel *chooseYourSizeLbl;

- (IBAction)zoomCloseBtnActn:(UIButton *)sender;

- (IBAction)zoomBtnActn:(UIButton *)sender;

- (IBAction)moreInfoBttnActn:(UIButton *)sender;
- (IBAction)addToBasketBtnActn:(UIButton *)sender;
- (IBAction)addToFavoriteBtnActn:(UIButton *)sender;
- (IBAction)currencyChangeAction:(UIButton *)sender;
- (IBAction)rightArrowBtnActn:(UIButton *)sender;
- (IBAction)leftArrowBtnActn:(UIButton *)sender;
- (IBAction)shareBtnActn:(UIButton *)sender;

- (IBAction)sizeChangeAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *size35Btn;
@property (weak, nonatomic) IBOutlet UIButton *size36Btn;
@property (weak, nonatomic) IBOutlet UIButton *size37Btn;
@property (weak, nonatomic) IBOutlet UIButton *size38Btn;
@property (weak, nonatomic) IBOutlet UIButton *size39Btn;
@property (weak, nonatomic) IBOutlet UIButton *size40Btn;
@property (weak, nonatomic) IBOutlet UIButton *size41Btn;
@property (weak, nonatomic) IBOutlet UIButton *size42Btn;

@property (weak, nonatomic) IBOutlet UILabel *soldOutLbl;
@property (weak, nonatomic) IBOutlet UIView *soldOutLblBackView;

@property (weak, nonatomic) IBOutlet UIImageView *frameview;

@end
