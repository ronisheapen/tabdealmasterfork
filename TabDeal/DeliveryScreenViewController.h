//
//  DeliveryScreenViewController.h
//  TabDeal
//
//  Created by satheesh on 10/7/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface DeliveryScreenViewController : UIViewController<ListSelectionProtocol,UITextFieldDelegate>
{
    NSMutableArray * countryNameArray;
    NSMutableArray * countryCodeArray;
    NSUserDefaults * actionTracking;
    int cashOnDeliveryFlag;
    NSString * deliveryMobileBuffer;
    NSString * deliveryCodeBuffer;
    NSString * deliveryPostalCodeBuffer;
    int dropDownTag;
}

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *backWhiteView;

@property (weak, nonatomic) IBOutlet UITextField *nameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *addressLine1TxtField;
@property (weak, nonatomic) IBOutlet UITextField *addressLine2TxtField;
@property (weak, nonatomic) IBOutlet UITextField *cityTxtField;
@property (weak, nonatomic) IBOutlet UITextField *postalCodeTxtField;
@property (weak, nonatomic) IBOutlet UITextField *codeTxtField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumTxtField;


@property (weak, nonatomic) IBOutlet UIButton *proceedBtn;
@property (weak, nonatomic) IBOutlet UIButton *dropBtn;
@property (weak, nonatomic) IBOutlet UITextField *countryTextView;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownDwnArrow;
@property (weak, nonatomic) IBOutlet UITextField *enterDeliveryAddressTxt;
@property (weak, nonatomic) IBOutlet UITextField *numberBackTxt;
@property (strong, nonatomic) DropDownView *countryDropDownObj;

- (IBAction)proceedBtnAction:(UIButton *)sender;
- (IBAction)dropDownActn:(UIButton *)sender;
- (IBAction)editAddressBtnActn:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UILabel *SubTotal;
@property (strong, nonatomic) IBOutlet UILabel *DeliveryCharge;
@property (strong, nonatomic) IBOutlet UILabel *TotalCharge;
@property (strong, nonatomic) NSString * orderId;

@property (strong, nonatomic)NSMutableArray * countryCodeAndName;
@property (weak, nonatomic) IBOutlet UIView *topBackView;
@property (weak, nonatomic) IBOutlet UIView *downBackView;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;
@property (weak, nonatomic) IBOutlet UIButton *editAddressBtn;

@property (weak, nonatomic) IBOutlet UILabel *subTotalLbl;
@property (weak, nonatomic) IBOutlet UILabel *deliveryChargesLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalLbl;
@property (weak, nonatomic) IBOutlet UITextField *commentline1;
@property (weak, nonatomic) IBOutlet UITextField *commentline2;

@property (strong, nonatomic) IBOutlet UIView *editDeliveryAddressBackView;
@property (weak, nonatomic) IBOutlet UITextField *nameEditTxtField;
@property (weak, nonatomic) IBOutlet UITextField *addressLine1EditTxt;
@property (weak, nonatomic) IBOutlet UITextField *addressLine2EditTxt;
@property (weak, nonatomic) IBOutlet UITextField *cityEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *countryEditTxt;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownUpArrowEdit;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownDwnArrowEdit;
@property (weak, nonatomic) IBOutlet UIButton *dropBtnEdit;

@property (weak, nonatomic) IBOutlet UITextField *postalCodeEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *codeDelEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobileDelEditTxt;

- (IBAction)dropDownEditActn:(UIButton *)sender;

- (IBAction)closeBtnActn:(UIButton *)sender;

- (IBAction)saveBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *editScroll;

@property (weak, nonatomic) IBOutlet UIView *editBackView;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressHeading;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@end
