//
//  MyBasketCell.h
//  TabDeal
//
//  Created by Sreeraj VR on 15/10/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BasketItemDelegate;

@interface MyBasketCell : UIView

@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property(weak,nonatomic)id<BasketItemDelegate> delegateBasketItem;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *brandName;
@property (weak, nonatomic) IBOutlet UILabel *itemName;
@property (weak, nonatomic) IBOutlet UILabel *quantityValue;
@property (weak, nonatomic) IBOutlet UILabel *priceValue;
@property (weak, nonatomic) IBOutlet UILabel *stockAvailableLbl;
@property (weak, nonatomic) IBOutlet UIView *stockAvailableBackView;

- (IBAction)deleteItem:(id)sender;
- (IBAction)editItem:(id)sender;
- (IBAction)itemDetails:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *deleteButtonView;
@property (strong, nonatomic) IBOutlet UIButton *detailsButtonView;
@property (weak, nonatomic) IBOutlet UIButton *editBtnView;
@property (weak, nonatomic) IBOutlet UILabel *quantityLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@end

@protocol BasketItemDelegate <NSObject>

- (void) deleteDidClicked:(MyBasketCell *) BasketItem;

-(void)editDidClicked:(MyBasketCell *) BasketItem;

-(void)detailsDidClicked:(MyBasketCell *) BasketItem;


@end


