//
//  MyFavoriteCell.h
//  TabDeal
//
//  Created by Sreeraj VR on 21/10/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FavoriteItemDelegate;


@interface MyFavoriteCell : UIView

@property(weak,nonatomic)id<FavoriteItemDelegate> delegateFavItem;
@property (strong, nonatomic) IBOutlet UIImageView *FavImageView;
@property (strong, nonatomic) IBOutlet UIButton *deleteButtonView;
- (IBAction)deleteButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *detailsButtonView;
- (IBAction)detailsButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *brandName;
@property (strong, nonatomic) IBOutlet UILabel *itemName;

@end


@protocol FavoriteItemDelegate <NSObject>

- (void) deleteDidClicked:(MyFavoriteCell *) FavItem;

-(void)detailsDidClicked:(MyFavoriteCell *) FavItem;

@end