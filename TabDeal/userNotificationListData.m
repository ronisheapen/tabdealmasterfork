//
//  userNotificationListData.m
//  TabDeal
//
//  Created by satheesh on 11/27/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "userNotificationListData.h"

@implementation userNotificationListData
@synthesize image;
@synthesize message;
@synthesize date;
@synthesize status;
@synthesize referenceId;
@synthesize typeId;
@synthesize messageId;
@end
