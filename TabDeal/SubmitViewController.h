//
//  SubmitViewController.h
//  TabDeal
//
//  Created by satheesh on 10/27/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitViewController : UIViewController<UINavigationControllerDelegate ,UIActionSheetDelegate, UIImagePickerControllerDelegate,UIWebViewDelegate>{
    UIImage * civilImage;
    NSUserDefaults * actionStatus;
    int loadingFlag;
}

@property (nonatomic, retain) NSDictionary *SellPageData;
@property ()NSString * civilIdStr;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *sellBackView;

@property (weak, nonatomic) IBOutlet UIButton *uploadCivilBtn;
- (IBAction)uploadCivilBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *uploadCivilIdImgBtn;
- (IBAction)uploadCivilIdBtnActn:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIView *uploadImageDetailBackView;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImageDetailImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)deleteBtnActn:(UIButton *)sender;
- (IBAction)closeBtnActn:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIView *moreInfo;
- (IBAction)moreInfoAction:(UIButton*)sender;
- (IBAction)closeBtnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *infoWebView;

- (IBAction)SellDataSubmit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *ownershipBtn;
@property (weak, nonatomic) IBOutlet UIButton *conditionBtn;
@property (weak, nonatomic) IBOutlet UIButton *consignmentBtn;
- (IBAction)ownershipBtnActn:(UIButton *)sender;
- (IBAction)conditionBtnActn:(UIButton *)sender;
- (IBAction)consignmentBtnActn:(UIButton *)sender;
- (IBAction)confirmBtnActn:(UIButton *)sender;
@property (strong, nonatomic)NSString * contextString;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *uploadCivilIdLbl;
@property (weak, nonatomic) IBOutlet UILabel *confirmOwnerShipLbl;
@property (weak, nonatomic) IBOutlet UILabel *confirmConditionLbl;
@property (weak, nonatomic) IBOutlet UILabel *confirnTermsLbl;
@property (weak, nonatomic) IBOutlet UILabel *moreInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *readTermsAndConditionLbl;

@property (strong, nonatomic) IBOutlet UIView *responseBackView;
@property (weak, nonatomic) IBOutlet UIWebView *responseWebView;
@property (weak, nonatomic) IBOutlet UIButton *backToHomeBtn;
- (IBAction)backToHomeBtnActn:(UIButton *)sender;
@end
