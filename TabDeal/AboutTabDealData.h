//
//  AboutTabDealData.h
//  TabDeal
//
//  Created by satheesh on 10/19/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AboutTabDealData : NSObject
@property(nonatomic,retain)NSString *aboutUsPageData;
@end
