//
//  User.h
//  TabDeal
//
//  Created by Sreeraj VR on 22/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property(nonatomic,retain)NSString *email;
@property(nonatomic,retain)NSString *firstname;
@property(nonatomic,retain)NSString *languagekey;
@property(nonatomic,retain)NSString *lastName;
@property(nonatomic,retain)NSString *mobilenumber;
@property(nonatomic,retain)NSString *userId;

@end
