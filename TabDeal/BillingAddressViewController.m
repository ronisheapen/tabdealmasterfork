//
//  BillingAddressViewController.m
//  TabDeal
//
//  Created by satheesh on 10/7/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "BillingAddressViewController.h"

@interface BillingAddressViewController ()

@end

@implementation BillingAddressViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mainScroll.contentSize = CGSizeMake(self.mainScroll.frame.size.width, (self.backWhiteView.frame.origin.y * 2) + self.backWhiteView.frame.size.height);
    self.responseWebView.scalesPageToFit = YES;
    scrollFrameSize = self.mainScroll.contentSize;
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    //    self.postalCodeTxtView.delegate = self;
    //    self.mobileNumberTxtView.delegate=self;
    //    self.mobileCodeTxtView.delegate=self;
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileNumberTxtView];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileCodeTxtView];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.postalCodeTxtView];
    //
    //
    //    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    //    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    //    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
    //                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
    //                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    //    [numberToolbar sizeToFit];
    //    self.mobileNumberTxtView.inputAccessoryView = numberToolbar;
    //
    //    UIToolbar* numberToolbarCode = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    //    numberToolbarCode.barStyle = UIBarStyleBlackTranslucent;
    //    numberToolbarCode.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadCode)],
    //                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
    //                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadCode)]];
    //    [numberToolbarCode sizeToFit];
    //    self.mobileCodeTxtView.inputAccessoryView = numberToolbarCode;
    //
    //
    //    UIToolbar* PostalCodeToolbarCode = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    //    PostalCodeToolbarCode.barStyle = UIBarStyleBlackTranslucent;
    //    PostalCodeToolbarCode.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPostalCode)],
    //                                    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
    //                                    [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithPostalCode)]];
    //    [PostalCodeToolbarCode sizeToFit];
    //    self.postalCodeTxtView.inputAccessoryView = PostalCodeToolbarCode;
    //
    //    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]])
    //    {
    //        [self fetchCountryDataformSever];
    //    }
    //    [self registerForKeyboardNotifications];
    
    
    [self payActionForknet ];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)dropDownActn:(UIButton *)sender
{
    //countryNameArray = [[NSMutableArray alloc] initWithObjects:@"India",@"Pakistan",@"India1",@"Pakistan1",@"India2",@"Pakistan2", nil];
    self.dropBtn.selected = !self.dropBtn.selected;
    if (self.dropBtn.selected)
    {
        self.dropDownUpArrow.hidden = NO;
        self.dropDownDwnArrow.hidden = YES;
        if (self.countryDropDownObj.view.superview)
        {
            [self.countryDropDownObj.view removeFromSuperview];
        }
        self.countryDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        self.countryDropDownObj.cellHeight = 40.0f;
        self.countryDropDownObj.view.frame = CGRectMake((self.countryTextView.frame.origin.x),(self.countryTextView.frame.origin.y+self.countryTextView.frame.size.height), (self.countryTextView.frame.size.width), 0);
        
        self.countryDropDownObj.dropDownDelegate = self;
        self.countryDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:countryNameArray];
        
        self.countryDropDownObj.view.layer.borderWidth = 0.1;
        self.countryDropDownObj.view.layer.shadowOpacity = 1.0;
        self.countryDropDownObj.view.layer.shadowRadius = 5.0;
        self.countryDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        self.countryDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.countryDropDownObj.textLabelColor = [UIColor blackColor];
        self.countryDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (countryNameArray.count>0)
        {
            [self.dropBtn.superview addSubview:self.countryDropDownObj.view];
        }
        else
        {
            self.dropBtn.selected=NO;
        }
        
        CGFloat tableHT = 120.0f;
        
        if ((self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.countryDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.countryDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x+2,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.dropDownUpArrow.hidden = YES;
        self.dropDownDwnArrow.hidden = NO;
        self.countryDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.countryDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        for (UIView *vw in self.view.subviews)
        {
            [vw setUserInteractionEnabled:YES];
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
             if (finished)
             {
                 [self.countryDropDownObj.view removeFromSuperview];
             }
         }];
    }
}

- (IBAction)payBtnActn:(UIButton *)sender {
    
    if ((self.addressline1TxtView.text.length == 0) && (self.addressline2TxtView.text.length == 0) && (self.cityTxtView.text.length == 0) && (self.countryTextView.text.length == 0) && (self.postalCodeTxtView.text.length == 0) &&(self.mobileCodeTxtView.text.length == 0) &&(self.mobileNumberTxtView.text.length == 0))
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address details") title:localize(@"Sorry..!")];
    }
    else if(self.addressline1TxtView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address") title:localize(@"Sorry..!")];
    }
    else if(self.addressline2TxtView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address") title:localize(@"Sorry..!")];
    }
    else if(self.countryTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your city") title:localize(@"Sorry..!")];
    }
    else if(self.countryTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country") title:localize(@"Sorry..!")];
    }
    else if(self.postalCodeTxtView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your postal code") title:localize(@"Sorry..!")];
    }
    else if(self.mobileCodeTxtView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter country code") title:localize(@"Sorry..!")];
    }
    else if(self.mobileNumberTxtView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
    }
    else
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableDictionary *updatePurchaseData = [[NSMutableDictionary alloc] initWithDictionary:[self getUpdatePurchaseData]];
        
        NSLog(@"%@",updatePurchaseData);
        
        [ApplicationDelegate.engine loadUpdatePurchaseOnCompleteWithDictionary:updatePurchaseData CompletionHandler:^(NSMutableDictionary *responseDict)
         {
             {
                 if ([ApplicationDelegate isValid:responseDict])
                 {
                     //NSLog(@"%@",responseDict);
                     if (responseDict.count>0)
                     {
                         if ([[responseDict objectForKey:@"Success"] integerValue])
                         {
                             
                             self.responseBackView.frame = self.view.frame;
                             [self.view addSubview:self.responseBackView];
                             self.topBackView.hidden = YES;
                             self.seperatorView.hidden = YES;
                             self.downBackView.hidden = YES;
                             
                             [HomeViewController sharedViewController].backArrowView.hidden = YES;
                             [HomeViewController sharedViewController].backBttn.hidden = YES;
                             
                             NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.responseWebView.frame.size.width-10))];
                             
                             //[self.responseWebView loadHTMLString:[responseDict objectForKey:@"Message"] baseURL:nil];
                             
                             activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                             activityIndicator.frame =CGRectMake(100, 100, 100, 100);
                             CGAffineTransform transform = CGAffineTransformMakeScale(2.5f, 2.5f);
                             activityIndicator.transform = transform;
                             activityIndicator.center=self.view.center;
                             [self.responseWebView addSubview:activityIndicator];
                             [activityIndicator startAnimating];
                             
                             [ApplicationDelegate addProgressHUDToView:self.view];
                             
                             [self.responseWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[responseDict objectForKey:@"Message"]]]];
                             
                             [[HomeViewController sharedViewController] globalLabel:@"0"];
                             
                             self.responseWebView.delegate=self;
                             
                             
                             //[ApplicationDelegate removeProgressHUD];
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDict objectForKey:@"Message"] title:nil];
                             
                             [ApplicationDelegate removeProgressHUD];
                         }
                         
                         [ApplicationDelegate removeProgressHUD];
                     }
                 }
             }
         } errorHandler:^(NSError *error)
         {
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
}

- (IBAction)BackToHome:(UIButton *)sender {
    [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [ApplicationDelegate addProgressHUDToView:self.view];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [ApplicationDelegate removeProgressHUD];
    [activityIndicator removeFromSuperview];
    NSString *currentURL = webView.request.URL.absoluteString;
    NSLog(@"currenturl=%@",currentURL);
    
    if ([currentURL rangeOfString:@"Thankyou"].location == NSNotFound) {
        //  [ApplicationDelegate showAlertWithMessage:@"KNET payment gateway Error.Please try after some time"  title:nil];
        
        self.responseWebView.scalesPageToFit = YES;
        
        
        
        
    }
    else
        
    {
        self.responseWebView.scalesPageToFit = NO;
        //  ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:0];
        
        [[HomeViewController sharedViewController] globalLabel:@"0"];
        
        
    }
    
    
    if ([currentURL rangeOfString:@"PaymentID=ERROR"].location ==NSNotFound) {
        
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"KNET payment gateway Error.Please try after some time"  title:nil];
    }
    
    /// Disabled the below codes
    
    /*
     
     
     if ([currentURL rangeOfString:@"Result="].location == NSNotFound) {
     //  [ApplicationDelegate showAlertWithMessage:@"KNET payment gateway Error.Please try after some time"  title:nil];
     }
     else
     
     {
     NSArray *stringArray = [currentURL componentsSeparatedByString:@"&"];
     self.responseWebView.scalesPageToFit = NO;
     NSLog(@"stringarray=%@",stringArray);
     ////////////////////////////////////////////////////////
     
     
     [ApplicationDelegate addProgressHUDToView:self.view];
     
     /*
     
     "Auth":"String content",
     "OrderId":"String content",
     "PaymentID":"String content",
     "PostDate":"String content",
     "Ref":"String content",
     "Result":"String content",
     "TrackID":"String content",
     "TranID":"String content",
     "UserId":"String content"
     
     */
    
    /*
     
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     NSMutableDictionary *insertPaymentHistoryData = [[NSMutableDictionary alloc] init];
     [insertPaymentHistoryData setObject:[self populateString:[stringArray objectAtIndex:4] arg:@"="]  forKey:@"Auth"];
     [insertPaymentHistoryData setObject:[self populateString:[stringArray objectAtIndex:7] arg:@"="] forKey:@"OrderId"];
     [insertPaymentHistoryData setObject:[self populateString:[stringArray objectAtIndex:8] arg:@"="] forKey:@"PaymentID"];
     [insertPaymentHistoryData setObject:[self populateString:[stringArray objectAtIndex:2] arg:@"="] forKey:@"PostDate"];
     [insertPaymentHistoryData setObject:[self populateString:[stringArray objectAtIndex:5] arg:@"="] forKey:@"Ref"];
     [insertPaymentHistoryData setObject:[self populateString:[stringArray objectAtIndex:1] arg:@"="] forKey:@"Result"];
     [insertPaymentHistoryData setObject:[self populateString:[stringArray objectAtIndex:6] arg:@"="] forKey:@"TrackID"];
     [insertPaymentHistoryData setObject:[self populateString:[stringArray objectAtIndex:3] arg:@"="] forKey:@"TranID"];
     [insertPaymentHistoryData setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
     
     
     [ApplicationDelegate.engine InsertPaymentHistory:insertPaymentHistoryData CompletionHandler:^(NSMutableDictionary *responseDict)
     {
     
     
     {
     if ([ApplicationDelegate isValid:responseDict])
     {
     if (responseDict.count>0)
     {
     if ([[responseDict objectForKey:@"Success"] integerValue])
     {
     //                             BillingAddressViewController * billingAddressVC = [[BillingAddressViewController alloc]initWithNibName:@"BillingAddressViewController" bundle:nil];
     //                             billingAddressVC.view.backgroundColor = [UIColor clearColor];
     //                             billingAddressVC.SubTotal.text=self.SubTotal.text;
     //                             billingAddressVC.DeliveryCharge.text=self.DeliveryCharge.text;
     //                             billingAddressVC.TotalCharge.text=self.TotalCharge.text;
     //                             billingAddressVC.orderId = self.orderId;
     //                             if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[BillingAddressViewController class]])
     //                             {
     //
     //                             }
     self.thankYouPageBackView.frame = self.view.frame;
     [self.view addSubview:self.thankYouPageBackView];
     self.topBackView.hidden = YES;
     self.seperatorView.hidden = YES;
     self.downBackView.hidden = YES;
     
     [self.responseBackView removeFromSuperview];
     
     [HomeViewController sharedViewController].backArrowView.hidden = YES;
     [HomeViewController sharedViewController].backBttn.hidden = YES;
     
     NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.thankYouPageWebView.frame.size.width-10))];
     
     [self.thankYouPageWebView loadHTMLString:[responseDict objectForKey:@"Message"] baseURL:nil];
     
     [[HomeViewController sharedViewController] globalLabel:@"0"];
     
     [ApplicationDelegate removeProgressHUD];
     
     }
     else
     {
     [ApplicationDelegate showAlertWithMessage:[responseDict objectForKey:@"Message"] title:nil];
     
     [ApplicationDelegate removeProgressHUD];
     }
     
     [ApplicationDelegate removeProgressHUD];
     }
     }
     }
     } errorHandler:^(NSError *error)
     {
     [ApplicationDelegate removeProgressHUD];
     [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
     
     
     /////////////////////////////////////////////////////
     
     }
     
     */
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [activityIndicator removeFromSuperview];
    [ApplicationDelegate removeProgressHUD];
    NSLog(@"Error : %@",error);
    
}

-(void)selectList:(int)selectedIndex
{
    [UIView animateWithDuration:0.4f animations:^{
        self.countryDropDownObj.view.frame =
        CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                   self.countryDropDownObj.view.frame.origin.y,
                   self.countryDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.countryDropDownObj.view removeFromSuperview];
        }
    }];
    
    self.dropBtn.selected = NO;
    self.dropDownUpArrow.hidden = YES;
    self.dropDownDwnArrow.hidden = NO;
    self.countryTextView.text =[countryNameArray objectAtIndex:selectedIndex];
}


-(NSMutableDictionary *)getUpdatePurchaseData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * sampDelChar = self.DeliveryCharge.text;
    sampDelChar = [sampDelChar stringByReplacingOccurrencesOfString:@" " withString:@""];
    sampDelChar = [sampDelChar stringByReplacingOccurrencesOfString:@"KD" withString:@""];
    [postDic setObject:sampDelChar forKey:@"DeliveryCharges"];
    [postDic setObject:self.orderId forKey:@"OrderId"];
    [postDic setObject:self.paymentMethod forKey:@"PaymentMethod"];
    NSString * sampSubTot = self.SubTotal.text;
    sampSubTot = [sampSubTot stringByReplacingOccurrencesOfString:@" " withString:@""];
    sampSubTot = [sampSubTot stringByReplacingOccurrencesOfString:@"KD" withString:@""];
    [postDic setObject:sampSubTot forKey:@"SubTotal"];
    NSString * sampTotal = self.TotalCharge.text;
    sampTotal = [sampTotal stringByReplacingOccurrencesOfString:@" " withString:@""];
    sampTotal = [sampTotal stringByReplacingOccurrencesOfString:@"KD" withString:@""];
    [postDic setObject:sampTotal forKey:@"Total"];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    
    
    [postDic setObject:@"" forKey:@"AddressLine1"];
    [postDic setObject:@"" forKey:@"AddressLine2"];
    [postDic setObject:@"" forKey:@"City"];
    [postDic setObject:@"" forKey:@"Country"];
    [postDic setObject:@"" forKey:@"PostalCode"];
    [postDic setObject:@"" forKey:@"MobCode"];
    [postDic setObject:@"" forKey:@"MobileNumber"];
    
    return postDic;
}

-(void)fetchCountryDataformSever
{
    NSMutableDictionary *countryData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserCountryPostData]];
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getCountryDetailsWithDataDictionary:countryData CompletionHandler:^(NSMutableArray *responseArray)
     {
         if ([ApplicationDelegate isValid:responseArray])
         {
             if (responseArray.count>0)
             {
                 self.countryCodeAndName = [[NSMutableArray alloc]init];
                 countryNameArray = [[NSMutableArray alloc]init];
                 countryCodeArray = [[NSMutableArray alloc]init];
                 for (int i=0; i<responseArray.count; i++)
                 {
                     [self.countryCodeAndName addObject:[responseArray objectAtIndex:i]];
                     [countryNameArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"CountryName"]];
                     [countryCodeArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"CountryCode"]];
                 }
             }
             [ApplicationDelegate removeProgressHUD];
         }
         
     } errorHandler:^(NSError *error)
     {
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}

-(NSMutableDictionary *)getUserCountryPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else
    {
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)cancelNumberPad
{
    [ self.mobileNumberTxtView resignFirstResponder];
    self.mobileNumberTxtView.text = @"";
}

-(void)doneWithNumberPad
{
    [self.mobileNumberTxtView resignFirstResponder];
}

-(void)cancelNumberPadCode
{
    [ self.mobileCodeTxtView resignFirstResponder];
    self.mobileCodeTxtView.text = @"";
}

-(void)doneWithNumberPadCode
{
    [self.mobileCodeTxtView resignFirstResponder];
}

-(void)cancelPostalCode
{
    [ self.postalCodeTxtView resignFirstResponder];
    self.postalCodeTxtView.text = @"";
}

-(void)doneWithPostalCode
{
    [self.postalCodeTxtView resignFirstResponder];
}

- (void)limitTextField:(NSNotification *)note
{
    int limit = 10;
    int codeLimit = 3;
    int postcodeLimit=6;
    
    if(self.postalCodeTxtView.text.length> postcodeLimit)
    {
        [self.postalCodeTxtView setText:[self.postalCodeTxtView.text substringToIndex:postcodeLimit]];
    }
    else if (self.mobileNumberTxtView.text.length > limit)
    {
        [self.mobileNumberTxtView setText:[self.mobileNumberTxtView.text substringToIndex:limit]];
    }
    else if(self.mobileCodeTxtView.text.length > codeLimit)
    {
        [self.mobileCodeTxtView setText:[self.mobileCodeTxtView.text substringToIndex:codeLimit]];
    }
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height - 100, 0.0);
    self.mainScroll.contentInset = contentInsets;
    self.mainScroll.scrollIndicatorInsets = contentInsets;
    
    //CGRect aRect = self.editBackView.frame;
    CGRect aRect = CGRectMake(self.mainScroll.frame.origin.x, self.mainScroll.frame.origin.y, scrollFrameSize.width, scrollFrameSize.height);
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.addressline1TxtView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.addressline1TxtView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.addressline2TxtView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.addressline2TxtView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.cityTxtView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.cityTxtView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.postalCodeTxtView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.postalCodeTxtView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.mobileCodeTxtView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.mobileCodeTxtView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.mobileNumberTxtView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.mobileNumberTxtView.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.mainScroll.contentSize = CGSizeMake(self.mainScroll.frame.size.width, scrollFrameSize.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainScroll.contentInset = contentInsets;
    self.mainScroll.scrollIndicatorInsets = contentInsets;
}


-(void)payActionForknet
{
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableDictionary *updatePurchaseData = [[NSMutableDictionary alloc] initWithDictionary:[self getUpdatePurchaseData]];
        
        NSLog(@"%@",updatePurchaseData);
        
        [ApplicationDelegate.engine loadUpdatePurchaseOnCompleteWithDictionary:updatePurchaseData CompletionHandler:^(NSMutableDictionary *responseDict)
         {
             {
                 if ([ApplicationDelegate isValid:responseDict])
                 {
                     NSLog(@"%@",responseDict);
                     if (responseDict.count>0)
                     {
                         if ([[responseDict objectForKey:@"Success"] integerValue])
                         {
                             
                             self.responseBackView.frame = self.view.frame;
                             [self.view addSubview:self.responseBackView];
                             self.topBackView.hidden = YES;
                             self.seperatorView.hidden = YES;
                             self.downBackView.hidden = YES;
                             
                             [HomeViewController sharedViewController].backArrowView.hidden = YES;
                             [HomeViewController sharedViewController].backBttn.hidden = YES;
                             
                             NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.responseWebView.frame.size.width-10))];
                             
                             //[self.responseWebView loadHTMLString:[responseDict objectForKey:@"Message"] baseURL:nil];
                             
                             activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                             activityIndicator.frame =CGRectMake(100, 100, 100, 100);
                             CGAffineTransform transform = CGAffineTransformMakeScale(2.5f, 2.5f);
                             activityIndicator.transform = transform;
                             activityIndicator.center=self.view.center;
                             [self.responseWebView addSubview:activityIndicator];
                             [activityIndicator startAnimating];
                             
                             [ApplicationDelegate addProgressHUDToView:self.view];
                             
                             [self.responseWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[responseDict objectForKey:@"Message"]]]];
                             
                             [[HomeViewController sharedViewController] globalLabel:@"0"];
                             
                             self.responseWebView.delegate=self;
                             
                             
                             //[ApplicationDelegate removeProgressHUD];
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDict objectForKey:@"Message"] title:nil];
                             
                             [ApplicationDelegate removeProgressHUD];
                         }
                         
                         [ApplicationDelegate removeProgressHUD];
                     }
                 }
             }
         } errorHandler:^(NSError *error)
         {
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
}



-(NSString *)populateString:(NSString *)convertString arg:(NSString *)after
{
    NSString *str =convertString;
    NSRange equalRange = [str rangeOfString:after options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        NSString *result = [str substringFromIndex:equalRange.location + equalRange.length];
        NSLog(@"The result = %@", result);
        return result;
    } else {
        NSLog(@"There is no = in the string");
        return nil;
    }
}
@end
