//
//  FavouriteItemsViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "FavouriteItemsViewController.h"
#import "MyFavoriteCell.h"
#import "UserFavouriteData.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface FavouriteItemsViewController (){

  MyFavoriteCell * CellView;
}

@end


@implementation FavouriteItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headingLbl.text = localize(@"MY FAVORITES :");
     [self.headingLbl setFont:EBRI_NORMAL_FONT(17)];
     [self.warningLbl setFont:EBRI_NORMAL_FONT(15)];
    
    self.warningLbl.text = localize(@"No items added to the favorites!");
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.headingLbl.textAlignment = NSTextAlignmentRight;
    }
    else{
        self.headingLbl.textAlignment = NSTextAlignmentLeft;
    }
    [self fetchUserFavouriteDataformSever];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)fetchUserFavouriteDataformSever{
    [ApplicationDelegate addProgressHUDToView:self.view];

    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserData]];
    
   
    [ApplicationDelegate.engine loadUsersFavoriteDictionary:userData CompletionHandler:^(NSMutableArray *responseArr) {
        
        {
            
            if ([ApplicationDelegate isValid:responseArr])
            {
                
                
                self.FavouriteDataArray = responseArr;
                
                if (self.FavouriteDataArray.count == 0) {
                    self.warningBackView.hidden = NO;
                    self.warningLbl.font = [UIFont fontWithName:@"Ebrima" size:15];
                    self.warningLbl.hidden = NO;
                    [self loadScroll];
                } else {
                    [self loadScroll];
                }
                    
                
                
               
            }
            
            [ApplicationDelegate removeProgressHUD];
            
        }
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        
    }];
}




-(void)loadScroll {
    
    
    for (UIView *sub in self.FavouriteScroll.subviews) {
        [sub removeFromSuperview];
    }
    
    [self.FavouriteScroll setContentOffset:CGPointZero];
    
    float setYvalue = 0.0f;
    float scrollHeight = 0.0f;
    
    for (int i = 0; i<self.FavouriteDataArray.count; i++) {
        
        CellView = [[MyFavoriteCell alloc]init];
        
        CellView.frame = CGRectMake(0, setYvalue, CellView.frame.size.width, CellView.frame.size.height);
        
        CellView.delegateFavItem = self;
       CellView.deleteButtonView.tag=i;
        CellView.detailsButtonView.tag=i;
        setYvalue = CellView.frame.origin.y + CellView.frame.size.height;
        scrollHeight = CellView.frame.origin.y + CellView.frame.size.height;
        UserFavouriteData *userFavObj = [ApplicationDelegate.mapper getUserFavouriteFromDictionary:[self.FavouriteDataArray objectAtIndex:i]];
        NSURL * imageurl = [[NSURL alloc]init];
        imageurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:userFavObj.image]];
        CellView.brandName.text = userFavObj.brand;
         [CellView.brandName setFont:EBRI_BOLD_FONT(15)];
         [CellView.itemName setFont:EBRI_NORMAL_FONT(13)];
        CellView.brandName .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
        CellView.itemName .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
        CellView.itemName.text = userFavObj.name;
         [ApplicationDelegate loadMarqueeLabelWithText:CellView.itemName.text Font:CellView.itemName.font InPlaceOfLabel:CellView.itemName];        
        [CellView.FavImageView sd_setImageWithURL:imageurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
        CellView.FavImageView.layer.cornerRadius = 3.0;
        CellView.FavImageView.layer.masksToBounds = YES;
        [self.FavouriteScroll addSubview:CellView];
        
    }
    
   
    self.FavouriteScroll.contentSize = CGSizeMake(self.FavouriteScroll.frame.size.width,scrollHeight);
    
    
}



-(NSMutableDictionary *)getUserData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}



-(void) deleteDidClicked:(MyFavoriteCell *)FavItem
{
    
   NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
     UserFavouriteData *userFavObj = [ApplicationDelegate.mapper getUserFavouriteFromDictionary:[self.FavouriteDataArray objectAtIndex:FavItem.deleteButtonView.tag]];
    
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    [postDic setObject:userFavObj.Dealid forKey:@"Deal_id"];
    [postDic setObject:userFavObj.size forKey:@"size"];
     [ApplicationDelegate.engine DeleteFavouriteItemWithPostDataDictionary:postDic CompletionHandler:^(NSMutableDictionary *responseDictionary) {
          if ([ApplicationDelegate isValid:responseDictionary])
        {
           
            
            if (responseDictionary.count>0)
            {
                
                if ([responseDictionary objectForKey:@"Success"])
                {
                    
                    [self fetchUserFavouriteDataformSever];
                    
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                }
                
                else{
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                }
                
            }
            
            
        }
         
         
    } errorHandler:^(NSError *error) {
        
    }];
    
    
}

-(void)detailsDidClicked:(MyFavoriteCell *) FavItem
{
    
   UserFavouriteData *userFavObj = [ApplicationDelegate.mapper getUserFavouriteFromDictionary:[self.FavouriteDataArray objectAtIndex:FavItem.detailsButtonView.tag]];
    NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
    newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:userFavObj.Dealid forKey:@"TrackId"];
    [actionTracking synchronize];
    newDealsDetailVC.productId = userFavObj.Dealid;
    [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
    
    
}


@end
