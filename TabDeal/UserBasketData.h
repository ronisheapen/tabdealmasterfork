//
//  UserBasketData.h
//  TabDeal
//
//  Created by satheesh on 10/14/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserBasketData : NSObject
@property(nonatomic,retain)NSString *brand;
@property(nonatomic,retain)NSString *dealId;
@property(nonatomic,retain)NSString *image;
@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSString *quantity;
@property(nonatomic,retain)NSString *rate;
@property(nonatomic,retain)NSString *stock;
@property(nonatomic,retain)NSString *size;
@end
