//
//  MyBankDetailsViewController.m
//  TabDeal
//
//  Created by satheesh on 10/8/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "MyBankDetailsViewController.h"
#import "BankInformation.h"

@interface MyBankDetailsViewController ()

@end

@implementation MyBankDetailsViewController


-(void) viewWillAppear:(BOOL)animated{
    //[self updateTabViewSelection];
    //NSLog(@"%@",self.orderId);
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:([ApplicationDelegate.innerViewCount intValue]+1)];
    if (ApplicationDelegate.innerViewCount.intValue==0) {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else{
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]]) {
        [self fetchCountryDataformSever];
        [self fetchBankDataformSever];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    self.headdingLbl.text = localize(@"MY BANK DETAILS");
    [self.saveBtn setTitle:localize(@"SAVE") forState:UIControlStateNormal];
    self.myBankDetailsHeading.text = localize(@"MY BANK DETAILS");
    self.fullNameTxt.placeholder = localize(@"Full Name");
    self.bankNameTxt.placeholder = localize(@"Bank Name");
    self.bankAddressTxt.placeholder = localize(@"Bank Address");
    self.countryTxt.placeholder = localize(@"Country");
    self.ibanTxt.placeholder = localize(@"IBAN");
    self.swiftCodeTxt.placeholder = localize(@"SWIFT Code");
   // [self.saveChangesBtn setTitle:localize(@"SAVE CHANGES") forState:UIControlStateNormal];
    
    self.fullnameEditTxt.placeholder = localize(@"Full Name");
    self.bankNameEditTxt.placeholder = localize(@"Bank Name");
    self.bankAddressEditTxt.placeholder = localize(@"Bank Address");
    self.countryEditTxt.placeholder = localize(@"Country");
    self.ibanEditTxt.placeholder = localize(@"IBAN");
    self.swiftCodeEditTxt.placeholder = localize(@"SWIFT Code");
    [self.AddBankButton setTitle:localize(@"Add Bank Details") forState:UIControlStateNormal];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xibLoading];
    
    self.mainScrollView.hidden=YES;
    self.AddBankButton.hidden=YES;
    
    UIToolbar* numberToolbarIban = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarIban.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarIban.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbarIban sizeToFit];
    self.ibanEditTxt.inputAccessoryView = numberToolbarIban;
    
    
    UIToolbar* numberToolbarSwift = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarSwift.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarSwift.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadCode)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadCode)]];
    [numberToolbarSwift sizeToFit];
    self.swiftCodeEditTxt.inputAccessoryView = numberToolbarSwift;
    
    [self registerForKeyboardNotifications];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)fetchCountryDataformSever{
    
    NSMutableDictionary *countryData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserCountryPostData]];
    NSLog(@"%@",countryData);
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getCountryDetailsWithDataDictionary:countryData CompletionHandler:^(NSMutableArray *responseArray)
     {
         
         if ([ApplicationDelegate isValid:responseArray])
         {
             if (responseArray.count>0)
             {
                 self.countryCodeAndName = [[NSMutableArray alloc]init];
                 countryNameArray = [[NSMutableArray alloc]init];
                 countryCodeArray = [[NSMutableArray alloc]init];
                 for (int i=0; i<responseArray.count; i++)
                 {
                     [self.countryCodeAndName addObject:[responseArray objectAtIndex:i]];
                     [countryNameArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"CountryName"]];
                     [countryCodeArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"CountryCode"]];
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
         }
         
     } errorHandler:^(NSError *error) {
         
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}


-(NSMutableDictionary *)getUserCountryPostData{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(void)fetchBankDataformSever{
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    NSLog(@"%@",userData);
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getBankDetailsWithDataDictionary:userData CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         
         if ([ApplicationDelegate isValid:responseDictionary])
         {
             if (responseDictionary.count>0)
             {
                 self.mainScrollView.hidden = NO;
                 BankInformation *bankObj = [ApplicationDelegate.mapper getBankinformationFromDictionary:responseDictionary];
                 NSLog(@"%@",bankObj);
                 NSLog(@"%@",responseDictionary);
                 
                 self.bankAddressTxt.text = bankObj.bankAddress;
                 self.bankNameTxt.text = bankObj.bankName;
                 self.countryTxt.text = bankObj.country;
                 self.fullNameTxt.text = bankObj.fullName;
                 self.ibanTxt.text = bankObj.iBAN;
                 self.swiftCodeTxt.text = bankObj.swiftCode;
                 
                 self.bankAddressEditTxt.text = bankObj.bankAddress;
                 self.bankNameEditTxt.text = bankObj.bankName;
                 self.countryEditTxt.text = bankObj.country;
                 self.fullnameEditTxt.text = bankObj.fullName;
                 self.ibanEditTxt.text = bankObj.iBAN;
                 self.swiftCodeEditTxt.text = bankObj.swiftCode;
                 
                 if ((self.bankAddressTxt.text.length == 0) && (self.bankNameTxt.text.length == 0) && (self.countryTxt.text.length == 0) && (self.fullNameTxt.text.length == 0)&& (self.ibanTxt.text.length == 0)&& (self.swiftCodeTxt.text.length == 0))
                 {
                     
                     self.mainScrollView.hidden=YES;
                     self.AddBankButton.hidden=NO;
                     
                 }
                 
                 else
                 {
                     self.mainScrollView.hidden=NO;
                     self.AddBankButton.hidden=YES;
                 }
                 
             }
         }
         
         [ApplicationDelegate removeProgressHUD];
     } errorHandler:^(NSError *error) {
         
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}

-(NSMutableDictionary *)getUserPostData{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

- (IBAction)dropDownActn:(UIButton *)sender {
    
    //countryNameArray = [[NSMutableArray alloc] initWithObjects:@"India",@"Pakistan",@"India1",@"Pakistan1",@"India2",@"Pakistan2", nil];
    
    self.dropBtn.selected = !self.dropBtn.selected;
    if (self.dropBtn.selected)
    {
        self.dropDownUpArrow.hidden = NO;
        self.dropDownDwnArrow.hidden = YES;
        if (self.countryDropDownObj.view.superview) {
            [self.countryDropDownObj.view removeFromSuperview];
        }
        
        self.countryDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.countryDropDownObj.cellHeight = 40.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.countryDropDownObj.view.frame = CGRectMake((self.countryTxt.frame.origin.x),(self.countryTxt.frame.origin.y+self.countryTxt.frame.size.height), (self.countryTxt.frame.size.width), 0);
        
        self.countryDropDownObj.dropDownDelegate = self;
        self.countryDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:countryNameArray];
        
        self.countryDropDownObj.view.layer.borderWidth = 0.1;
        self.countryDropDownObj.view.layer.shadowOpacity = 1.0;
        self.countryDropDownObj.view.layer.shadowRadius = 5.0;
        self.countryDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.countryDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:8.0f];
        self.countryDropDownObj.textLabelColor = [UIColor blackColor];
        self.countryDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (countryNameArray.count>0)
        {
            [self.dropBtn.superview addSubview:self.countryDropDownObj.view];
        }
        else
        {
            self.dropBtn.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)+18.0f;
        }
        //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.countryDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.countryDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x+2,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.dropDownUpArrow.hidden = YES;
        self.dropDownDwnArrow.hidden = NO;
        self.countryDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.countryDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.countryDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
}

-(void)selectList:(int)selectedIndex
{
    [UIView animateWithDuration:0.4f animations:^{
        self.countryDropDownObj.view.frame =
        CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                   self.countryDropDownObj.view.frame.origin.y,
                   self.countryDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.countryDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.dropBtn.selected = NO;
    self.dropDownUpArrow.hidden = YES;
    self.dropDownDwnArrow.hidden = NO;
    self.countryEditTxt.text = [countryNameArray objectAtIndex:selectedIndex];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveChangesBtnActn:(UIButton *)sender {
    
    if ((self.fullnameEditTxt.text.length == 0) && (self.bankNameEditTxt.text.length == 0) && (self.bankAddressEditTxt.text.length == 0) && (self.countryEditTxt.text.length == 0) && (self.ibanEditTxt.text.length == 0)&& (self.swiftCodeEditTxt.text.length == 0))
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your bank details") title:localize(@"Sorry..!")];
        
        
    } else if(self.fullnameEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your name") title:localize(@"Sorry..!")];
        
    }
    else if(self.bankNameEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your bank name") title:localize(@"Sorry..!")];
        
    }
    else if(self.bankAddressEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your bank adderss") title:localize(@"Sorry..!")];
        
    }
    else if(self.countryEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter country") title:localize(@"Sorry..!")];
        
    }
    else if(self.ibanEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your IBAN") title:localize(@"Sorry..!")];
        
    }
    else if(self.swiftCodeEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your SWIFT code") title:localize(@"Sorry..!")];
        
    }
    
    else{
        [self postAddressDetails];
    }
}

-(void)postAddressDetails{
    NSMutableDictionary *addressData = [[NSMutableDictionary alloc] initWithDictionary:[self getBillingAddressPostData]];
    NSLog(@"%@",addressData);
    if (addressData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine EditBillingAddressWithDataDictionary:addressData CompletionHandler:^(NSMutableDictionary *responseDic)
         
         {
             if ([ApplicationDelegate isValid:responseDic])
             {
                 
                 if (responseDic.count>0)
                 {
                     NSLog(@"%@",responseDic);
                     if ([[responseDic objectForKey:@"Success"] integerValue])
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                         [self.navigationController popViewControllerAnimated:NO];
                         //  [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:@"Message"];
                         
//                         PaymentMethodViewController * paymentScreenVC = [[PaymentMethodViewController alloc]initWithNibName:@"PaymentMethodViewController" bundle:nil];
//                         paymentScreenVC.view.backgroundColor = [UIColor clearColor];
//                         paymentScreenVC.SubTotal.text=self.SubTotal.text;
//                         paymentScreenVC.DeliveryCharge.text=self.DeliveryCharge.text;
//                         paymentScreenVC.TotalCharge.text=self.TotalCharge.text;
//                         paymentScreenVC.orderId = self.orderId;
//                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PaymentMethodViewController class]])
//                         {
//                             [ApplicationDelegate.homeTabNav pushViewController:paymentScreenVC animated:NO];
//                         }
                         
                         [ApplicationDelegate removeProgressHUD];
                         
                     }
                     
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                         
                         [ApplicationDelegate removeProgressHUD];
                         //self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
                         
                     }
                     
                     
                     
                 }
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
             
         }];
        
    }
}

-(NSMutableDictionary *)getBillingAddressPostData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.bankAddressEditTxt.text forKey:@"BankAddress"];
    [postDic setObject:self.bankNameEditTxt.text forKey:@"BankName"];
    [postDic setObject:self.countryEditTxt.text forKey:@"Country"];
    [postDic setObject:self.fullnameEditTxt.text forKey:@"FullName"];
    [postDic setObject:self.ibanEditTxt.text forKey:@"IBAN"];
    [postDic setObject:self.swiftCodeEditTxt.text forKey:@"SwiftCode"];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    //[postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    return postDic;
}

- (IBAction)editBankBtnActn:(UIButton *)sender {
    self.bankAddressEditTxt.text = self.bankAddressTxt.text;
    self.bankNameEditTxt.text = self.bankNameTxt.text;
    self.countryEditTxt.text = self.countryTxt.text;
    self.fullnameEditTxt.text = self.fullNameTxt.text;
    self.ibanEditTxt.text = self.ibanTxt.text;
    self.swiftCodeEditTxt.text = self.swiftCodeTxt.text;
    [self.view addSubview:self.editBankDetailsBackView];
    [self.editBankDetailsBackView setFrame:self.editBankDetailsBackView.superview.bounds];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)cancelNumberPad{
    [ self.ibanEditTxt resignFirstResponder];
    self.ibanEditTxt.text = @"";
}

-(void)doneWithNumberPad{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.ibanEditTxt resignFirstResponder];
}


-(void)cancelNumberPadCode{
    [ self.swiftCodeEditTxt resignFirstResponder];
    self.swiftCodeEditTxt.text = @"";
}

-(void)doneWithNumberPadCode{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.swiftCodeEditTxt resignFirstResponder];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.whiteBackView.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.fullNameTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.fullNameTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.bankNameTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.bankNameTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.bankAddressTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.bankAddressTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.ibanTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.ibanTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.swiftCodeTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.swiftCodeTxt.frame animated:YES];
    }
    
    /* NSDictionary* info = [aNotification userInfo];
     CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
     CGRect bkgndRect = self.mTextField.superview.frame;
     bkgndRect.size.height += kbSize.height;
     [self.mTextField.superview setFrame:bkgndRect];
     [self.mScrollView setContentOffset:CGPointMake(0.0, self.mTextField.frame.origin.y-kbSize.height) animated:YES];*/
    
    
    
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.whiteBackView.frame.size.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
}


-(void)xibLoading
{
    NSString *nibName = @"MyBankDetailsViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
    
    
}

- (IBAction)closeBtnAction:(UIButton *)sender {
    
    
    [self.editBankDetailsBackView removeFromSuperview];
   
    
}
@end
