//
//  PreviousOrderDetailViewController.m
//  TabDeal
//
//  Created by satheesh on 10/8/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "PreviousOrderDetailViewController.h"
#import "PreviousOrderDetailCell.h"
#import "OrderDetailsList.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NewDealsDetailsViewController.h"

@interface PreviousOrderDetailViewController (){
    UIView * customSeperator;
}

@end

@implementation PreviousOrderDetailViewController

@synthesize orderId;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xibLoading];
    
   self.headingLbl.text = localize(@"ORDER NO : 123456");
    
    self.subTotalLbl.text = localize(@"Sub Total");
    self.deliveryChargesLbl.text = localize(@"Delivery Charges");
    self.totalLbl.text = localize(@"Total");
    
    //self.warningLbl.text = localize(@"");
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.headingLbl.textAlignment = NSTextAlignmentRight;
    }
    else{
        self.headingLbl.textAlignment = NSTextAlignmentLeft;
    }
    // Do any additional setup after loading the view from its nib.
    
   // [self fetchUserPreviousOrderDetailsformSever];
}

-(void) viewWillAppear:(BOOL)animated
{
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:([ApplicationDelegate.innerViewCount intValue]+1)];
    if (ApplicationDelegate.innerViewCount.intValue==0) {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else{
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    [self fetchUserPreviousOrderDetailsformSever];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.PreviousOrderDetailsList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    PreviousOrderDetailCell *cell = (PreviousOrderDetailCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PreviousOrderDetailCell_a" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else{
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PreviousOrderDetailCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
    }
    
    OrderDetailsList *userPreOrderListObj = [ApplicationDelegate.mapper getUserOrderListDetailsFromDictionary:[self.PreviousOrderDetailsList objectAtIndex:indexPath.row]];
    
    cell.brandName.text=userPreOrderListObj.Brand;
    cell.itemName.text=userPreOrderListObj.Name;
    
    cell.quantityLbl.text = localize(@"Quantity:");
    cell.priceLbl.text = localize(@"Price:");
    
    [cell.itemName setFont:EBRI_NORMAL_FONT(13)];
    [cell.brandName setFont:EBRI_BOLD_FONT(14)];
    cell.brandName .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    cell.itemName .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    
    [cell.quantityLbl setFont:EBRI_NORMAL_FONT(12)];
    [cell.priceLbl setFont:EBRI_NORMAL_FONT(12)];
    cell.quantityLbl.textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    cell.priceLbl .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    
    [self.totalLbl setFont:EBRI_BOLD_FONT(17)];
    
    [self.total setFont:EBRI_BOLD_FONT(17)];
    
    [self.subTotalLbl setFont:EBRI_NORMAL_FONT(16)];
    
    [self.subTotal setFont:EBRI_NORMAL_FONT(16)];
    
    [self.deliveryChargesLbl setFont:EBRI_NORMAL_FONT(16)];
    
    [self.deliveryCharge setFont:EBRI_NORMAL_FONT(16)];
    
    [ApplicationDelegate loadMarqueeLabelWithText:cell.itemName.text Font:cell.itemName.font InPlaceOfLabel:cell.itemName];
    cell.quantity.text=userPreOrderListObj.Quantity;
//    cell.price.text=userPreOrderListObj.Rate;
    cell.price.text = [NSString stringWithFormat:localize(@"%@ KD"),userPreOrderListObj.Rate];
    
    NSURL * Imageurl = [[NSURL alloc]init];
    
    Imageurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:userPreOrderListObj.Image]];
    
    [cell.image sd_setImageWithURL:Imageurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
    
    customSeperator=[[UIView alloc]initWithFrame:CGRectMake(0, (cell.frame.origin.y), 320, 1)];
    customSeperator.backgroundColor=[UIColor lightGrayColor];
    
    [cell.contentView addSubview:customSeperator];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath");
    /*UIAlertView *messageAlert = [[UIAlertView alloc]
     initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];*/
    //    UIAlertView *messageAlert = [[UIAlertView alloc]
    //                                 initWithTitle:@"Row Selected" message:[tableData objectAtIndex:indexPath.row] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display the Hello World Message
    //[messageAlert show];
    
    // Checked the selected row
    NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
    newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
    
    
    //newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%ld",(long)newDealsCategoryItem.btnView.tag];
    NSLog(@"%@",[self.PreviousOrderDetailsList objectAtIndex:indexPath.row]);
    NSLog(@"%@",[[self.PreviousOrderDetailsList objectAtIndex:indexPath.row]objectForKey:@"DealId"]);
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:[[self.PreviousOrderDetailsList objectAtIndex:indexPath.row]objectForKey:@"DealId"] forKey:@"TrackId"];
    [actionTracking synchronize];
    newDealsDetailVC.productId = [[self.PreviousOrderDetailsList objectAtIndex:indexPath.row]objectForKey:@"DealId"];
    //newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%d",newDealsCategoryItem.btnView.tag];
    
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
    }
    
}

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"willSelectRowAtIndexPath");
//    if (indexPath.row == 0) {
//        return nil;
//    }
//    
//    return indexPath;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)fetchUserPreviousOrderDetailsformSever{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *PreOrderData = [[NSMutableDictionary alloc] initWithDictionary:[self getPreOrderData]];
    [ApplicationDelegate.engine loadPreviousOrderDetailsDictionary:PreOrderData CompletionHandler:^(NSMutableDictionary *responseDic) {
        
        {
            NSLog(@"%@",responseDic);
            if ([ApplicationDelegate isValid:responseDic])
            {
                
                
                self.PreviousOrderDetailsDic = responseDic;
 
                if ([ApplicationDelegate isValid:[self.PreviousOrderDetailsDic objectForKey:@"OrderId"]])
                {
                    self.orderNumberLabel.text = [NSString stringWithFormat:localize(@"ORDER NO: %@"),[self.PreviousOrderDetailsDic objectForKey:@"OrderId"]];
                }
                else
                {
                    self.total.text = @"";
                }
                
                if ([ApplicationDelegate isValid:[self.PreviousOrderDetailsDic objectForKey:@"SubTotal"]])
                {
                   self.subTotal.text = [NSString stringWithFormat:localize(@"KD %@"),[self.PreviousOrderDetailsDic objectForKey:@"SubTotal"]];
                }
                else
                {
                    self.subTotal.text = @"";
                }
                
                if ([ApplicationDelegate isValid:[self.PreviousOrderDetailsDic objectForKey:@"DeliveryCharges"]])
                {
                    self.deliveryCharge.text = [NSString stringWithFormat:localize(@"KD %@"),[self.PreviousOrderDetailsDic objectForKey:@"DeliveryCharges"]];
                }
                else
                {
                    self.deliveryCharge.text = @"";
                }
                
                if ([ApplicationDelegate isValid:[self.PreviousOrderDetailsDic objectForKey:@"Total"]])
                {
                    self.total.text = [NSString stringWithFormat:localize(@"KD %@"),[self.PreviousOrderDetailsDic objectForKey:@"Total"]];
                }
                else
                {
                    self.total.text = @"";
                }
                
                
                self.PreviousOrderDetailsList=[self.PreviousOrderDetailsDic objectForKey:@"list"];
                
                [self.previousOrderDetailTableview reloadData];
            }
            
            [ApplicationDelegate removeProgressHUD];
            
        }
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        
    }];
}



-(NSMutableDictionary *)getPreOrderData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:orderId forKey:@"OrderId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(void)xibLoading
{
    NSString *nibName = @"PreviousOrderDetailViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
    
    
}

@end
