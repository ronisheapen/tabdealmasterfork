//
//  AboutTabDealViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AboutTabDealData.h"

@interface AboutTabDealViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *aboutUsWebView;
@property (weak, nonatomic) IBOutlet UIView *aboutBackView;
@property (weak, nonatomic) IBOutlet UILabel *aboutUsHeadingLbl;

@end
