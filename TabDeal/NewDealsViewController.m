//
//  NewDealsViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "NewDealsViewController.h"

#import "HomeViewController.h"
#import "NewDealsDetailsViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NewDealsCell.h"
#import "NewDealsData.h"

#define OFFSETHEIGHT 10
#define OFFSETWIDTH 10
@interface NewDealsViewController ()
{
    NewDealsCell * CellView;
}

@end

@implementation NewDealsViewController
@synthesize mainScrollView;

-(void) viewWillAppear:(BOOL)animated
{
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:0];
    
    [HomeViewController sharedViewController].homeButton.selected=YES;
    
    [HomeViewController sharedViewController].buyButton.selected=NO;
    
    if (ApplicationDelegate.innerViewCount.intValue==0)
    {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else
    {
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    [self xibLoading];
}

-(void)viewDidAppear:(BOOL)animated
{
    //[self xibLoading];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.headdingLbl setFont:EBRI_NORMAL_FONT(17)];
    [self fetchDealsDataformSever];
}

-(void)fetchDealsDataformSever
{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *signInData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserLanguageData]];
    
    [ApplicationDelegate.engine loadNewDealsWithLanguageDictionary:signInData CompletionHandler:^(NSMutableDictionary *responseAry)
    {
        {
            if ([ApplicationDelegate isValid:responseAry])
            {
                if (responseAry.count>0)
                {
                   [[HomeViewController sharedViewController] globalLabel:[responseAry objectForKey:@"BasketCount"]];
                    self.DealsMainAry = [responseAry objectForKey:@"list"];
                    NSLog(@"%@",self.DealsMainAry);
                    if (self.DealsMainAry.count>0)
                    {
                        [self loadScroll];
                    }
                    else
                    {
                        
                    }
                    [ApplicationDelegate removeProgressHUD];
                }
            }
        }
    } errorHandler:^(NSError *error)
    {
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }];
}

-(void)loadScroll
{
    for (UIView *sub in self.mainScrollView.subviews)
    {
        [sub removeFromSuperview];
    }
    
    [self.mainScrollView setContentOffset:CGPointZero];
    
    float setYvalue = 0.0f;
    float scrollHeight = 0.0f;
    NSLog(@"%d",self.DealsMainAry.count);
    for (int i = 0; i<self.DealsMainAry.count; i++)
    {
        CellView = [[NewDealsCell alloc]init];
        CellView.delegateNewDealsItem = self;
        CellView.btnView.tag = i;
        NewDealsData *newDealsItem = [[NewDealsData alloc]init];
        newDealsItem.Id = [[self.DealsMainAry objectAtIndex:i]objectForKey:@"Id"];
        newDealsItem.Image = [[self.DealsMainAry objectAtIndex:i]objectForKey:@"Image"];
        newDealsItem.isSoldOut = [[self.DealsMainAry objectAtIndex:i]objectForKey:@"IsSoldOut"];
        newDealsItem.isOnSale = [[self.DealsMainAry objectAtIndex:i]objectForKey:@"IsOnSale"];
        NSLog(@"%@",newDealsItem.isSoldOut);
        NSLog(@"%@",newDealsItem.Icon);
        CellView.dealsNewObj=newDealsItem;
        
        CellView.frame = CGRectMake(OFFSETWIDTH, setYvalue + OFFSETHEIGHT , CellView.frame.size.width, CellView.frame.size.height);
        
        setYvalue = CellView.frame.origin.y + CellView.frame.size.height;
        
        scrollHeight = CellView.frame.origin.y + CellView.frame.size.height;
        
        if ([newDealsItem.isSoldOut isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            [CellView.frameView setImage:[UIImage imageNamed:@"silver_frame.png"]];
            CellView.soldoutLbl.hidden = NO;
            CellView.soldOutLblBackView.hidden = NO;
            CellView.soldoutLbl.text = localize(@"SOLD OUT!");
            
            [CellView.soldoutLbl  setFont:EBRI_BOLD_FONT(15)];
        }
        
        if ([newDealsItem.isOnSale isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            CellView.onSaleBadge.hidden = NO;
            if (![ApplicationDelegate.language isEqualToString:kENGLISH])
            {
                CellView.onSaleBadge.frame = CGRectMake(CellView.onSaleBadge.frame.origin.x + 200, CellView.onSaleBadge.frame.origin.y, CellView.onSaleBadge.frame.size.width, CellView.onSaleBadge.frame.size.height);
                [CellView.onSaleBadge setImage:[UIImage imageNamed:@"onsale_ar.png"]];
            }
            else
            {
                [CellView.onSaleBadge setImage:[UIImage imageNamed:@"onsale_badge.png"]];
            }
        }
        
        NSURL * sampurl = [[NSURL alloc]init];
        
        sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:newDealsItem.Image]];
    
        [CellView.iconView sd_setImageWithURL:sampurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
        
        CellView.iconView.layer.cornerRadius = 3.0;
        CellView.iconView.layer.masksToBounds = YES;

        [self.mainScrollView addSubview:CellView];
    }
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width,  (scrollHeight + OFFSETHEIGHT));
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSMutableDictionary *)getUserLanguageData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    NSLog(@"%@",[prefs objectForKey:@"User Id"]);
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]])
    {
        [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [postDic setObject:kARABIC forKey:@"lang_key"];
        }
        else
        {
            [postDic setObject:kENGLISH forKey:@"lang_key"];
        }
    }
    return postDic;
}

-(void) newDealsItemDidClicked:(NewDealsCell *)newDealsCategoryItem
{
        NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
    newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
    
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:newDealsCategoryItem.dealsNewObj.Id forKey:@"TrackId"];
    [actionTracking synchronize];
    newDealsDetailVC.productId = newDealsCategoryItem.dealsNewObj.Id;
    newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%d",newDealsCategoryItem.btnView.tag];

    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
    }
}

-(void)xibLoading
{
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.headdingLbl.textAlignment = NSTextAlignmentRight;
    }
    else
    {
        self.headdingLbl.textAlignment = NSTextAlignmentLeft;
    }
    self.headdingLbl.text = localize(@"NEW DEALS :");
}

@end
