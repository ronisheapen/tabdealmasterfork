//
//  NotificationsViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#define OFFSETHEIGHT 1
#define OFFSETWIDTH 0

#import "NotificationsViewController.h"
#import "NotificationCell.h"
#import "userNotificationListData.h"
#import "PreviousOrderDetailViewController.h"
#import "SellViewController.h"
#import "MyBankDetailsViewController.h"
#import "NewDealsDetailsViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NotificationScrollViewCell.h"

@interface NotificationsViewController (){
    UIView * customSeperator;
    //NotificationScrollViewCell * CellView;
}

@end

@implementation NotificationsViewController

-(void)viewWillAppear:(BOOL)animated{
    [self fetchUsernotificationListDataformSever];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headingLbl.text = localize(@"NOTIFICATIONS :");
    self.warningLbl.text = localize(@"No notifications!");
    [self.headingLbl setFont:EBRI_NORMAL_FONT(17)];
    [self.warningLbl setFont:EBRI_NORMAL_FONT(15)];
    
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.headingLbl.textAlignment = NSTextAlignmentRight;
    }
    else{
        self.headingLbl.textAlignment = NSTextAlignmentLeft;
    }
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notificationListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    NotificationCell *cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell_a" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else{
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    userNotificationListData *userNotifiListObj = [ApplicationDelegate.mapper getUserNotificationListFromDictionary:[self.notificationListArray objectAtIndex:indexPath.row]];
    
    if ([userNotifiListObj.status isEqualToString:@"False"]) {
        
    }else{
        cell.backgroundColor = [UIColor colorWithRed:223.0f/255.0f green:223.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
        [cell.desWebView setOpaque:NO];
        cell.desWebView.backgroundColor = [UIColor lightGrayColor];
        //[cell.desWebView setBackgroundColor:[UIColor lightGrayColor]];
    }
    //cell.orderNumber.text = localize(@"Order No :");
    //cell.orderDate.text = localize(@"Order date :");
    //cell.orderStatus.text = localize(@"Status :");
    //cell.orderStatusLabel.text=userPreOrderObj.status;
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:userNotifiListObj.message];
    //[attributedText setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:self.font.pointSize]} range:range];
    
    //self.attributedText = attributedText;
    
    //cell.desctiptionTxtView.attributedText = attributedText;
    [cell.desWebView loadHTMLString:userNotifiListObj.message baseURL:nil];
    
    //[cell.descriptionTxtView setValue:userNotifiListObj.message forKey:@"contentToHTMLString"];
    
    NSURL * Imageurl = [[NSURL alloc]init];
    
    Imageurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:userNotifiListObj.image]];
    
    
    [cell.imgView sd_setImageWithURL:Imageurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
    
    cell.dateLbl.text=userNotifiListObj.date;
    
    //cell.nameLabel.text = [tableData objectAtIndex:indexPath.row];
    //cell.thumbnailImageView.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
    //cell.prepTimeLabel.text = [prepTime objectAtIndex:indexPath.row];
    
    customSeperator=[[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height-1,cell.frame.size.width,1)];
    customSeperator.backgroundColor=[UIColor blackColor];
    
    [cell.contentView addSubview:customSeperator];
    
    cell.desWebView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    userNotificationListData *userNotifiListObj = [ApplicationDelegate.mapper getUserNotificationListFromDictionary:[self.notificationListArray objectAtIndex:indexPath.row]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:@"cf2dd77e-0bf7-4135-ac54-7d8ef597c3e4" forKey:@"UserId"];
    [postDic setObject:userNotifiListObj.messageId forKey:@"MessageId"];
    
    if ([userNotifiListObj.typeId isEqualToString:@"1"]) {
        PreviousOrderDetailViewController * orderDetailsVC = [[PreviousOrderDetailViewController alloc]initWithNibName:@"PreviousOrderDetailViewController" bundle:nil];
        orderDetailsVC.view.backgroundColor = [UIColor clearColor];
        
        //UserPreviousOrder *userPreOrderObj = [ApplicationDelegate.mapper getUserPreviousOrderFromDictionary:[self.PreviousOrderDataArray objectAtIndex:indexPath.row]];
        
        //NSLog(@"%@",userPreOrderObj.ordernumber);
        actionTracking = [NSUserDefaults standardUserDefaults];
        [actionTracking setObject:userNotifiListObj.referenceId forKey:@"userPreOrder"];
        
        [actionTracking synchronize];
        
        orderDetailsVC.orderId=userNotifiListObj.referenceId;
        
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousOrderDetailViewController class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:orderDetailsVC animated:NO];
        }
    }
    else if ([userNotifiListObj.typeId isEqualToString:@"2"]){
        SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
        sellVc.view.backgroundColor = [UIColor clearColor];
        sellVc.contextString = @"viewing";
        actionTracking = [NSUserDefaults standardUserDefaults];
        [actionTracking setObject:@"viewing" forKey:@"trackingContext"];
        [actionTracking setObject:userNotifiListObj.referenceId forKey:@"refferenceString"];
        [actionTracking synchronize];
        //    actionStatus = [NSUserDefaults standardUserDefaults];
        //    [actionStatus setObject:@"viewing" forKey:@"Action"];
        //    [actionStatus synchronize];
        sellVc.refferenceString = userNotifiListObj.referenceId;
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
        }
    }
    else if ([userNotifiListObj.typeId isEqualToString:@"3"]){
        MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
        bankDetailsVC.view.backgroundColor = [UIColor clearColor];
        
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
        }
    }
    else if ([userNotifiListObj.typeId isEqualToString:@"4"]){
        /*
        if ([ApplicationDelegate isValid:userNotifiListObj.referenceId]) {
            NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
            newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
            
            
            //newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%ld",(long)newDealsCategoryItem.btnView.tag];
            actionTracking = [NSUserDefaults standardUserDefaults];
            [actionTracking setObject:userNotifiListObj.referenceId forKey:@"TrackId"];
            [actionTracking synchronize];
            newDealsDetailVC.productId = userNotifiListObj.referenceId;
            ///newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%d",newDealsCategoryItem.btnView.tag];
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
            }
        } else {
            [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
        }
        */
        [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
    }
    else if ([userNotifiListObj.typeId isEqualToString:@"5"]){
        [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
    }
    /*
     
     // dont delete this code
    if ([userNotifiListObj.status isEqualToString:@"False"]) {
        NSMutableDictionary *addressData = [[NSMutableDictionary alloc] initWithDictionary:postDic];
        NSLog(@"%@",addressData);
        [ApplicationDelegate addProgressHUDToView:self.view];
     
        [ApplicationDelegate.engine EditStatusWithDataDictionary:addressData CompletionHandler:^(NSMutableDictionary *responseDic)
     
         {
             NSLog(@"%@",responseDic);
             if ([ApplicationDelegate isValid:responseDic])
             {
                 if ([[responseDic objectForKey:@"Success"] integerValue])
                 {
                     if ([userNotifiListObj.typeId isEqualToString:@"1"]) {
                         PreviousOrderDetailViewController * orderDetailsVC = [[PreviousOrderDetailViewController alloc]initWithNibName:@"PreviousOrderDetailViewController" bundle:nil];
                         orderDetailsVC.view.backgroundColor = [UIColor clearColor];
     
                         //UserPreviousOrder *userPreOrderObj = [ApplicationDelegate.mapper getUserPreviousOrderFromDictionary:[self.PreviousOrderDataArray objectAtIndex:indexPath.row]];
     
                         //NSLog(@"%@",userPreOrderObj.ordernumber);
                         actionTracking = [NSUserDefaults standardUserDefaults];
                         [actionTracking setObject:userNotifiListObj.referenceId forKey:@"userPreOrder"];
                         
                         [actionTracking synchronize];
                         
                         orderDetailsVC.orderId=userNotifiListObj.referenceId;
                         
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousOrderDetailViewController class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:orderDetailsVC animated:NO];
                         }
                     }
                     else if ([userNotifiListObj.typeId isEqualToString:@"2"]){
                         SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
                         sellVc.view.backgroundColor = [UIColor clearColor];
                         sellVc.contextString = @"viewing";
                         actionTracking = [NSUserDefaults standardUserDefaults];
                         [actionTracking setObject:@"viewing" forKey:@"trackingContext"];
                         [actionTracking setObject:userNotifiListObj.referenceId forKey:@"refferenceString"];
                         [actionTracking synchronize];
                         //    actionStatus = [NSUserDefaults standardUserDefaults];
                         //    [actionStatus setObject:@"viewing" forKey:@"Action"];
                         //    [actionStatus synchronize];
                         sellVc.refferenceString = userNotifiListObj.referenceId;
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
                         }
                     }
                     else if ([userNotifiListObj.typeId isEqualToString:@"3"]){
                         MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
                         bankDetailsVC.view.backgroundColor = [UIColor clearColor];
                         
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
                         }
                     }
                     else if ([userNotifiListObj.typeId isEqualToString:@"4"]){
                         SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
                         sellVc.view.backgroundColor = [UIColor clearColor];
                         sellVc.contextString = @"viewing";
                         actionTracking = [NSUserDefaults standardUserDefaults];
                         [actionTracking setObject:@"viewing" forKey:@"trackingContext"];
                         [actionTracking setObject:userNotifiListObj.referenceId forKey:@"refferenceString"];
                         [actionTracking synchronize];
                         //    actionStatus = [NSUserDefaults standardUserDefaults];
                         //    [actionStatus setObject:@"viewing" forKey:@"Action"];
                         //    [actionStatus synchronize];
                         sellVc.refferenceString = userNotifiListObj.referenceId;
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
                         }
                         
                         NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
                         newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
                         
                         
                         //newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%ld",(long)newDealsCategoryItem.btnView.tag];
                         actionTracking = [NSUserDefaults standardUserDefaults];
                         [actionTracking setObject:userNotifiListObj.referenceId forKey:@"TrackId"];
                         [actionTracking synchronize];
                         newDealsDetailVC.productId = userNotifiListObj.referenceId;
                         ///newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%d",newDealsCategoryItem.btnView.tag];
                         
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
                         }
                     }
                 }
                 
             }
             else{
                 [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                 
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
             
         }];
    } else {
        if ([userNotifiListObj.typeId isEqualToString:@"1"]) {
            PreviousOrderDetailViewController * orderDetailsVC = [[PreviousOrderDetailViewController alloc]initWithNibName:@"PreviousOrderDetailViewController" bundle:nil];
            orderDetailsVC.view.backgroundColor = [UIColor clearColor];
            
            //UserPreviousOrder *userPreOrderObj = [ApplicationDelegate.mapper getUserPreviousOrderFromDictionary:[self.PreviousOrderDataArray objectAtIndex:indexPath.row]];
            
            //NSLog(@"%@",userPreOrderObj.ordernumber);
            actionTracking = [NSUserDefaults standardUserDefaults];
            [actionTracking setObject:userNotifiListObj.referenceId forKey:@"userPreOrder"];
            
            [actionTracking synchronize];
            
            orderDetailsVC.orderId=userNotifiListObj.referenceId;
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousOrderDetailViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:orderDetailsVC animated:NO];
            }
        }
        else if ([userNotifiListObj.typeId isEqualToString:@"2"]){
            SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
            sellVc.view.backgroundColor = [UIColor clearColor];
            sellVc.contextString = @"viewing";
            actionTracking = [NSUserDefaults standardUserDefaults];
            [actionTracking setObject:@"viewing" forKey:@"trackingContext"];
            [actionTracking setObject:userNotifiListObj.referenceId forKey:@"refferenceString"];
            [actionTracking synchronize];
            //    actionStatus = [NSUserDefaults standardUserDefaults];
            //    [actionStatus setObject:@"viewing" forKey:@"Action"];
            //    [actionStatus synchronize];
            sellVc.refferenceString = userNotifiListObj.referenceId;
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
            }
        }
        else if ([userNotifiListObj.typeId isEqualToString:@"3"]){
            MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
            bankDetailsVC.view.backgroundColor = [UIColor clearColor];
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
            }
        }
        else if ([userNotifiListObj.typeId isEqualToString:@"4"]){
            SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
            sellVc.view.backgroundColor = [UIColor clearColor];
            sellVc.contextString = @"viewing";
            actionTracking = [NSUserDefaults standardUserDefaults];
            [actionTracking setObject:@"viewing" forKey:@"trackingContext"];
            [actionTracking setObject:userNotifiListObj.referenceId forKey:@"refferenceString"];
            [actionTracking synchronize];
            //    actionStatus = [NSUserDefaults standardUserDefaults];
            //    [actionStatus setObject:@"viewing" forKey:@"Action"];
            //    [actionStatus synchronize];
            sellVc.refferenceString = userNotifiListObj.referenceId;
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
            }
            
            NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
            newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
            
            
            //newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%ld",(long)newDealsCategoryItem.btnView.tag];
            actionTracking = [NSUserDefaults standardUserDefaults];
            [actionTracking setObject:userNotifiListObj.referenceId forKey:@"TrackId"];
            [actionTracking synchronize];
            newDealsDetailVC.productId = userNotifiListObj.referenceId;
            ///newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%d",newDealsCategoryItem.btnView.tag];
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
            }
        }
    }
    */
}

-(void)fetchUsernotificationListDataformSever{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserData]];
    [ApplicationDelegate.engine loadUsersnotificationDictionary:userData CompletionHandler:^(NSMutableArray *responseArr) {
        
        {
            
            if ([ApplicationDelegate isValid:responseArr])
            {
                
                self.notificationListArray = responseArr;
                NSLog(@"%@",self.notificationListArray);
                //self.PreviousNotificationListArray = [[NSMutableArray alloc]init];
                
                if (self.notificationListArray.count == 0) {
                    self.warningBackView.hidden = NO;
                    self.warningLbl.font = [UIFont fontWithName:@"Ebrima" size:15];
                    self.warningLbl.hidden = NO;
                } else {
                    //[self.notifocationTable reloadData];
                    [self loadScrollView];
                }
                
            }
            
            [ApplicationDelegate removeProgressHUD];
            
        }
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        
    }];
}



-(NSMutableDictionary *)getUserData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:@"cf2dd77e-0bf7-4135-ac54-7d8ef597c3e4" forKey:@"UserId"];
    
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

////////////////////////////////////////////////////////////


//- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
//    CGRect frame = aWebView.frame;
//    frame.size.height = 1;
//    aWebView.frame = frame;
//    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
//    frame.size = fittingSize;
//    aWebView.frame = frame;
//    
//    NSLog(@"size: %f, %f", fittingSize.width, fittingSize.height);
//    aWebView.superview.frame = CGRectMake(aWebView.frame.origin.x, aWebView.frame.origin.y, aWebView.frame.size.width, aWebView.frame.size.height+30);
//}

-(void)loadScrollView{
    NSLog(@"%d",self.notificationListArray.count);
    for (UIView *sub in self.notificationScrollView.subviews) {
        [sub removeFromSuperview];
    }
    
    [self.notificationScrollView setContentOffset:CGPointZero];
    
    float setYvalue = 0.0f;
    float scrollHeight = 0.0f;
    
    int count;
    
    if (self.notificationListArray.count > 20) {
        count = 20;
    }
    else{
        count = self.notificationListArray.count;
    }
    
    for (int i = 0; i < count; i++) {
        self.CellView = [[NotificationScrollViewCell alloc]init];
        
        //NSLog(@"%@",CellView);
        
        userNotificationListData *userNotifiListObj = [ApplicationDelegate.mapper getUserNotificationListFromDictionary:[self.notificationListArray objectAtIndex:i]];
        
        [self.CellView.desWebView loadHTMLString:userNotifiListObj.message baseURL:nil];
        
        //[self.CellView.descriptionTxtView setValue:userNotifiListObj.message forKey:@"contentToHTMLString"];
        
        self.CellView.descriptionTxtView.attributedText = [[NSAttributedString alloc] initWithData:[userNotifiListObj.message dataUsingEncoding:NSUTF8StringEncoding]
                                                                             options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                       NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                  documentAttributes:nil error:nil];
        
        [self.CellView.descriptionTxtView setScrollEnabled:NO];
        [self.CellView.descriptionTxtView sizeToFit];
        
        //NSLog(@"height...%f",self.CellView.descriptionTxtView.frame.size.height);
        //NSLog(@"content   %f",self.CellView.descriptionTxtView.contentSize.height);
        
        UITextView * descriptionTxtView = [[UITextView alloc]init];
        descriptionTxtView.editable = NO;
        descriptionTxtView.backgroundColor = [UIColor clearColor];
        descriptionTxtView.frame = self.CellView.descriptionTxtView.frame;
        //[descriptionTxtView setValue:userNotifiListObj.message forKey:@"contentToHTMLString"];
        
        //NSData *tempdata = [userNotifiListObj.message dataUsingEncoding:NSUTF8StringEncoding];
         //descriptionTxtView.text = [[NSString alloc] initWithData:tempdata encoding:NSUTF8StringEncoding ];
        
        descriptionTxtView.attributedText = [[NSAttributedString alloc] initWithData:[userNotifiListObj.message dataUsingEncoding:NSUTF8StringEncoding]
                                                                                    options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                              NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                         documentAttributes:nil error:nil];
        
        //[self.CellView addSubview:descriptionTxtView];
        
        //[self.CellView.descriptionTxtView removeFromSuperview];
        UIImageView *notificationIconImage = [[UIImageView alloc]initWithFrame:CGRectMake(17, descriptionTxtView.frame.origin.y + descriptionTxtView.frame.size.height + 2, 35, 35)];
        [notificationIconImage setImage:[UIImage imageNamed:@"notification_icon2.png"]];
        [self.CellView addSubview:notificationIconImage];
        UILabel * dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, notificationIconImage.frame.origin.y, 232, 35)];
        dateLabel.text = userNotifiListObj.date;
        dateLabel.font = [UIFont systemFontOfSize:15];
        [self.CellView addSubview:dateLabel];
        
        
        //CellView.descriptionTxtView.frame = CGRectMake(CellView.descriptionTxtView.frame.origin.x, CellView.descriptionTxtView.frame.origin.y, CellView.descriptionTxtView.frame.size.width, CellView.descriptionTxtView.contentSize.height);
        self.CellView.dateLbl.frame = CGRectMake(self.CellView.dateLbl.frame.origin.x, self.CellView.descriptionTxtView.frame.origin.y + self.CellView.descriptionTxtView.frame.size.height + 4, self.CellView.dateLbl.frame.size.width, self.CellView.dateLbl.frame.size.height);
        self.CellView.frame = CGRectMake(self.CellView.frame.origin.x, self.CellView.frame.origin.y, self.CellView.frame.size.width,dateLabel.frame.origin.y + dateLabel.frame.size.height + 4 );
        
        if ([userNotifiListObj.status isEqualToString:@"False"]) {
            
        }else{
            self.CellView.backgroundColor = [UIColor colorWithRed:223.0f/255.0f green:223.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
        }
        
        UIButton * cellButton = [[UIButton alloc]initWithFrame:CGRectMake(self.CellView.frame.origin.x, self.CellView.frame.origin.y, self.CellView.frame.size.width, self.CellView.frame.size.height)];
        [cellButton addTarget:self action:@selector(cellButtonActn:) forControlEvents:UIControlEventTouchUpInside];
        [self.CellView addSubview:cellButton];
        cellButton.tag = i;
        [self.notificationScrollView addSubview:self.CellView];
        
        NSURL * Imageurl = [[NSURL alloc]init];
        
        Imageurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:userNotifiListObj.image]];
        
        [self.CellView.imgView sd_setImageWithURL:Imageurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
        
        //self.CellView.dateLbl.text=userNotifiListObj.date;
        
        //self.CellView.dateLbl.backgroundColor = [UIColor redColor];
        
        self.CellView.frame = CGRectMake(OFFSETWIDTH, setYvalue + OFFSETHEIGHT , self.CellView.frame.size.width, self.CellView.frame.size.height);
        
        setYvalue = self.CellView.frame.origin.y + self.CellView.frame.size.height;
        
        scrollHeight = self.CellView.frame.origin.y + self.CellView.frame.size.height;
        
        
        //NSLog(@"%@",CellView);
    }
    self.notificationScrollView.contentSize = CGSizeMake(self.notificationScrollView.frame.size.width,  (scrollHeight + OFFSETHEIGHT));
}

- (IBAction) cellButtonActn: (UIButton *)sender
{
    userNotificationListData *userNotifiListObj = [ApplicationDelegate.mapper getUserNotificationListFromDictionary:[self.notificationListArray objectAtIndex:sender.tag]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:@"cf2dd77e-0bf7-4135-ac54-7d8ef597c3e4" forKey:@"UserId"];
    [postDic setObject:userNotifiListObj.messageId forKey:@"MessageId"];
    
    ////////////////////////////////////////////////////////////////////////
    
    if ([userNotifiListObj.status isEqualToString:@"False"]) {
        NSMutableDictionary *addressData = [[NSMutableDictionary alloc] initWithDictionary:postDic];
        NSLog(@"%@",addressData);
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine EditStatusWithDataDictionary:addressData CompletionHandler:^(NSMutableDictionary *responseDic)
         
         {
             NSLog(@"%@",responseDic);
             if ([ApplicationDelegate isValid:responseDic])
             {
                 if ([[responseDic objectForKey:@"Success"] integerValue])
                 {
                     //userNotifiListObj.status = @"True";
                     if ([userNotifiListObj.typeId isEqualToString:@"1"]) {
                         PreviousOrderDetailViewController * orderDetailsVC = [[PreviousOrderDetailViewController alloc]initWithNibName:@"PreviousOrderDetailViewController" bundle:nil];
                         orderDetailsVC.view.backgroundColor = [UIColor clearColor];
                         
                         //UserPreviousOrder *userPreOrderObj = [ApplicationDelegate.mapper getUserPreviousOrderFromDictionary:[self.PreviousOrderDataArray objectAtIndex:indexPath.row]];
                         
                         //NSLog(@"%@",userPreOrderObj.ordernumber);
                         actionTracking = [NSUserDefaults standardUserDefaults];
                         [actionTracking setObject:userNotifiListObj.referenceId forKey:@"userPreOrder"];
                         
                         [actionTracking synchronize];
                         
                         orderDetailsVC.orderId=userNotifiListObj.referenceId;
                         
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousOrderDetailViewController class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:orderDetailsVC animated:NO];
                         }
                     }
                     else if ([userNotifiListObj.typeId isEqualToString:@"2"]){
                         SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
                         sellVc.view.backgroundColor = [UIColor clearColor];
                         sellVc.contextString = @"viewing";
                         actionTracking = [NSUserDefaults standardUserDefaults];
                         [actionTracking setObject:@"viewing" forKey:@"trackingContext"];
                         [actionTracking setObject:userNotifiListObj.referenceId forKey:@"refferenceString"];
                         [actionTracking synchronize];
                         //    actionStatus = [NSUserDefaults standardUserDefaults];
                         //    [actionStatus setObject:@"viewing" forKey:@"Action"];
                         //    [actionStatus synchronize];
                         sellVc.refferenceString = userNotifiListObj.referenceId;
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
                         }
                     }
                     else if ([userNotifiListObj.typeId isEqualToString:@"3"]){
                         MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
                         bankDetailsVC.view.backgroundColor = [UIColor clearColor];
                         
                         if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
                         {
                             [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
                         }
                     }
                     else if ([userNotifiListObj.typeId isEqualToString:@"4"]){
                         /*
                         if ([ApplicationDelegate isValid:userNotifiListObj.referenceId]) {
                             NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
                             newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
                             
                             
                             //newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%ld",(long)newDealsCategoryItem.btnView.tag];
                             actionTracking = [NSUserDefaults standardUserDefaults];
                             [actionTracking setObject:userNotifiListObj.referenceId forKey:@"TrackId"];
                             [actionTracking synchronize];
                             newDealsDetailVC.productId = userNotifiListObj.referenceId;
                             ///newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%d",newDealsCategoryItem.btnView.tag];
                             
                             if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
                             {
                                 [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
                             }
                         } else {
                             [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
                         }
                         */
                         [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
                     }
                     else if ([userNotifiListObj.typeId isEqualToString:@"5"]){
                         [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
                     }
                 }
                 
             }
             else{
                 [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                 
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
             
         }];
    } else {
        
        if ([userNotifiListObj.typeId isEqualToString:@"1"]) {
            PreviousOrderDetailViewController * orderDetailsVC = [[PreviousOrderDetailViewController alloc]initWithNibName:@"PreviousOrderDetailViewController" bundle:nil];
            orderDetailsVC.view.backgroundColor = [UIColor clearColor];
            
            //UserPreviousOrder *userPreOrderObj = [ApplicationDelegate.mapper getUserPreviousOrderFromDictionary:[self.PreviousOrderDataArray objectAtIndex:indexPath.row]];
            
            //NSLog(@"%@",userPreOrderObj.ordernumber);
            actionTracking = [NSUserDefaults standardUserDefaults];
            [actionTracking setObject:userNotifiListObj.referenceId forKey:@"userPreOrder"];
            
            [actionTracking synchronize];
            
            orderDetailsVC.orderId=userNotifiListObj.referenceId;
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousOrderDetailViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:orderDetailsVC animated:NO];
            }
        }
        else if ([userNotifiListObj.typeId isEqualToString:@"2"]){
            SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
            sellVc.view.backgroundColor = [UIColor clearColor];
            sellVc.contextString = @"viewing";
            actionTracking = [NSUserDefaults standardUserDefaults];
            [actionTracking setObject:@"viewing" forKey:@"trackingContext"];
            [actionTracking setObject:userNotifiListObj.referenceId forKey:@"refferenceString"];
            [actionTracking synchronize];
            //    actionStatus = [NSUserDefaults standardUserDefaults];
            //    [actionStatus setObject:@"viewing" forKey:@"Action"];
            //    [actionStatus synchronize];
            sellVc.refferenceString = userNotifiListObj.referenceId;
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
            }
        }
        else if ([userNotifiListObj.typeId isEqualToString:@"3"]){
            MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
            bankDetailsVC.view.backgroundColor = [UIColor clearColor];
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
            }
        }
        else if ([userNotifiListObj.typeId isEqualToString:@"4"]){
            /*
            if ([ApplicationDelegate isValid:userNotifiListObj.referenceId]) {
                NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
                newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
                
                
                //newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%ld",(long)newDealsCategoryItem.btnView.tag];
                actionTracking = [NSUserDefaults standardUserDefaults];
                [actionTracking setObject:userNotifiListObj.referenceId forKey:@"TrackId"];
                [actionTracking synchronize];
                newDealsDetailVC.productId = userNotifiListObj.referenceId;
                ///newDealsDetailVC.Lbl.text = [NSString stringWithFormat:@"%d",newDealsCategoryItem.btnView.tag];
                
                if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
                {
                    [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
                }
            } else {
                [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
            }
             */
            [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
            
        }
        else if ([userNotifiListObj.typeId isEqualToString:@"5"]){
            [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
        }
    }
    
    ////////////////////////////////////////////////////////////////////////
    NSLog(@"Tap . tag %ld", (long)sender.tag);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
