//
//  BillingAddressViewController.h
//  TabDeal
//
//  Created by satheesh on 10/7/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface BillingAddressViewController : UIViewController<ListSelectionProtocol,UITextFieldDelegate,UIWebViewDelegate>
{
    NSMutableArray * countryNameArray;
    NSMutableArray * countryCodeArray;
    CGSize scrollFrameSize;
    UIActivityIndicatorView *activityIndicator;
}

@property (weak, nonatomic) IBOutlet UITextField *addressline1TxtView;
@property (weak, nonatomic) IBOutlet UITextField *addressline2TxtView;
@property (weak, nonatomic) IBOutlet UITextField *cityTxtView;
@property (weak, nonatomic) IBOutlet UITextField *postalCodeTxtView;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTxtView;
@property (weak, nonatomic) IBOutlet UITextField *mobileCodeTxtView;
@property (strong, nonatomic)NSMutableArray * countryCodeAndName;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIImageView *backWhiteView;
@property (weak, nonatomic) IBOutlet UIButton *dropBtn;
@property (weak, nonatomic) IBOutlet UITextField *countryTextView;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownDwnArrow;
@property (strong, nonatomic) DropDownView *countryDropDownObj;

@property (strong, nonatomic) IBOutlet UILabel *SubTotal;
@property (strong, nonatomic) IBOutlet UILabel *DeliveryCharge;
@property (strong, nonatomic) IBOutlet UILabel *TotalCharge;
@property (strong, nonatomic) NSString * orderId;
@property (strong, nonatomic) NSString * paymentMethod;
@property (strong, nonatomic) NSString * UserId;

@property (strong, nonatomic) IBOutlet UIView *responseBackView;
@property (weak, nonatomic) IBOutlet UIWebView *responseWebView;

@property (strong, nonatomic) IBOutlet UIView *thankYouPageBackView;
@property (weak, nonatomic) IBOutlet UIWebView *thankYouPageWebView;

@property (weak, nonatomic) IBOutlet UIView *topBackView;
@property (weak, nonatomic) IBOutlet UIView *downBackView;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;
-(NSString *)populateString:(NSString *)convertString arg:(NSString *)after;

- (IBAction)dropDownActn:(UIButton *)sender;
- (IBAction)payBtnActn:(UIButton *)sender;
- (IBAction)BackToHome:(UIButton *)sender;

@end
