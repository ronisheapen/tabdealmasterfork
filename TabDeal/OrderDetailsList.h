//
//  OrderDetailsList.h
//  TabDeal
//
//  Created by Sreeraj VR on 26/10/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailsList : NSObject

@property(nonatomic,retain)NSString *Brand;
@property(nonatomic,retain)NSString *DealId;
@property(nonatomic,retain)NSString *Image;
@property(nonatomic,retain)NSString *Name;
@property(nonatomic,retain)NSString *Quantity;
@property(nonatomic,retain)NSString *Rate;
@end
