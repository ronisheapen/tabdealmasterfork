//
//  Userinformation.h
//  TabDeal
//
//  Created by satheesh on 10/29/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Userinformation : NSObject
@property(nonatomic,retain)NSString *addressLine1;
@property(nonatomic,retain)NSString *addressLine2;
@property(nonatomic,retain)NSString *city;
@property(nonatomic,retain)NSString *country;
@property(nonatomic,retain)NSString *mobCode;
@property(nonatomic,retain)NSString *mobileNumber;
@property(nonatomic,retain)NSString *postalCode;
@property(nonatomic,retain)NSString *recieverName;
@property(nonatomic,retain)NSString *email;
@property(nonatomic,retain)NSString *firstName;
@property(nonatomic,retain)NSString *lastName;
@property(nonatomic,retain)NSString *personalMobCode;
@property(nonatomic,retain)NSString *personalMobileNumber;
@property(nonatomic, retain)NSNumber * isSeller;
@end
