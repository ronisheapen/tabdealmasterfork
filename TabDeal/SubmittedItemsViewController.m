//
//  SubmittedItemsViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "SubmittedItemsViewController.h"
#import "SubmittedItemsCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SellViewController.h"

@interface SubmittedItemsViewController (){
    UIView * customSeperator;
    SubmittedItemsCell *cell;
    
    NSString *temp;
}

@end

@implementation SubmittedItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headingLbl.text = localize(@"SUBMITTED ITEMS :");
    [self.headingLbl setFont:EBRI_NORMAL_FONT(17)];
    self.warningLbl.text = localize(@"No items submitted for sell!");
    [self.warningLbl setFont:EBRI_NORMAL_FONT(15)];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.headingLbl.textAlignment = NSTextAlignmentRight;
    }
    else{
        self.headingLbl.textAlignment = NSTextAlignmentLeft;
    }
    // Do any additional setup after loading the view from its nib.
    
    [self fetchSubmittedItemListformSever];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.SubmittedDataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    cell = (SubmittedItemsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubmittedItemsCell_a" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else{
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubmittedItemsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
      SubmittedData *SubListObj = [ApplicationDelegate.mapper getSubmittedResultsFromDictionary:[self.SubmittedDataArray objectAtIndex:indexPath.row]];
    
    cell.brandName.text=SubListObj.BrandName;
    
    cell.referenceNoLbl.text = localize(@"Reference No:");
    cell.dateLbl.text = localize(@"Date:");
    cell.statusLbl.text = localize(@"Status:");
    cell.date.text=SubListObj.Date;
    
    cell.refNumber.text=SubListObj.RefferenceNumber;
    
    cell.status.text=SubListObj.Status;
    
    cell.itemName.text=SubListObj.Type;
    
    cell.editBtn.tag = indexPath.row;
    
    [cell.editBtn addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.refferenceNumber = SubListObj.RefferenceNumber;
    
    NSURL * Imageurl = [[NSURL alloc]init];
    
    Imageurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:SubListObj.Image]];
    
    [cell.subimage sd_setImageWithURL:Imageurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
    
    customSeperator=[[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height-1,cell.frame.size.width,1)];
    customSeperator.backgroundColor=[UIColor lightGrayColor];
    
    [cell.contentView addSubview:customSeperator];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubmittedData *SubListObj = [ApplicationDelegate.mapper getSubmittedResultsFromDictionary:[self.SubmittedDataArray objectAtIndex:indexPath.row]];
    
    SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
    sellVc.view.backgroundColor = [UIColor clearColor];
    sellVc.contextString = @"viewing";
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:@"viewing" forKey:@"trackingContext"];
    [actionTracking setObject:SubListObj.RefferenceNumber forKey:@"refferenceString"];
    [actionTracking synchronize];
//    actionStatus = [NSUserDefaults standardUserDefaults];
//    [actionStatus setObject:@"viewing" forKey:@"Action"];
//    [actionStatus synchronize];
    sellVc.refferenceString = SubListObj.RefferenceNumber;
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)fetchSubmittedItemListformSever{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserData]];
    [ApplicationDelegate.engine SubmittedItemList:userData CompletionHandler:^(NSMutableArray *responseArr) {
        
        {
            
            if ([ApplicationDelegate isValid:responseArr])
            {
              
                
                NSLog(@"%@",responseArr);
                
                self.SubmittedDataArray = responseArr;
                
                if (self.SubmittedDataArray.count == 0) {
                    self.warningBackView.hidden = NO;
                    self.warningLbl.font = [UIFont fontWithName:@"Ebrima" size:15];
                    self.warningLbl.hidden = NO;
                } else {
                    [self.SubmittedTable reloadData];
                }
                
                
  
            }
            
            [ApplicationDelegate removeProgressHUD];
            
        }
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        
    }];
}



-(NSMutableDictionary *)getUserData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(void)editButtonClicked:(UIButton*)sender
{
    SubmittedData *SubListObj = [ApplicationDelegate.mapper getSubmittedResultsFromDictionary:[self.SubmittedDataArray objectAtIndex:sender.tag]];
    SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
    sellVc.view.backgroundColor = [UIColor clearColor];
    sellVc.contextString = @"editing";
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:@"editing" forKey:@"trackingContext"];
    [actionTracking setObject:SubListObj.RefferenceNumber forKey:@"refferenceString"];
    [actionTracking synchronize];
//    actionStatus = [NSUserDefaults standardUserDefaults];
//    [actionStatus setObject:@"editing" forKey:@"Action"];
//    [actionStatus synchronize];
    sellVc.refferenceString = SubListObj.RefferenceNumber;
    //sellVc.contextString = [NSString stringWithFormat:@"editing %ld",(long)sender.tag];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
    }
}

@end
