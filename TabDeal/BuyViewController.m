

//  BuyViewController.m
//  TabDeal
//
//  Created by satheesh on 9/22/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "BuyViewController.h"
#import "NewDealsDetailsViewController.h"
#import "DeliveryScreenViewController.h"
#import "HomeViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "MyBasketCell.h"
@interface BuyViewController ()
{
    UIView * customSeperator;
    MyBasketCell * CellView;
    NSString* bufferTag;
}

@end

@implementation BuyViewController

-(void)viewWillAppear:(BOOL)animated
{
    [self xibLoading];
    
    [self fontIntilization];
    
    self.view.hidden=YES;
    bufferTag = [[NSString alloc]init];
    self.tableBackView.layer.cornerRadius = 5;
    self.buyViewDataArray = [[NSMutableArray alloc]init];
    self.sizeTableView.scrollEnabled = NO;
    self.tableBackScrollView.contentSize = CGSizeMake(self.tableBackScrollView.frame.size.width, self.tenPlusBackView.frame.origin.y + self.tenPlusBackView.frame.size.height);
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.tenPlusSizeTxtField.inputAccessoryView = numberToolbar;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.tenPlusSizeTxtField];
    
    scrollFrame = self.tableBackScrollView.frame;
    
    [self registerForKeyboardNotifications];
    
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:0];
    if (ApplicationDelegate.innerViewCount.intValue==0)
    {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else
    {
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]])
    {
        [self fetchUserBasketDataformSever];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    self.headdingLbl.text = localize(@"MY SHOPPING BASKET :");
    self.warningLbl.text = localize(@"No items added to basket!");
    self.subTotalLbl.text = localize(@"Sub Total");
    self.deliveryChargesLbl.text = localize(@"Delivery Charges");
    self.totalLbl.text = localize(@"Total");
    [self.proceedToPayBtn setTitle:localize(@"PROCEED TO PAY") forState:UIControlStateNormal];
    self.selectQuantityLbl.text = localize(@"Select Quantity");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)proceedToPayBtnActn:(UIButton *)sender
{
    if (sender.tag == 0)
    {
        DeliveryScreenViewController * deliveryScreenVC = [[DeliveryScreenViewController alloc]initWithNibName:@"DeliveryScreenViewController" bundle:nil];
        deliveryScreenVC.view.backgroundColor = [UIColor clearColor];
        actionTracking = [NSUserDefaults standardUserDefaults];
        [actionTracking setObject:self.SubTotal.text forKey:@"SubTotal"];
        [actionTracking setObject:self.DeliveryCharge.text forKey:@"DeliveryCharge"];
        [actionTracking setObject:self.TotalCharge.text forKey:@"TotalCharge"];
        [actionTracking setObject:self.orderId forKey:@"orderId"];

        [actionTracking synchronize];
        deliveryScreenVC.SubTotal.text=self.SubTotal.text;
        deliveryScreenVC.DeliveryCharge.text=self.DeliveryCharge.text;
        deliveryScreenVC.TotalCharge.text=self.TotalCharge.text;
        deliveryScreenVC.orderId = self.orderId;
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[DeliveryScreenViewController class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:deliveryScreenVC animated:NO];
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Quantity that you have selected is not available") title:nil];
    }
}

-(void)fetchUserBasketDataformSever
{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserData]];

    [ApplicationDelegate.engine loadUsersBasketLanguageDictionary:userData CompletionHandler:^(NSMutableDictionary *responseDic)
    {
        {
            if ([ApplicationDelegate isValid:responseDic])
            {
                self.view.hidden=NO;
                self.orderId = [[NSString alloc]init];
                if ([ApplicationDelegate isValid:[responseDic objectForKey:@"OrderId"]])
                {
                    self.orderId = [responseDic objectForKey:@"OrderId"];
                }
                else
                {
                    
                }
                if ([ApplicationDelegate isValid:[responseDic objectForKey:@"SubTotal"]])
                {
                    self.SubTotal.text= [NSString stringWithFormat:localize(@"KD %@"),[responseDic objectForKey:@"SubTotal"]];
                }
                else
                {
                    self.SubTotal.text = @"";
                }
                if ([ApplicationDelegate isValid:[responseDic objectForKey:@"DeliveryCharges"]])
                {
                    self.DeliveryCharge.text= [NSString stringWithFormat:localize(@"KD %@"),[responseDic objectForKey:@"DeliveryCharges"]];
                }
                else
                {
                    self.DeliveryCharge.text = @"";
                }
                if ([ApplicationDelegate isValid:[responseDic objectForKey:@"Total"]])
                {
                    self.TotalCharge.text= [NSString stringWithFormat:localize(@"KD %@"),[responseDic objectForKey:@"Total"]];
                }
                else
                {
                    self.TotalCharge.text = @"";
                }

                NSMutableArray *responseAry=[responseDic objectForKey:@"list"];
                
                if ([ApplicationDelegate isValid:responseAry])
                {
                    if (responseAry.count>0)
                    {
                        self.buyViewDataArray = responseAry;
                        if (self.buyViewDataArray.count>0)
                        {
                            self.proceedToPayBtn.tag = 0;
                            [self loadScroll];
                        }
                        else
                        {
                            
                        }
                        [ApplicationDelegate removeProgressHUD];
                    }
                    else
                    {
                        self.seperatorView.hidden = YES;
                        self.downView.hidden = YES;
                        self.warningBackView.hidden = NO;
                        self.warningLbl.font = [UIFont fontWithName:@"Ebrima" size:15];
                        self.warningLbl.hidden = NO;
                        
                        self.seperatorView.hidden = NO;
                        self.downView.hidden = NO;
                    }
                }
                else
                {
                    self.seperatorView.hidden = YES;
                    self.downView.hidden = YES;
                    self.warningBackView.hidden = NO;
                    self.warningLbl.font = [UIFont fontWithName:@"Ebrima" size:15];
                    self.warningLbl.hidden = NO;
                    
                    self.seperatorView.hidden = NO;
                    self.downView.hidden = NO;
                }
            }
            
            [ApplicationDelegate removeProgressHUD];
        }
    } errorHandler:^(NSError *error)
    {
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }];
}

-(NSMutableDictionary *)getUserData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(void)loadScroll
{
    for (UIView *sub in self.BasketScrollView.subviews)
    {
        [sub removeFromSuperview];
    }
    [self.BasketScrollView setContentOffset:CGPointZero];
    float setYvalue = 0.0f;
    float scrollHeight = 0.0f;
    for (int i = 0; i<self.buyViewDataArray.count; i++)
    {
        CellView = [[MyBasketCell alloc]init];
        
        CellView.frame = CGRectMake(0, setYvalue, CellView.frame.size.width, CellView.frame.size.height);
        
        CellView.delegateBasketItem = self;
        CellView.deleteButtonView.tag=i;
        CellView.detailsButtonView.tag=i;
        CellView.editBtnView.tag = i;
        
        setYvalue = CellView.frame.origin.y + CellView.frame.size.height;
        
        scrollHeight = CellView.frame.origin.y + CellView.frame.size.height;
        UserBasketData *userBsketObj = [ApplicationDelegate.mapper getUserBasketFromDictionary:[self.buyViewDataArray objectAtIndex:i]];
        NSURL * sampurl = [[NSURL alloc]init];
        
        sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:userBsketObj.image]];
        [CellView.iconImg sd_setImageWithURL:sampurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
        CellView.iconImg.layer.cornerRadius = 3.0;
        CellView.iconImg.layer.masksToBounds = YES;
        CellView.brandName.text = userBsketObj.brand;
        [CellView.brandName setFont:EBRI_BOLD_FONT(14)];
        
        CellView.itemName.text = userBsketObj.name;
        
         [CellView.itemName setFont:EBRI_NORMAL_FONT(14)];
        
        CellView.quantityLbl.text = localize(@"Quantity:");
        
        [CellView.quantityLbl setFont:EBRI_NORMAL_FONT(12)];
        
        [CellView.priceLbl setFont:EBRI_NORMAL_FONT(12)];
        
        CellView.priceLbl.text = localize(@"Price:");
        if ([userBsketObj.quantity integerValue] > [userBsketObj.stock integerValue])
        {
            self.proceedToPayBtn.tag = 1;
            CellView.stockAvailableBackView.hidden = NO;
            CellView.stockAvailableLbl.backgroundColor = [UIColor lightGrayColor];
            CellView.stockAvailableLbl.text = [NSString stringWithFormat:localize(@"In stock %@"),userBsketObj.stock];
        }
        [ApplicationDelegate loadMarqueeLabelWithText:CellView.itemName.text Font:CellView.itemName.font InPlaceOfLabel:CellView.itemName];
        CellView.quantityValue.text = userBsketObj.quantity;
        CellView.priceValue.text = [NSString stringWithFormat:@"%@ %@" ,userBsketObj.rate,localize(@"KD")];
         [CellView.priceValue setFont:EBRI_BOLD_FONT(14)];
        
        [self.BasketScrollView addSubview:CellView];
    }
    
    self.proceedView.frame = CGRectMake(0, setYvalue, self.proceedView.frame.size.width, self.proceedView.frame.size.height);
    
    [self.BasketScrollView addSubview:self.proceedView];
    
    self.BasketScrollView.contentSize = CGSizeMake(self.BasketScrollView.frame.size.width,(scrollHeight+self.proceedView.frame.size.height));
}


-(void) deleteDidClicked:(MyBasketCell *)BasketItem
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    UserBasketData *userBsketObj = [ApplicationDelegate.mapper getUserBasketFromDictionary:[self.buyViewDataArray objectAtIndex:BasketItem.deleteButtonView.tag]];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    [postDic setObject:userBsketObj.dealId forKey:@"Deal_id"];
    [postDic setObject:userBsketObj.size forKey:@"size"];
    [ApplicationDelegate.engine DeleteBasketItemWithPostDataDictionary:postDic CompletionHandler:^(NSMutableDictionary *responseDictionary) {
        
        if ([ApplicationDelegate isValid:responseDictionary])
        {
            if ([[responseDictionary objectForKey:@"Success"] integerValue])
            {
                [[HomeViewController sharedViewController] globalLabel:[responseDictionary objectForKey:@"BasketCount"]];
                
                for (UIView *sub in self.BasketScrollView.subviews)
                {
                    [sub removeFromSuperview];
                }
                [self fetchUserBasketDataformSever];
            }
        }
        
    } errorHandler:^(NSError *error) {
        
    }];
    
}

-(void)editDidClicked:(MyBasketCell*) BasketItem
{
    bufferTag = [NSString stringWithFormat:@"%ld",(long)BasketItem.editBtnView.tag];
    self.tableBackView.center = self.view.center;
    [self.view addSubview:self.tableBackView];
}

-(void)detailsDidClicked:(MyBasketCell *) BasketItem
{
    UserBasketData *userBsketObj = [ApplicationDelegate.mapper getUserBasketFromDictionary:[self.buyViewDataArray objectAtIndex:BasketItem.deleteButtonView.tag]];
    NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
    newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:userBsketObj.dealId forKey:@"TrackId"];
    [actionTracking synchronize];
    newDealsDetailVC.productId = userBsketObj.dealId;
    [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        UILabel *countLabel;
        countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0 , 0, self.tableBackView.frame.size.width, cell.frame.size.height)];
        countLabel.textAlignment = NSTextAlignmentCenter;
        countLabel.tag = 100;
        [cell addSubview:countLabel];
    }
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    ((UILabel *)[cell viewWithTag:100]).text = [NSString stringWithFormat:@"%ld",((long)indexPath.row)+1];
    customSeperator=[[UIView alloc]initWithFrame:CGRectMake(0, (cell.frame.origin.y), 320, 1)];
    customSeperator.backgroundColor=[UIColor lightGrayColor];
    
    [cell.contentView addSubview:customSeperator];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserBasketData *userBsketObj = [ApplicationDelegate.mapper getUserBasketFromDictionary:[self.buyViewDataArray objectAtIndex:[bufferTag integerValue]]];
    if ((indexPath.row+1) > [userBsketObj.stock integerValue])
    {
        [ApplicationDelegate showAlertWithMessage:[NSString stringWithFormat:localize(@"In stock %ld available"),(long)[userBsketObj.stock integerValue]] title:nil];
    }
    else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
        [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
        [postDic setObject:userBsketObj.dealId forKey:@"Deal_id"];
        [postDic setObject:[NSString stringWithFormat:@"%ld",((long)indexPath.row)+1] forKey:@"Quantity"];
        [postDic setObject:userBsketObj.size forKey:@"size"];
        NSMutableDictionary *editItemPostData = [[NSMutableDictionary alloc] initWithDictionary:postDic];
        
        [ApplicationDelegate.engine EditBasketItemWithPostDataDictionary:editItemPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
        {
            if ([ApplicationDelegate isValid:responseDictionary])
            {
                if ([[responseDictionary objectForKey:@"Success"] integerValue])
                {
                    [self.tableBackView removeFromSuperview];
                    
                    [[HomeViewController sharedViewController] globalLabel:[responseDictionary objectForKey:@"BasketCount"]];
                    
                    [self fetchUserBasketDataformSever];
                }
            }
            
        } errorHandler:^(NSError *error)
        {
            
        }];
    }
}

- (IBAction)tableCloseBtnActn:(UIButton *)sender
{
    [self.tableBackView removeFromSuperview];
}
- (IBAction)tenPlusSubmitBtnActn:(UIButton *)sender
{
    UserBasketData *userBsketObj = [ApplicationDelegate.mapper getUserBasketFromDictionary:[self.buyViewDataArray objectAtIndex:[bufferTag integerValue]]];
    if ((self.tenPlusSizeTxtField.text.length == 0)||([self.tenPlusSizeTxtField.text intValue] <= 0 ))
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter a valid value") title:nil];
    }
    else if ([self.tenPlusSizeTxtField.text integerValue] > [userBsketObj.stock integerValue])
    {
        self.tenPlusSizeTxtField.text = @"";
        [ApplicationDelegate showAlertWithMessage:[NSString stringWithFormat:localize(@"In stock %ld available"),(long)[userBsketObj.stock integerValue]] title:nil];
    }
    else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
        UserBasketData *userBsketObj = [ApplicationDelegate.mapper getUserBasketFromDictionary:[self.buyViewDataArray objectAtIndex:[bufferTag integerValue]]];
        [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
        [postDic setObject:userBsketObj.dealId forKey:@"Deal_id"];
        [postDic setObject:[NSString stringWithFormat:@"%@",self.tenPlusSizeTxtField.text] forKey:@"Quantity"];
        [postDic setObject:userBsketObj.size forKey:@"size"];
        NSMutableDictionary *editItemPostData = [[NSMutableDictionary alloc] initWithDictionary:postDic];
        
        [ApplicationDelegate.engine EditBasketItemWithPostDataDictionary:editItemPostData CompletionHandler:^(NSMutableDictionary *responseDictionary) {
            
            if ([ApplicationDelegate isValid:responseDictionary])
            {
                if ([[responseDictionary objectForKey:@"Success"] integerValue])
                {
                    self.tenPlusSizeTxtField.text = @"";
                    
                    [self.tableBackView removeFromSuperview];
                    
                    [[HomeViewController sharedViewController] globalLabel:[responseDictionary objectForKey:@"BasketCount"]];
                    
                    [self fetchUserBasketDataformSever];
                }
            }
        } errorHandler:^(NSError *error) {
            
        }];
    }
}

- (void)limitTextField:(NSNotification *)note
{
    int limit = 2;
    if (self.tenPlusSizeTxtField.text.length > limit)
    {
        [self.tenPlusSizeTxtField setText:[self.tenPlusSizeTxtField.text substringToIndex:limit]];
    }
}

-(void)cancelNumberPad
{
    [ self.tenPlusSizeTxtField resignFirstResponder];
    self.tenPlusSizeTxtField.text = @"";
}

-(void)doneWithNumberPad
{
    [self.tenPlusSizeTxtField resignFirstResponder];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    float tempvalue = self.view.frame.size.height - self.tableBackView.frame.size.height;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    UIEdgeInsets contentInsets;
    if(result.height == 480)
    {
       contentInsets  = UIEdgeInsetsMake(0.0, 0.0, (kbSize.height) - tempvalue, 0.0);
    }
    else
    {
    contentInsets = UIEdgeInsetsMake(0.0, 0.0, (kbSize.height + 50) - tempvalue, 0.0);
    }
    self.tableBackScrollView.contentInset = contentInsets;
    self.tableBackScrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.tableBackView.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.tableBackScrollView.frame.origin) )
    {
        [self.tableBackScrollView scrollRectToVisible:self.tableBackScrollView.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.tableBackScrollView.contentSize = CGSizeMake(self.tableBackScrollView.frame.size.width, self.tenPlusBackView.frame.origin.y + self.tenPlusBackView.frame.size.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableBackScrollView.contentInset = contentInsets;
    self.tableBackScrollView.scrollIndicatorInsets = contentInsets;
}

-(void)xibLoading
{
    NSString *nibName = @"BuyViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}


-(void) fontIntilization
{
    [self.headdingLbl setFont:EBRI_NORMAL_FONT(17)];
    
    [self.totalLbl setFont:EBRI_BOLD_FONT(17)];
    
    [self.TotalCharge setFont:EBRI_BOLD_FONT(17)];
    
    [self.subTotalLbl setFont:EBRI_NORMAL_FONT(16)];
    
    [self.SubTotal setFont:EBRI_NORMAL_FONT(16)];
    
    [self.deliveryChargesLbl setFont:EBRI_NORMAL_FONT(16)];
    
    [self.DeliveryCharge setFont:EBRI_NORMAL_FONT(16)];
}

@end
