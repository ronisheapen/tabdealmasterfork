//
//  AppEngine.m
//  TabDeal
//
//  Created by Sreeraj VR on 21/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "AppEngine.h"
#import "SBJson.h"

@implementation AppEngine

-(void)RegisterUserWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kSignUp_URL params:postDic httpMethod:@"POST"];
    
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    

     [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];

    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"signup_ststus"]);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)loginUserWithLoginDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kSignIn_URL params:logDic httpMethod:@"POST"];
    

    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];

    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];

    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)forgetPasswordDataDictionary:(NSMutableDictionary *)forDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kForgetPassword_URL params:forDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

-(void)loginwithFacebookDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kFacebookSignIn_URL params:logDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}



-(void)loginwithTwitterDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kTwitterSignIn_URL params:logDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

-(void)loadNewDealsWithLanguageDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kNewDeals_URL params:logDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)loadNewDealsDetailsWithProductId:(NSMutableDictionary *)proDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kDealsDetails_URL params:proDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"deals_list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)loadCurrencyConversionWithCurrencyCode:(NSMutableDictionary *)proDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kDealsDetails_URL params:proDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"deals_list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)postAddToBasketWithDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kAddToBasket_URL params:postDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}


-(void)loadUsersFavoriteDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    
    MKNetworkOperation *op = [self operationWithPath:kUsersFavourite_URL params:userDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
           responce(jsonObject[@"list"]);

        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];


    
}


-(void)loadUsersPreviousOrderDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUsersPreviousOrder_URL params:userDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
          responce(jsonObject[@"list"]);

        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
    
}


-(void)loadPreviousOrderDetailsDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    
    MKNetworkOperation *op = [self operationWithPath:kUsersPreviousOrderDetails_URL params:userDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
    
    
}




-(void)loadUsersBasketLanguageDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUsersBasket_URL params:userDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)postAddToFavoriteWithDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kAddToFavourate_URL params:postDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}



-(void)DeleteBasketItemWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kDeleteBasketItem_URL params:postDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
    
}

-(void)EditUserAddressWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kEditAddress_URL params:postDic httpMethod:@"POST"];
    
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    
    [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)EditBasketItemWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kEditBasketItem_URL params:postDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
    
}

-(void)getCountryDetailsWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kGetCountry_URL params:postDic httpMethod:@"POST"];
   
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json; charset=utf-8" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)loadUpdatePurchaseWithDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUpdatePurchase_URL params:logDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)getPageDetailsWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kAboutTabdeal_URL params:pageDic httpMethod:@"POST"];
    
   
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
   
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}


-(void)getUserDetailsWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kMyAccount_URL params:pageDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
   
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}


-(void)DeleteFavouriteItemWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    
    MKNetworkOperation *op = [self operationWithPath:kDeleteFavItem_URL params:postDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
 
    
}


-(void)getPushNotification:(NSDictionary *)postDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{

    MKNetworkOperation *op = [self operationWithPath:kPushNotificaitonURL params:postDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

-(void)getBrandDetailsWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kGetBrand_URL params:postDic httpMethod:@"POST"];
    
  
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json; charset=utf-8" forKey:@"Content-Type"];
    [op addHeaders:header];
   
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
   
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)updateUserAccount:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUpdateUserAccountURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

-(void)search:(NSDictionary *)postData CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kSearchURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
           responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
    
    
}


-(void)EditBillingAddressWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUpdate_bank_details_URL params:postDic httpMethod:@"POST"];
    
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    
    [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getBankDetailsWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kget_bank_details_URL params:pageDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
   
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)EditPasswordWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kChangePassword_URL params:postDic httpMethod:@"POST"];

    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    
    [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)SellmoreInfo:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock

{
    
    MKNetworkOperation *op = [self operationWithPath:kSellMoreInfoURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    
    [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
}


-(void)SellTermsAndC:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
   
    MKNetworkOperation *op = [self operationWithPath:kSellTCURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    
    [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}


-(void)SellImageUpload:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kSellImageUploadURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    
    [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
    
}

-(void)getSaleParametersWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGetSaleParameters_URL params:pageDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)SubmittedItemList:(NSDictionary *)postData CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kSubmittedItemsURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
             responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

-(void)getSaleEditableParametersDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kEditSale_URL params:pageDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}


-(void)SellUpdate:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    
    MKNetworkOperation *op = [self operationWithPath:kSellUpdateURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    
    [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
}


-(void)loadUsersnotificationDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kGetPushNotificationList_URL params:userDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"list"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
    
}

-(void)EditStatusWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUpdateNotificationStatus_URL params:postDic httpMethod:@"POST"];
    
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    
    [header setValue:@"application/json; charset=utf-8" forKey:@"Content-Type"];
    
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)UpdateDeliveryAddress:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUpdateDeliveryAddressURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

-(void)UpdatePersonalInfo:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUpdatePersonalInfoURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    
}

-(void)loadUpdatePurchaseOnCompleteWithDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kUpdatePurchaseOnComplete_URL params:logDic httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)InsertPaymentHistory:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kInsertPaymentHistoryURL params:postData httpMethod:@"POST"];
    
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];

    
}



@end
