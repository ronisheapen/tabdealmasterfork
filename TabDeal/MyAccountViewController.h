//
//  MyAccountViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface MyAccountViewController : UIViewController<ListSelectionProtocol,UITextFieldDelegate,UIScrollViewDelegate>{
    NSMutableArray * countryNameArray;
    NSMutableArray * countryCodeArray;
    
    NSString * personalMobileBuffer;
    NSString * personalCodeBuffer;
    NSString * deliveryMobileBuffer;
    NSString * deliveryCodeBuffer;
    NSString * deliveryPostalCodeBuffer;
    int dropDownTag;
    //CGRect scrollFrame;
}
@property (weak, nonatomic) IBOutlet UIImageView *backWhiteView;
@property (weak, nonatomic) IBOutlet UIButton *bankDetailsBtn;
- (IBAction)bankDetailsBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIButton *dropBtn;
- (IBAction)dropDownActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownDwnArrow;
@property (strong, nonatomic) DropDownView *countryDropDownObj;
@property (weak, nonatomic) IBOutlet UITextField *countryTextView;
@property (strong, nonatomic)NSMutableArray * countryCodeAndName;
@property (weak, nonatomic) IBOutlet UIView *myAccountBackView;

@property (weak, nonatomic) IBOutlet UITextField *firstNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTxt;
@property (weak, nonatomic) IBOutlet UITextField *codeTxt;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordBtn;
- (IBAction)changePasswordBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *nameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *addressLine1Txt;
@property (weak, nonatomic) IBOutlet UITextField *addressLine2Txt;
@property (weak, nonatomic) IBOutlet UITextField *cityTxt;
@property (weak, nonatomic) IBOutlet UITextField *postalCodeTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveChangesBtn;
- (IBAction)saveChangesBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *editPersonalInfoBtn;
- (IBAction)editPersonalInfoBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *editDeliveryAddressBtn;
- (IBAction)editDeliveryAdderssBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailAddressTxt;
@property (strong, nonatomic) IBOutlet UIView *changePasswordBackView;
- (IBAction)closeBtnAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewTxt;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTxt;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *headingLbl;
@property (weak, nonatomic) IBOutlet UITextField *personalinfoTxt;
@property (weak, nonatomic) IBOutlet UITextField *enterDeliveryAddressTxt;
@property (weak, nonatomic) IBOutlet UILabel *conditionLbl;
@property (weak, nonatomic) IBOutlet UILabel *changePassHeaddingLbl;

@property (strong, nonatomic) IBOutlet UIView *editPersonalInfoBackView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *lastNameEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *codeEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailEditTxt;

@property (strong, nonatomic) IBOutlet UIView *editDeliveryAddressBackView;

@property (weak, nonatomic) IBOutlet UITextField *nameEditTxtField;
@property (weak, nonatomic) IBOutlet UITextField *addressLine1EditTxt;
@property (weak, nonatomic) IBOutlet UITextField *addressLine2EditTxt;
@property (weak, nonatomic) IBOutlet UITextField *cityEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *countryEditTxt;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownUpArrowEdit;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownDwnArrowEdit;
@property (weak, nonatomic) IBOutlet UIButton *dropBtnEdit;

@property (weak, nonatomic) IBOutlet UITextField *postalCodeEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *codeDelEditTxt;
@property (weak, nonatomic) IBOutlet UITextField *mobileDelEditTxt;

- (IBAction)dropDownEditActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *seperatorTxtView;
@property (weak, nonatomic) IBOutlet UILabel *plusLbl;
@property (weak, nonatomic) IBOutlet UIButton *addDeliveryAddressBtn;
//@property (weak, nonatomic) IBOutlet UILabel *warningLbl;
- (IBAction)addDeliveryAddressBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *saveDeliveryAddressBtn;
- (IBAction)saveDeliveryAddressBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *savePersonalInformationBtn;
- (IBAction)savePersonalInfoBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *editScroll;
@property (weak, nonatomic) IBOutlet UIView *editBackView;
@property (weak, nonatomic) IBOutlet UIScrollView *editScrollPersonal;
@property (weak, nonatomic) IBOutlet UIView *editPersonalBackView;

@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressHeading;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *personalInfomationHeading;

@end
