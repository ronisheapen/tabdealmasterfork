//
//  UserSellInformation.m
//  TabDeal
//
//  Created by satheesh on 11/17/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "UserSellInformation.h"

@implementation UserSellInformation
@synthesize authImages;
@synthesize brandId;
@synthesize country;
@synthesize currency_code;
@synthesize idProof;
@synthesize productImageCode;
@synthesize rate;
@synthesize reciept;
@synthesize status;
@synthesize type;
@synthesize userId;
@synthesize yearOfPurchase;
@synthesize lang_key;
@end
