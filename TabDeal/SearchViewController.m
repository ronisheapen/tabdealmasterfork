//
//  SearchViewController.m
//  TabDeal
//
//  Created by satheesh on 9/22/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchResultViewController.h"
#import "HomeViewController.h"
#import "SearchByCell.h"
#import "searchByCell_a.h"

#define OFFSETHEIGHT 5
#define OFFSETCELLHEIGHT 32

@interface SearchViewController (){
    //UIView * customSeperator;
}

@end

@implementation SearchViewController

@synthesize genderContentArray;
@synthesize itemTypeContentArray;
@synthesize brandsContentArray;
@synthesize brandCodeArray;
@synthesize brandNameArray;
@synthesize statusContentArray;

@synthesize genderTickArray;
@synthesize itemTypeTickArray;
@synthesize brandsTickArray;
@synthesize statusTickArray;

-(void)viewWillAppear:(BOOL)animated{
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:0];
    if (ApplicationDelegate.innerViewCount.intValue==0) {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else{
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]]) {
        [self fetchBrandDataformSever];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    self.searchByHeading.text = localize(@"SEARCH BY:");
     [self.searchByHeading setFont:EBRI_BOLD_FONT(17)];
    self.genderLbl.text = localize(@"Gender");
     [self.genderLbl setFont:EBRI_NORMAL_FONT(16)];
    [self.itemTypeLbl setFont:EBRI_NORMAL_FONT(16)];
    [self.brandsLbl setFont:EBRI_NORMAL_FONT(16)];
    [self.statusLbl setFont:EBRI_NORMAL_FONT(16)];
    [self.submitBtn.titleLabel setFont:EBRI_BOLD_FONT(16)];
    
    self.itemTypeLbl.text = localize(@"Item Type");
    self.brandsLbl.text = localize(@"Brands");
    self.statusLbl.text = localize(@"Status");
    [self.submitBtn setTitle:localize(@"SUBMIT SEARCH") forState:UIControlStateNormal];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xibLoading];
    
    genderFrame = self.genderBtn.frame;
    itemTypeFrame = self.itemTypeBtn.frame;
    branchFrame = self.brandBtn.frame;
    statusFrame = self.statusBtn.frame;
    submitFrame = self.submitBtn.frame;
    
    genderCngFrame = self.genderBtn.frame;
    itemTypeCngFrame = self.itemTypeBtn.frame;
    branchCngFrame = self.brandBtn.frame;
    statusCngFrame = self.statusBtn.frame;
    submitCngFrame = self.submitBtn.frame;
    
    self.genderContentArray = [[NSMutableArray alloc]initWithObjects:localize(@"Male"),localize(@"Female"), nil];
    self.itemTypeContentArray = [[NSMutableArray alloc]initWithObjects:localize(@"Accessories"),localize(@"Jewellery"),localize(@"Shoes"),localize(@"Bags"), nil];
    //self.brandsContentArray = [[NSMutableArray alloc]initWithObjects:@"Brand1",@"Brand2",@"Brand3",@"Brand4",@"Brand5",@"Brand6", nil];
    self.brandsContentArray = [[NSMutableArray alloc]init];
    self.brandNameArray = [[NSMutableArray alloc]init];
    self.brandCodeArray = [[NSMutableArray alloc]init];
    self.statusContentArray = [[NSMutableArray alloc]initWithObjects:localize(@"New"),localize(@"Used"), nil];
    
    self.genderTickArray = [[NSMutableArray alloc]init];
    for (int i = 0; i< self.genderContentArray.count; i++) {
        [self.genderTickArray addObject:@"NO"];
    }
    
    self.itemTypeTickArray = [[NSMutableArray alloc]init];
    for (int i = 0; i< self.itemTypeContentArray.count; i++) {
        [self.itemTypeTickArray addObject:@"NO"];
    }
    
    self.brandsTickArray = [[NSMutableArray alloc]init];
    for (int i = 0; i< self.brandsContentArray.count; i++) {
        [self.brandsTickArray addObject:@"NO"];
    }
    
    self.statusTickArray = [[NSMutableArray alloc]init];
    for (int i = 0; i< self.statusContentArray.count; i++) {
        [self.statusTickArray addObject:@"NO"];
    }
    
    self.genderTableView.scrollEnabled = NO;
    self.itemTableView.scrollEnabled = NO;
    self.brandTableView.scrollEnabled = NO;
    self.statusTableView.scrollEnabled = NO;
    
    self.genderTableView.layer.cornerRadius = 4;
    self.brandTableView.layer.cornerRadius = 4;
    self.itemTableView.layer.cornerRadius = 4;
    self.statusTableView.layer.cornerRadius = 4;
    
    // Do any additional setup after loading the view from its nib.
}

-(void)fetchBrandDataformSever{
    
    NSMutableDictionary *brandData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserBrandPostData]];
    NSLog(@"%@",brandData);
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getBrandDetailsWithDataDictionary:brandData CompletionHandler:^(NSMutableArray *responseArray)
     {
         
         if ([ApplicationDelegate isValid:responseArray])
         {
             if (responseArray.count>0)
             {
                 self.brandsContentArray = [[NSMutableArray alloc]init];
                 self.brandNameArray = [[NSMutableArray alloc]init];
                 self.brandCodeArray = [[NSMutableArray alloc]init];
                 for (int i=0; i<responseArray.count; i++)
                 {
                     [self.brandsContentArray addObject:[responseArray objectAtIndex:i]];
                     [self.brandNameArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"BrandName"]];
                     [self.brandCodeArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"BrandId"]];
                 }
                 self.brandsTickArray = [[NSMutableArray alloc]init];
                 for (int i = 0; i< self.brandsContentArray.count; i++) {
                     [self.brandsTickArray addObject:@"NO"];
                 }
                 [self.brandTableView reloadData];
             }
             
             [ApplicationDelegate removeProgressHUD];
         }
         
     } errorHandler:^(NSError *error) {
         
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}


-(NSMutableDictionary *)getUserBrandPostData{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return 10;
    if (tableView == self.genderTableView) {
        return self.genderContentArray.count;
    }
    else if(tableView == self.itemTableView){
        return self.itemTypeContentArray.count;
    }
    else if(tableView == self.brandTableView){
//        return self.brandsContentArray.count;
        return self.brandNameArray.count;
    }
    else if(tableView == self.statusTableView){
        return self.statusContentArray.count;
    }
    else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 32;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    SearchByCell *cell = (SearchByCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchByCell_a" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else{
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchByCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    if (tableView == self.genderTableView) {
        cell.Name.text = [self.genderContentArray objectAtIndex:indexPath.row];
        
        [cell.Name setFont:EBRI_NORMAL_FONT(16)];
        if ([[self.genderTickArray objectAtIndex:indexPath.row]isEqualToString:@"NO"]) {
            cell.tickImage.hidden = YES;
        } else {
            cell.tickImage.hidden = NO;
        }
    }
    else if(tableView == self.itemTableView){
        cell.Name.text = [self.itemTypeContentArray objectAtIndex:indexPath.row];
         [cell.Name setFont:EBRI_NORMAL_FONT(16)];
        if ([[self.itemTypeTickArray objectAtIndex:indexPath.row]isEqualToString:@"NO"]) {
            cell.tickImage.hidden = YES;
        } else {
            cell.tickImage.hidden = NO;
        }
    }
    else if(tableView == self.brandTableView){
        cell.Name.text = [self.brandNameArray objectAtIndex:indexPath.row];
         [cell.Name setFont:EBRI_NORMAL_FONT(16)];
        if ([[self.brandsTickArray objectAtIndex:indexPath.row]isEqualToString:@"NO"]) {
            cell.tickImage.hidden = YES;
        } else {
            cell.tickImage.hidden = NO;
        }
    }
    else if(tableView == self.statusTableView){
        cell.Name.text = [self.statusContentArray objectAtIndex:indexPath.row];
         [cell.Name setFont:EBRI_NORMAL_FONT(16)];
        if ([[self.statusTickArray objectAtIndex:indexPath.row]isEqualToString:@"NO"]) {
            cell.tickImage.hidden = YES;
        } else {
            cell.tickImage.hidden = NO;
        }
    }
    //cell.nameLabel.text = [tableData objectAtIndex:indexPath.row];
    //cell.thumbnailImageView.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
    //cell.prepTimeLabel.text = [prepTime objectAtIndex:indexPath.row];
    
    //customSeperator=[[UIView alloc]initWithFrame:CGRectMake(0, (cell.frame.origin.y+3), 320, 1)];
    //customSeperator.backgroundColor=[UIColor whiteColor];
    
    //[cell.contentView addSubview:customSeperator];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath");
    /*UIAlertView *messageAlert = [[UIAlertView alloc]
     initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];*/
    //    UIAlertView *messageAlert = [[UIAlertView alloc]
    //                                 initWithTitle:@"Row Selected" message:[tableData objectAtIndex:indexPath.row] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display the Hello World Message
    //[messageAlert show];
    
    // Checked the selected row
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == self.genderTableView) {
        if ([[self.genderTickArray objectAtIndex:indexPath.row]isEqualToString:@"NO"]) {
            [self.genderTickArray setObject:@"YES" atIndexedSubscript:indexPath.row];
        } else {
            [self.genderTickArray setObject:@"NO" atIndexedSubscript:indexPath.row];
        }
        [self.genderTableView reloadData];
    }
    else if(tableView == self.itemTableView){
        if ([[self.itemTypeTickArray objectAtIndex:indexPath.row]isEqualToString:@"NO"]) {
            [self.itemTypeTickArray setObject:@"YES" atIndexedSubscript:indexPath.row];
        } else {
            [self.itemTypeTickArray setObject:@"NO" atIndexedSubscript:indexPath.row];
        }
        [self.itemTableView reloadData];
    }
    else if(tableView == self.brandTableView){
        if ([[self.brandsTickArray objectAtIndex:indexPath.row]isEqualToString:@"NO"]) {
            [self.brandsTickArray setObject:@"YES" atIndexedSubscript:indexPath.row];
        } else {
            [self.brandsTickArray setObject:@"NO" atIndexedSubscript:indexPath.row];
        }
        [self.brandTableView reloadData];
    }
    else if(tableView == self.statusTableView){
        if ([[self.statusTickArray objectAtIndex:indexPath.row]isEqualToString:@"NO"]) {
            [self.statusTickArray setObject:@"YES" atIndexedSubscript:indexPath.row];
        } else {
            [self.statusTickArray setObject:@"NO" atIndexedSubscript:indexPath.row];
        }
        [self.statusTableView reloadData];
    }
}

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"willSelectRowAtIndexPath");
//    if (indexPath.row == 0) {
//        return nil;
//    }
//    
//    return indexPath;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)genderBtnActn:(UIButton *)sender {
    
    self.genderBtn.selected = !self.genderBtn.selected;
    NSLog(@"%d",self.genderContentArray.count);
    float change = self.genderContentArray.count * OFFSETCELLHEIGHT;
    if (self.genderBtn.selected) {
        NSLog(@"notselected");
        [UIView animateWithDuration:.5 animations:^{
            self.genderTableView.frame = CGRectMake(self.genderTableView.frame.origin.x, self.genderBtn.frame.origin.y + self.genderBtn.frame.size.height, self.genderTableView.frame.size.width, change);
            
            self.itemTypeBtn.frame = CGRectMake(itemTypeFrame.origin.x, self.genderTableView.frame.origin.y + self.genderTableView.frame.size.height + OFFSETHEIGHT, self.itemTypeBtn.frame.size.width, self.itemTypeBtn.frame.size.height);
            
            self.itemTypeLbl.frame = CGRectMake(self.itemTypeLbl.frame.origin.x, self.itemTypeBtn.frame.origin.y, self.itemTypeLbl.frame.size.width, self.itemTypeLbl.frame.size.height);
            
            self.itemTypeStsLbl.frame = CGRectMake(self.itemTypeStsLbl.frame.origin.x, self.itemTypeBtn.frame.origin.y, self.itemTypeStsLbl.frame.size.width, self.itemTypeStsLbl.frame.size.height);
            
            self.itemTableView.frame = CGRectMake(self.itemTableView.frame.origin.x, self.itemTableView.frame.origin.y + self.genderTableView.frame.size.height, self.itemTableView.frame.size.width, self.itemTableView.frame.size.height);
            
            self.brandBtn.frame = CGRectMake(branchFrame.origin.x, self.itemTableView.frame.origin.y + self.itemTableView.frame.size.height + OFFSETHEIGHT, self.brandBtn.frame.size.width, self.brandBtn.frame.size.height);
            
            self.brandsLbl.frame = CGRectMake(self.brandsLbl.frame.origin.x, self.brandBtn.frame.origin.y, self.brandsLbl.frame.size.width, self.brandsLbl.frame.size.height);
            
            self.brandsStsLbl.frame = CGRectMake(self.brandsStsLbl.frame.origin.x, self.brandBtn.frame.origin.y, self.brandsStsLbl.frame.size.width, self.brandsStsLbl.frame.size.height);
            
            self.brandTableView.frame = CGRectMake(self.brandTableView.frame.origin.x, self.brandTableView.frame.origin.y + self.genderTableView.frame.size.height, self.brandTableView.frame.size.width, self.brandTableView.frame.size.height);
            
            self.statusBtn.frame = CGRectMake(branchFrame.origin.x, self.brandTableView.frame.origin.y + self.brandTableView.frame.size.height + OFFSETHEIGHT, self.statusBtn.frame.size.width, self.statusBtn.frame.size.height);
            
            self.statusLbl.frame = CGRectMake(self.statusLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusLbl.frame.size.width, self.statusLbl.frame.size.height);
            
            self.statusStsLbl.frame = CGRectMake(self.statusStsLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusStsLbl.frame.size.width, self.statusStsLbl.frame.size.height);
            
            self.statusTableView.frame = CGRectMake(self.statusTableView.frame.origin.x, self.statusTableView.frame.origin.y + self.genderTableView.frame.size.height, self.statusTableView.frame.size.width, self.statusTableView.frame.size.height);
            
            self.submitBtn.frame = CGRectMake(submitFrame.origin.x, self.submitBtn.frame.origin.y + self.genderTableView.frame.size.height + OFFSETHEIGHT, self.submitBtn.frame.size.width, self.submitBtn.frame.size.height);
            
            self.genderTableView.hidden = NO;
            
            //self.genderStsLbl.text = @"-";
            
        } completion:^(BOOL finished) {
            self.genderTableView.hidden = NO;
            self.genderStsLbl.text = @"-";
            self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.submitBtn.frame.origin.y + self.submitBtn.frame.size.height + 40);
        }];
    } else {
        NSLog(@"selected");
        [UIView animateWithDuration:.5 animations:^{
            self.genderTableView.frame = CGRectMake(self.genderTableView.frame.origin.x, self.genderBtn.frame.origin.y, self.genderTableView.frame.size.width, self.genderBtn.frame.size.height);
            
            self.itemTypeBtn.frame = CGRectMake(itemTypeFrame.origin.x, self.genderTableView.frame.origin.y + self.genderTableView.frame.size.height + OFFSETHEIGHT, self.itemTypeBtn.frame.size.width, self.itemTypeBtn.frame.size.height);
            
            self.itemTypeLbl.frame = CGRectMake(self.itemTypeLbl.frame.origin.x, self.itemTypeBtn.frame.origin.y, self.itemTypeLbl.frame.size.width, self.itemTypeLbl.frame.size.height);
            
            self.itemTypeStsLbl.frame = CGRectMake(self.itemTypeStsLbl.frame.origin.x, self.itemTypeBtn.frame.origin.y, self.itemTypeStsLbl.frame.size.width, self.itemTypeStsLbl.frame.size.height);
            
            self.itemTableView.frame = CGRectMake(self.itemTableView.frame.origin.x, self.itemTableView.frame.origin.y - change , self.itemTableView.frame.size.width, self.itemTableView.frame.size.height);
            
            self.brandBtn.frame = CGRectMake(branchFrame.origin.x, self.brandBtn.frame.origin.y - change, self.brandBtn.frame.size.width, self.brandBtn.frame.size.height);
            
            self.brandsLbl.frame = CGRectMake(self.brandsLbl.frame.origin.x, self.brandBtn.frame.origin.y, self.brandsLbl.frame.size.width, self.brandsLbl.frame.size.height);
            
            self.brandsStsLbl.frame = CGRectMake(self.brandsStsLbl.frame.origin.x, self.brandBtn.frame.origin.y, self.brandsStsLbl.frame.size.width, self.brandsStsLbl.frame.size.height);
            
            self.brandTableView.frame = CGRectMake(self.brandTableView.frame.origin.x, self.brandTableView.frame.origin.y - change, self.brandTableView.frame.size.width, self.brandTableView.frame.size.height);
            
            self.statusBtn.frame = CGRectMake(branchFrame.origin.x, self.brandTableView.frame.origin.y + self.brandTableView.frame.size.height + OFFSETHEIGHT, self.statusBtn.frame.size.width, self.statusBtn.frame.size.height);
            
            self.statusLbl.frame = CGRectMake(self.statusLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusLbl.frame.size.width, self.statusLbl.frame.size.height);
            
            self.statusStsLbl.frame = CGRectMake(self.statusStsLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusStsLbl.frame.size.width, self.statusStsLbl.frame.size.height);
            
            self.statusTableView.frame = CGRectMake(self.statusTableView.frame.origin.x, self.statusTableView.frame.origin.y - change, self.statusTableView.frame.size.width, self.statusTableView.frame.size.height);
            
            self.submitBtn.frame = CGRectMake(submitFrame.origin.x, self.submitBtn.frame.origin.y - change, self.submitBtn.frame.size.width, self.submitBtn.frame.size.height);
            
            //self.genderStsLbl.text = @"+";
            
            //self.itemTypeBtn.frame = itemTypeFrame;
        } completion:^(BOOL finished) {
            self.genderTableView.hidden = YES;
            self.genderStsLbl.text = @"+";
            self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.submitBtn.frame.origin.y + self.submitBtn.frame.size.height + 40);
        }];
    }
}

- (IBAction)itemTypeBtnActn:(UIButton *)sender {
    self.itemTypeBtn.selected = !self.itemTypeBtn.selected;
    float change = self.itemTypeContentArray.count * OFFSETCELLHEIGHT;
    if (self.itemTypeBtn.selected) {
        NSLog(@"notselected");
        [UIView animateWithDuration:.5 animations:^{
            
            self.itemTableView.frame = CGRectMake(self.itemTableView.frame.origin.x, self.itemTableView.frame.origin.y + self.itemTypeBtn.frame.size.height, self.itemTableView.frame.size.width, change);
            
            self.brandBtn.frame = CGRectMake(branchFrame.origin.x, self.itemTableView.frame.origin.y + self.itemTableView.frame.size.height + OFFSETHEIGHT, self.brandBtn.frame.size.width, self.brandBtn.frame.size.height);
            
            self.brandsLbl.frame = CGRectMake(self.brandsLbl.frame.origin.x, self.brandBtn.frame.origin.y, self.brandsLbl.frame.size.width, self.brandsLbl.frame.size.height);
            
            self.brandsStsLbl.frame = CGRectMake(self.brandsStsLbl.frame.origin.x, self.brandBtn.frame.origin.y, self.brandsStsLbl.frame.size.width, self.brandsStsLbl.frame.size.height);
            
            self.brandTableView.frame = CGRectMake(self.brandTableView.frame.origin.x, self.brandTableView.frame.origin.y + self.itemTableView.frame.size.height, self.brandTableView.frame.size.width, self.brandTableView.frame.size.height);
            
            self.statusBtn.frame = CGRectMake(branchFrame.origin.x, self.brandTableView.frame.origin.y + self.brandTableView.frame.size.height + OFFSETHEIGHT, self.statusBtn.frame.size.width, self.statusBtn.frame.size.height);
            
            self.statusLbl.frame = CGRectMake(self.statusLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusLbl.frame.size.width, self.statusLbl.frame.size.height);
            
            self.statusStsLbl.frame = CGRectMake(self.statusStsLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusStsLbl.frame.size.width, self.statusStsLbl.frame.size.height);
            
            self.statusTableView.frame = CGRectMake(self.statusTableView.frame.origin.x, self.statusTableView.frame.origin.y + self.itemTableView.frame.size.height, self.statusTableView.frame.size.width, self.statusTableView.frame.size.height);
            
            self.submitBtn.frame = CGRectMake(submitFrame.origin.x, self.submitBtn.frame.origin.y + self.itemTableView.frame.size.height, self.submitBtn.frame.size.width, self.submitBtn.frame.size.height);
            
            self.itemTableView.hidden = NO;
            //self.itemTypeStsLbl.text = @"-";
        } completion:^(BOOL finished) {
            self.itemTypeStsLbl.text = @"-";
            self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.submitBtn.frame.origin.y + self.submitBtn.frame.size.height + 40);
        }];
    } else {
        NSLog(@"selected");
        [UIView animateWithDuration:.5 animations:^{
            //self.genderTableView.frame = CGRectMake(self.genderTableView.frame.origin.x, self.genderBtn.frame.origin.y, self.genderTableView.frame.size.width, self.genderBtn.frame.size.height);
            
            //self.itemTypeBtn.frame = CGRectMake(itemTypeFrame.origin.x, self.genderTableView.frame.origin.y + self.genderTableView.frame.size.height + OFFSETHEIGHT, self.itemTypeBtn.frame.size.width, self.itemTypeBtn.frame.size.height);
            
            self.itemTableView.frame = CGRectMake(self.itemTableView.frame.origin.x, self.itemTypeBtn.frame.origin.y , self.itemTableView.frame.size.width, self.itemTypeBtn.frame.size.height);
            
            self.brandBtn.frame = CGRectMake(branchFrame.origin.x, self.brandBtn.frame.origin.y - change, self.brandBtn.frame.size.width, self.brandBtn.frame.size.height);
            
            self.brandsLbl.frame = CGRectMake(self.brandsLbl.frame.origin.x, self.brandBtn.frame.origin.y, self.brandsLbl.frame.size.width, self.brandsLbl.frame.size.height);
            
            self.brandsStsLbl.frame = CGRectMake(self.brandsStsLbl.frame.origin.x, self.brandBtn.frame.origin.y, self.brandsStsLbl.frame.size.width, self.brandsStsLbl.frame.size.height);
            
            self.brandTableView.frame = CGRectMake(self.brandTableView.frame.origin.x, self.brandTableView.frame.origin.y - change, self.brandTableView.frame.size.width, self.brandTableView.frame.size.height);
            
            self.statusBtn.frame = CGRectMake(self.statusBtn.frame.origin.x, self.statusBtn.frame.origin.y - change, self.statusBtn.frame.size.width, self.statusBtn.frame.size.height);
            
            self.statusLbl.frame = CGRectMake(self.statusLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusLbl.frame.size.width, self.statusLbl.frame.size.height);
            
            self.statusStsLbl.frame = CGRectMake(self.statusStsLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusStsLbl.frame.size.width, self.statusStsLbl.frame.size.height);
            
            self.statusTableView.frame = CGRectMake(self.statusTableView.frame.origin.x, self.statusTableView.frame.origin.y - change, self.statusTableView.frame.size.width, self.statusTableView.frame.size.height);
            
            self.submitBtn.frame = CGRectMake(submitFrame.origin.x, self.submitBtn.frame.origin.y - change, self.submitBtn.frame.size.width, self.submitBtn.frame.size.height);
            
            //self.itemTypeBtn.frame = itemTypeFrame;
            //self.itemTypeStsLbl.text = @"+";
        } completion:^(BOOL finished) {
            self.itemTableView.hidden = YES;
            self.itemTypeStsLbl.text = @"+";
            self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.submitBtn.frame.origin.y + self.submitBtn.frame.size.height + 40);
        }];
    }
}

- (IBAction)brandBtnActn:(UIButton *)sender {
    self.brandBtn.selected = !self.brandBtn.selected;
    float change = self.brandsContentArray.count * OFFSETCELLHEIGHT;
    if (self.brandBtn.selected) {
        NSLog(@"notselected");
        [UIView animateWithDuration:.5 animations:^{
            
            //self.itemTableView.frame = CGRectMake(self.itemTableView.frame.origin.x, self.itemTypeBtn.frame.origin.y + self.itemTypeBtn.frame.size.height, self.itemTableView.frame.size.width, self.itemTableView.frame.size.height + 100);
            
            //self.brandBtn.frame = CGRectMake(branchFrame.origin.x, self.itemTableView.frame.origin.y + self.itemTableView.frame.size.height + OFFSETHEIGHT, self.brandBtn.frame.size.width, self.brandBtn.frame.size.height);
            
            self.brandTableView.frame = CGRectMake(self.brandTableView.frame.origin.x, self.brandBtn.frame.origin.y + self.brandBtn.frame.size.height, self.brandTableView.frame.size.width, change);
            
            self.statusBtn.frame = CGRectMake(self.statusBtn.frame.origin.x, self.statusBtn.frame.origin.y + self.brandTableView.frame.size.height , self.statusBtn.frame.size.width, self.statusBtn.frame.size.height);
            
            self.statusLbl.frame = CGRectMake(self.statusLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusLbl.frame.size.width, self.statusLbl.frame.size.height);
            
            self.statusStsLbl.frame = CGRectMake(self.statusStsLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusStsLbl.frame.size.width, self.statusStsLbl.frame.size.height);
            
            self.statusTableView.frame = CGRectMake(self.statusTableView.frame.origin.x, self.statusTableView.frame.origin.y + self.brandTableView.frame.size.height , self.statusTableView.frame.size.width, self.statusTableView.frame.size.height);
            
            self.submitBtn.frame = CGRectMake(submitFrame.origin.x, self.submitBtn.frame.origin.y + self.brandTableView.frame.size.height, self.submitBtn.frame.size.width, self.submitBtn.frame.size.height);
            
            self.brandTableView.hidden = NO;
            //self.brandsStsLbl.text = @"-";
        } completion:^(BOOL finished) {
            self.brandsStsLbl.text = @"-";
            self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.submitBtn.frame.origin.y + self.submitBtn.frame.size.height + 40);
        }];
    } else {
        NSLog(@"selected");
        [UIView animateWithDuration:.5 animations:^{
            
            
            self.brandTableView.frame = CGRectMake(self.brandTableView.frame.origin.x, self.brandBtn.frame.origin.y, self.brandTableView.frame.size.width, self.brandBtn.frame.size.height);
            
            self.statusBtn.frame = CGRectMake(self.statusBtn.frame.origin.x, self.statusBtn.frame.origin.y - change, self.statusBtn.frame.size.width, self.statusBtn.frame.size.height);
            
            self.statusLbl.frame = CGRectMake(self.statusLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusLbl.frame.size.width, self.statusLbl.frame.size.height);
            
            self.statusStsLbl.frame = CGRectMake(self.statusStsLbl.frame.origin.x, self.statusBtn.frame.origin.y, self.statusStsLbl.frame.size.width, self.statusStsLbl.frame.size.height);
            
            self.statusTableView.frame = CGRectMake(self.statusTableView.frame.origin.x, self.statusTableView.frame.origin.y - change, self.statusTableView.frame.size.width, self.statusTableView.frame.size.height);
            
            self.submitBtn.frame = CGRectMake(submitFrame.origin.x, self.submitBtn.frame.origin.y - change, self.submitBtn.frame.size.width, self.submitBtn.frame.size.height);
            
            //self.itemTypeBtn.frame = itemTypeFrame;
            //self.brandsStsLbl.text = @"+";
        } completion:^(BOOL finished) {
            self.brandTableView.hidden = YES;
            self.brandsStsLbl.text = @"+";
            self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.submitBtn.frame.origin.y + self.submitBtn.frame.size.height + 40);
        }];
    }
}

- (IBAction)statusBtnActn:(UIButton *)sender {
    self.statusBtn.selected = !self.statusBtn.selected;
    float change = self.statusContentArray.count * OFFSETCELLHEIGHT;
    if (self.statusBtn.selected) {
        NSLog(@"notselected");
        [UIView animateWithDuration:.5 animations:^{
            
            //self.itemTableView.frame = CGRectMake(self.itemTableView.frame.origin.x, self.itemTypeBtn.frame.origin.y + self.itemTypeBtn.frame.size.height, self.itemTableView.frame.size.width, self.itemTableView.frame.size.height + 100);
            
            //self.brandBtn.frame = CGRectMake(branchFrame.origin.x, self.itemTableView.frame.origin.y + self.itemTableView.frame.size.height + OFFSETHEIGHT, self.brandBtn.frame.size.width, self.brandBtn.frame.size.height);
            
            //self.brandTableView.frame = CGRectMake(self.brandTableView.frame.origin.x, self.brandBtn.frame.origin.y + self.brandBtn.frame.size.height, self.brandTableView.frame.size.width, self.brandTableView.frame.size.height + 100);
            
            //self.statusBtn.frame = CGRectMake(branchFrame.origin.x, self.brandTableView.frame.origin.y + self.brandTableView.frame.size.height + OFFSETHEIGHT, self.statusBtn.frame.size.width, self.statusBtn.frame.size.height);
            
            self.statusTableView.frame = CGRectMake(self.statusTableView.frame.origin.x, self.statusBtn.frame.origin.y + self.statusBtn.frame.size.height, self.statusTableView.frame.size.width, change);
            
            self.submitBtn.frame = CGRectMake(submitFrame.origin.x, self.submitBtn.frame.origin.y + self.statusTableView.frame.size.height, self.submitBtn.frame.size.width, self.submitBtn.frame.size.height);
            
            self.statusTableView.hidden = NO;
            //self.statusStsLbl.text = @"-";
            
        } completion:^(BOOL finished) {
            self.statusStsLbl.text = @"-";
            self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.submitBtn.frame.origin.y + self.submitBtn.frame.size.height + 40);
        }];
    } else {
        NSLog(@"selected");
        [UIView animateWithDuration:.5 animations:^{
            
            self.statusTableView.frame = CGRectMake(self.statusTableView.frame.origin.x, self.statusBtn.frame.origin.y , self.statusTableView.frame.size.width, self.statusBtn.frame.size.height);
            
            self.submitBtn.frame = CGRectMake(submitFrame.origin.x, self.submitBtn.frame.origin.y - change, self.submitBtn.frame.size.width, self.submitBtn.frame.size.height);
            //self.statusStsLbl.text = @"+";
        
        } completion:^(BOOL finished) {
            self.statusTableView.hidden = YES;
            self.statusStsLbl.text = @"+";
            self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.submitBtn.frame.origin.y + self.submitBtn.frame.size.height + 40);
        }];
    }
}
- (IBAction)submitBtnActn:(UIButton *)sender {
    
    
    NSDictionary *requestDictionary = @{@"Gender":self.getGenderArray,@"Type": self.getItemArray,@"Brands": self.getBrandsArray,@"Status": self.getStatusArray,@"SortType": @"HTL",@"lang_key": kENGLISH};
    
    NSLog(@"%@",requestDictionary);
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine search:requestDictionary CompletionHandler:^(NSMutableArray *responseArray) {
        
          [ApplicationDelegate removeProgressHUD];
        
        SearchResultViewController * searchResultVC = [[SearchResultViewController alloc]initWithNibName:@"SearchResultViewController" bundle:nil];
        searchResultVC.view.backgroundColor = [UIColor clearColor];
        
        searchResultVC.searchResultArray=[[NSMutableArray alloc] init];
        
        actionTracking = [NSUserDefaults standardUserDefaults];
        [actionTracking setObject:responseArray forKey:@"searchArray"];
        [actionTracking setObject:[[NSMutableDictionary alloc]initWithDictionary:requestDictionary] forKey:@"searchDictn"];
        [actionTracking synchronize];
        
        searchResultVC.searchResultArray=responseArray;
        
        //searchResultVC.searchDictionary = [[NSMutableDictionary alloc]init];
        
        searchResultVC.searchDictionary = [[NSMutableDictionary alloc]initWithDictionary:requestDictionary];
        
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SearchResultViewController class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:searchResultVC animated:NO];
        }
        
        
    } errorHandler:^(NSError *error) {
        
          [ApplicationDelegate removeProgressHUD];
    }
     ];
    
 

}


-(NSMutableArray*) getGenderArray
{
  
    NSMutableArray *selectedGenderArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<self.genderContentArray.count;i++)
    {
        
        if ([[self.genderTickArray objectAtIndex:i] isEqualToString:@"YES"])
        {
            if ([[self.genderContentArray objectAtIndex:i] isEqualToString:@"Male"])
            {
            [selectedGenderArray addObject:@"1"];
            }
            
            else
            {
               [selectedGenderArray addObject:@"0"];
            }
            
        }
        
        
    }
    
    return selectedGenderArray;
    
    
}

-(NSMutableArray*) getItemArray
{
    NSMutableArray *selectedItemArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<self.itemTypeContentArray.count;i++)
    {
        
        if ([[self.itemTypeTickArray objectAtIndex:i] isEqualToString:@"YES"])
        {
            [selectedItemArray addObject:[self.itemTypeContentArray objectAtIndex:i]];
            
        }
        
        
    }
    
    return selectedItemArray;
    
}

-(NSMutableArray*) getBrandsArray
{
  
    NSMutableArray *selectedBrandArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<self.brandNameArray.count;i++)
    {
        
        if ([[self.brandsTickArray objectAtIndex:i] isEqualToString:@"YES"])
        {
            [selectedBrandArray addObject:[self.brandNameArray objectAtIndex:i]];
            
        }
        
       
    }
    
    return selectedBrandArray;
    
}

-(NSMutableArray*) getStatusArray
{
    NSMutableArray *selectedStatusArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<self.statusContentArray.count;i++)
    {
        
        if ([[self.statusTickArray objectAtIndex:i] isEqualToString:@"YES"])
        {
            
            if ([[self.statusContentArray objectAtIndex:i] isEqualToString:@"New"])
            {
                [selectedStatusArray addObject:@"1"];
            }
            
            else
            {
                [selectedStatusArray addObject:@"0"];
            }

        }

        
    }
    
    return selectedStatusArray;
    
}

-(NSMutableDictionary *)getUserPostData{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(void)xibLoading
{
    NSString *nibName = @"SearchViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
    
    
}


@end
