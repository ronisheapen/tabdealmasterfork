//
//  HomeViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "HomeViewController.h"
#import "NewDealsViewController.h"
#import "BuyViewController.h"
#import "SellViewController.h"
#import "SearchViewController.h"
#import "AboutTabDealViewController.h"
#import "MyAccountViewController.h"
#import "FavouriteItemsViewController.h"
#import "PreviousOrdersViewController.h"
#import "SubmittedItemsViewController.h"
#import "NotificationsViewController.h"
#import "DeliveryScreenViewController.h"
#import "PaymentMethodViewController.h"
#import "SubmitViewController.h"
#import "SearchResultViewController.h"
#import "MyBankDetailsViewController.h"
#include "PreviousOrderDetailViewController.h"
#include "NewDealsDetailsViewController.h"
#import <FacebookSDK/FacebookSDK.h>

static HomeViewController *sharedObj = NULL;

@interface HomeViewController ()

@end

@implementation HomeViewController
{
    NSArray *moreMenuListNameArray;
    
    BOOL moreMenuItemSelectionFlag;
    NSUInteger moreItemSelectedIndex;
    NSUInteger tabSelectIndex;
}

-(void) viewWillAppear:(BOOL)animated
{
    NSLog(@"%@",ApplicationDelegate.language);
    moreMenuListNameArray = [[NSArray alloc] initWithObjects:localize(@"About Tabdeal"),localize(@"My Account"),localize(@"Favourite Items"),localize(@"Previous Orders"),localize(@"Submitted Items"),localize(@"Notifications"),localize(@"Log Out"), nil];
    
    self.homeButton.selected=YES;
    moreMenuItemSelectionFlag = NO;
    [self footerUITabArrangement];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.languageSelectionBttn.selected = YES;
        self.languageSelectionBttn.frame = CGRectMake(16, self.languageSelectionBttn.frame.origin.y, self.languageSelectionBttn.frame.size.width, self.languageSelectionBttn.frame.size.height);
        self.backArrowView.frame = CGRectMake(292, self.backArrowView.frame.origin.y, self.backArrowView.frame.size.width, self.backArrowView.frame.size.height);
        self.backBttn.frame = CGRectMake(222, self.backBttn.frame.origin.y, self.backBttn.frame.size.width, self.backBttn.frame.size.height);
        
        self.backArrowView.image = [UIImage imageNamed:@"rightarrow.png"];
    }
    else
    {
        self.languageSelectionBttn.selected = NO;
        self.languageSelectionBttn.frame = CGRectMake(284, self.languageSelectionBttn.frame.origin.y, self.languageSelectionBttn.frame.size.width, self.languageSelectionBttn.frame.size.height);
        self.backArrowView.frame = CGRectMake(9, self.backArrowView.frame.origin.y, self.backArrowView.frame.size.width, self.backArrowView.frame.size.height);
        self.backBttn.frame = CGRectMake(1, self.backBttn.frame.origin.y, self.backBttn.frame.size.width, self.backBttn.frame.size.height);
        
        self.backArrowView.image = [UIImage imageNamed:@"backarrow.png"];
    }
    
    [self registerUserWithUserIDandPushTocken];
    
    if (ApplicationDelegate.window.rootViewController)
    {
        ApplicationDelegate.window.rootViewController = nil;
    }
    
    ApplicationDelegate.window.rootViewController = ApplicationDelegate.mainNav;
    
    self.homeTabContainerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"HomePageBackground.png"]];
    
    self.homeButton.selected=YES;
    
    moreMenuListNameArray = [[NSArray alloc] initWithObjects:localize(@"About Tabdeal"),localize(@"My Account"),localize(@"Favourite Items"),localize(@"Previous Orders"),localize(@"Submitted Items"),localize(@"Notifications"),localize(@"Log Out"), nil];
    [self setUpInitialView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)homeTabButtonAction:(UIButton *)sender
{
    [self didSelectTabItemAtIndex:sender.tag];
}

-(void)showMoreMenu
{
    [self.homeTabContainerView setUserInteractionEnabled:NO];
    float xFact,moreMenuViewHeight;
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        moreMenuViewHeight = 295.0f;
    }
    else
    {
        moreMenuViewHeight = 392.0f;
    }
    
    float xPos = - self.moreMenuView.frame.size.width;
    
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        xPos = self.view.frame.size.width;
    }
    
    self.moreMenuView.frame = CGRectMake(xPos, (self.view.frame.size.height-moreMenuViewHeight-self.footerTabView.frame.size.height-1), self.moreMenuView.frame.size.width, moreMenuViewHeight);
    
    self.moreMenuView.frame = CGRectMake(xPos, (self.view.frame.size.height-moreMenuViewHeight-self.footerTabView.frame.size.height-1), self.moreMenuView.frame.size.width, moreMenuViewHeight);
    xFact=0;
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        xFact=self.view.frame.size.width-self.moreMenuView.frame.size.width;
    }
    
    self.moreMenuTable.delegate=self;
    self.moreMenuTable.dataSource=self;
    
    [self.view insertSubview:self.moreMenuView atIndex:10];
    
    [UIView beginAnimations:@"animateView" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect viewFrame = [self.moreMenuView frame];
    
    CGRect aViewFreme =  [self.moreMenuView frame];
    viewFrame.origin.x  = xFact;
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.moreMenuView.frame = aViewFreme;
    }
    self.moreMenuView.frame = viewFrame;
    
    [UIView commitAnimations];
    [self.moreMenuTable reloadData];
    
}
-(void)hideMoreMenu
{
    [self.homeTabContainerView setUserInteractionEnabled:YES];
    
    [UIView beginAnimations:@"animateView" context:nil];
    CGRect viewFrame = [self.moreMenuView frame];
    
    viewFrame.origin.x = - self.moreMenuView.frame.size.width;
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        viewFrame.origin.x  = self.view.frame.size.width;
    }
    
    [UIView setAnimationDuration:0.5];
    self.moreMenuView.frame = viewFrame;
    [UIView commitAnimations];
    [self performSelector:@selector(removeMoreMenu) withObject:nil afterDelay:0.5];
}
-(void)removeMoreMenu
{
    [self.moreMenuView removeFromSuperview];
}

-(void)didSelectTabItemAtIndex:(NSInteger)itemIndex
{
    [self resetTabSelection];
    
    if (itemIndex==5)
    {
        self.moreButton.selected=YES;
        if ([self.moreMenuView superview])
        {
            [self hideMoreMenu];
        }
        else
        {
            [self showMoreMenu];
        }
    }
    else
    {
        moreMenuItemSelectionFlag = NO;
        if ([self.moreMenuView superview])
        {
            [self hideMoreMenu];
        }
        
        switch (itemIndex)
        {
            case 1:
            {
                self.homeButton.selected = YES;
                [self loadHomeView];
            }
                break;
            case 2:
            {
                self.buyButton.selected = YES;
                [self loadBuyView];
            }
                break;
            case 3:
            {
                self.sellButton.selected = YES;
                [self loadSellView];
            }
                break;
            case 4:
            {
                self.searchButton.selected = YES;
                [self loadSearchView];
            }
                break;
            default:
                break;
        }
    }
}

-(void)resetTabSelection
{
    self.homeButton.selected=NO;
    self.searchButton.selected=NO;
    self.buyButton.selected=NO;
    self.sellButton.selected=NO;
    self.moreButton.selected=NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return moreMenuListNameArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        return 42;
    }
    else
    {
        return 48;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"more_divider.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        UIImageView *iconImgView;
        UILabel *nameLabel;
        
        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            iconImgView= [[UIImageView alloc] initWithFrame:CGRectMake(12, 9, 26, 26)];
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(48, 9, (tableView.frame.size.width-58), 26)];
        }
        else
        {
            iconImgView= [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 38, 38)];
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(56, 8, (tableView.frame.size.width-63), 38)];
        }
        
        iconImgView.backgroundColor = [UIColor clearColor];
        iconImgView.tag=50;
        nameLabel.tag=60;
        [cell addSubview:iconImgView];
        [cell addSubview:nameLabel];
    }
    
    UILabel* cellLabel = (UILabel*)[cell viewWithTag: 60];
    [cellLabel setBackgroundColor:[UIColor clearColor]];
    cellLabel.text = [self getMoreTabItemTitle:indexPath.row];
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        float xLabelPos = 0.0f;
        cellLabel.textAlignment = NSTextAlignmentLeft;
        
        cellLabel.frame = CGRectMake(xLabelPos, 9, (tableView.frame.size.width-58), 26);
    }
    else
    {
        float xLabelPos = 0.0f;
        cellLabel.textAlignment = NSTextAlignmentLeft;
        cellLabel.frame = CGRectMake(xLabelPos, 12, (tableView.frame.size.width-60), 30);
    }
    
    if (moreMenuItemSelectionFlag &&(moreItemSelectedIndex==indexPath.row))
    {
        cellLabel.textColor=[UIColor whiteColor];
    }
    else
    {
        cellLabel.textColor=[UIColor colorWithHex:kUNIVERSAL_THEME_COLOR];
        [cellLabel setFont:EBRI_NORMAL_FONT(20)];
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    moreItemSelectedIndex=indexPath.row;
    moreMenuItemSelectionFlag = YES;
    
    [self selectMoreMenuItem:indexPath.row];
    [self.moreMenuTable reloadData];
}

-(NSString *)getMoreTabItemTitle:(NSUInteger)itemIndex
{
    NSString *titleString=@"";
    titleString = [moreMenuListNameArray objectAtIndex:itemIndex];
    return localize(titleString);
}

-(void)selectMoreMenuItem:(NSInteger)index
{
    [self hideMoreMenu];
    
    switch (index)
    {
        case 0:
        {
            [self loadaboutView];
        }
            break;
        case 1:
        {
            [self loadmyAccountView];
        }
            break;
        case 2:
        {
            [self loadFavouriteView];
        }
            break;
        case 3:
        {
            [self loadPreviousOrdersView];
        }
            break;
        case 4:
        {
            [self loadSubmittedItemsView];
        }
            break;
            
        case 5:
        {
            [self loadNotificationsView];
        }
            break;
        case 6:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:localize(@"Do you want to log out?")
                                                           delegate:self
                                                  cancelButtonTitle:localize(@"No")
                                                  otherButtonTitles:localize(@"Yes"), nil];
            
            [alert show];
        }
            break;
            
        default:
            break;
    }
}

+(HomeViewController*)sharedViewController
{
    if ( !sharedObj || sharedObj == NULL )
    {
        NSString *nibName = @"HomeViewController";
        sharedObj = [[HomeViewController alloc] initWithNibName:nibName bundle:nil];
    }
    return sharedObj;
}


-(void)setUpInitialView
{
    for (UIView *vw in self.homeTabContainerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    NewDealsViewController *newDealsDetailVc = [[NewDealsViewController alloc] initWithNibName:@"NewDealsViewController" bundle:nil];
    newDealsDetailVc.view.backgroundColor = [UIColor clearColor];
    
    ApplicationDelegate.homeTabNav=[[UINavigationController alloc] initWithRootViewController:newDealsDetailVc];
    
    ApplicationDelegate.homeTabNav.navigationBar.translucent = NO;
    ApplicationDelegate.homeTabNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.homeTabNav.navigationBarHidden=YES;
    
    ApplicationDelegate.homeTabNav.view.frame=CGRectMake(0, 0, self.homeTabContainerView.frame.size.width, self.homeTabContainerView.frame.size.height);
    
    [self.homeTabContainerView addSubview:ApplicationDelegate.homeTabNav.view];
}


-(void)loadHomeView
{
    NewDealsViewController *dealsVc = [[NewDealsViewController alloc] initWithNibName:@"NewDealsViewController" bundle:nil];
    dealsVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[dealsVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:dealsVc animated:NO];
    }
    
}

-(void)loadBuyView
{
    BuyViewController *buyVc = [[BuyViewController alloc] initWithNibName:@"BuyViewController" bundle:nil];
    buyVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[buyVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:buyVc animated:NO];
    }
}

-(void)loadSellView
{
    SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
    sellVc.view.backgroundColor = [UIColor clearColor];
    sellVc.contextString = @"initialization";
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:@"initialization" forKey:@"trackingContext"];
    [actionTracking setObject:@"" forKey:@"refferenceString"];
     [actionTracking synchronize];
    sellVc.refferenceString = @"";
    [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
    {
        
    }
}

-(void)loadSearchView
{
    SearchViewController *searchVc = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    searchVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[searchVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:searchVc animated:NO];
    }
}

-(void) loadaboutView
{
    AboutTabDealViewController *aboutVc = [[AboutTabDealViewController alloc] initWithNibName:@"AboutTabDealViewController" bundle:nil];
    aboutVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[aboutVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:aboutVc animated:NO];
    }
}

-(void) loadmyAccountView
{
    MyAccountViewController *myaccountVc = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
    myaccountVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[myaccountVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:myaccountVc animated:NO];
    }
}

-(void) loadFavouriteView
{
    FavouriteItemsViewController *FavViewVc = [[FavouriteItemsViewController alloc] initWithNibName:@"FavouriteItemsViewController" bundle:nil];
    FavViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[FavViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:FavViewVc animated:NO];
    }
}

-(void) loadPreviousOrdersView
{
    PreviousOrdersViewController *PreviousViewVc = [[PreviousOrdersViewController alloc] initWithNibName:@"PreviousOrdersViewController" bundle:nil];
    PreviousViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:PreviousViewVc animated:NO];
    }
}

-(void) loadSubmittedItemsView
{
    SubmittedItemsViewController *SubmittedViewVc = [[SubmittedItemsViewController alloc] initWithNibName:@"SubmittedItemsViewController" bundle:nil];
    SubmittedViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SubmittedViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:SubmittedViewVc animated:NO];
    }
}

-(void) loadNotificationsView
{
    NotificationsViewController *NotificationViewVc = [[NotificationsViewController alloc] initWithNibName:@"NotificationsViewController" bundle:nil];
    NotificationViewVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NotificationViewVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:NotificationViewVc animated:NO];
    }
}

-(void) loadLogOutView
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [prefs setObject:@"logout" forKey:@"loginflag"];
    
    [prefs synchronize];
    
    [[Twitter sharedInstance] logOut];
    [FBSession.activeSession closeAndClearTokenInformation];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    [self loadHomeView];
    
    [self loadloginView];
}

-(void) loadloginView
{
    LoginViewController *loginVc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [self presentViewController:loginVc animated:NO completion:nil];
}

- (IBAction)backBttnActn:(UIButton *)sender
{
    if (ApplicationDelegate.innerViewCount.intValue>=1)
    {
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
    }
}

-(void)globalLabel:(NSString *)cartcount
{
    NSString *CartLabel= [NSString stringWithFormat:@"(%@)",cartcount];
    
    if([cartcount intValue]>0)
    {
        self.cartBadge.text = CartLabel;
    }
    else
    {
        self.cartBadge.text = @"";
    }
}

- (IBAction)changeLanguage:(id)sender
{
    ApplicationDelegate.changeLanguage;
    
    NewDealsViewController *dealsVc = [[NewDealsViewController alloc] initWithNibName:@"NewDealsViewController" bundle:nil];
    dealsVc.view.backgroundColor = [UIColor clearColor];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[dealsVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:dealsVc animated:NO];
    }
    else
    {
        
    }
}


-(void)footerUITabArrangement
{
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.homeButton.frame = CGRectMake(19, self.homeButton.frame.origin.y, self.homeButton.frame.size.width, self.homeButton.frame.size.height);
        self.buyButton.frame = CGRectMake(79, self.buyButton.frame.origin.y, self.buyButton.frame.size.width, self.buyButton.frame.size.height);
        self.sellButton.frame = CGRectMake(141, self.sellButton.frame.origin.y, self.sellButton.frame.size.width, self.sellButton.frame.size.height);
        self.searchButton.frame = CGRectMake(206, self.searchButton.frame.origin.y, self.searchButton.frame.size.width, self.searchButton.frame.size.height);
        self.moreButton.frame = CGRectMake(263, self.moreButton.frame.origin.y, self.moreButton.frame.size.width, self.moreButton.frame.size.height);
        self.cartBadge.frame=CGRectMake(120, self.cartBadge.frame.origin.y, self.cartBadge.frame.size.width, self.cartBadge.frame.size.height);
    }
    else
    {
        self.homeButton.frame = CGRectMake(263, self.homeButton.frame.origin.y, self.homeButton.frame.size.width, self.homeButton.frame.size.height);
        self.buyButton.frame = CGRectMake(206, self.buyButton.frame.origin.y, self.buyButton.frame.size.width, self.buyButton.frame.size.height);
        self.sellButton.frame = CGRectMake(141, self.sellButton.frame.origin.y, self.sellButton.frame.size.width, self.sellButton.frame.size.height);
        self.searchButton.frame = CGRectMake(79, self.searchButton.frame.origin.y, self.searchButton.frame.size.width, self.searchButton.frame.size.height);
        self.moreButton.frame = CGRectMake(19, self.moreButton.frame.origin.y, self.moreButton.frame.size.width, self.moreButton.frame.size.height);
        self.cartBadge.frame=CGRectMake(194, self.cartBadge.frame.origin.y, self.cartBadge.frame.size.width, self.cartBadge.frame.size.height);
    }
}

- (IBAction)languageSelectionBttnActn:(UIButton *)sender
{
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.languageSelectionBttn.selected = NO;
        self.languageSelectionBttn.frame = CGRectMake(284, self.languageSelectionBttn.frame.origin.y, self.languageSelectionBttn.frame.size.width, self.languageSelectionBttn.frame.size.height);
        self.backArrowView.frame = CGRectMake(9, self.backArrowView.frame.origin.y, self.backArrowView.frame.size.width, self.backArrowView.frame.size.height);
        self.backBttn.frame = CGRectMake(1, self.backBttn.frame.origin.y, self.backBttn.frame.size.width, self.backBttn.frame.size.height);
        
        self.backArrowView.image = [UIImage imageNamed:@"backarrow.png"];
    }
    else{
        self.languageSelectionBttn.selected = YES;
        self.languageSelectionBttn.frame = CGRectMake(16, self.languageSelectionBttn.frame.origin.y, self.languageSelectionBttn.frame.size.width, self.languageSelectionBttn.frame.size.height);
        self.backArrowView.frame = CGRectMake(292, self.backArrowView.frame.origin.y, self.backArrowView.frame.size.width, self.backArrowView.frame.size.height);
        self.backBttn.frame = CGRectMake(222, self.backBttn.frame.origin.y, self.backBttn.frame.size.width, self.backBttn.frame.size.height);
        
        self.backArrowView.image = [UIImage imageNamed:@"rightarrow.png"];
    }

    ApplicationDelegate.changeLanguage;
    
    [self footerUITabArrangement];
    
    NewDealsViewController *dealsVc = [[NewDealsViewController alloc] initWithNibName:@"NewDealsViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[dealsVc class]])
    {
        dealsVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav pushViewController:dealsVc animated:NO];
    }
    
    BuyViewController *buyVc = [[BuyViewController alloc] initWithNibName:@"BuyViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[buyVc class]])
    {
        buyVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav pushViewController:buyVc animated:NO];
    }
    
    SearchViewController *searchVc = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[searchVc class]])
    {
        searchVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav pushViewController:searchVc animated:NO];
    }
    
    AboutTabDealViewController *aboutVc = [[AboutTabDealViewController alloc] initWithNibName:@"AboutTabDealViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[aboutVc class]])
    {
        aboutVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:aboutVc animated:NO];
    }
    
    MyAccountViewController *myaccountVc = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[myaccountVc class]])
    {
        myaccountVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:myaccountVc animated:NO];
    }
    
    FavouriteItemsViewController *FavViewVc = [[FavouriteItemsViewController alloc] initWithNibName:@"FavouriteItemsViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[FavViewVc class]])
    {
        FavViewVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:FavViewVc animated:NO];
    }
    
    PreviousOrdersViewController *PreviousViewVc = [[PreviousOrdersViewController alloc] initWithNibName:@"PreviousOrdersViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousViewVc class]])
    {
        PreviousViewVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:PreviousViewVc animated:NO];
    }
    
    SubmittedItemsViewController *SubmittedViewVc = [[SubmittedItemsViewController alloc] initWithNibName:@"SubmittedItemsViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SubmittedViewVc class]])
    {
        SubmittedViewVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:SubmittedViewVc animated:NO];
    }
    
    NotificationsViewController *NotificationViewVc = [[NotificationsViewController alloc] initWithNibName:@"NotificationsViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NotificationViewVc class]])
    {
        NotificationViewVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:NotificationViewVc animated:NO];
    }
    
    NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
    
    newDealsDetailVC.Lbl.text = @"";
    actionTracking = [NSUserDefaults standardUserDefaults];
    newDealsDetailVC.productId = [actionTracking objectForKey:@"TrackId"];
    [actionTracking synchronize];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[NewDealsDetailsViewController class]])
    {
        newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
    }
    
    DeliveryScreenViewController * deliveryScreenVC = [[DeliveryScreenViewController alloc]initWithNibName:@"DeliveryScreenViewController" bundle:nil];
    
    actionTracking = [NSUserDefaults standardUserDefaults];
    deliveryScreenVC.SubTotal.text=[actionTracking objectForKey:@"SubTotal"];
    deliveryScreenVC.DeliveryCharge.text=[actionTracking objectForKey:@"DeliveryCharge"];
    deliveryScreenVC.TotalCharge.text=[actionTracking objectForKey:@"TotalCharge"];
    deliveryScreenVC.orderId = [actionTracking objectForKey:@"orderId"];
    NSLog(@"%@",[actionTracking objectForKey:@"SubTotal"]);
    NSLog(@"%@",[actionTracking objectForKey:@"DeliveryCharge"]);
    NSLog(@"%@",[actionTracking objectForKey:@"TotalCharge"]);
    NSLog(@"%@",[actionTracking objectForKey:@"orderId"]);
    [actionTracking synchronize];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[DeliveryScreenViewController class]])
    {
        deliveryScreenVC.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:deliveryScreenVC animated:NO];
    }
    
    PaymentMethodViewController * paymentScreenVC = [[PaymentMethodViewController alloc]initWithNibName:@"PaymentMethodViewController" bundle:nil];
    paymentScreenVC.view.backgroundColor = [UIColor clearColor];
    actionTracking = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@",[actionTracking objectForKey:@"SubTotal"]);
    NSLog(@"%@",[actionTracking objectForKey:@"DeliveryCharge"]);
    NSLog(@"%@",[actionTracking objectForKey:@"TotalCharge"]);
    NSLog(@"%@",[actionTracking objectForKey:@"orderId"]);
    paymentScreenVC.SubTotal.text=[actionTracking objectForKey:@"SubTotal"];
    paymentScreenVC.DeliveryCharge.text=[actionTracking objectForKey:@"DeliveryCharge"];
    paymentScreenVC.TotalCharge.text=[actionTracking objectForKey:@"TotalCharge"];
    paymentScreenVC.orderId = [actionTracking objectForKey:@"orderId"];
    paymentScreenVC.cashOnDelFlag = [actionTracking objectForKey:@"cashOnDeliveryFlag"];
    [actionTracking synchronize];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PaymentMethodViewController class]])
    {
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:paymentScreenVC animated:NO];
    }
    
    SellViewController *sellVc = [[SellViewController alloc] initWithNibName:@"SellViewController" bundle:nil];
    
    actionTracking = [NSUserDefaults standardUserDefaults];
    sellVc.contextString = [actionTracking objectForKey:@"trackingContext"];
    sellVc.refferenceString = [actionTracking objectForKey:@"refferenceString"];
    [actionTracking synchronize];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[sellVc class]])
    {
        sellVc.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:sellVc animated:NO];
    }
    
    SubmitViewController * submitScreenVC = [[SubmitViewController alloc]initWithNibName:@"SubmitViewController" bundle:nil];
    
    actionTracking = [NSUserDefaults standardUserDefaults];
    submitScreenVC.contextString=[actionTracking objectForKey:@"contextStringforSubmission"];
    submitScreenVC.SellPageData=[[NSMutableDictionary alloc] init];
    submitScreenVC.SellPageData = [actionTracking objectForKey:@"SellData"];
    submitScreenVC.civilIdStr = [actionTracking objectForKey:@"civilId"];
    [actionTracking synchronize];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SubmitViewController class]])
    {
        submitScreenVC.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:submitScreenVC animated:NO];
    }
    
    SearchResultViewController * searchResultVC = [[SearchResultViewController alloc]initWithNibName:@"SearchResultViewController" bundle:nil];
    
    searchResultVC.searchResultArray=[[NSMutableArray alloc] init];
    
    actionTracking = [NSUserDefaults standardUserDefaults];
    searchResultVC.searchResultArray = [actionTracking objectForKey:@"searchArray"];
    searchResultVC.searchDictionary = [actionTracking objectForKey:@"searchDictn"];
    
    [actionTracking synchronize];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SearchResultViewController class]])
    {
        searchResultVC.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:searchResultVC animated:NO];
    }
    
    MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
    {
        bankDetailsVC.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
    }

    PreviousOrderDetailViewController * orderDetailsVC = [[PreviousOrderDetailViewController alloc]initWithNibName:@"PreviousOrderDetailViewController" bundle:nil];
    
    actionTracking = [NSUserDefaults standardUserDefaults];
    orderDetailsVC.orderId = [actionTracking objectForKey:@"userPreOrder"];
    
    [actionTracking synchronize];
    
    if ([ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousOrderDetailViewController class]])
    {
        orderDetailsVC.view.backgroundColor = [UIColor clearColor];
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
        [ApplicationDelegate.homeTabNav pushViewController:orderDetailsVC animated:NO];
    }
}

-(void) registerUserWithUserIDandPushTocken
{
    NSMutableDictionary *tokenDic = [[NSMutableDictionary alloc] init];
    [tokenDic setObject:@"1" forKey:@"Platform"];
    [tokenDic setObject:@"" forKey:@"DeviceId"];
    
    if([getuser length] == 0)
    {
        [tokenDic setObject:@"" forKey:@"UserId"];
    }
    else
    {
        [tokenDic setObject:getuser forKey:@"UserId"];
    }
    if([getpushtoken length] == 0)
    {
        [tokenDic setObject:@"" forKey:@"DeviceToken"];
    }
    else
    {
        [tokenDic setObject:getpushtoken forKey:@"DeviceToken"];
    }
    
    [ApplicationDelegate.engine getPushNotification:tokenDic CompletionHandler:^(NSMutableArray *responseArray)
     {
         if ([ApplicationDelegate isValid:responseArray])
         {
             
         }
     }
                                       errorHandler:^(NSError *error)
     {
         
         
     }];
}


- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex])
    {
    
    }
    else
    {
        [self loadLogOutView];
    }
}

@end
