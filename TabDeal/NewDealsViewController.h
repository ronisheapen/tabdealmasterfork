//
//  NewDealsViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewDealsCell.h"

@interface NewDealsViewController : UIViewController<NewDealItemDelegate>
{
    NSUserDefaults * actionTracking;
}
@property (strong,nonatomic)NSMutableArray * DealsMainAry;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UILabel *headdingLbl;
@end
