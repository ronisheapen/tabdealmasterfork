//
//  SearchResultData.h
//  TabDeal
//
//  Created by Sreeraj VR on 03/11/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchResultData : NSObject
@property(nonatomic,retain)NSString *Brandname;
@property(nonatomic,retain)NSString *DealId;
@property(nonatomic,retain)NSString *Image;
@property(nonatomic,retain)NSString *ItemName;
@property(nonatomic, retain)NSNumber * isOnSale;
@end
