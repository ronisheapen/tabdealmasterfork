//
//  ObjectMapper.m
//  TabDeal
//
//  Created by Sreeraj VR on 25/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "ObjectMapper.h"

@implementation ObjectMapper

-(BankInformation *)getBankinformationFromDictionary:(NSMutableDictionary *)userDictionary
{
    BankInformation *bnk = [[BankInformation alloc] init];
    
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"BankAddress"]])
    {
        bnk.bankAddress = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"BankAddress"]];
    }
    else
    {
        bnk.bankAddress = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"BankName"]])
    {
        bnk.bankName = [userDictionary objectForKey:@"BankName"];
    }
    else
    {
        bnk.bankName = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Country"]])
    {
        bnk.country = [userDictionary objectForKey:@"Country"];
    }
    else
    {
        bnk.country = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"FullName"]])
    {
        bnk.fullName = [userDictionary objectForKey:@"FullName"];
    }
    else
    {
        bnk.fullName = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"IBAN"]])
    {
        bnk.iBAN = [userDictionary objectForKey:@"IBAN"];
    }
    else
    {
        bnk.iBAN = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"SwiftCode"]])
    {
        bnk.swiftCode = [userDictionary objectForKey:@"SwiftCode"];
    }
    else
    {
        bnk.swiftCode = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"UserId"]])
    {
        bnk.userId = [userDictionary objectForKey:@"UserId"];
    }
    else
    {
        bnk.userId = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"lang_key"]])
    {
        bnk.lang_key = [userDictionary objectForKey:@"lang_key"];
    }
    else
    {
        bnk.lang_key = @"";
    }
    
    return bnk;
}

-(Userinformation *)getUserinformationFromDictionary:(NSMutableDictionary *)userDictionary
{
    Userinformation *usr = [[Userinformation alloc] init];
    
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"AddressLine1"]])
    {
        usr.addressLine1 = [NSString stringWithFormat:@"%@",[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"AddressLine1"]];
    }
    else
    {
        usr.addressLine1 = @"";
    }
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"AddressLine2"]])
    {
        usr.addressLine2 = [[userDictionary objectForKey:@"Dinfo"] objectForKey:@"AddressLine2"];
    }
    else
    {
        usr.addressLine2 = @"";
    }
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"City"]])
    {
        usr.city = [[userDictionary objectForKey:@"Dinfo"] objectForKey:@"City"];
    }
    else
    {
        usr.city = @"";
    }
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"Country"]])
    {
        usr.country = [[userDictionary objectForKey:@"Dinfo"] objectForKey:@"Country"];
    }
    else
    {
        usr.country = @"";
    }
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"MobCode"]])
    {
        usr.mobCode = [[userDictionary objectForKey:@"Dinfo"] objectForKey:@"MobCode"];
    }
    else
    {
        usr.mobCode = @"";
    }
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"MobileNumber"]])
    {
        usr.mobileNumber = [[userDictionary objectForKey:@"Dinfo"] objectForKey:@"MobileNumber"];
    }
    else
    {
        usr.mobileNumber = @"";
    }
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"PostalCode"]])
    {
        usr.postalCode = [[userDictionary objectForKey:@"Dinfo"] objectForKey:@"PostalCode"];
    }
    else
    {
        usr.postalCode = @"";
    }
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Dinfo"] objectForKey:@"RecieverName"]])
    {
        usr.recieverName = [[userDictionary objectForKey:@"Dinfo"] objectForKey:@"RecieverName"];
    }
    else
    {
        usr.recieverName = @"";
    }
    
    
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Pinfo"] objectForKey:@"Email"]])
    {
        usr.email = [[userDictionary objectForKey:@"Pinfo"] objectForKey:@"Email"];
    }
    else
    {
        usr.email = @"";
    }
    
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Pinfo"] objectForKey:@"FirstName"]])
    {
        usr.firstName = [[userDictionary objectForKey:@"Pinfo"] objectForKey:@"FirstName"];
    }
    else
    {
        usr.firstName = @"";
    }
    
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Pinfo"] objectForKey:@"LastName"]])
    {
        usr.lastName = [[userDictionary objectForKey:@"Pinfo"] objectForKey:@"LastName"];
    }
    else
    {
        usr.lastName = @"";
    }
    
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Pinfo"] objectForKey:@"MobCode"]])
    {
        usr.personalMobCode = [[userDictionary objectForKey:@"Pinfo"] objectForKey:@"MobCode"];
    }
    else
    {
        usr.personalMobCode = @"";
    }
    
    if ([ApplicationDelegate isValid:[[userDictionary objectForKey:@"Pinfo"] objectForKey:@"MobileNumber"]])
    {
        usr.personalMobileNumber = [[userDictionary objectForKey:@"Pinfo"] objectForKey:@"MobileNumber"];
    }
    else
    {
        usr.personalMobileNumber = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"IsSeller"]])
    {
        if ([[userDictionary objectForKey:@"IsSeller"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            usr.isSeller = [NSNumber numberWithInt:1];
        }
        else{
            usr.isSeller = [NSNumber numberWithInt:0];
        }
    }
    else
    {
        usr.isSeller = [NSNumber numberWithInt:0];
    }
    return usr;
}

-(User *)getUserObjectFromDictionary:(NSMutableDictionary *)userDictionary
{
    User *usr = [[User alloc] init];
    
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Email"]])
    {
        usr.email = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"Email"]];
    }
    else
    {
        usr.email = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"FirstName"]])
    {
        usr.firstname = [userDictionary objectForKey:@"FirstName"];
    }
    else
    {
        usr.firstname = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"LanguageKey"]])
    {
        usr.languagekey = [userDictionary objectForKey:@"LanguageKey"];
    }
    else
    {
        usr.languagekey = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"LastName"]])
    {
        usr.lastName = [userDictionary objectForKey:@"LastName"];
    }
    else
    {
        usr.lastName = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"MobileNumber"]])
    {
        usr.mobilenumber = [userDictionary objectForKey:@"MobileNumber"];
    }
    else
    {
        usr.mobilenumber = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"UserId"]])
    {
        usr.userId = [userDictionary objectForKey:@"UserId"];
    }
    else
    {
        usr.userId = @"";
    }
    
    return usr;
}


-(UserBasketData *)getUserBasketFromDictionary:(NSMutableDictionary *)userDictionary
{
    UserBasketData *usrbsketDta = [[UserBasketData alloc] init];
    
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Brand"]])
    {
        usrbsketDta.brand = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"Brand"]];
    }
    else
    {
        usrbsketDta.brand = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Deal_id"]])
    {
        usrbsketDta.dealId = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"Deal_id"]];
    }
    else
    {
        usrbsketDta.dealId = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Image"]])
    {
        usrbsketDta.image = [userDictionary objectForKey:@"Image"];
    }
    else
    {
        usrbsketDta.image = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Name"]])
    {
        usrbsketDta.name = [userDictionary objectForKey:@"Name"];
    }
    else
    {
        usrbsketDta.name = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Quantity"]])
    {
        usrbsketDta.quantity = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"Quantity"]];
    }
    else
    {
        usrbsketDta.quantity = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Rate"]])
    {
        usrbsketDta.rate = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"Rate"]];
    }
    else
    {
        usrbsketDta.rate = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"Stock"]])
    {
        usrbsketDta.stock = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"Stock"]];
    }
    else
    {
        usrbsketDta.stock = @"";
    }
    if ([ApplicationDelegate isValid:[userDictionary objectForKey:@"size"]])
    {
        usrbsketDta.size = [NSString stringWithFormat:@"%@",[userDictionary objectForKey:@"size"]];
    }
    else
    {
        usrbsketDta.size = @"";
    }
    
    return usrbsketDta;
}

-(UserFavouriteData *)getUserFavouriteFromDictionary:(NSMutableDictionary *)userFavouriteDictionary
{
    
    UserFavouriteData  * FavObj = [[UserFavouriteData alloc] init];
    
    if ([ApplicationDelegate isValid:[userFavouriteDictionary objectForKey:@"Brand"]])
    {
        FavObj.brand = [NSString stringWithFormat:@"%@",[userFavouriteDictionary objectForKey:@"Brand"]];
    }
    else
    {
        FavObj.brand = @"";
    }
    
    if ([ApplicationDelegate isValid:[userFavouriteDictionary objectForKey:@"Image"]])
    {
        FavObj.image = [NSString stringWithFormat:@"%@",[userFavouriteDictionary objectForKey:@"Image"]];
    }
    else
    {
        FavObj.image = @"";
    }
    
    if ([ApplicationDelegate isValid:[userFavouriteDictionary objectForKey:@"Name"]])
    {
        FavObj.name = [NSString stringWithFormat:@"%@",[userFavouriteDictionary objectForKey:@"Name"]];
    }
    else
    {
        FavObj.name = @"";
    }
    
    if ([ApplicationDelegate isValid:[userFavouriteDictionary objectForKey:@"Deal_id"]])
    {
        FavObj.Dealid = [NSString stringWithFormat:@"%@",[userFavouriteDictionary objectForKey:@"Deal_id"]];
    }
    else
    {
        FavObj.Dealid = @"";
    }
    
    if ([ApplicationDelegate isValid:[userFavouriteDictionary objectForKey:@"size"]])
    {
        FavObj.size = [NSString stringWithFormat:@"%@",[userFavouriteDictionary objectForKey:@"size"]];
    }
    else
    {
        FavObj.size = @"0";
    }
    
    
    return FavObj;
}

-(OrderDetailsList *)getUserOrderListDetailsFromDictionary:(NSMutableDictionary *)userOrderListDetailsDictionary
{
    
    OrderDetailsList  * OrderListObj = [[OrderDetailsList alloc] init];
    
    if ([ApplicationDelegate isValid:[userOrderListDetailsDictionary objectForKey:@"Brand"]])
    {
        OrderListObj.Brand = [NSString stringWithFormat:@"%@",[userOrderListDetailsDictionary objectForKey:@"Brand"]];
    }
    else
    {
       OrderListObj.Brand = @"";
    }
    
    if ([ApplicationDelegate isValid:[userOrderListDetailsDictionary objectForKey:@"DealId"]])
    {
        OrderListObj.DealId = [NSString stringWithFormat:@"%@",[userOrderListDetailsDictionary objectForKey:@"DealId"]];
    }
    else
    {
        OrderListObj.DealId = @"";
    }
    
    if ([ApplicationDelegate isValid:[userOrderListDetailsDictionary objectForKey:@"Image"]])
    {
        OrderListObj.Image = [NSString stringWithFormat:@"%@",[userOrderListDetailsDictionary objectForKey:@"Image"]];
    }
    else
    {
        OrderListObj.Image = @"";
    }
    
    if ([ApplicationDelegate isValid:[userOrderListDetailsDictionary objectForKey:@"Name"]])
    {
        OrderListObj.Name = [NSString stringWithFormat:@"%@",[userOrderListDetailsDictionary objectForKey:@"Name"]];
    }
    else
    {
        OrderListObj.Name = @"";
    }
    
    if ([ApplicationDelegate isValid:[userOrderListDetailsDictionary objectForKey:@"Quantity"]])
    {
        OrderListObj.Quantity = [NSString stringWithFormat:@"%@",[userOrderListDetailsDictionary objectForKey:@"Quantity"]];
    }
    else
    {
         OrderListObj.Quantity = @"";
    }
    
    if ([ApplicationDelegate isValid:[userOrderListDetailsDictionary objectForKey:@"Rate"]])
    {
        OrderListObj.Rate = [NSString stringWithFormat:@"%@",[userOrderListDetailsDictionary objectForKey:@"Rate"]];
    }
    else
    {
        OrderListObj.Rate = @"";
    }
    
    
    return OrderListObj;
    
}




-(UserPreviousOrder *)getUserPreviousOrderFromDictionary:(NSMutableDictionary *)userPreOrderDictionary
{
    
    UserPreviousOrder  * PreOrderObj = [[UserPreviousOrder alloc] init];
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"Date"]])
    {
        PreOrderObj.date = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"Date"]];
    }
    else
    {
        PreOrderObj.date = @"";
    }
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"Image"]])
    {
        PreOrderObj.image = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"Image"]];
    }
    else
    {
        PreOrderObj.image = @"";
    }
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"OrderNumber"]])
    {
        PreOrderObj.ordernumber = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"OrderNumber"]];
    }
    else
    {
        PreOrderObj.ordernumber = @"";
    }

    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"Status"]])
    {
        PreOrderObj.status = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"Status"]];
    }
    else
    {
        PreOrderObj.status = @"";
    }
 
    
    return PreOrderObj;
    
}

-(AboutTabDealData *) getAboutUsPageDetailFromDictionary:(NSMutableDictionary *)AboutpageDetailDictionary
{
    AboutTabDealData  * aboutUsObj = [[AboutTabDealData alloc] init];
    if(![ApplicationDelegate isValid:[AboutpageDetailDictionary objectForKey:@"about_tabdel"]] )
    {
        aboutUsObj.aboutUsPageData = @"";
    }
    else
    {
        aboutUsObj.aboutUsPageData = [AboutpageDetailDictionary objectForKey:@"about_tabdel"];
    }
    
    return aboutUsObj;
}

-(SearchResultData *)getSearchResultsFromDictionary:(NSMutableDictionary *)searchResultsDictionary
{
   SearchResultData  * searchResultObj = [[SearchResultData alloc] init];
    
    if ([ApplicationDelegate isValid:[searchResultsDictionary objectForKey:@"BrandName"]])
    {
        searchResultObj.Brandname = [NSString stringWithFormat:@"%@",[searchResultsDictionary objectForKey:@"BrandName"]];
    }
    else
    {
        searchResultObj.Brandname = @"";
    }
    
    if ([ApplicationDelegate isValid:[searchResultsDictionary objectForKey:@"Deal_id"]])
    {
        searchResultObj.DealId = [NSString stringWithFormat:@"%@",[searchResultsDictionary objectForKey:@"Deal_id"]];
    }
    else
    {
        searchResultObj.DealId = @"";
    }
    
    if ([ApplicationDelegate isValid:[searchResultsDictionary objectForKey:@"Image"]])
    {
        searchResultObj.Image = [NSString stringWithFormat:@"%@",[searchResultsDictionary objectForKey:@"Image"]];
    }
    else
    {
        searchResultObj.Image = @"";
    }
    
    if ([ApplicationDelegate isValid:[searchResultsDictionary objectForKey:@"ItemName"]])
    {
        searchResultObj.ItemName = [NSString stringWithFormat:@"%@",[searchResultsDictionary objectForKey:@"ItemName"]];
    }
    else
    {
        searchResultObj.ItemName = @"";
    }
    
    if ([ApplicationDelegate isValid:[searchResultsDictionary objectForKey:@"IsOnSale"]])
    {
        if ([[searchResultsDictionary objectForKey:@"IsOnSale"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            searchResultObj.isOnSale = [NSNumber numberWithInt:1];
        }
        else{
            searchResultObj.isOnSale = [NSNumber numberWithInt:0];
        }
    }
    else
    {
        searchResultObj.isOnSale = [NSNumber numberWithInt:0];
    }
    
    return searchResultObj;
    
}


-(SubmittedData *)getSubmittedResultsFromDictionary:(NSMutableDictionary *)submittedDictionary
{
    
    SubmittedData  * submittedObj = [[SubmittedData alloc] init];
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"BrandName"]])
    {
        submittedObj.BrandName = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"BrandName"]];
    }
    else
    {
        submittedObj.BrandName = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Date"]])
    {
        submittedObj.Date = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Date"]];
    }
    else
    {
        submittedObj.Date = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Image"]])
    {
        submittedObj.Image = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Image"]];
    }
    else
    {
        submittedObj.Image = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"RefferenceNumber"]])
    {
        submittedObj.RefferenceNumber = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"RefferenceNumber"]];
    }
    else
    {
        submittedObj.RefferenceNumber = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Status"]])
    {
        submittedObj.Status = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Status"]];
    }
    else
    {
        submittedObj.Status = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Type"]])
    {
        submittedObj.Type = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Type"]];
    }
    else
    {
         submittedObj.Type = @"";
    }
    
    
    return submittedObj;
    
}

-(UserSellInformation *)getSellInformationFromDictionary:(NSMutableDictionary *)submittedDictionary
{
    
    UserSellInformation  * sellObj = [[UserSellInformation alloc] init];
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"AuthImages"]])
    {
        sellObj.authImages = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"AuthImages"]];
    }
    else
    {
        sellObj.authImages = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"BrandId"]])
    {
        sellObj.brandId = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"BrandId"]];
    }
    else
    {
        sellObj.brandId = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Country"]])
    {
        sellObj.country = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Country"]];
    }
    else
    {
        sellObj.country = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Currency_code"]])
    {
        sellObj.currency_code = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Currency_code"]];
    }
    else
    {
        sellObj.currency_code = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"IdProof"]])
    {
        sellObj.idProof = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"IdProof"]];
    }
    else
    {
        sellObj.idProof = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"ProductImageCode"]])
    {
//        sellObj.productImageCode = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"ProductImageCode"]];
        sellObj.productImageCode = [[NSMutableArray alloc]init];
        sellObj.productImageCode = [submittedDictionary objectForKey:@"ProductImageCode"];
    }
    else
    {
        sellObj.productImageCode = [[NSMutableArray alloc]init];
    }
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Rate"]])
    {
        sellObj.rate = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Rate"]];
    }
    else
    {
        sellObj.rate = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Reciept"]])
    {
        sellObj.reciept = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Reciept"]];
    }
    else
    {
        sellObj.reciept = @"";
    }
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Status"]])
    {
        sellObj.status = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Status"]];
    }
    else
    {
        sellObj.status = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"Type"]])
    {
        sellObj.type = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"Type"]];
    }
    else
    {
        sellObj.type = @"";
    }
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"UserId"]])
    {
        sellObj.userId = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"UserId"]];
    }
    else
    {
        sellObj.userId = @"";
    }
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"YOP"]])
    {
        sellObj.yearOfPurchase = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"YOP"]];
    }
    else
    {
        sellObj.yearOfPurchase = @"";
    }
    
    if ([ApplicationDelegate isValid:[submittedDictionary objectForKey:@"lang_key"]])
    {
        sellObj.lang_key = [NSString stringWithFormat:@"%@",[submittedDictionary objectForKey:@"lang_key"]];
    }
    else
    {
        sellObj.lang_key = @"";
    }
    return sellObj;
    
}

-(userNotificationListData *)getUserNotificationListFromDictionary:(NSMutableDictionary *)userPreOrderDictionary
{
    
    userNotificationListData  * notiListObj = [[userNotificationListData alloc] init];
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"Date"]])
    {
        notiListObj.date = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"Date"]];
    }
    else
    {
        notiListObj.date = @"";
    }
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"Image"]])
    {
        notiListObj.image = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"Image"]];
    }
    else
    {
        notiListObj.image = @"";
    }
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"Message"]])
    {
        notiListObj.message = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"Message"]];
    }
    else
    {
        notiListObj.message = @"";
    }
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"ReadStatus"]])
    {
        notiListObj.status = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"ReadStatus"]];
    }
    else
    {
        notiListObj.status = @"";
    }
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"ReferenceId"]])
    {
        notiListObj.referenceId = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"ReferenceId"]];
    }
    else
    {
        notiListObj.referenceId = @"";
    }
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"TypeId"]])
    {
        notiListObj.typeId = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"TypeId"]];
    }
    else
    {
        notiListObj.typeId = @"";
    }
    
    if ([ApplicationDelegate isValid:[userPreOrderDictionary objectForKey:@"MessageId"]])
    {
        notiListObj.messageId = [NSString stringWithFormat:@"%@",[userPreOrderDictionary objectForKey:@"MessageId"]];
    }
    else
    {
        notiListObj.messageId = @"";
    }
    
    return notiListObj;
    
}
@end
