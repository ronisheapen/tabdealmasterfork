//
//  UserPreviousOrder.h
//  TabDeal
//
//  Created by Sreeraj VR on 26/10/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserPreviousOrder : NSObject

@property(nonatomic,retain)NSString *date;
@property(nonatomic,retain)NSString *image;
@property(nonatomic,retain)NSString *ordernumber;
@property(nonatomic,retain)NSString *status;

@end
