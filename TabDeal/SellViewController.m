//
//  SellViewController.m
//  TabDeal
//
//  Created by satheesh on 9/22/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "SellViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "SubmitViewController.h"
#import "UserSellInformation.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SDWebImage/UIButton+WebCache.h"

#define OFFSETHEIGHT 3
#define OFFSETWIDTH 3

@interface SellViewController ()
{
    NSString *encodedString;
    
    NSString *ReceiptencodedString;
    
    NSString *CertificateencodedString;
    
    NSDictionary *requestDictionary;
}

@end

@implementation SellViewController
@synthesize ProductImageArray;

-(void) viewWillAppear:(BOOL)animated
{
    [HomeViewController sharedViewController].homeButton.selected=NO;
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:0];
    if (ApplicationDelegate.innerViewCount.intValue==0)
    {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else
    {
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]])
    {
        if ([self.contextString isEqualToString:@"initialization"])
        {
            self.contextStringforSubmission=@"initialization";
            [self fetchInitialDataformSever];
        } else if ([self.contextString isEqualToString:@"editing"])
        {
            actionStatus = [NSUserDefaults standardUserDefaults];
            [actionStatus setObject:@"editing" forKey:@"Action"];
            [actionStatus synchronize];
            self.contextString = @"";
            self.contextStringforSubmission=@"editing";
            [self fetchSellDataformSever];
        }else if ([self.contextString isEqualToString:@"viewing"])
        {
            self.contextStringforSubmission=@"viewing";
            [self.confirmBtn setTitle:@"NEXT PAGE" forState:UIControlStateNormal];
            self.contextString = @"";
            self.dropDwnBrandBtn.enabled = NO;
            self.dropDwnItemBtn.enabled = NO;
            self.dropDwnYearBtn.enabled = NO;
            self.statusNewBtn.userInteractionEnabled = NO;
            self.statusUsedBtn.userInteractionEnabled = NO;
            self.itemPriceTextView.enabled = NO;
            self.dropDwnCurrencyBtn.enabled = NO;
            self.dropDwnCountryBtn.enabled = NO;
            self.deleteBtn.hidden = YES;
            [self fetchSellDataformSever];
        }
        else
        {
            actionStatus = [NSUserDefaults standardUserDefaults];
            if ([[actionStatus objectForKey:@"Action"] isEqualToString:@"initialization"])
            {
                [actionStatus setObject:@"" forKey:@"initialization"];
                self.contextStringforSubmission = @"initialization";
                [self fetchInitialDataformSever];
            }
            else if ([[actionStatus objectForKey:@"Action"] isEqualToString:@"viewing"])
            {
                [self loadUploadScroll];
            }
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    self.headdingLbl.text = localize(@"SELL YOUR ITEMS WITH US :");
    self.brandTextView.placeholder = localize(@"Select Brand");
    [self.brandTextView setFont:EBRI_NORMAL_FONT(14)];
    [self.itemTextView setFont:EBRI_NORMAL_FONT(14)];
    self.itemTextView.placeholder = localize(@"Select Item Type");
    self.selectItemStsLbl.text = localize(@"Select Item Status:");
    [self.selectItemStsLbl setFont:EBRI_NORMAL_FONT(14)];
    self.usedLbl.text = localize(@"Used");
    self.lblNew.text = localize(@"New");
    [self.usedLbl setFont:EBRI_NORMAL_FONT(14)];
    [self.lblNew setFont:EBRI_NORMAL_FONT(14)];
    [self.yearTextView setFont:EBRI_NORMAL_FONT(14)];
    [self.itemPriceTextView setFont:EBRI_NORMAL_FONT(14)];
    [self.countryTextView setFont:EBRI_NORMAL_FONT(14)];
    [self.uploadimageHeaddingLbl setFont:EBRI_NORMAL_FONT(14)];
    [self.uploadCertificateLbl setFont:EBRI_NORMAL_FONT(14)];
    [self.uploadReceiptLbl setFont:EBRI_NORMAL_FONT(14)];
    [self.confirmBtn.titleLabel setFont:EBRI_NORMAL_FONT(14)];
    [self.warningmainLbl setFont:EBRI_NORMAL_FONT(13)];
    self.yearTextView.placeholder = localize(@"Select Year of Purchase");
    self.itemPriceTextView.placeholder = localize(@"Original Item Price");
    self.countryTextView.placeholder = localize(@"Country of Purchase");
    self.currencyTextView.placeholder = localize(@"KD");
    
    self.uploadimageHeaddingLbl.text = localize(@"●  Upload up to 5 photos:");
    self.uploadCertificateLbl.text = localize(@"●  Upload Certificate of Authenticity / Serial Number Card:");
    self.uploadReceiptLbl.text = localize(@"●  Upload Item Receipt:");
    self.warningmainLbl.text = localize(@"In case the receipt cannot be located then the certificate of Authenticity or Serial Number Card must be uploaded otherwise item will be rejected immediately.");
    [self.confirmBtn setTitle:localize(@"CONFIRM") forState:UIControlStateNormal];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.headdingLbl setFont:EBRI_BOLD_FONT(17)];
    
    NSString *nibName = @"SellViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
    
    civilId = [[NSString alloc]init];
    
    itemNameArray = [[NSMutableArray alloc]initWithObjects:@"Accessories",@"Jewellery",@"Shoes",@"Bags", nil];
    currencyNameArray = [[NSMutableArray alloc]init];
    dropDownTag = 0;
    uploadImageBtnFrame = self.uploadfrmGallCamBtn.frame;
    uploadImageFrame = self.uploadImageView.frame;
    certificateImage = [[UIImage alloc]init];
    ReceiptImage = [[UIImage alloc]init];
    certificateImageView = [[UIImageView alloc]init];
    ReceiptImageView = [[UIImageView alloc]init];
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.sellBackView.frame.size.height);
    self.uploadBackView.layer.cornerRadius = 5;
    self.uploadWhiteBackView.layer.cornerRadius = 5;
    uploadPhotoCount = 0;
    uploadImageArray = [[NSMutableArray alloc]init];
    uploadImageViewArray = [[NSMutableArray alloc]init];
    uploadImageBtnArray = [[NSMutableArray alloc]init];
    uploadButtonTag = 0;
    self.statusNewBtn.selected = YES;
    self.statusUsedBtn.selected = NO;
    ProductImageArray = [[NSMutableArray alloc] init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"cached.png"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.itemPriceTextView.inputAccessoryView = numberToolbar;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)uploadfrmGallCamBtnActn:(UIButton *)sender
{
    uploadButtonTag = 1;
    if (uploadImageArray.count < 5)
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil
                                      delegate:self
                                      cancelButtonTitle:@"Cancel"
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:@"Take a new photo",
                                      @"Choose from existing",nil];
        actionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;
        [actionSheet showInView:self.view];
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Maximum Five Images Only") title:nil];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self.view.window.rootViewController presentViewController:picker animated:YES completion:NULL];
    }
    
    if (buttonIndex == 1)
    {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self.view.window.rootViewController presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *selectedImage;
    
    NSURL *mediaUrl;
    
    mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    
    if (mediaUrl == nil)
    {
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil)
        {
            selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
            if (uploadButtonTag == 1)
            {
                NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.90f);
                
                encodedString = [[NSString alloc] init];
                
                encodedString = [imageData base64EncodedStringWithOptions:0];
                
                [ProductImageArray addObject:encodedString];
                
                UIImageView * sampImageView = [[UIImageView alloc]init];
                
                [sampImageView setImage:selectedImage];
                
                [uploadImageArray addObject:sampImageView];
                
                UIButton * sampBtn = [[UIButton alloc]init];
                
                [sampBtn setImage:selectedImage forState:UIControlStateNormal];
                
                [uploadImageBtnArray addObject:sampBtn];
                
                uploadPhotoCount++;
                [self loadUploadScroll];
            }
            else if (uploadButtonTag == 2)
            {
                certificateImage = selectedImage;
                
                certificateImageView.image = selectedImage;
                
                NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.90f);
                
                CertificateencodedString = [[NSString alloc] init];
                
                CertificateencodedString = [imageData base64EncodedStringWithOptions:0];
                
                [self.uploadCertificateImgBtn setImage:certificateImage forState:UIControlStateNormal];
                self.uploadCertificateImgBtn.layer.cornerRadius = 4;
                
                self.uploadCertificateImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
                self.uploadCertificateImgBtn.layer.borderWidth = 1;
                self.uploadCertificateImgBtn.tag = 100;
                
                self.uploadCertificateImgBtn.hidden = NO;
                self.uploadCertificateBtn.hidden = YES;
            }
            else if (uploadButtonTag == 3)
            {
                ReceiptImage = selectedImage;
                
                NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.90f);
                
                ReceiptencodedString = [[NSString alloc] init];
                
                ReceiptencodedString = [imageData base64EncodedStringWithOptions:0];
                
                [self.uploadReceiptImgBtn setImage:ReceiptImage forState:UIControlStateNormal];
                self.uploadReceiptImgBtn.layer.cornerRadius = 4;
                
                self.uploadReceiptImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
                self.uploadReceiptImgBtn.layer.borderWidth = 1;
                self.uploadReceiptImgBtn.tag = 200;
                
                self.uploadReceiptImgBtn.hidden = NO;
                self.uploadReceiptBtn.hidden = YES;
            }
        }
        else
        {
            selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
            
            if (uploadButtonTag == 1)
            {
                NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.90f);
                
                encodedString = [imageData base64EncodedStringWithOptions:0];
                
                [ProductImageArray addObject:encodedString];
                UIImageView * sampImageView = [[UIImageView alloc]init];
                
                [sampImageView setImage:selectedImage];
                
                [uploadImageArray addObject:sampImageView];
                
                UIButton * sampBtn = [[UIButton alloc]init];
                
                [sampBtn setImage:selectedImage forState:UIControlStateNormal];
                
                [uploadImageBtnArray addObject:sampBtn];
                
                uploadPhotoCount++;
                
                [self loadUploadScroll];
            }
            else if (uploadButtonTag == 2)
            {
                certificateImage = selectedImage;
                
                NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.90f);
                
                CertificateencodedString = [imageData base64EncodedStringWithOptions:0];
                
                [self.uploadCertificateImgBtn setImage:certificateImage forState:UIControlStateNormal];
                self.uploadCertificateImgBtn.layer.cornerRadius = 4;
                
                self.uploadCertificateImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
                self.uploadCertificateImgBtn.layer.borderWidth = 1;
                self.uploadCertificateImgBtn.tag = 100;
                
                self.uploadCertificateImgBtn.hidden = NO;
                self.uploadCertificateBtn.hidden = YES;
            }
            else if (uploadButtonTag == 3)
            {
                ReceiptImage = selectedImage;
                
                NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.90f);
                
                ReceiptencodedString = [imageData base64EncodedStringWithOptions:0];
                
                [self.uploadReceiptImgBtn setImage:ReceiptImage forState:UIControlStateNormal];
                self.uploadReceiptImgBtn.layer.cornerRadius = 4;
                
                self.uploadReceiptImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
                self.uploadReceiptImgBtn.layer.borderWidth = 1;
                self.uploadReceiptImgBtn.tag = 200;
                
                self.uploadReceiptImgBtn.hidden = NO;
                self.uploadReceiptBtn.hidden = YES;
            }
        }
    }
    
    [picker dismissModalViewControllerAnimated:YES];
}

-(void)loadUploadScroll
{
    for (UIView *sub in self.uploadImageScrollView.subviews)
    {
        [sub removeFromSuperview];
    }
    float setXvalue = 0.0f;
    float scrollWidth = 0.0f;
    for (int i = 0; i < uploadImageArray.count; i++)
    {
        UIButton * tempBtn = [[UIButton alloc]init];
        if (uploadImageBtnArray.count>0)
        {
            tempBtn = [uploadImageBtnArray objectAtIndex:i];
        }
        else
        {
            UIImageView * sampImgView = [[UIImageView alloc]init];
            
            sampImgView = [uploadImageArray objectAtIndex:i];
            
            [tempBtn setImage:sampImgView.image forState:UIControlStateNormal];
        }
        
        tempBtn.frame = CGRectMake(uploadImageFrame.origin.x, uploadImageFrame.origin.y, uploadImageFrame.size.width, uploadImageFrame.size.height);
        tempBtn.layer.cornerRadius = 4;
        
        tempBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
        
        tempBtn.layer.borderWidth = 1;
        tempBtn.tag = i;
        
        tempBtn.frame = CGRectMake(OFFSETWIDTH + setXvalue , OFFSETHEIGHT , tempBtn.frame.size.width, tempBtn.frame.size.height);
        
        UILabel * badgeLbl= [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        badgeLbl.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_Imagebadge_C0lOR];
        badgeLbl.text = [NSString stringWithFormat:@"%d",(i+1)];
        badgeLbl.textAlignment = NSTextAlignmentCenter;
        badgeLbl.center = tempBtn.center;
        badgeLbl.layer.cornerRadius = 8;
        badgeLbl.textColor=[UIColor whiteColor];
        badgeLbl.layer.masksToBounds = YES;
        
        setXvalue = tempBtn.frame.origin.x + tempBtn.frame.size.width;
        
        scrollWidth = tempBtn.frame.origin.x + tempBtn.frame.size.width;
        
        [tempBtn addTarget:self action:@selector(detailBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [tempBtn.layer setMasksToBounds:YES];
        
        [self.uploadImageScrollView addSubview:tempBtn];
        [self.uploadImageScrollView addSubview:badgeLbl];
    }
    
    if (uploadImageArray.count == 0)
    {
        self.uploadfrmGallCamBtn.frame = uploadImageBtnFrame;
        scrollWidth = self.uploadfrmGallCamBtn.frame.origin.x + self.uploadfrmGallCamBtn.frame.size.width;
        [self.uploadImageScrollView addSubview:self.uploadfrmGallCamBtn];
    }
    else
    {
        if (![self.contextStringforSubmission isEqualToString:@"viewing"])
        {
            self.uploadfrmGallCamBtn.frame = CGRectMake(OFFSETWIDTH + setXvalue , uploadImageBtnFrame.origin.y , uploadImageBtnFrame.size.width, uploadImageBtnFrame.size.height);
            scrollWidth = self.uploadfrmGallCamBtn.frame.origin.x + self.uploadfrmGallCamBtn.frame.size.width;
            [self.uploadImageScrollView addSubview:self.uploadfrmGallCamBtn];
        }
    }
    self.uploadImageScrollView.contentSize = CGSizeMake((scrollWidth + OFFSETHEIGHT),self.uploadImageScrollView.frame.size.height);
    if (uploadImageArray.count == 0)
    {
        
    }
    else
    {
        if (![self.contextStringforSubmission isEqualToString:@"viewing"])
        {
            CGPoint bottomOffset = CGPointMake(self.uploadImageScrollView.contentSize.width - self.uploadImageScrollView.bounds.size.width , 0);
            [self.uploadImageScrollView setContentOffset:bottomOffset animated:YES];
        }
        else
        {
            if (uploadImageArray.count == 1)
            {
                CGPoint bottomOffset = CGPointMake(-((self.uploadImageScrollView.frame.size.width/2) - (uploadImageFrame.size.width/2) - 5) , 0);
                [self.uploadImageScrollView setContentOffset:bottomOffset animated:YES];
            }
        }
    }
}

-(void)detailBtnAction:(UIButton*)sender
{
    if (uploadImageArray.count>0)
    {
        UIImageView * tempimg = [[UIImageView alloc]init];
        tempimg = [uploadImageArray objectAtIndex:sender.tag];
        [self.uploadImageDetailImageView setImage:tempimg.image];
        self.deleteBtn.tag = sender.tag;
    }
    self.uploadImageDetailImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.view addSubview:self.uploadImageDetailBackView];
    
    [self.uploadImageDetailBackView setFrame:self.uploadImageDetailBackView.superview.bounds];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:nil];
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)deleteBtnActn:(UIButton *)sender
{
    if (sender.tag == 100)
    {
        certificateImage = nil;
        CertificateencodedString = [[NSString alloc] init];
        [self.uploadImageDetailBackView removeFromSuperview];
        self.uploadCertificateImgBtn.hidden = YES;
        self.uploadCertificateBtn.hidden = NO;
    }
    else if (sender.tag == 200)
    {
        ReceiptImage = nil;
        ReceiptencodedString = [[NSString alloc] init];
        [self.uploadImageDetailBackView removeFromSuperview];
        self.uploadReceiptImgBtn.hidden = YES;
        self.uploadReceiptBtn.hidden = NO;
    }
    else
    {
        [uploadImageArray removeObjectAtIndex:sender.tag];
        [uploadImageBtnArray removeObjectAtIndex:sender.tag];
        [self.uploadImageDetailBackView removeFromSuperview];
        [ProductImageArray removeObjectAtIndex:sender.tag];
        
        [self loadUploadScroll];
    }
}

- (IBAction)closeBtnActn:(UIButton *)sender
{
    [self.uploadImageDetailBackView removeFromSuperview];
}
- (IBAction)uploadCertificateBtnActn:(UIButton *)sender
{
    uploadButtonTag = 2;
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Take a new photo",
                                  @"Choose from existing",nil];
    actionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;
    [actionSheet showInView:self.view];
    if (certificateImage == nil)
    {
        
    }
}
- (IBAction)uploadCertificateImgBtnActn:(UIButton *)sender
{
    [self.uploadImageDetailImageView setImage:certificateImage];
    self.deleteBtn.tag = sender.tag;
    
    self.uploadImageDetailImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.view addSubview:self.uploadImageDetailBackView];
    
    [self.uploadImageDetailBackView setFrame:self.uploadImageDetailBackView.superview.bounds];
}

- (IBAction)uploadReceiptImgBtnActn:(UIButton *)sender
{
    [self.uploadImageDetailImageView setImage:ReceiptImage];
    self.deleteBtn.tag = sender.tag;
    
    self.uploadImageDetailImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.view addSubview:self.uploadImageDetailBackView];
    
    [self.uploadImageDetailBackView setFrame:self.uploadImageDetailBackView.superview.bounds];
}

- (IBAction)uploadReceiptBtnActn:(UIButton *)sender
{
    uploadButtonTag = 3;
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Take a new photo",
                                  @"Choose from existing",nil];
    actionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;
    [actionSheet showInView:self.view];
    if (ReceiptImage == nil)
    {
        
    }
}

- (IBAction)confirmBtnActn:(UIButton *)sender
{
    if (self.brandTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please select your brand details") title:localize(@"Sorry..!")];
    }
    else if(self.itemTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please select your item type") title:localize(@"Sorry..!")];
    }
    else if(self.yearTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please select year of purchase") title:localize(@"Sorry..!")];
    }
    else if(self.itemPriceTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter item price") title:localize(@"Sorry..!")];
    }
    else if(self.currencyTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your currency type") title:localize(@"Sorry..!")];
    }
    else if(self.countryTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country of purchase") title:localize(@"Sorry..!")];
    }
    else if(self.countryTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your brand details") title:localize(@"Sorry..!")];
    }
    else if(self.ProductImageArray.count == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please upload your product image") title:localize(@"Sorry..!")];
    }
    else if(CertificateencodedString == (id)[NSNull null] || CertificateencodedString.length == 0 )
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please upload your certificate of authenticity/serial number card") title:localize(@"Sorry..!")];
    }
    else
    {
        if(ReceiptencodedString == (id)[NSNull null] || ReceiptencodedString.length == 0 )
        {
            ReceiptencodedString=@"";
        }
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        SubmitViewController * submitScreenVC = [[SubmitViewController alloc]initWithNibName:@"SubmitViewController" bundle:nil];
        submitScreenVC.view.backgroundColor = [UIColor clearColor];
        submitScreenVC.contextString=self.contextStringforSubmission;
        submitScreenVC.SellPageData=[[NSMutableDictionary alloc] init];
        
        NSString * statusString = [[NSString alloc]init];
        if (self.statusNewBtn.selected)
        {
            statusString = @"New";
        } else {
            statusString = @"Used";
        }
        if ([self.contextStringforSubmission isEqualToString:@"initialization"])
        {
            requestDictionary = @{@"AuthImages":CertificateencodedString,@"BrandId": brandId,@"Country": self.countryTextView.text,@"Currency_code": self.currencyTextView.text,@"ProductImageCode": ProductImageArray,@"Rate": self.itemPriceTextView.text,@"Reciept": ReceiptencodedString,@"Status": statusString,@"Type": self.itemTextView.text,@"UserId": [prefs objectForKey:@"User Id"],@"YOP": self.yearTextView.text,@"lang_key": kENGLISH};
        }
        if ([self.contextStringforSubmission isEqualToString:@"editing"])
        {
            requestDictionary = @{@"AuthImages":CertificateencodedString,@"BrandId": brandId,@"Country": self.countryTextView.text,@"Currency_code": self.currencyTextView.text,@"ProductImageCode": ProductImageArray,@"Rate": self.itemPriceTextView.text,@"Reciept": ReceiptencodedString,@"Status": statusString,@"Type": self.itemTextView.text,@"UserId": [prefs objectForKey:@"User Id"],@"YOP": self.yearTextView.text,@"lang_key": kENGLISH,@"RefferenceNumber": self.refferenceString};
        }
        
        submitScreenVC.SellPageData = requestDictionary;
        
        actionTracking = [NSUserDefaults standardUserDefaults];
        [actionTracking setObject:requestDictionary forKey:@"SellData"];
        [actionTracking setObject:@"" forKey:@"civilId"];
        [actionTracking setObject:self.contextStringforSubmission forKey:@"contextStringforSubmission"];
        
        if (civilId.length > 0)
        {
            submitScreenVC.civilIdStr = civilId;
            [actionTracking setObject:civilId forKey:@"civilId"];
        }
        [actionTracking synchronize];
        
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SubmitViewController class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:submitScreenVC animated:NO];
        }
    }
}



-(void)fetchInitialDataformSever
{
    NSMutableDictionary *countryData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserCountryPostData]];
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getSaleParametersWithDataDictionary:countryData CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         if ([ApplicationDelegate isValid:responseDictionary])
         {
             if (responseDictionary.count>0)
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Currency"]])
                 {
                     currencyNameArray = [responseDictionary objectForKey:@"Currency"];
                 }
                 self.brandCodeAndName = [[NSMutableArray alloc]init];
                 brandNameArray = [[NSMutableArray alloc]init];
                 brandCodeArray = [[NSMutableArray alloc]init];
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Brandlist"]])
                 {
                     self.brandCodeAndName = [responseDictionary objectForKey:@"Brandlist"];
                     
                     for (int i=0; i<self.brandCodeAndName.count; i++)
                     {
                         [brandNameArray addObject:[[self.brandCodeAndName objectAtIndex:i]objectForKey:@"BrandName"]];
                         [brandCodeArray addObject:[[self.brandCodeAndName objectAtIndex:i]objectForKey:@"BrandId"]];
                     }
                 }
                 
                 self.countryCodeAndName = [[NSMutableArray alloc]init];
                 countryNameArray = [[NSMutableArray alloc]init];
                 countryCodeArray = [[NSMutableArray alloc]init];
                 
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"countrylist"]])
                 {
                     self.countryCodeAndName = [responseDictionary objectForKey:@"countrylist"];
                     for (int i=0; i<self.countryCodeAndName.count; i++)
                     {
                         [countryNameArray addObject:[[self.countryCodeAndName objectAtIndex:i]objectForKey:@"CountryName"]];
                         [countryCodeArray addObject:[[self.countryCodeAndName objectAtIndex:i]objectForKey:@"CountryCode"]];
                     }
                 }
                 yearArray = [[NSMutableArray alloc]init];
                 
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"year"]])
                 {
                     yearArray = [responseDictionary objectForKey:@"year"];
                 }
             }
         }
         [ApplicationDelegate removeProgressHUD];
     } errorHandler:^(NSError *error) {
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}

-(void)fetchSellDataformSever
{
    NSMutableDictionary *countryData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserCountryPostData]];
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getSaleParametersWithDataDictionary:countryData CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         if ([ApplicationDelegate isValid:responseDictionary])
         {
             if (responseDictionary.count>0)
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Currency"]])
                 {
                     currencyNameArray = [responseDictionary objectForKey:@"Currency"];
                 }
                 self.brandCodeAndName = [[NSMutableArray alloc]init];
                 brandNameArray = [[NSMutableArray alloc]init];
                 brandCodeArray = [[NSMutableArray alloc]init];
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Brandlist"]])
                 {
                     self.brandCodeAndName = [responseDictionary objectForKey:@"Brandlist"];
                     for (int i=0; i<self.brandCodeAndName.count; i++)
                     {
                         [brandNameArray addObject:[[self.brandCodeAndName objectAtIndex:i]objectForKey:@"BrandName"]];
                         [brandCodeArray addObject:[[self.brandCodeAndName objectAtIndex:i]objectForKey:@"BrandId"]];
                     }
                 }
                 
                 self.countryCodeAndName = [[NSMutableArray alloc]init];
                 countryNameArray = [[NSMutableArray alloc]init];
                 countryCodeArray = [[NSMutableArray alloc]init];
                 
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"countrylist"]])
                 {
                     self.countryCodeAndName = [responseDictionary objectForKey:@"countrylist"];
                     
                     for (int i=0; i<self.countryCodeAndName.count; i++)
                     {
                         [countryNameArray addObject:[[self.countryCodeAndName objectAtIndex:i]objectForKey:@"CountryName"]];
                         [countryCodeArray addObject:[[self.countryCodeAndName objectAtIndex:i]objectForKey:@"CountryCode"]];
                     }
                 }
                 yearArray = [[NSMutableArray alloc]init];
                 
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"year"]])
                 {
                     yearArray = [responseDictionary objectForKey:@"year"];
                 }
                 
                 NSMutableDictionary * sellData = [[NSMutableDictionary alloc] initWithDictionary:[self getSellReffrencePostData]];
                 
                 [ApplicationDelegate addProgressHUDToView:self.view];
                 [ApplicationDelegate.engine getSaleEditableParametersDictionary:sellData CompletionHandler:^(NSMutableDictionary *responseDictionary)
                  {
                      if ([ApplicationDelegate isValid:responseDictionary])
                      {
                          if (responseDictionary.count>0)
                          {
                              UserSellInformation *sellObj = [ApplicationDelegate.mapper getSellInformationFromDictionary:responseDictionary];
                              int i;
                              for (i =0 ;i<brandCodeArray.count; i++)
                              {
                                  if ([[brandCodeArray objectAtIndex:i]isEqualToString:sellObj.brandId])
                                  {
                                      break;
                                  }
                              }
                              if (i == brandCodeArray.count)
                              {
                                  self.brandTextView.text = @"";
                              }
                              else
                              {
                                  self.brandTextView.text = [brandNameArray objectAtIndex:i];
                                  brandId = [brandCodeArray objectAtIndex:i];
                              }
                              self.itemTextView.text = sellObj.type;
                              if ([sellObj.status isEqualToString:@"New"])
                              {
                                  self.statusNewBtn.selected = YES;
                                  self.statusUsedBtn.selected = NO;
                              }
                              else
                              {
                                  self.statusNewBtn.selected = NO;
                                  self.statusUsedBtn.selected = YES;
                              }
                              self.yearTextView.text = sellObj.yearOfPurchase;
                              self.itemPriceTextView.text = sellObj.rate;
                              self.currencyTextView.text = sellObj.currency_code;
                              self.countryTextView.text = sellObj.country;
                              civilId = sellObj.idProof;
                              if (sellObj.productImageCode.count>0)
                              {
                                  for (int i = 0; i<sellObj.productImageCode.count; i++)
                                  {
                                      UIImageView * sampImgView = [[UIImageView alloc]init];
                                      UIButton * sampBtn = [[UIButton alloc]init];
                                      NSURL * sampurl = [[NSURL alloc]init];
                                      sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:[sellObj.productImageCode objectAtIndex:i]]];
                                      [sampImgView sd_setImageWithURL:sampurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
                                      [sampBtn sd_setImageWithURL:sampurl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"loading.png"]];
                                      [uploadImageArray addObject:sampImgView];
                                      [ProductImageArray addObject:[sellObj.productImageCode objectAtIndex:i]];
                                      [uploadImageBtnArray addObject:sampBtn];
                                  }
                                  
                                  [self loadUploadScroll];
                              }
                              if (sellObj.authImages.length>0)
                              {
                                  NSURL * sampurl = [[NSURL alloc]init];
                                  sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:sellObj.authImages]];
                                  NSData *imageData = [NSData dataWithContentsOfURL:sampurl];
                                  [self.uploadCertificateImgBtn sd_setImageWithURL:sampurl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"loading.png"]];
                                  certificateImage=[UIImage imageWithData:imageData];
                                  CertificateencodedString = [[NSString alloc] init];
                                  CertificateencodedString = sellObj.authImages;
                                  self.uploadCertificateImgBtn.layer.cornerRadius = 4;
                                  self.uploadCertificateImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
                                  self.uploadCertificateImgBtn.layer.borderWidth = 1;
                                  self.uploadCertificateImgBtn.tag = 100;
                                  self.uploadCertificateImgBtn.hidden = NO;
                                  self.uploadCertificateBtn.hidden = YES;
                              }
                              if (sellObj.reciept.length>0)
                              {
                                  NSURL * sampurl = [[NSURL alloc]init];
                                  sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:sellObj.reciept]];
                                  NSData *imageData = [NSData dataWithContentsOfURL:sampurl];
                                  if ([UIImage imageWithData:imageData])
                                  {
                                      ReceiptencodedString = [[NSString alloc] init];
                                      ReceiptencodedString = sellObj.reciept;
                                      [self.uploadReceiptImgBtn sd_setImageWithURL:sampurl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"loading.png"]];
                                      ReceiptImage = [UIImage imageWithData:imageData];
                                      self.uploadReceiptImgBtn.layer.cornerRadius = 4;
                                      self.uploadReceiptImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
                                      self.uploadReceiptImgBtn.layer.borderWidth = 1;
                                      self.uploadReceiptImgBtn.tag = 200;
                                      self.uploadReceiptImgBtn.hidden = NO;
                                      self.uploadReceiptBtn.hidden = YES;
                                  }
                                  else
                                  {
                                      self.uploadReceiptBtn.hidden = NO;
                                  }
                              }
                          }
                      }
                  } errorHandler:^(NSError *error)
                  {
                      [ApplicationDelegate removeProgressHUD];
                      [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
                  }];
             }
         }
         
         [ApplicationDelegate removeProgressHUD];
     } errorHandler:^(NSError *error)
     {
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}

-(NSMutableDictionary *)getUserCountryPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else
    {
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(NSMutableDictionary *)getSellReffrencePostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.refferenceString forKey:@"RefferenceNumber"];
    return postDic;
}

- (IBAction)dropDwnBrandBtnActn:(UIButton *)sender
{
    if (self.dropDwnItemBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.itemDropDownObj.view.frame =
            CGRectMake(self.itemDropDownObj.view.frame.origin.x,
                       self.itemDropDownObj.view.frame.origin.y,
                       self.itemDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
             if (finished)
             {
                 [self.itemDropDownObj.view removeFromSuperview];
             }
             
         }];
        
        self.dropDwnItemBtn.selected = NO;
        self.uparrowItem.hidden = YES;
        self.downarrowItem.hidden = NO;
    }
    if (self.dropDwnYearBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.YearDropDownObj.view.frame =
            CGRectMake(self.YearDropDownObj.view.frame.origin.x,
                       self.YearDropDownObj.view.frame.origin.y,
                       self.YearDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
             if (finished)
             {
                 [self.YearDropDownObj.view removeFromSuperview];
             }
         }];
        self.dropDwnYearBtn.selected = NO;
        self.uparrowYear.hidden = YES;
        self.downarrowYear.hidden = NO;
    }
    if (self.dropDwnCurrencyBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.currencyDropDownObj.view.frame =
            CGRectMake(self.currencyDropDownObj.view.frame.origin.x,
                       self.currencyDropDownObj.view.frame.origin.y,
                       self.currencyDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
             if (finished)
             {
                 [self.currencyDropDownObj.view removeFromSuperview];
             }
         }];
        self.dropDwnCurrencyBtn.selected = NO;
        self.uparrowCurrency.hidden = YES;
        self.downarrowCurrency.hidden = NO;
    }
    if (self.dropDwnCountryBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
             if (finished)
             {
                 [self.countryDropDownObj.view removeFromSuperview];
             }
             
         }];
        self.dropDwnCountryBtn.selected = NO;
        self.uparrowCountry.hidden = YES;
        self.downarrowCountry.hidden = NO;
    }
    
    dropDownTag = 1;
    self.dropDwnBrandBtn.selected = !self.dropDwnBrandBtn.selected;
    if (self.dropDwnBrandBtn.selected)
    {
        self.downarrowBrand.hidden = YES;
        self.uparrowBrand.hidden = NO;
        if (self.brandDropDownObj.view.superview)
        {
            [self.brandDropDownObj.view removeFromSuperview];
        }
        
        self.brandDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        self.brandDropDownObj.cellHeight = 40.0f;
        
        self.brandDropDownObj.view.frame = CGRectMake((self.brandTextView.frame.origin.x),(self.brandTextView.frame.origin.y+self.brandTextView.frame.size.height), (self.brandTextView.frame.size.width), 0);
        
        self.brandDropDownObj.dropDownDelegate = self;
        self.brandDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:brandNameArray];
        
        self.brandDropDownObj.view.layer.borderWidth = 0.1;
        self.brandDropDownObj.view.layer.shadowOpacity = 1.0;
        self.brandDropDownObj.view.layer.shadowRadius = 5.0;
        self.brandDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        self.brandDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.brandDropDownObj.textLabelColor = [UIColor blackColor];
        self.brandDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (brandNameArray.count>0)
        {
            [self.dropDwnBrandBtn.superview addSubview:self.brandDropDownObj.view];
        }
        else
        {
            self.dropDwnBrandBtn.selected=NO;
        }
        
        CGFloat tableHT = 120.0f;
        
        if ((self.brandDropDownObj.cellHeight*self.brandDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.brandDropDownObj.cellHeight*self.brandDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.brandDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.brandDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.brandDropDownObj.view.frame =
            CGRectMake(self.brandDropDownObj.view.frame.origin.x+2,
                       self.brandDropDownObj.view.frame.origin.y,
                       self.brandDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.uparrowBrand.hidden = YES;
        self.downarrowBrand.hidden = NO;
        self.brandDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.brandDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        for (UIView *vw in self.view.subviews)
        {
            [vw setUserInteractionEnabled:YES];
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.brandDropDownObj.view.frame =
            CGRectMake(self.brandDropDownObj.view.frame.origin.x,
                       self.brandDropDownObj.view.frame.origin.y,
                       self.brandDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.brandDropDownObj.view removeFromSuperview];
            }
        }];
    }
}

-(void)selectList:(int)selectedIndex
{
    if (dropDownTag == 1)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.brandDropDownObj.view.frame =
            CGRectMake(self.brandDropDownObj.view.frame.origin.x,
                       self.brandDropDownObj.view.frame.origin.y,
                       self.brandDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.brandDropDownObj.view removeFromSuperview];
            }
        }];
        
        self.dropDwnBrandBtn.selected = NO;
        self.uparrowBrand.hidden = YES;
        self.downarrowBrand.hidden = NO;
        self.brandTextView.text = [brandNameArray objectAtIndex:selectedIndex];
        brandId = [[NSString alloc]initWithString:[brandCodeArray objectAtIndex:selectedIndex]];
    }
    else if (dropDownTag == 2)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.itemDropDownObj.view.frame =
            CGRectMake(self.itemDropDownObj.view.frame.origin.x,
                       self.itemDropDownObj.view.frame.origin.y,
                       self.itemDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.itemDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnItemBtn.selected = NO;
        self.uparrowItem.hidden = YES;
        self.downarrowItem.hidden = NO;
        self.itemTextView.text =[itemNameArray objectAtIndex:selectedIndex];
    }
    else if (dropDownTag == 3)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.YearDropDownObj.view.frame =
            CGRectMake(self.YearDropDownObj.view.frame.origin.x,
                       self.YearDropDownObj.view.frame.origin.y,
                       self.YearDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
            if (finished)
            {
                [self.YearDropDownObj.view removeFromSuperview];
            }
        }];
        
        self.dropDwnYearBtn.selected = NO;
        self.uparrowYear.hidden = YES;
        self.downarrowYear.hidden = NO;
        self.yearTextView.text =[yearArray objectAtIndex:selectedIndex];
    }
    else if (dropDownTag == 4)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.currencyDropDownObj.view.frame =
            CGRectMake(self.currencyDropDownObj.view.frame.origin.x,
                       self.currencyDropDownObj.view.frame.origin.y,
                       self.currencyDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.currencyDropDownObj.view removeFromSuperview];
            }
        }];
        
        self.dropDwnCurrencyBtn.selected = NO;
        self.uparrowCurrency.hidden = YES;
        self.downarrowCurrency.hidden = NO;
        self.currencyTextView.text =[currencyNameArray objectAtIndex:selectedIndex];
    }
    else if (dropDownTag == 5)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
            if (finished)
            {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
        
        self.dropDwnCountryBtn.selected = NO;
        self.uparrowCountry.hidden = YES;
        self.downarrowCountry.hidden = NO;
        self.countryTextView.text =[countryNameArray objectAtIndex:selectedIndex];
    }
}

- (IBAction)dropDwnItemBtnActn:(UIButton *)sender
{
    if (self.dropDwnBrandBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.brandDropDownObj.view.frame =
            CGRectMake(self.brandDropDownObj.view.frame.origin.x,
                       self.brandDropDownObj.view.frame.origin.y,
                       self.brandDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.brandDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnBrandBtn.selected = NO;
        self.uparrowBrand.hidden = YES;
        self.downarrowBrand.hidden = NO;
    }
    if (self.dropDwnYearBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.YearDropDownObj.view.frame =
            CGRectMake(self.YearDropDownObj.view.frame.origin.x,
                       self.YearDropDownObj.view.frame.origin.y,
                       self.YearDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.YearDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnYearBtn.selected = NO;
        self.uparrowYear.hidden = YES;
        self.downarrowYear.hidden = NO;
    }
    if (self.dropDwnCurrencyBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.currencyDropDownObj.view.frame =
            CGRectMake(self.currencyDropDownObj.view.frame.origin.x,
                       self.currencyDropDownObj.view.frame.origin.y,
                       self.currencyDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.currencyDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnCurrencyBtn.selected = NO;
        self.uparrowCurrency.hidden = YES;
        self.downarrowCurrency.hidden = NO;
    }
    if (self.dropDwnCountryBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
        
        self.dropDwnCountryBtn.selected = NO;
        self.uparrowCountry.hidden = YES;
        self.downarrowCountry.hidden = NO;
    }
    
    dropDownTag = 2;
    self.dropDwnItemBtn.selected = !self.dropDwnItemBtn.selected;
    if (self.dropDwnItemBtn.selected)
    {
        self.downarrowItem.hidden = YES;
        self.uparrowItem.hidden = NO;
        if (self.itemDropDownObj.view.superview)
        {
            [self.itemDropDownObj.view removeFromSuperview];
        }
        self.itemDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        self.itemDropDownObj.cellHeight = 40.0f;
        
        self.itemDropDownObj.view.frame = CGRectMake((self.itemTextView.frame.origin.x),(self.itemTextView.frame.origin.y+self.itemTextView.frame.size.height), (self.itemTextView.frame.size.width), 0);
        
        self.itemDropDownObj.dropDownDelegate = self;
        self.itemDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:itemNameArray];
        
        self.itemDropDownObj.view.layer.borderWidth = 0.1;
        self.itemDropDownObj.view.layer.shadowOpacity = 1.0;
        self.itemDropDownObj.view.layer.shadowRadius = 5.0;
        self.itemDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        self.itemDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.itemDropDownObj.textLabelColor = [UIColor blackColor];
        self.itemDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (itemNameArray.count>0)
        {
            [self.dropDwnItemBtn.superview addSubview:self.itemDropDownObj.view];
        }
        else
        {
            self.dropDwnItemBtn.selected=NO;
        }
        
        CGFloat tableHT = 120.0f;
        
        if ((self.itemDropDownObj.cellHeight*self.itemDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.itemDropDownObj.cellHeight*self.itemDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.itemDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.itemDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.itemDropDownObj.view.frame =
            CGRectMake(self.itemDropDownObj.view.frame.origin.x+2,
                       self.itemDropDownObj.view.frame.origin.y,
                       self.itemDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.uparrowItem.hidden = YES;
        self.downarrowItem.hidden = NO;
        self.itemDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.itemDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        for (UIView *vw in self.view.subviews)
        {
            [vw setUserInteractionEnabled:YES];
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.itemDropDownObj.view.frame =
            CGRectMake(self.itemDropDownObj.view.frame.origin.x,
                       self.itemDropDownObj.view.frame.origin.y,
                       self.itemDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.itemDropDownObj.view removeFromSuperview];
            }
        }];
    }
}

- (IBAction)dropDwnYearBtnActn:(UIButton *)sender
{
    if (self.dropDwnBrandBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.brandDropDownObj.view.frame =
            CGRectMake(self.brandDropDownObj.view.frame.origin.x,
                       self.brandDropDownObj.view.frame.origin.y,
                       self.brandDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.brandDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnBrandBtn.selected = NO;
        self.uparrowBrand.hidden = YES;
        self.downarrowBrand.hidden = NO;
    }
    if (self.dropDwnItemBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.itemDropDownObj.view.frame =
            CGRectMake(self.itemDropDownObj.view.frame.origin.x,
                       self.itemDropDownObj.view.frame.origin.y,
                       self.itemDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.itemDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnItemBtn.selected = NO;
        self.uparrowItem.hidden = YES;
        self.downarrowItem.hidden = NO;
    }
    
    if (self.dropDwnCurrencyBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.currencyDropDownObj.view.frame =
            CGRectMake(self.currencyDropDownObj.view.frame.origin.x,
                       self.currencyDropDownObj.view.frame.origin.y,
                       self.currencyDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.currencyDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnCurrencyBtn.selected = NO;
        self.uparrowCurrency.hidden = YES;
        self.downarrowCurrency.hidden = NO;
    }
    if (self.dropDwnCountryBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnCountryBtn.selected = NO;
        self.uparrowCountry.hidden = YES;
        self.downarrowCountry.hidden = NO;
    }
    dropDownTag = 3;
    self.dropDwnYearBtn.selected = !self.dropDwnYearBtn.selected;
    if (self.dropDwnYearBtn.selected)
    {
        self.downarrowYear.hidden = YES;
        self.uparrowYear.hidden = NO;
        if (self.YearDropDownObj.view.superview)
        {
            [self.YearDropDownObj.view removeFromSuperview];
        }
        self.YearDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        self.YearDropDownObj.cellHeight = 40.0f;
        
        self.YearDropDownObj.view.frame = CGRectMake((self.yearTextView.frame.origin.x),(self.yearTextView.frame.origin.y+self.yearTextView.frame.size.height), (self.yearTextView.frame.size.width), 0);
        
        self.YearDropDownObj.dropDownDelegate = self;
        self.YearDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:yearArray];
        self.YearDropDownObj.view.layer.borderWidth = 0.1;
        self.YearDropDownObj.view.layer.shadowOpacity = 1.0;
        self.YearDropDownObj.view.layer.shadowRadius = 5.0;
        self.YearDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        self.YearDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.YearDropDownObj.textLabelColor = [UIColor blackColor];
        self.YearDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (yearArray.count>0)
        {
            [self.dropDwnYearBtn.superview addSubview:self.YearDropDownObj.view];
        }
        else
        {
            self.dropDwnYearBtn.selected=NO;
        }
        
        CGFloat tableHT = 120.0f;
        
        if ((self.YearDropDownObj.cellHeight*self.YearDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.YearDropDownObj.cellHeight*self.YearDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.YearDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.YearDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.YearDropDownObj.view.frame =
            CGRectMake(self.YearDropDownObj.view.frame.origin.x+2,
                       self.YearDropDownObj.view.frame.origin.y,
                       self.YearDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.uparrowYear.hidden = YES;
        self.downarrowYear.hidden = NO;
        self.YearDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.YearDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        for (UIView *vw in self.view.subviews)
        {
            [vw setUserInteractionEnabled:YES];
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.YearDropDownObj.view.frame =
            CGRectMake(self.YearDropDownObj.view.frame.origin.x,
                       self.YearDropDownObj.view.frame.origin.y,
                       self.YearDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.YearDropDownObj.view removeFromSuperview];
            }
        }];
    }
}
- (IBAction)dropDwnCurrencyBtnActn:(UIButton *)sender
{
    if (self.dropDwnBrandBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.brandDropDownObj.view.frame =
            CGRectMake(self.brandDropDownObj.view.frame.origin.x,
                       self.brandDropDownObj.view.frame.origin.y,
                       self.brandDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.brandDropDownObj.view removeFromSuperview];
            }
        }];
        
        self.dropDwnBrandBtn.selected = NO;
        self.uparrowBrand.hidden = YES;
        self.downarrowBrand.hidden = NO;
    }
    if (self.dropDwnItemBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.itemDropDownObj.view.frame =
            CGRectMake(self.itemDropDownObj.view.frame.origin.x,
                       self.itemDropDownObj.view.frame.origin.y,
                       self.itemDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.itemDropDownObj.view removeFromSuperview];
            }
        }];
        
        self.dropDwnItemBtn.selected = NO;
        self.uparrowItem.hidden = YES;
        self.downarrowItem.hidden = NO;
    }
    if (self.dropDwnYearBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.YearDropDownObj.view.frame =
            CGRectMake(self.YearDropDownObj.view.frame.origin.x,
                       self.YearDropDownObj.view.frame.origin.y,
                       self.YearDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.YearDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnYearBtn.selected = NO;
        self.uparrowYear.hidden = YES;
        self.downarrowYear.hidden = NO;
    }
    
    if (self.dropDwnCountryBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnCountryBtn.selected = NO;
        self.uparrowCountry.hidden = YES;
        self.downarrowCountry.hidden = NO;
    }
    dropDownTag = 4;
    self.dropDwnCurrencyBtn.selected = !self.dropDwnCurrencyBtn.selected;
    if (self.dropDwnCurrencyBtn.selected)
    {
        self.downarrowCurrency.hidden = YES;
        self.uparrowCurrency.hidden = NO;
        if (self.currencyDropDownObj.view.superview)
        {
            [self.currencyDropDownObj.view removeFromSuperview];
        }
        self.currencyDropDownObj = [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        self.currencyDropDownObj.cellHeight = 40.0f;
        
        self.currencyDropDownObj.view.frame = CGRectMake((self.currencyTextView.frame.origin.x),(self.currencyTextView.frame.origin.y+self.currencyTextView.frame.size.height), (self.currencyTextView.frame.size.width), 0);
        
        self.currencyDropDownObj.dropDownDelegate = self;
        self.currencyDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:currencyNameArray];
        self.currencyDropDownObj.view.layer.borderWidth = 0.1;
        self.currencyDropDownObj.view.layer.shadowOpacity = 1.0;
        self.currencyDropDownObj.view.layer.shadowRadius = 5.0;
        self.currencyDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        self.currencyDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.currencyDropDownObj.textLabelColor = [UIColor blackColor];
        self.currencyDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (currencyNameArray.count>0)
        {
            [self.dropDwnCurrencyBtn.superview addSubview:self.currencyDropDownObj.view];
        }
        else
        {
            self.dropDwnCurrencyBtn.selected=NO;
        }
        CGFloat tableHT = 120.0f;
        
        if ((self.currencyDropDownObj.cellHeight*self.currencyDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.currencyDropDownObj.cellHeight*self.currencyDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.currencyDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.currencyDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.currencyDropDownObj.view.frame =
            CGRectMake(self.currencyDropDownObj.view.frame.origin.x+2,
                       self.currencyDropDownObj.view.frame.origin.y,
                       self.currencyDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.uparrowCurrency.hidden = YES;
        self.downarrowCurrency.hidden = NO;
        self.currencyDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.currencyDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        for (UIView *vw in self.view.subviews)
        {
            [vw setUserInteractionEnabled:YES];
        }
        [UIView animateWithDuration:0.4f animations:^{
            self.currencyDropDownObj.view.frame =
            CGRectMake(self.currencyDropDownObj.view.frame.origin.x,
                       self.currencyDropDownObj.view.frame.origin.y,
                       self.currencyDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.currencyDropDownObj.view removeFromSuperview];
            }
        }];
    }
}

- (IBAction)dropDwnCountryBtnActn:(UIButton *)sender
{
    if (self.dropDwnBrandBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.brandDropDownObj.view.frame =
            CGRectMake(self.brandDropDownObj.view.frame.origin.x,
                       self.brandDropDownObj.view.frame.origin.y,
                       self.brandDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.brandDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnBrandBtn.selected = NO;
        self.uparrowBrand.hidden = YES;
        self.downarrowBrand.hidden = NO;
    }
    if (self.dropDwnItemBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.itemDropDownObj.view.frame =
            CGRectMake(self.itemDropDownObj.view.frame.origin.x,
                       self.itemDropDownObj.view.frame.origin.y,
                       self.itemDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.itemDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnItemBtn.selected = NO;
        self.uparrowItem.hidden = YES;
        self.downarrowItem.hidden = NO;
    }
    if (self.dropDwnYearBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.YearDropDownObj.view.frame =
            CGRectMake(self.YearDropDownObj.view.frame.origin.x,
                       self.YearDropDownObj.view.frame.origin.y,
                       self.YearDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
            if (finished)
            {
                [self.YearDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnYearBtn.selected = NO;
        self.uparrowYear.hidden = YES;
        self.downarrowYear.hidden = NO;
    }
    if (self.dropDwnCurrencyBtn.selected)
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.currencyDropDownObj.view.frame =
            CGRectMake(self.currencyDropDownObj.view.frame.origin.x,
                       self.currencyDropDownObj.view.frame.origin.y,
                       self.currencyDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
         {
            if (finished)
            {
                [self.currencyDropDownObj.view removeFromSuperview];
            }
        }];
        self.dropDwnCurrencyBtn.selected = NO;
        self.uparrowCurrency.hidden = YES;
        self.downarrowCurrency.hidden = NO;
    }
    dropDownTag = 5;
    self.dropDwnCountryBtn.selected = !self.dropDwnCountryBtn.selected;
    if (self.dropDwnCountryBtn.selected)
    {
        self.downarrowCountry.hidden = YES;
        self.uparrowCountry.hidden = NO;
        if (self.countryDropDownObj.view.superview)
        {
            [self.countryDropDownObj.view removeFromSuperview];
        }
        self.countryDropDownObj = [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        self.countryDropDownObj.cellHeight = 40.0f;
        self.countryDropDownObj.view.frame = CGRectMake((self.countryTextView.frame.origin.x),(self.countryTextView.frame.origin.y+self.countryTextView.frame.size.height), (self.countryTextView.frame.size.width), 0);
        self.countryDropDownObj.dropDownDelegate = self;
        self.countryDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:countryNameArray];
        self.countryDropDownObj.view.layer.borderWidth = 0.1;
        self.countryDropDownObj.view.layer.shadowOpacity = 1.0;
        self.countryDropDownObj.view.layer.shadowRadius = 5.0;
        self.countryDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        self.countryDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.countryDropDownObj.textLabelColor = [UIColor blackColor];
        self.countryDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (countryNameArray.count>0)
        {
            [self.dropDwnCountryBtn.superview addSubview:self.countryDropDownObj.view];
        }
        else
        {
            self.dropDwnCountryBtn.selected=NO;
        }
        
        CGFloat tableHT = 120.0f;
        
        if ((self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.countryDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x+2,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.uparrowCountry.hidden = YES;
        self.downarrowCountry.hidden = NO;
        self.countryDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.countryDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        for (UIView *vw in self.view.subviews)
        {
            [vw setUserInteractionEnabled:YES];
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished)
        {
            if (finished)
            {
                [self.countryDropDownObj.view removeFromSuperview];
            }
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)statusBtnActn:(UIButton *)sender
{
    if (sender.tag == 1)
    {
        if (self.statusNewBtn.selected)
        {
            
        }
        else
        {
            self.statusNewBtn.selected = YES;
            self.statusUsedBtn.selected = NO;
        }
    }
    else
    {
        if (self.statusUsedBtn.selected)
        {
            
        }
        else
        {
            self.statusNewBtn.selected = NO;
            self.statusUsedBtn.selected = YES;
        }
    }
}

-(void)cancelNumberPad
{
    [ self.itemPriceTextView resignFirstResponder];
    self.itemPriceTextView.text = @"";
}

-(void)doneWithNumberPad
{
    [self.itemPriceTextView resignFirstResponder];
}

-(void)xibLoading
{
    NSString *nibName = @"SellViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
