//
//  SubmitViewController.m
//  TabDeal
//
//  Created by satheesh on 10/27/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "SubmitViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SDWebImage/UIButton+WebCache.h"
#import "SellViewController.h"
#import "NewDealsViewController.h"

@interface SubmitViewController ()
{
    NSString* encodedCivilId;
}

@end

@implementation SubmitViewController

@synthesize SellPageData;

-(void)viewWillAppear:(BOOL)animated
{
     [HomeViewController sharedViewController].homeButton.selected=NO;
}
-(void)viewDidAppear:(BOOL)animated
{
    [self.uploadCivilIdLbl setFont:EBRI_NORMAL_FONT(14)];
    [self.submitBtn.titleLabel setFont:EBRI_NORMAL_FONT(14)];
    [self.cancelBtn.titleLabel setFont:EBRI_NORMAL_FONT(14)];
    self.uploadCivilIdLbl.text = localize(@"●  Upload Civil ID / Passport image:");
    self.confirmOwnerShipLbl.text = localize(@"I confirm ownership of submitted item");
    self.confirmConditionLbl.text = localize(@"I confirm all items are in Pristine or Excellent condition");
    self.confirnTermsLbl.text = localize(@"I confirm to all of TABDEAL Consignment Terms and Conditions");
    self.moreInfoLbl.text = localize(@"More Info");
    self.readTermsAndConditionLbl.text = localize(@"Read Terms and Conditions");
    [self.submitBtn setTitle:localize(@"SUBMIT") forState:UIControlStateNormal];
    [self.cancelBtn setTitle:localize(@"CANCEL") forState:UIControlStateNormal];
    
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:([ApplicationDelegate.innerViewCount intValue]+1)];
    if (ApplicationDelegate.innerViewCount.intValue==0)
    {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else
    {
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    if ([self.contextString isEqualToString:@"editing"])
    {
        actionStatus = [NSUserDefaults standardUserDefaults];
        [actionStatus setObject:@"editing" forKey:@"Action"];
        [actionStatus synchronize];
        [self.submitBtn setTitle:localize(@"UPDATE") forState:UIControlStateNormal];
    } else if([self.contextString isEqualToString:@"viewing"])
    {
        actionStatus = [NSUserDefaults standardUserDefaults];
        [actionStatus setObject:@"viewing" forKey:@"Action"];
        [actionStatus synchronize];
        
        self.deleteBtn.hidden = YES;
        self.ownershipBtn.userInteractionEnabled = NO;
        self.consignmentBtn.userInteractionEnabled = NO;
        self.conditionBtn.userInteractionEnabled = NO;
        self.confirmBtn.hidden = YES;
        
        self.submitBtn.hidden = YES;
        self.cancelBtn.hidden = YES;
    }
    
    if (self.civilIdStr.length>0)
    {
        [self loadViewSellData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *nibName = @"SubmitViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
    
    civilImage = [[UIImage alloc]init];
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.sellBackView.frame.size.height);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"cached.png"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        UIImage *Cachedimg = [UIImage imageWithContentsOfFile:filePath];
        
        encodedCivilId = [[NSString alloc] init];
        encodedCivilId = [[NSData dataWithContentsOfFile:filePath] base64EncodedStringWithOptions:0];
        civilImage = Cachedimg;
        [self.uploadCivilIdImgBtn setImage:civilImage forState:UIControlStateNormal];
        self.uploadCivilIdImgBtn.layer.cornerRadius = 4;
        self.uploadCivilIdImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
        self.uploadCivilIdImgBtn.layer.borderWidth = 1;
        self.uploadCivilIdImgBtn.hidden = NO;
        self.uploadCivilBtn.hidden = YES;
    }
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)loadViewSellData
{
    NSURL * sampurl = [[NSURL alloc]init];
    
    self.ownershipBtn.selected = YES;
    self.conditionBtn.selected = YES;
    self.consignmentBtn.selected = YES;
    
    sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.civilIdStr]];

    [self.uploadCivilIdImgBtn sd_setImageWithURL:sampurl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"loading.png"]];
    
    encodedCivilId = [[NSString alloc] init];
    encodedCivilId = self.civilIdStr;
    
    self.uploadCivilIdImgBtn.layer.cornerRadius = 4;
    
    self.uploadCivilIdImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
    self.uploadCivilIdImgBtn.layer.borderWidth = 1;
    
    self.uploadCivilIdImgBtn.hidden = NO;
    self.uploadCivilBtn.hidden = YES;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self.view.window.rootViewController presentViewController:picker animated:YES completion:NULL];
    }
    
    if (buttonIndex == 1)
    {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self.view.window.rootViewController presentViewController:picker animated:YES completion:NULL];
    }
    
    if (!(buttonIndex == [actionSheet cancelButtonIndex]))
    {
        NSString *msg = nil;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *selectedImage;
    
    NSURL *mediaUrl;
    
    mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    
    if (mediaUrl == nil)
    {
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil)
        {
            selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
            NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.90f);
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",@"cached"]];
            
            [imageData writeToFile:imagePath atomically:NO];
            
            encodedCivilId = [[NSString alloc] init];
            encodedCivilId = [imageData base64EncodedStringWithOptions:0];
            
            civilImage = selectedImage;
            
            [self.uploadCivilIdImgBtn setImage:civilImage forState:UIControlStateNormal];
            self.uploadCivilIdImgBtn.layer.cornerRadius = 4;
            
            self.uploadCivilIdImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
            self.uploadCivilIdImgBtn.layer.borderWidth = 1;
            
            self.uploadCivilIdImgBtn.hidden = NO;
            self.uploadCivilBtn.hidden = YES;
        }
        else
        {
            selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
            NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0.90f);
            
            encodedCivilId = [[NSString alloc] init];
            encodedCivilId = [imageData base64EncodedStringWithOptions:0];
            civilImage = selectedImage;
            [self.uploadCivilIdImgBtn setImage:civilImage forState:UIControlStateNormal];
            self.uploadCivilIdImgBtn.layer.cornerRadius = 4;
            
            self.uploadCivilIdImgBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];
            self.uploadCivilIdImgBtn.layer.borderWidth = 1;
            
            self.uploadCivilIdImgBtn.hidden = NO;
            self.uploadCivilBtn.hidden = YES;
        }
        
    }
    
    [picker dismissModalViewControllerAnimated:YES];
}

- (IBAction)uploadCivilBtnActn:(UIButton *)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Take a new photo",
                                  @"Choose from existing",nil];
    actionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;
    [actionSheet showInView:self.view];
    if (civilImage == nil)
    {
        
    }
}

- (IBAction)deleteBtnActn:(UIButton *)sender
{
    encodedCivilId = [[NSString alloc] init];
    self.civilIdStr = [[NSString alloc]init];
    [self.uploadImageDetailBackView removeFromSuperview];
    self.uploadCivilIdImgBtn.hidden = YES;
    self.uploadCivilBtn.hidden = NO;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"cached.png"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    
}

- (IBAction)closeBtnActn:(UIButton *)sender {
    [self.uploadImageDetailBackView removeFromSuperview];
}

- (IBAction)uploadCivilIdBtnActn:(UIButton *)sender {
    //NSLog(@"%ld",(long)sender.tag);
    
    //[self.uploadImageDetailImageView setImage:civilImage];
    [self.uploadImageDetailImageView setImage:self.uploadCivilIdImgBtn.imageView.image];
    self.deleteBtn.tag = sender.tag;
    
    self.uploadImageDetailImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.view addSubview:self.uploadImageDetailBackView];
    
    [self.uploadImageDetailBackView setFrame:self.uploadImageDetailBackView.superview.bounds];
}


- (IBAction)moreInfoAction:(UIButton*)sender {
    loadingFlag = 10;
    [self.infoWebView loadHTMLString:@"" baseURL:nil];
    //self.infoWebView = [[UIWebView alloc]init];
    if(sender.tag==0)
    {
        loadingFlag = 0;
        self.confirmBtn.tag = sender.tag;
    
    NSMutableDictionary *MoreInfoData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    [ApplicationDelegate.engine SellmoreInfo:MoreInfoData CompletionHandler:^(NSMutableDictionary *responseDic) {
        
        
        if ([ApplicationDelegate isValid:responseDic])
        {
            
            if ([ApplicationDelegate isValid:[responseDic objectForKey:@"MoreInfo"]])
            {
    
                NSString *HTML = [responseDic objectForKey:@"MoreInfo"];
                [self.infoWebView loadHTMLString:HTML baseURL:nil];
                
                 [ApplicationDelegate removeProgressHUD];
                
                 NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.infoWebView.frame.size.width-10))];
                
                NSString *formattedHTMLText = [NSString stringWithFormat:@"<html> \n"
                                               "<head> \n"
                                               "<style type=\"text/css\"> \n"
                                               "body {font-family: \"%@\"; font-size: %@;} b {font-weight:bolder; font-size:%@; }\n"
                                               "</style> \n"
                                               "</head> \n"
                                               "<body><div style=\"width: %@; word-wrap: break-word\">%@</div></body> \n"
                                               "</html>", @"BREESERIF-REGULAR", [NSNumber numberWithInt:11],[NSNumber numberWithInt:11],widthVal, HTML];
             
                [self.infoWebView loadHTMLString:formattedHTMLText baseURL:nil];
                
                
            }
            
            else
            {
                
               [ApplicationDelegate removeProgressHUD];
            }
        }
        
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }];
    
    }
    
    else
    
    
    {
        loadingFlag = 1;
        self.confirmBtn.tag = sender.tag;
        
        NSMutableDictionary *TCData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine  SellTermsAndC:TCData CompletionHandler:^(NSMutableDictionary *responseDic) {
            
            if ([ApplicationDelegate isValid:responseDic])
            {
                
                if ([ApplicationDelegate isValid:[responseDic objectForKey:@"TermsAndCondition"]])
                {
                    NSString *HTML = [responseDic objectForKey:@"TermsAndCondition"];
                    [self.infoWebView loadHTMLString:HTML baseURL:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                    NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.infoWebView.frame.size.width-10))];
                    
                    NSString *formattedHTMLText = [NSString stringWithFormat:@"<html> \n"
                                                   "<head> \n"
                                                   "<style type=\"text/css\"> \n"
                                                   "body {font-family: \"%@\"; font-size: %@;} b {font-weight:bolder; font-size:%@; }\n"
                                                   "</style> \n"
                                                   "</head> \n"
                                                   "<body><div style=\"width: %@; word-wrap: break-word\">%@</div></body> \n"
                                                   "</html>", @"BREESERIF-REGULAR", [NSNumber numberWithInt:11],[NSNumber numberWithInt:11],widthVal, HTML];
                    
                    [self.infoWebView loadHTMLString:formattedHTMLText baseURL:nil];
                    
                    
                }
                
                else
                {
                    
                    [ApplicationDelegate removeProgressHUD];
                }
            }
            
        } errorHandler:^(NSError *error) {
            
            [ApplicationDelegate removeProgressHUD];
            [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        }];
        
        
        
    }
    
    [HomeViewController sharedViewController].backArrowView.hidden = YES;
    [HomeViewController sharedViewController].backBttn.hidden = YES;
    [self.view addSubview:self.moreInfo];
    [self.moreInfo setFrame:self.moreInfo.superview.bounds];
}

- (IBAction)closeBtnAction:(id)sender {
    
        [self.moreInfo removeFromSuperview];
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    
}


-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}


- (IBAction)SellDataSubmit:(id)sender {
    
    if (encodedCivilId == (id)[NSNull null] || encodedCivilId.length == 0 ) {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please upload your civil ID / passport image") title:localize(@"Sorry..!")];
    } else if (!self.ownershipBtn.selected){
        [ApplicationDelegate showAlertWithMessage:localize(@"Please confirm your ownership") title:localize(@"Sorry..!")];
    }else if (!self.conditionBtn.selected){
        [ApplicationDelegate showAlertWithMessage:localize(@"Please confirm your items condition ") title:localize(@"Sorry..!")];
    }else if (!self.consignmentBtn.selected){
        [ApplicationDelegate showAlertWithMessage:localize(@"Please confirm terms & conditions") title:localize(@"Sorry..!")];
    }else{
        
        
        NSMutableDictionary *SellUploadForm = [SellPageData mutableCopy];
        
        [SellUploadForm setObject:encodedCivilId forKey:@"IdProof"];
        
        //NSLog(@"%@",SellUploadForm);
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:SellUploadForm options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        //NSLog(@"%@",myString);
        //NSLog(@"%@",self.contextString);
                
        if ([self.contextString isEqualToString:@"initialization"])
        {
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            [ApplicationDelegate.engine SellImageUpload:SellUploadForm CompletionHandler:^(NSMutableDictionary *responseDic) {
                
                
                if ([ApplicationDelegate isValid:responseDic])
                {
                    ////////////////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    
                    //NSLog(@"%@",responseDic);
                    
                    if ([[responseDic objectForKey:@"Success"] integerValue])
                    {
//                        BillingAddressViewController * billingAddressVC = [[BillingAddressViewController alloc]initWithNibName:@"BillingAddressViewController" bundle:nil];
//                        billingAddressVC.view.backgroundColor = [UIColor clearColor];
//                        billingAddressVC.SubTotal.text=self.SubTotal.text;
//                        billingAddressVC.DeliveryCharge.text=self.DeliveryCharge.text;
//                        billingAddressVC.TotalCharge.text=self.TotalCharge.text;
//                        billingAddressVC.orderId = self.orderId;
//                        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[BillingAddressViewController class]])
//                        {
//                            //[ApplicationDelegate.homeTabNav pushViewController:billingAddressVC animated:NO];
//                        }
                        self.responseBackView.frame = self.view.frame;
                        [self.view addSubview:self.responseBackView];
                        //self.topBackView.hidden = YES;
                        //self.seperatorView.hidden = YES;
                        //self.downBackView.hidden = YES;
                        
                        [HomeViewController sharedViewController].backArrowView.hidden = YES;
                        [HomeViewController sharedViewController].backBttn.hidden = YES;
                        
                        NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.responseWebView.frame.size.width-10))];
                        
                        [self.responseWebView loadHTMLString:[responseDic objectForKey:@"Message"] baseURL:nil];
                        
                        [[HomeViewController sharedViewController] globalLabel:@"0"];
                        
                        
                        [ApplicationDelegate removeProgressHUD];
                    } else {
                        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                        
                        [ApplicationDelegate removeProgressHUD];
                    }
                    
                    ////////////////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    
                    
                    //@@@@@@@@@@@@@@@@@@@@@@@@?/////////////////////////////////
                    /*
                    if ([responseDic objectForKey:@"Success"])
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                        
                        SellViewController * SellVC = [[SellViewController alloc]initWithNibName:@"SellViewController" bundle:nil];
                        SellVC.view.backgroundColor = [UIColor clearColor];
                      //  SellVC.contextString = @"initialization";
                        
                        actionStatus = [NSUserDefaults standardUserDefaults];
                        [actionStatus setObject:@"initialization" forKey:@"Action"];
                        [actionStatus synchronize];
                        
                        
                        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SellViewController class]])
                        {
                            [ApplicationDelegate.homeTabNav pushViewController:SellVC animated:NO];
                        }
                        
                        [ApplicationDelegate removeProgressHUD];
                        
                    }
                    
                    else{
                        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                        
                        
                        [ApplicationDelegate removeProgressHUD];
                        
                    }
                    */
                    //@@@@@@@@@@@@@@@@@@@@@@@@?/////////////////////////////////
                }
                
            } errorHandler:^(NSError *error) {
                
                [ApplicationDelegate removeProgressHUD];
                [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
            }];
        }
        
        if ([self.contextString isEqualToString:@"editing"])
        {
        [ApplicationDelegate addProgressHUDToView:self.view];
            
        [ApplicationDelegate.engine SellUpdate:SellUploadForm CompletionHandler:^(NSMutableDictionary *responseDic) {
            
            
            if ([ApplicationDelegate isValid:responseDic])
            {
                
                ////////////////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                
                //NSLog(@"%@",responseDic);
                
                if ([[responseDic objectForKey:@"Success"] integerValue])
                {
                    //                        BillingAddressViewController * billingAddressVC = [[BillingAddressViewController alloc]initWithNibName:@"BillingAddressViewController" bundle:nil];
                    //                        billingAddressVC.view.backgroundColor = [UIColor clearColor];
                    //                        billingAddressVC.SubTotal.text=self.SubTotal.text;
                    //                        billingAddressVC.DeliveryCharge.text=self.DeliveryCharge.text;
                    //                        billingAddressVC.TotalCharge.text=self.TotalCharge.text;
                    //                        billingAddressVC.orderId = self.orderId;
                    //                        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[BillingAddressViewController class]])
                    //                        {
                    //                            //[ApplicationDelegate.homeTabNav pushViewController:billingAddressVC animated:NO];
                    //                        }
                    //NSLog(@"%f",self.view.frame.size.height);
                    //NSLog(@"%f",self.responseBackView.frame.size.height);
                    self.responseBackView.frame = self.view.frame;
                    [self.view addSubview:self.responseBackView];
                    //self.topBackView.hidden = YES;
                    //self.seperatorView.hidden = YES;
                    //self.downBackView.hidden = YES;
                    
                    [HomeViewController sharedViewController].backArrowView.hidden = YES;
                    [HomeViewController sharedViewController].backBttn.hidden = YES;
                    
                    NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.responseWebView.frame.size.width-10))];
                    
                    [self.responseWebView loadHTMLString:[responseDic objectForKey:@"Message"] baseURL:nil];
                    
                    [[HomeViewController sharedViewController] globalLabel:@"0"];
                    
                    [ApplicationDelegate removeProgressHUD];
                } else {
                    [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                }
                
                ////////////////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                /*
                if ([responseDic objectForKey:@"Success"])
                {
                    [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    
                    SellViewController * SellVC = [[SellViewController alloc]initWithNibName:@"SellViewController" bundle:nil];
                    SellVC.view.backgroundColor = [UIColor clearColor];
                    actionStatus = [NSUserDefaults standardUserDefaults];
                    [actionStatus setObject:@"initialization" forKey:@"Action"];
                    [actionStatus synchronize];
                    
                    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[SellViewController class]])
                    {
                        [ApplicationDelegate.homeTabNav pushViewController:SellVC animated:NO];
                    }
                    
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                }
                
                else{
                    [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                }
                 */
            }
            
        } errorHandler:^(NSError *error) {
            
            [ApplicationDelegate removeProgressHUD];
            [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        }];
    }
    }
    
}


- (IBAction)ownershipBtnActn:(UIButton *)sender {
    self.ownershipBtn.selected = !self.ownershipBtn.selected;
}

- (IBAction)conditionBtnActn:(UIButton *)sender {
    self.conditionBtn.selected = !self.conditionBtn.selected;
}

- (IBAction)consignmentBtnActn:(UIButton *)sender {
    self.consignmentBtn.selected = !self.consignmentBtn.selected;
}

- (IBAction)confirmBtnActn:(UIButton *)sender {
    if(sender.tag==0)
    {
        [self.moreInfo removeFromSuperview];
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
        self.conditionBtn.selected = YES;
    }
    else
    {
        [self.moreInfo removeFromSuperview];
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
        self.consignmentBtn.selected = YES;
    }
}
- (IBAction)cancelBtnActn:(UIButton *)sender {
    
    NewDealsViewController *dealsVc = [[NewDealsViewController alloc] initWithNibName:@"NewDealsViewController" bundle:nil];
    dealsVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[dealsVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:dealsVc animated:NO];
    }
    
}

- (IBAction)backToHomeBtnActn:(UIButton *)sender {
    [HomeViewController sharedViewController].sellButton.selected=NO;
    [HomeViewController sharedViewController].moreButton.selected=NO;
    [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (loadingFlag == 10) {
        [ApplicationDelegate addProgressHUDToView:self.view];
    }
    else{
        [ApplicationDelegate addProgressHUDToView:self.view];
        //[ApplicationDelegate removeProgressHUD];
    }
    
    
    //-- Add further custom actions if needed
}

@end
