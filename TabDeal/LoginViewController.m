//
//  LoginViewController.m
//  TabDeal
//
//  Created by satheesh on 9/17/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "LoginViewController.h"
#import "CreateNewAccountViewController.h"
#import "AppDelegate.h"
#import "UserData.h"
#import <TwitterKit/TwitterKit.h>

static LoginViewController *sharedlogObj = NULL;

@interface LoginViewController (){
    CreateNewAccountViewController* createNewAccObj;
    UIViewController *mainView;
    HomeViewController * homePageViewObj;
}

@end

@implementation LoginViewController

-(void)viewWillAppear:(BOOL)animated{
    
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        ApplicationDelegate.language=kARABIC;
        [[Localization sharedInstance] setPreferred:@"ar" fallback:@"en"];
    }
    else
    {
        ApplicationDelegate.language=kENGLISH;
        [[Localization sharedInstance] setPreferred:@"en" fallback:@"ar"];
    }
    self.logInToYourAccountLbl.text = localize(@"LOGIN TO YOUR ACCOUNT");
    self.emailHeadingLbl.text = localize(@"Email");
    self.emailTextView.placeholder = localize(@"Email id ");
    self.passwordHeadingLbl.text = localize(@"Password");
    self.passwordTextView.placeholder = localize(@"Password");
    [self.loginBttn setTitle:localize(@"LOGIN") forState:UIControlStateNormal];
    [self.registerBttn setTitle:localize(@"REGISTER") forState:UIControlStateNormal];
    self.keepMeSignInLbl.text = localize(@"Keep me signed in");
    [self.forgotPasswordBtn setTitle:localize(@"Forgot Password?") forState:UIControlStateNormal];
    
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    self.emailIMg.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordIMg.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    FBSession* session = [FBSession activeSession];
    [session closeAndClearTokenInformation];
    [session close];
    [FBSession setActiveSession:nil];
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL         URLWithString:@"https://facebook.com/"]];
    
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
    NSURL *urltweet = [NSURL URLWithString:@"https://api.twitter.com"];
    NSHTTPCookieStorage* cookiestw = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookiestweet = [cookiestw cookiesForURL:[NSURL         URLWithString:@"https://api.twitter.com"]];
    for (NSHTTPCookie *cookie in cookiestweet)
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    
    self.navigationController.navigationBarHidden = YES;
    self.loginBackView.layer.cornerRadius = 4;
    self.emailBackView.layer.cornerRadius = 3;
    self.passwordBackView.layer.cornerRadius = 3;
    self.loginBttn.layer.cornerRadius = 3;
    self.registerBttn.layer.cornerRadius = 3;
    
    self.emailTextView.delegate = self;
    self.passwordTextView.delegate=self;
    [self.keepMeSignInBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.keepMeSignInBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    
    userDataStore = [NSUserDefaults standardUserDefaults];
    /*
    if ([[userDataStore objectForKey:@"keepMeLogin"]isEqualToString:@"Ticked"]) {
        self.keepMeSignInBttn.selected = YES;
    } else {
        [self.keepMeSignInBttn setSelected:NO];
    }
     */
    
    self.keepMeSignInBttn.selected = YES;
    [self.signinwithFBBttn setReadPermissions:@[@"public_profile"]];
    [self.signinwithFBBttn setDelegate:self];
    
    self.postFbDic = [[NSMutableDictionary alloc] init];
    self.postTweetDic = [[NSMutableDictionary alloc] init];
}

-(void)viewDidAppear:(BOOL)animated{
    [self xibLoading];
    
    self.logInToYourAccountLbl.text = localize(@"LOGIN TO YOUR ACCOUNT");
    self.emailHeadingLbl.text = localize(@"Email");
    self.emailTextView.placeholder = localize(@"Email id ");
    self.passwordHeadingLbl.text = localize(@"Password");
    self.passwordTextView.placeholder = localize(@"Password");
    [self.loginBttn setTitle:localize(@"LOGIN") forState:UIControlStateNormal];
    [self.registerBttn setTitle:localize(@"REGISTER") forState:UIControlStateNormal];
    self.keepMeSignInLbl.text = localize(@"Keep me signed in");
    [self.forgotPasswordBtn setTitle:localize(@"Forgot Password?") forState:UIControlStateNormal];
    
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    self.emailIMg.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordIMg.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    FBSession* session = [FBSession activeSession];
    [session closeAndClearTokenInformation];
    [session close];
    [FBSession setActiveSession:nil];
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL         URLWithString:@"https://facebook.com/"]];
    
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
    NSURL *urltweet = [NSURL URLWithString:@"https://api.twitter.com"];
    NSHTTPCookieStorage* cookiestw = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookiestweet = [cookiestw cookiesForURL:[NSURL         URLWithString:@"https://api.twitter.com"]];
    for (NSHTTPCookie *cookie in cookiestweet)
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    
    self.navigationController.navigationBarHidden = YES;
    self.loginBackView.layer.cornerRadius = 4;
    self.emailBackView.layer.cornerRadius = 3;
    self.passwordBackView.layer.cornerRadius = 3;
    self.loginBttn.layer.cornerRadius = 3;
    self.registerBttn.layer.cornerRadius = 3;
    
    self.emailTextView.delegate = self;
    self.passwordTextView.delegate=self;
    [self.keepMeSignInBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.keepMeSignInBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    
    userDataStore = [NSUserDefaults standardUserDefaults];
    /*
    if ([[userDataStore objectForKey:@"keepMeLogin"]isEqualToString:@"Ticked"]) {
        self.keepMeSignInBttn.selected = YES;
    } else {
        [self.keepMeSignInBttn setSelected:NO];
    }
     */
    
    self.keepMeSignInBttn.selected = YES;
    
    [self.signinwithFBBttn setReadPermissions:@[@"public_profile"]];
    [self.signinwithFBBttn setDelegate:self];
    
    self.postFbDic = [[NSMutableDictionary alloc] init];
    self.postTweetDic = [[NSMutableDictionary alloc] init];
    
    self.EmailPopupInnerView.layer.cornerRadius = 5;
    self.EmailPopupInnerView.layer.masksToBounds = YES;
    self.EmailSubmitOutlet.backgroundColor=[UIColor colorWithHex:kUNIVERSAL_Button_COLOR];
    self.EmailSubmitOutlet.titleLabel.font = EBRI_NORMAL_FONT(15);
    self.EmailSubmitOutlet.layer.cornerRadius = 5;
    self.EmailSubmitOutlet.layer.masksToBounds = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.EmailPopup.hidden = YES;
    
    NSLog(@"%@",[Twitter sharedInstance].session);
    
    if (!(self.navigationController)) {
        //[[Twitter sharedInstance] logOut];
    }
    [[Localization sharedInstance] setPreferred:@"en" fallback:@"ar"];
}


-(void)xibLoading
{
    NSString *nibName = @"LoginViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    self.view = aView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerBtnActn:(UIButton *)sender {
    createNewAccObj = [[CreateNewAccountViewController alloc]initWithNibName:@"CreateNewAccountViewController" bundle:nil];
    if (!(self.navigationController)) {
        [self presentViewController:createNewAccObj animated:NO completion:nil];
    }
    else{
        [self.navigationController pushViewController:createNewAccObj animated:NO];
    }
}

- (IBAction)loginBttnActn:(UIButton *)sender {
    if ((self.emailTextView.text.length == 0) && (self.passwordTextView.text.length == 0)) {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter email id and password") title:localize(@"Sorry..!")];
        
    } else if(self.emailTextView.text.length == 0){
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter email") title:localize(@"Sorry..!")];
        
    }else if(self.passwordTextView.text.length == 0){
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter password") title:localize(@"Sorry..!")];
        
    }else{
        if ([self NSStringIsValidEmail:self.emailTextView.text]) {
            [self loginUser];
        } else {
            
            [ApplicationDelegate showAlertWithMessage:localize(@"Please enter valid email id") title:localize(@"Sorry..!")];
        }
    }
}

- (IBAction)keepMeSignedInBttnActn:(UIButton *)sender {
    self.keepMeSignInBttn.selected = !self.keepMeSignInBttn.selected;
}

- (IBAction)emailtext:(UITextField *)sender {
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.emailIMg.image = [UIImage imageNamed:@"user_sel-1.png"];
    self.passwordIMg.image = [UIImage imageNamed:@"password-2.png"];
}

-(NSMutableDictionary *)getUserSignInPostData {
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.emailTextView.text forKey:@"Email"];
    [postDic setObject:self.passwordTextView.text forKey:@"Password"];
    
    return postDic;
}

#pragma NSURLConnection Delegates

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (!self.receivedData){
        self.receivedData = [NSMutableData data];
    }
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *responseString = [[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",responseString);
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    
    if ([user objectForKey:@"mobile"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"MobileNumber"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"mobile"] forKey:@"MobileNumber"];
    }
    if ([user objectForKey:@"email"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"Email"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"email"] forKey:@"Email"];
        
    }
    if ([user objectForKey:@"id"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"Id"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"id"] forKey:@"Id"];
    }
    if ([user objectForKey:@"first_name"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"first_name"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"first_name"] forKey:@"FirstName"];
    }
    if ([user objectForKey:@"last_name"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"last_name"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"last_name"] forKey:@"LastName"];
    }
    if ([user objectForKey:@"name"]==nil) {
        [self.postFbDic setObject:@"" forKey:@"name"];
    } else {
        [self.postFbDic setObject:[user objectForKey:@"name"] forKey:@"UserName"];
    }
    
    if ([self.postFbDic objectForKey:@"Email"]== [NSNull null]||[[self.postFbDic objectForKey:@"Email"] isEqualToString:@""]) {
        self.EmailPopup.hidden = NO;
        self.EmailSubmitOutlet.tag=1001;
    }
    
    else{
        
        [self LoginwithFacebook];
    }
    
    
}

-(void)loginUser
{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *signInData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserSignInPostData]];
    
    [ApplicationDelegate.engine loginUserWithLoginDataDictionary:signInData CompletionHandler:^(NSMutableDictionary *responseDic) {
        
        {
            if ([ApplicationDelegate isValid:responseDic])
            {
                if (responseDic.count>0)
                {
                    if ([[responseDic objectForKey:@"Success"] integerValue])
                    {
                        self.emailTextView.text=@"";
                        self.passwordTextView.text=@"";
                        if( self.keepMeSignInBttn.selected)
                        {
                            User *userObj = [ApplicationDelegate.mapper getUserObjectFromDictionary:[[responseDic objectForKey:@"users"] objectAtIndex:0]];
                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                            [prefs setObject:userObj.email forKey:@"Email"];
                            [prefs setObject:userObj.firstname forKey:@"First Name"];
                            [prefs setObject:userObj.languagekey forKey:@"Language Key"];
                            [prefs setObject:userObj.lastName forKey:@"Last Name"];
                            [prefs setObject:userObj.userId forKey:@"User Id"];
                            [prefs setObject:userObj.mobilenumber forKey:@"Mobile Number"];
                            [prefs setObject:@"login" forKey:@"loginflag"];
                            [prefs synchronize];
                            setuser(userObj.userId);
                            NSLog(@"%@",[prefs objectForKey:@"User Id"]);
                        }
                        else{
                            User *userObj = [ApplicationDelegate.mapper getUserObjectFromDictionary:[[responseDic objectForKey:@"users"] objectAtIndex:0]];
                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                            [prefs setObject:userObj.email forKey:@"Email"];
                            [prefs setObject:userObj.firstname forKey:@"First Name"];
                            [prefs setObject:userObj.languagekey forKey:@"Language Key"];
                            [prefs setObject:userObj.lastName forKey:@"Last Name"];
                            [prefs setObject:userObj.userId forKey:@"User Id"];
                            [prefs setObject:userObj.mobilenumber forKey:@"Mobile Number"];
                            [prefs setObject:@"logout" forKey:@"loginflag"];
                            [prefs synchronize];
                            NSLog(@"%@",[prefs objectForKey:@"User Id"]);
                            setuser(userObj.userId);
                        }
                        
                        homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
                        if (!(self.navigationController)) {
                            [self dismissViewControllerAnimated:NO completion:nil];
                        }
                        else{
                            [self.navigationController pushViewController:homePageViewObj animated:NO];
                        }
                        
                        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    }
                    
                    else
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                        
                        [ApplicationDelegate removeProgressHUD];
                    }
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                }
            }
        }
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}


- (IBAction)forgetPassword:(id)sender {
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    if(self.emailTextView.text.length != 0)
    {
        
        NSMutableDictionary *ForgetEmailData = [[NSMutableDictionary alloc] initWithDictionary:[self getForgetPasswordPostData]];
        
        
        [ApplicationDelegate.engine forgetPasswordDataDictionary:ForgetEmailData CompletionHandler:^(NSMutableDictionary *responseDictionary) {
            
            
            if ([ApplicationDelegate isValid:responseDictionary])
            {
                
                if (responseDictionary.count>0)
                {
                    if ([responseDictionary objectForKey:@"Success"])
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                        
                        [ApplicationDelegate removeProgressHUD];
                    }
                    else
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                        
                        [ApplicationDelegate removeProgressHUD];
                    }
                    
                }
            }
            
        } errorHandler:^(NSError *error) {
            
            [ApplicationDelegate removeProgressHUD];
            [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        }];
        
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please Enter Email") title:nil];
        
        [ApplicationDelegate removeProgressHUD];
    }
    
    self.emailTextView.text=@"";
}


-(NSMutableDictionary *)getForgetPasswordPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.emailTextView.text forKey:@"Email"];
    
    return postDic;
}

- (IBAction)passwordtext:(UITextField *)sender {
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
    self.emailIMg.image = [UIImage imageNamed:@"user-1.png"];
    self.passwordIMg.image = [UIImage imageNamed:@"password_sel-1.png"];
}


-(void)LoginwithFacebook
{
    NSMutableDictionary *FacebookUserData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserFacebookPostData]];
    
    NSLog(@"FFFFFFF%@",FacebookUserData);
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    [ApplicationDelegate.engine loginwithFacebookDataDictionary:FacebookUserData CompletionHandler:^(NSMutableDictionary *responseDictionary) {
        
        
        
        if ([ApplicationDelegate isValid:responseDictionary])
        {
            if (responseDictionary.count>0)
            {
                if ([responseDictionary objectForKey:@"Success"])
                {
                    
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                    if( self.keepMeSignInBttn.selected)
                    {
                        User *userObj = [ApplicationDelegate.mapper getUserObjectFromDictionary:[[responseDictionary objectForKey:@"users"] objectAtIndex:0]];
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:userObj.email forKey:@"Email"];
                        [prefs setObject:userObj.firstname forKey:@"First Name"];
                        [prefs setObject:userObj.languagekey forKey:@"Language Key"];
                        [prefs setObject:userObj.lastName forKey:@"Last Name"];
                        [prefs setObject:userObj.userId forKey:@"User Id"];
                        [prefs setObject:userObj.mobilenumber forKey:@"Mobile Number"];
                        [prefs setObject:@"login" forKey:@"loginflag"];
                        [prefs synchronize];
                        setuser(userObj.userId);
                        NSLog(@"%@",[prefs objectForKey:@"User Id"]);
                    }
                    else{
                        User *userObj = [ApplicationDelegate.mapper getUserObjectFromDictionary:[[responseDictionary objectForKey:@"users"] objectAtIndex:0]];
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:userObj.email forKey:@"Email"];
                        [prefs setObject:userObj.firstname forKey:@"First Name"];
                        [prefs setObject:userObj.languagekey forKey:@"Language Key"];
                        [prefs setObject:userObj.lastName forKey:@"Last Name"];
                        [prefs setObject:userObj.userId forKey:@"User Id"];
                        [prefs setObject:userObj.mobilenumber forKey:@"Mobile Number"];
                        [prefs setObject:@"logout" forKey:@"loginflag"];
                        [prefs synchronize];
                        setuser(userObj.userId);
                        NSLog(@"%@",[prefs objectForKey:@"User Id"]);
                    }
                    
                    homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
                    if (!(self.navigationController)) {
                        [self dismissViewControllerAnimated:NO completion:nil];
                    }
                    else{
                        [self.navigationController pushViewController:homePageViewObj animated:NO];
                    }
                }
                
                else{
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                }
            }
        }
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        
    }];
}

-(NSMutableDictionary *)getUserFacebookPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[self.postFbDic objectForKey:@"Email"] forKey:@"Email"];
    [postDic setObject:[self.postFbDic objectForKey:@"FirstName"] forKey:@"FirstName"];
    [postDic setObject:[self.postFbDic objectForKey:@"LastName"] forKey:@"LastName"];
    [postDic setObject:[self.postFbDic objectForKey:@"Id"] forKey:@"UserName"];
    return postDic;
}


-(void)LoginwithTwitter
{
    NSMutableDictionary *TwitterUserData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserTwitterPostData]];
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    [ApplicationDelegate.engine loginwithTwitterDataDictionary:TwitterUserData CompletionHandler:^(NSMutableDictionary *responseDictionary) {
        
        
        
        if ([ApplicationDelegate isValid:responseDictionary])
        {
            if (responseDictionary.count>0)
            {
                NSLog(@"%@",responseDictionary);
                if ([responseDictionary objectForKey:@"Success"])
                {
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    
                    if( self.keepMeSignInBttn.selected)
                    {
                        User *userObj = [ApplicationDelegate.mapper getUserObjectFromDictionary:[[responseDictionary objectForKey:@"users"] objectAtIndex:0]];
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:userObj.email forKey:@"Email"];
                        [prefs setObject:userObj.firstname forKey:@"First Name"];
                        [prefs setObject:userObj.languagekey forKey:@"Language Key"];
                        [prefs setObject:userObj.lastName forKey:@"Last Name"];
                        [prefs setObject:userObj.userId forKey:@"User Id"];
                        [prefs setObject:userObj.mobilenumber forKey:@"Mobile Number"];
                        [prefs setObject:@"login" forKey:@"loginflag"];
                        [prefs synchronize];
                        setuser(userObj.userId);
                        NSLog(@"%@",[prefs objectForKey:@"User Id"]);
                    }
                    else{
                        User *userObj = [ApplicationDelegate.mapper getUserObjectFromDictionary:[[responseDictionary objectForKey:@"users"] objectAtIndex:0]];
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:userObj.email forKey:@"Email"];
                        [prefs setObject:userObj.firstname forKey:@"First Name"];
                        [prefs setObject:userObj.languagekey forKey:@"Language Key"];
                        [prefs setObject:userObj.lastName forKey:@"Last Name"];
                        [prefs setObject:userObj.userId forKey:@"User Id"];
                        [prefs setObject:userObj.mobilenumber forKey:@"Mobile Number"];
                        [prefs setObject:@"logout" forKey:@"loginflag"];
                        [prefs synchronize];
                        setuser(userObj.userId);
                        NSLog(@"%@",[prefs objectForKey:@"User Id"]);
                    }
                    if (!(self.navigationController)) {
                        [self dismissViewControllerAnimated:NO completion:nil];
                    }
                    else{
                        homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
                        [self.navigationController pushViewController:homePageViewObj animated:NO];
                    }
                }
                
                else{
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    [ApplicationDelegate removeProgressHUD];
                }
            }
        }
    } errorHandler:^(NSError *error) {
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }];
}

-(NSMutableDictionary *)getUserTwitterPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[self.postTweetDic objectForKey:@"FirstName"] forKey:@"FirstName"];
    [postDic setObject:[self.postTweetDic objectForKey:@"Id"] forKey:@"UserName"];
    [postDic setObject:[self.postTweetDic objectForKey:@"Email"] forKey:@"Email"];
    return postDic;
}

- (IBAction)twitterLoginBtnActn:(UIButton *)sender {
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
            NSLog(@"%@",[session userID]);
            
            if ([session userID]==nil) {
                [self.postTweetDic setObject:@"" forKey:@"Id"];
            } else {
                [self.postTweetDic setObject:[session userID] forKey:@"Id"];
            }
            if ([session userName]==nil) {
                [self.postTweetDic setObject:@"" forKey:@"first_name"];
            } else {
                [self.postTweetDic setObject:[session userName] forKey:@"FirstName"];
            }
            if ([session userName]==nil) {
                [self.postTweetDic setObject:@"" forKey:@"last_name"];
            } else {
                [self.postTweetDic setObject:[session userName] forKey:@"LastName"];
            }
            if ([session userID]==nil) {
                [self.postTweetDic setObject:@"" forKey:@"name"];
            } else {
                [self.postTweetDic setObject:[session userID] forKey:@"UserID"];
            }
            
            self.EmailSubmitOutlet.tag=1000;
            self.EmailPopup.hidden = NO;
            
        }
        
        else
        {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
}

- (IBAction)languageSelection:(id)sender {
    
    ApplicationDelegate.changeLanguage;
    [self viewDidAppear:YES];
    
}


- (IBAction)SetEmail:(id)sender {
    
    
    
    if ([self NSStringIsValidEmail:self.EmailPopuptxt.text]) {
        
        
        if([sender tag] == 1001)
        {
            [self.postFbDic setObject:self.EmailPopuptxt.text forKey:@"Email"];
            
            [self LoginwithFacebook];
        }
        
        else
        {
            [self.postTweetDic setObject:self.EmailPopuptxt.text forKey:@"Email"];
            
            [self LoginwithTwitter];
        }
        
        
    } else {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter valid email id") title:localize(@"Sorry..!")];
    }
    
}
@end
