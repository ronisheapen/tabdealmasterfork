//
//  MyBasketCell.m
//  TabDeal
//
//  Created by Sreeraj VR on 15/10/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "MyBasketCell.h"

@implementation MyBasketCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"MyBasketCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (MyBasketCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"MyBasketCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (MyBasketCell*)[nibViews objectAtIndex: 0];
        }
        
    }
    return self;
}


- (IBAction)deleteItem:(id)sender
{
    [self.delegateBasketItem deleteDidClicked:self];
}

- (IBAction)editItem:(id)sender
{
    [self.delegateBasketItem editDidClicked:self];
}

- (IBAction)itemDetails:(id)sender
{
    [self.delegateBasketItem detailsDidClicked:self];
}
@end
