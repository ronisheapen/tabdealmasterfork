//
//  SubmittedItemsCell_a.h
//  TabDeal
//
//  Created by satheesh on 11/26/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmittedItemsCell_a : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *brandName;
@property (strong, nonatomic) IBOutlet UILabel *itemName;
@property (strong, nonatomic) NSString * refferenceNumber;
@property (strong, nonatomic) IBOutlet UILabel *refNumber;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UIImageView *subimage;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@property (weak, nonatomic) IBOutlet UILabel *referenceNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;

@end
