//
//  AboutTabDealViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "AboutTabDealViewController.h"

@interface AboutTabDealViewController ()

@end

@implementation AboutTabDealViewController

-(void)viewWillAppear:(BOOL)animated{
    [self loadPageDetails];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.aboutUsHeadingLbl.text = localize(@"ABOUT US");
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadPageDetails
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getPageDetailsWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     self.aboutBackView.hidden = NO;
                     AboutTabDealData *aboutPageItem = [ApplicationDelegate.mapper getAboutUsPageDetailFromDictionary:responseDictionary];
                     NSLog(@"%@",aboutPageItem.aboutUsPageData);
                     NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.aboutUsWebView.frame.size.width-10))];
                     
                     
                     NSString *formattedHTMLText = [NSString stringWithFormat:@"<html> \n"
                                                    "<head> \n"
                                                    "<style type=\"text/css\"> \n"
                                                    "body {font-family: \"%@\"; font-size: %@;} b {font-weight:bolder; font-size:%@; }\n"
                                                    "</style> \n"
                                                    "</head> \n"
                                                    "<body><div style=\"width: %@; word-wrap: break-word\">%@</div></body> \n"
                                                    "</html>", @"BREESERIF-REGULAR", [NSNumber numberWithInt:11],[NSNumber numberWithInt:11],widthVal, aboutPageItem.aboutUsPageData];
                     //[self.aboutUsWebView loadHTMLString:[NSString stringWithFormat:@"<div style=\"width: %@; word-wrap: break-word; color: #ffffff;\">%@</div>",widthVal,formattedHTMLText] baseURL:nil];
                      [self.aboutUsWebView loadHTMLString:aboutPageItem.aboutUsPageData baseURL:nil];
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
         
    
}

-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
//    [postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    //[postDic setObject:@"faq" forKey:@"PageName"];
    return postDic;
}
@end
