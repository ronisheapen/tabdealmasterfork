//
//  PreviousOrdersViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "PreviousOrdersViewController.h"
#import "PreviousOrderCell.h"
#import "PreviousOrderDetailViewController.h"
#import "UserPreviousOrder.h"
#import "SDWebImage/UIImageView+WebCache.h"


@interface PreviousOrdersViewController (){
    UIView * customSeperator;
}

@end

@implementation PreviousOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headingLbl.text = localize(@"PREVIOUS ORDERS :");
    self.warningLbl.text = localize(@"No previous order items in your cart!");
    [self.headingLbl setFont:EBRI_NORMAL_FONT(17)];
    [self.warningLbl setFont:EBRI_NORMAL_FONT(15)];
    
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.headingLbl.textAlignment = NSTextAlignmentRight;
    }
    else{
        self.headingLbl.textAlignment = NSTextAlignmentLeft;
    }
    // Do any additional setup after loading the view from its nib.
    
    [self fetchUserPreviousOrderDataformSever];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.PreviousOrderDataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    PreviousOrderCell *cell = (PreviousOrderCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PreviousOrderCell_a" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else{
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PreviousOrderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    
      UserPreviousOrder *userPreOrderObj = [ApplicationDelegate.mapper getUserPreviousOrderFromDictionary:[self.PreviousOrderDataArray objectAtIndex:indexPath.row]];
    cell.orderNumber.text = localize(@"Order No :");
    cell.orderDate.text = localize(@"Order date :");
    cell.orderStatus.text = localize(@"Status :");
    cell.orderStatusLabel.text=userPreOrderObj.status;
    
    [cell.orderNumber setFont:EBRI_NORMAL_FONT(13)];
     [cell.orderNumberLabel setFont:EBRI_NORMAL_FONT(13)];
    [cell.orderDate setFont:EBRI_NORMAL_FONT(13)];
     [cell.orderDateLabel setFont:EBRI_NORMAL_FONT(13)];
     [cell.orderStatus setFont:EBRI_NORMAL_FONT(13)];
    [cell.orderStatusLabel setFont:EBRI_BOLD_FONT(14)];
    cell.orderNumber .textColor = [UIColor colorWithHex:kUNIVERSAL_BROWN_COLOR];
    cell.orderNumberLabel .textColor = [UIColor colorWithHex:kUNIVERSAL_BROWN_COLOR];
    cell.orderDate .textColor = [UIColor colorWithHex:kUNIVERSAL_BROWN_COLOR];
    cell.orderDateLabel .textColor = [UIColor colorWithHex:kUNIVERSAL_BROWN_COLOR];
    cell.orderStatus .textColor = [UIColor colorWithHex:kUNIVERSAL_BROWN_COLOR];
    cell.orderNumberLabel.text=userPreOrderObj.ordernumber;
    NSURL * Imageurl = [[NSURL alloc]init];
    
    Imageurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:userPreOrderObj.image]];
    
    
    [cell.previousOrderImage sd_setImageWithURL:Imageurl placeholderImage:[UIImage imageNamed:@"loading.png"]];

    cell.orderDateLabel.text=userPreOrderObj.date;
    
    customSeperator=[[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height-1,cell.frame.size.width,1)];
    customSeperator.backgroundColor=[UIColor lightGrayColor];
    
    [cell.contentView addSubview:customSeperator];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
   
    PreviousOrderDetailViewController * orderDetailsVC = [[PreviousOrderDetailViewController alloc]initWithNibName:@"PreviousOrderDetailViewController" bundle:nil];
    orderDetailsVC.view.backgroundColor = [UIColor clearColor];
 
    UserPreviousOrder *userPreOrderObj = [ApplicationDelegate.mapper getUserPreviousOrderFromDictionary:[self.PreviousOrderDataArray objectAtIndex:indexPath.row]];
    
    NSLog(@"%@",userPreOrderObj.ordernumber);
    actionTracking = [NSUserDefaults standardUserDefaults];
    [actionTracking setObject:userPreOrderObj.ordernumber forKey:@"userPreOrder"];
    
    [actionTracking synchronize];
                         
   orderDetailsVC.orderId=userPreOrderObj.ordernumber;
    
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[PreviousOrderDetailViewController class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:orderDetailsVC animated:NO];
    }
}



-(void)fetchUserPreviousOrderDataformSever{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserData]];
[ApplicationDelegate.engine loadUsersPreviousOrderDictionary:userData CompletionHandler:^(NSMutableArray *responseArr) {
        
        {
            
            if ([ApplicationDelegate isValid:responseArr])
            {
                
                
                self.PreviousOrderDataArray = responseArr;
                //self.PreviousOrderDataArray = [[NSMutableArray alloc]init];
                
                if (self.PreviousOrderDataArray.count == 0) {
                    self.warningBackView.hidden = NO;
                    self.warningLbl.font = [UIFont fontWithName:@"Ebrima" size:15];
                    self.warningLbl.hidden = NO;
                } else {
                    [self.previousOrderTable reloadData];
                }
                
            }
            
            [ApplicationDelegate removeProgressHUD];
            
        }
    } errorHandler:^(NSError *error) {
        
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        
    }];
}



-(NSMutableDictionary *)getUserData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}


@end
