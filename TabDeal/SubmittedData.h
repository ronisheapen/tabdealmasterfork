//
//  SubmittedData.h
//  TabDeal
//
//  Created by Sreeraj VR on 13/11/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubmittedData : NSObject

@property(nonatomic,retain)NSString *BrandName;
@property(nonatomic,retain)NSString *Date;
@property(nonatomic,retain)NSString *Image;
@property(nonatomic,retain)NSString *RefferenceNumber;
@property(nonatomic,retain)NSString *Status;
@property(nonatomic,retain)NSString *Type;
@end
