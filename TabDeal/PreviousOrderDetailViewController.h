//
//  PreviousOrderDetailViewController.h
//  TabDeal
//
//  Created by satheesh on 10/8/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviousOrderDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSUserDefaults * actionTracking;
}
@property (weak, nonatomic) IBOutlet UITableView *previousOrderDetailTableview;

@property (strong, nonatomic)NSString * orderId;
@property (strong, nonatomic) NSMutableDictionary * PreviousOrderDetailsDic;
@property (strong, nonatomic) IBOutlet UILabel *subTotal;
@property (strong, nonatomic) IBOutlet UILabel *deliveryCharge;
@property (strong, nonatomic) IBOutlet UILabel *total;
@property (strong, nonatomic) NSMutableArray * PreviousOrderDetailsList;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *headingLbl;

@property (weak, nonatomic) IBOutlet UILabel *subTotalLbl;
@property (weak, nonatomic) IBOutlet UILabel *deliveryChargesLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalLbl;

@end
