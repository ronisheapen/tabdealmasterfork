//
//  NotificationScrollViewCell.m
//  TabDeal
//
//  Created by satheesh on 1/28/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "NotificationScrollViewCell.h"

@implementation NotificationScrollViewCell
@synthesize imgView;
@synthesize dateLbl;
@synthesize desWebView;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"NotificationScrollViewCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (NotificationScrollViewCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

@end
