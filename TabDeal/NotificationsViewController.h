//
//  NotificationsViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationScrollViewCell.h"

@interface NotificationsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIWebViewDelegate>{
    NSUserDefaults * actionTracking;
}
@property (weak, nonatomic) IBOutlet UITableView *notifocationTable;
@property (weak, nonatomic) IBOutlet UILabel *headingLbl;
@property (weak, nonatomic) IBOutlet UILabel *warningLbl;
@property (strong, nonatomic) NSMutableArray * notificationListArray;
@property (weak, nonatomic) IBOutlet UIImageView *warningBackView;
@property (weak, nonatomic) IBOutlet UIScrollView *notificationScrollView;
@property (strong, nonatomic) NotificationScrollViewCell * CellView;
@end
