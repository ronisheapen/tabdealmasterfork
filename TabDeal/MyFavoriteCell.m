//
//  MyFavoriteCell.m
//  TabDeal
//
//  Created by Sreeraj VR on 21/10/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "MyFavoriteCell.h"

@implementation MyFavoriteCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"MyFavoriteCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (MyFavoriteCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"MyFavoriteCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (MyFavoriteCell*)[nibViews objectAtIndex: 0];
        }
    }
    return self;
}



- (IBAction)deleteButtonAction:(id)sender {
    
    [self.delegateFavItem deleteDidClicked:self];
    

}
- (IBAction)detailsButtonAction:(id)sender {
    
   [self.delegateFavItem detailsDidClicked:self]; 
    
}
@end
