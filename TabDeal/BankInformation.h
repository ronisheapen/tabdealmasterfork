//
//  BankInformation.h
//  TabDeal
//
//  Created by satheesh on 11/2/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankInformation : NSObject
@property(nonatomic,retain)NSString *bankAddress;
@property(nonatomic,retain)NSString *bankName;
@property(nonatomic,retain)NSString *country;
@property(nonatomic,retain)NSString *fullName;
@property(nonatomic,retain)NSString *iBAN;
@property(nonatomic,retain)NSString *swiftCode;
@property(nonatomic,retain)NSString *userId;
@property(nonatomic,retain)NSString *lang_key;
@end
