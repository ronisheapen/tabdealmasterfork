//
//  NewDealsDetailsViewController.m
//  TabDeal
//
//  Created by satheesh on 10/1/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "NewDealsDetailsViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SDWebImage/UIButton+WebCache.h"


#define OFFSETHEIGHT 3
#define OFFSETWIDTH 3
static int bannerCount = 0;
@interface NewDealsDetailsViewController ()

@end

@implementation NewDealsDetailsViewController

@synthesize productId;
@synthesize mainScrollView;
@synthesize frameBackView;
@synthesize middleScrollView;
@synthesize middleScrollTopView;
@synthesize middleScrollDownView;
@synthesize downScrollBackView;
@synthesize youMightBeLabel;
@synthesize downScrollView;
@synthesize downScrollDummyFrame;

-(void)viewWillAppear:(BOOL)animated
{
    mainScrollView.hidden=YES;
    zoomCount = 0;
    size35Frame = self.size35Btn.frame;
    size36Frame = self.size36Btn.frame;
    size37Frame = self.size37Btn.frame;
    size38Frame = self.size38Btn.frame;
    size39Frame = self.size39Btn.frame;
    size40Frame = self.size40Btn.frame;
    size41Frame = self.size41Btn.frame;
    size42Frame = self.size42Btn.frame;
    
    self.size35Btn.hidden = YES;
    self.size36Btn.hidden = YES;
    self.size37Btn.hidden = YES;
    self.size38Btn.hidden = YES;
    self.size39Btn.hidden = YES;
    self.size40Btn.hidden = YES;
    self.size41Btn.hidden = YES;
    self.size42Btn.hidden = YES;
    
    self.crossedPriceLbl.hidden = YES;
    self.crossLbl.hidden = YES;
    self.offerPriceLbl.hidden = YES;
    self.onSaleBadge.hidden = YES;
    self.rightArrowBtn.hidden = YES;
    self.leftArrowBtn.hidden = YES;
    self.dolarBtn.selected = YES;
    
    [self fetchDealsDetailsformSever];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    //[self xibLoading];
    self.chooseYourSizeLbl.text = localize(@"Choose your size:");
    self.showInCurrenciesLbl.text = localize(@"Currencies :");
    self.youMightBeLabel.text = localize(@"You might be also interested in :");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self xibLoading];
    
    [self fontIntilization];
    
    bannerCount = 0;
    
    mainScrollView.hidden=YES;
    
    zoomCount = 0;
    
    self.globalSizeArray = [[NSMutableArray alloc]initWithObjects:@"35",@"36",@"37",@"38",@"39",@"40",@"41",@"42", nil];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.zoomScrollView addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.zoomScrollView addGestureRecognizer:swiperight];
    
    self.chooseSizeScrollView.contentSize = CGSizeMake(self.size42Btn.frame.origin.x + self.size42Btn.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
    
    globalShoeSizeVariable = 0;
    
    size35Frame = self.size35Btn.frame;
    size36Frame = self.size36Btn.frame;
    size37Frame = self.size37Btn.frame;
    size38Frame = self.size38Btn.frame;
    size39Frame = self.size39Btn.frame;
    size40Frame = self.size40Btn.frame;
    size41Frame = self.size41Btn.frame;
    size42Frame = self.size42Btn.frame;
    
    self.size35Btn.hidden = YES;
    self.size36Btn.hidden = YES;
    self.size37Btn.hidden = YES;
    self.size38Btn.hidden = YES;
    self.size39Btn.hidden = YES;
    self.size40Btn.hidden = YES;
    self.size41Btn.hidden = YES;
    self.size42Btn.hidden = YES;
    
    self.crossedPriceLbl.hidden = YES;
    self.crossLbl.hidden = YES;
    self.offerPriceLbl.hidden = YES;
    self.onSaleBadge.hidden = YES;
    self.rightArrowBtn.hidden = YES;
    self.leftArrowBtn.hidden = YES;
    self.dolarBtn.selected = YES;
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:([ApplicationDelegate.innerViewCount intValue]+1)];
    if (ApplicationDelegate.innerViewCount.intValue==0)
    {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else
    {
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    
    self.middleScrollView.contentSize = CGSizeMake(self.downScrollBackView.frame.size.width, self.downScrollBackView.frame.origin.y + self.downScrollBackView.frame.size.height);
    
    self.middleScrollView.frame = CGRectMake(self.middleScrollView.frame.origin.x, self.middleScrollView.frame.origin.y, self.middleScrollView.frame.size.width, self.middleScrollView.contentSize.height);
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.middleScrollView.frame.origin.y + self.middleScrollView.contentSize.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchDealsDetailsformSever
{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *productIdData = [[NSMutableDictionary alloc] initWithDictionary:[self getPrdData]];
    
    NSLog(@"%@", productIdData);
    
    [ApplicationDelegate.engine loadNewDealsDetailsWithProductId:productIdData CompletionHandler:^(NSMutableDictionary *responseDic) {
        
        {
            if ([ApplicationDelegate isValid:responseDic])
            {
                NSLog(@"%lu", (unsigned long)responseDic.count);
                NSLog(@"%@",responseDic);
                if (responseDic.count>0)
                {
                     mainScrollView.hidden=NO;
                    
                    self.dealsDetailDictionary = [[NSMutableDictionary alloc]initWithDictionary:responseDic];
                    
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Brand"]])
                    {
                        self.brandLbl.text = [self.dealsDetailDictionary objectForKey:@"Brand"];
                    }
                    else
                    {
                        self.brandLbl.text = @"";
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Category"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"Category"] isEqualToString:@"Accessories"])
                        {
                            self.productCategory.image = [UIImage imageNamed:@"necklace_icon.png"];
                        }
                        else if ([[self.dealsDetailDictionary objectForKey:@"Category"] isEqualToString:@"Shoes"])
                        {
                            self.productCategory.image = [UIImage imageNamed:@"shoes_icon.png"];
                        }
                        else if ([[self.dealsDetailDictionary objectForKey:@"Category"] isEqualToString:@"Jewlery"])
                        {
                            self.productCategory.image = [UIImage imageNamed:@"jewellery_icon.png"];
                        }
                        else if ([[self.dealsDetailDictionary objectForKey:@"Category"] isEqualToString:@"Bags"])
                        {
                            self.productCategory.image = [UIImage imageNamed:@"bag_icon.png"];
                        }
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Currency_code"]])
                    {
                        
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Currency_conversion"]])
                    {
                        NSLog(@"%lu",(unsigned long)[[self.dealsDetailDictionary objectForKey:@"Currency_conversion"] count]);
                        self.currencyConvertionDictionary = [[NSMutableDictionary alloc]init];
                        for (int i = 0; i<[[self.dealsDetailDictionary objectForKey:@"Currency_conversion"]count]; i++)
                        {
                            [self.currencyConvertionDictionary setObject:[[[self.dealsDetailDictionary objectForKey:@"Currency_conversion"]objectAtIndex:i]objectForKey:@"Result"] forKey:[[[self.dealsDetailDictionary objectForKey:@"Currency_conversion"]objectAtIndex:i]objectForKey:@"Code"]];
                        }
                        if ([ApplicationDelegate isValid:[self.currencyConvertionDictionary objectForKey:kTo_Currency_USD]])
                        {
                            self.priceBasedOnCurrency.text = [self.currencyConvertionDictionary objectForKey:kTo_Currency_USD];
                        }
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Desc"]])
                    {
                        self.descriptionTextView.text = [self.dealsDetailDictionary objectForKey:@"Desc"];
                        [self loadMiddleScroll];
                    }
                    else
                    {
                        self.descriptionTextView.text = @"";
                    }
                    
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"IsFavourite"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"IsFavourite"]integerValue])
                        {
                            self.addToFavoriteBtn.userInteractionEnabled = NO;
                            [self.addToFavoriteBtn setImage:[UIImage imageNamed:@"hearticonred.png"] forState:UIControlStateNormal];
                        }
                        else
                        {
                            
                        }
                    }
                    else
                    {
                        
                    }
             //Gender
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Gender"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"Gender"]integerValue] == 1) {
                            self.genderCriteria.image = [UIImage imageNamed:@"male_icon.png"];
                        }
                        else if ([[self.dealsDetailDictionary objectForKey:@"Gender"]integerValue] == 2)
                        {
                            self.genderCriteria.image = [UIImage imageNamed:@"female_icon.png"];
                        }
                        else{
                            self.genderCriteria.image = [UIImage imageNamed:@"uni.png"];
                        }
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"IsNew"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"IsNew"]integerValue])
                        {
                            self.statusLbl.text = localize(@"New");
                        }
                        else
                        {
                            self.statusLbl.text = localize(@"Used");
                        }
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"More_Info"]])
                    {
                        
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Name"]])
                    {
                        self.itemName.text = [self.dealsDetailDictionary objectForKey:@"Name"];
                        
                        [ApplicationDelegate loadMarqueeLabelWithText:self.itemName.text Font:self.itemName.font InPlaceOfLabel:self.itemName];
                        
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Offer"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"Offer"]intValue] == 0)
                        {
                            self.priceInKd.hidden = NO;
                            self.crossedPriceLbl.hidden = YES;
                            self.crossLbl.hidden = YES;
                            self.offerPriceLbl.hidden = YES;
                            self.onSaleBadge.hidden = YES;
                        }
                        else
                        {
                            self.priceInKd.hidden = YES;
                            if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Rate"]])
                            {
                                self.crossedPriceLbl.text = [NSString stringWithFormat:localize(@"KD %@"),[self.dealsDetailDictionary objectForKey:@"Rate"]];
                            }
                            self.crossedPriceLbl.hidden = NO;
                      
                            NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:localize(@"KD %@"),[self.dealsDetailDictionary objectForKey:@"Rate"]]];
                            
                            [titleString addAttribute: NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger: NSUnderlineStyleSingle] range: NSMakeRange(0, [titleString length])];

                            [self.crossedPriceLbl  setAttributedText: titleString];
                            
                            if (![ApplicationDelegate.language isEqualToString:kENGLISH])
                            {
                                [self.onSaleBadge setImage:[UIImage imageNamed:@"onsale_ar.png"]];
                            }
                            else
                            {
                                [self.onSaleBadge setImage:[UIImage imageNamed:@"onsale_badge.png"]];
                            }
                            self.onSaleBadge.hidden = NO;
                            
                            self.offerPriceLbl.hidden = NO;
                            self.offerPriceLbl.text = [NSString stringWithFormat:localize(@"KD %@"),[self.dealsDetailDictionary objectForKey:@"Offer"]];
                        }
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Rate"]])
                    {
                        self.priceInKd.text = [NSString stringWithFormat:localize(@"KD %@"),[self.dealsDetailDictionary objectForKey:@"Rate"]];
                    }
                    else
                    {
                        
                    }
                    
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Stock"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"Stock"]integerValue] == 0)
                        {
                            [self.frameview setImage:[UIImage imageNamed:@"silver_frame.png"]];
                            self.addToBasketBtn.hidden = YES;
                            self.soldOutLbl.hidden = NO;
                            self.soldOutLblBackView.hidden = NO;
                            self.soldOutLbl.text = localize(@"SOLD OUT!");
                        }
                    }
                    else
                    {
                        
                    }
                    
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"deals_image_list"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"deals_image_list"] count]>0)
                        {
                            if ([[self.dealsDetailDictionary objectForKey:@"deals_image_list"] count]>1)
                            {
                                self.rightArrowBtn.hidden = NO;
                            }
                            NSLog(@"Valid count ...%lu",(unsigned long)[[self.dealsDetailDictionary objectForKey:@"deals_image_list"] count]);
                            NSURL * sampurl = [[NSURL alloc]init];
                            
                            sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:[[[self.dealsDetailDictionary objectForKey:@"deals_image_list"]objectAtIndex:0]objectForKey:@"Image"]]];
                            [self.bannerView sd_setImageWithURL:sampurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
                            self.bannerView.layer.cornerRadius = 5.0;
                            self.bannerView.layer.masksToBounds = YES;
                            [self loadBannerImage];
                        }
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"related_item"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"related_item"] count]>0)
                        {
                            NSLog(@"Valid");
                            [self loadDownScroll];
                        }
                    }
                    else
                    {
                        
                    }
                    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"size"]])
                    {
                        if ([[self.dealsDetailDictionary objectForKey:@"size"] count]>0)
                        {
                            [self loadShoeSizeScroll];
                        }
                        else
                        {
                           
                        }
                    }
                    else
                    {
                        
                    }
                    [ApplicationDelegate removeProgressHUD];
                }
            }
        }
    } errorHandler:^(NSError *error)
    {
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }];
}

-(NSMutableDictionary *)getPrdData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    [postDic setObject:self.productId forKey:@"Deal_id"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else
    {
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(void)loadBannerImage
{
    self.imageArry = [[NSMutableArray alloc]init];
    for (int i = 0; i<[[self.dealsDetailDictionary objectForKey:@"deals_image_list"] count]; i++)
    {
        NSURL * sampurl = [[NSURL alloc]init];
        UIImageView * samImg = [[UIImageView alloc]init];
        NSString * stringUrl = [[NSString alloc]init];
        stringUrl = [[[self.dealsDetailDictionary objectForKey:@"deals_image_list"]objectAtIndex:i]objectForKey:@"Image"];
        sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:stringUrl]];
        [samImg sd_setImageWithURL:sampurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
        [self.imageArry addObject:samImg];
    }
}

-(void)loadShoeSizeScroll
{
    if ([[self.dealsDetailDictionary objectForKey:@"size"] count]>0)
    {
        NSArray * tempArry = [[NSArray alloc]initWithArray:[self.dealsDetailDictionary objectForKey:@"size"]];
        if (tempArry.count>0)
        {
            globalShoeSizeVariable = [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue];
        }
        sizeBttnArray = [[NSMutableArray alloc]init];
        NSMutableArray * activeNonactiveAry = [[NSMutableArray alloc]initWithObjects:@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO", nil];
        for (int i = 0; i< self.globalSizeArray.count; i++)
        {
            for (int j = 0; j< tempArry.count; j++)
            {
                if ([[self.globalSizeArray objectAtIndex:i] intValue] == [[[tempArry objectAtIndex:j] objectForKey:@"size"]intValue])
                {
                    [activeNonactiveAry setObject:@"YES" atIndexedSubscript:i];
                    break;
                }
                else
                {
                    
                }
            }
        }
        for (int i = 0; i < self.globalSizeArray.count; i++)
        {
            UIButton * sizeButton = [[UIButton alloc]init];
            [sizeButton addTarget:self action:@selector(sizeChangeAction:) forControlEvents:UIControlEventTouchUpInside];
            [sizeBttnArray addObject:sizeButton];
            if (i == 0)
            {
                sizeButton.tag = [[self.globalSizeArray objectAtIndex:i]intValue];
                sizeButton.frame = size35Frame;
                NSString * nameStr;
                NSString * nameSelStr;
                if ([[activeNonactiveAry objectAtIndex:i]isEqualToString:@"NO"])
                {
                    nameStr = [NSString stringWithFormat:@"unsel%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"unsel%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    sizeButton.userInteractionEnabled = NO;
                }
                else
                {
                    nameStr = [NSString stringWithFormat:@"size%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"size%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                }
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameStr] forState:UIControlStateNormal];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateSelected];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateHighlighted];
                self.chooseSizeScrollView.contentSize = CGSizeMake(sizeButton.frame.origin.x + sizeButton.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
                if (tempArry.count>0)
                {
                    if ([[self.globalSizeArray objectAtIndex:i]integerValue] == [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue])
                    {
                        sizeButton.selected = YES;
                    }
                }
                NSLog(@"%d",globalShoeSizeVariable);
                [self.chooseSizeScrollView addSubview:sizeButton];
            }
            else if (i == 1)
            {
                sizeButton.tag = [[self.globalSizeArray objectAtIndex:i]intValue];
                sizeButton.frame = size36Frame;
                NSString * nameStr;
                NSString * nameSelStr;
                if ([[activeNonactiveAry objectAtIndex:i]isEqualToString:@"NO"])
                {
                    nameStr = [NSString stringWithFormat:@"unsel%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"unsel%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    sizeButton.userInteractionEnabled = NO;
                } else
                {
                    nameStr = [NSString stringWithFormat:@"size%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"size%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                }
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameStr] forState:UIControlStateNormal];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateSelected];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateHighlighted];
                self.chooseSizeScrollView.contentSize = CGSizeMake(sizeButton.frame.origin.x + sizeButton.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
                if (tempArry.count>0)
                {
                    if ([[self.globalSizeArray objectAtIndex:i]integerValue] == [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue])
                    {
                        sizeButton.selected = YES;
                    }
                }
                [self.chooseSizeScrollView addSubview:sizeButton];
            }
            else if (i == 2)
            {
                sizeButton.tag = [[self.globalSizeArray objectAtIndex:i]intValue];
                sizeButton.frame = size37Frame;
                NSString * nameStr;
                NSString * nameSelStr;
                if ([[activeNonactiveAry objectAtIndex:i]isEqualToString:@"NO"])
                {
                    nameStr = [NSString stringWithFormat:@"unsel%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"unsel%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    sizeButton.userInteractionEnabled = NO;
                } else
                {
                    nameStr = [NSString stringWithFormat:@"size%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"size%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                }
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameStr] forState:UIControlStateNormal];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateSelected];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateHighlighted];
                self.chooseSizeScrollView.contentSize = CGSizeMake(sizeButton.frame.origin.x + sizeButton.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
                if (tempArry.count>0)
                {
                    if ([[self.globalSizeArray objectAtIndex:i]integerValue] == [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue])
                    {
                        sizeButton.selected = YES;
                    }
                }
                [self.chooseSizeScrollView addSubview:sizeButton];
            }
            else if (i == 3)
            {
                sizeButton.tag = [[self.globalSizeArray objectAtIndex:i]intValue];
                sizeButton.frame = size38Frame;
                NSString * nameStr;
                NSString * nameSelStr;
                if ([[activeNonactiveAry objectAtIndex:i]isEqualToString:@"NO"])
                {
                    nameStr = [NSString stringWithFormat:@"unsel%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"unsel%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    sizeButton.userInteractionEnabled = NO;
                }
                else
                {
                    nameStr = [NSString stringWithFormat:@"size%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"size%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                }
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameStr] forState:UIControlStateNormal];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateSelected];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateHighlighted];
                self.chooseSizeScrollView.contentSize = CGSizeMake(sizeButton.frame.origin.x + sizeButton.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
                if (tempArry.count>0)
                {
                    if ([[self.globalSizeArray objectAtIndex:i]integerValue] == [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue])
                    {
                        sizeButton.selected = YES;
                    }
                }
                [self.chooseSizeScrollView addSubview:sizeButton];
            }
            else if (i == 4)
            {
                sizeButton.tag = [[self.globalSizeArray objectAtIndex:i]intValue];
                sizeButton.frame = size39Frame;
                NSString * nameStr;
                NSString * nameSelStr;
                if ([[activeNonactiveAry objectAtIndex:i]isEqualToString:@"NO"])
                {
                    nameStr = [NSString stringWithFormat:@"unsel%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"unsel%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    sizeButton.userInteractionEnabled = NO;
                } else
                {
                    nameStr = [NSString stringWithFormat:@"size%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"size%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                }
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameStr] forState:UIControlStateNormal];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateSelected];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateHighlighted];
                self.chooseSizeScrollView.contentSize = CGSizeMake(sizeButton.frame.origin.x + sizeButton.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
                if (tempArry.count>0)
                {
                    if ([[self.globalSizeArray objectAtIndex:i]integerValue] == [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue])
                    {
                        sizeButton.selected = YES;
                    }
                }
                [self.chooseSizeScrollView addSubview:sizeButton];
            }
            else if (i == 5)
            {
                sizeButton.tag = [[self.globalSizeArray objectAtIndex:i]intValue];
                sizeButton.frame = size40Frame;
                NSString * nameStr;
                NSString * nameSelStr;
                if ([[activeNonactiveAry objectAtIndex:i]isEqualToString:@"NO"])
                {
                    nameStr = [NSString stringWithFormat:@"unsel%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"unsel%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    sizeButton.userInteractionEnabled = NO;
                }
                else
                {
                    nameStr = [NSString stringWithFormat:@"size%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"size%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                }
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameStr] forState:UIControlStateNormal];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateSelected];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateHighlighted];
                self.chooseSizeScrollView.contentSize = CGSizeMake(sizeButton.frame.origin.x + sizeButton.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
                if (tempArry.count>0)
                {
                    if ([[self.globalSizeArray objectAtIndex:i]integerValue] == [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue])
                    {
                        sizeButton.selected = YES;
                    }
                }
                [self.chooseSizeScrollView addSubview:sizeButton];
            }
            else if (i == 6)
            {
                sizeButton.tag = [[self.globalSizeArray objectAtIndex:i]intValue];
                sizeButton.frame = size41Frame;
                NSString * nameStr;
                NSString * nameSelStr;
                if ([[activeNonactiveAry objectAtIndex:i]isEqualToString:@"NO"])
                {
                    nameStr = [NSString stringWithFormat:@"unsel%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"unsel%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    sizeButton.userInteractionEnabled = NO;
                }
                else
                {
                    nameStr = [NSString stringWithFormat:@"size%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"size%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                }
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameStr] forState:UIControlStateNormal];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateSelected];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateHighlighted];
                self.chooseSizeScrollView.contentSize = CGSizeMake(sizeButton.frame.origin.x + sizeButton.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
                if (tempArry.count>0) {
                    if ([[self.globalSizeArray objectAtIndex:i]integerValue] == [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue])
                    {
                        sizeButton.selected = YES;
                    }
                }
                [self.chooseSizeScrollView addSubview:sizeButton];
            }
            else if (i == 7)
            {
                sizeButton.tag = [[self.globalSizeArray objectAtIndex:i]intValue];
                sizeButton.frame = size42Frame;
                NSString * nameStr;
                NSString * nameSelStr;
                if ([[activeNonactiveAry objectAtIndex:i]isEqualToString:@"NO"])
                {
                    nameStr = [NSString stringWithFormat:@"unsel%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"unsel%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    sizeButton.userInteractionEnabled = NO;
                }
                else
                {
                    nameStr = [NSString stringWithFormat:@"size%d.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                    nameSelStr = [NSString stringWithFormat:@"size%d_sel.png",[[self.globalSizeArray objectAtIndex:i]intValue]];
                }
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameStr] forState:UIControlStateNormal];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateSelected];
                [sizeButton setBackgroundImage:[UIImage imageNamed:nameSelStr] forState:UIControlStateHighlighted];
                
                if (![ApplicationDelegate.language isEqualToString:kENGLISH])
                {
                    self.chooseSizeScrollView.contentSize = CGSizeMake(321, self.chooseSizeScrollView.frame.size.height);
                }
                else
                {
                    self.chooseSizeScrollView.contentSize = CGSizeMake(sizeButton.frame.origin.x + sizeButton.frame.size.width + 1, self.chooseSizeScrollView.frame.size.height);
                }
                if (tempArry.count>0)
                {
                    if ([[self.globalSizeArray objectAtIndex:i]integerValue] == [[[tempArry objectAtIndex:0] objectForKey:@"size"]intValue])
                    {
                        sizeButton.selected = YES;
                    }
                }
                [self.chooseSizeScrollView addSubview:sizeButton];
            }
        }
    }
    else
    {
        
    }
}

-(void)loadDownScroll
{
    [self.downScrollView setContentOffset:CGPointZero];
    
    float setXvalue = 0.0f;
    float scrollWidth = 0.0f;
    
    for (int i = 0; i<[[self.dealsDetailDictionary objectForKey:@"related_item"] count]; i++)
    {
        UIButton * tempBtn = [[UIButton alloc]init];
        tempBtn.frame = CGRectMake(self.downScrollDummyFrame.frame.origin.x, self.downScrollDummyFrame.frame.origin.y, self.downScrollDummyFrame.frame.size.width, self.downScrollDummyFrame.frame.size.height);
        tempBtn.layer.cornerRadius = 4;

        tempBtn.layer.borderColor=[[UIColor colorWithHex:kUNIVERSAL_BORDER_C0lOR] CGColor];

        tempBtn.layer.borderWidth = 1;
        tempBtn.tag = i;
        
        tempBtn.frame = CGRectMake(OFFSETWIDTH + setXvalue , OFFSETHEIGHT , tempBtn.frame.size.width, tempBtn.frame.size.height);
        
        setXvalue = tempBtn.frame.origin.x + tempBtn.frame.size.width;
        
        scrollWidth = tempBtn.frame.origin.x + tempBtn.frame.size.width;
        
        NSURL * sampurl = [[NSURL alloc]init];
        NSString * stringUrl = [[NSString alloc]init];
        stringUrl = [[[self.dealsDetailDictionary objectForKey:@"related_item"]objectAtIndex:i]objectForKey:@"Thumb"];
        
        sampurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:stringUrl]];
        
        [tempBtn sd_setImageWithURL:sampurl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"loading.png"]];
        [tempBtn addTarget:self action:@selector(relatedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //tempBtn.layer.cornerRadius = 10;
        [tempBtn.layer setMasksToBounds:YES];
        [self.downScrollView addSubview:tempBtn];
        
    }
    self.downScrollView.contentSize = CGSizeMake((scrollWidth + OFFSETHEIGHT),self.downScrollView.frame.size.height);
}

-(void)relatedBtnAction:(UIButton*)sender
{
    NewDealsDetailsViewController * newDealsDetailVC = [[NewDealsDetailsViewController alloc]initWithNibName:@"NewDealsDetailsViewController" bundle:nil];
    newDealsDetailVC.view.backgroundColor = [UIColor clearColor];
    newDealsDetailVC.productId = [[[self.dealsDetailDictionary objectForKey:@"related_item"]objectAtIndex:sender.tag]objectForKey:@"Id"];
    [ApplicationDelegate.homeTabNav pushViewController:newDealsDetailVC animated:NO];
}

-(void)loadMiddleScroll
{
    [self.descriptionTextView setScrollEnabled:NO];
    [self.descriptionTextView sizeToFit];
    
    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Category"]])
    {
        if ([[self.dealsDetailDictionary objectForKey:@"Category"] isEqualToString:@"Shoes"])
        {
            self.middleScrollTopView.frame = CGRectMake(self.middleScrollTopView.frame.origin.x, self.middleScrollTopView.frame.origin.y, self.middleScrollTopView.frame.size.width,self.descriptionTextView.frame.origin.y + self.descriptionTextView.frame.size.height + 1);
            
            self.middleScrollShoeSizeView.frame = CGRectMake(self.middleScrollShoeSizeView.frame.origin.x, self.middleScrollTopView.frame.origin.y + self.middleScrollTopView.frame.size.height + 1, self.middleScrollShoeSizeView.frame.size.width, self.middleScrollShoeSizeView.frame.size.height);
            
            self.middleScrollDownView.frame = CGRectMake(self.middleScrollDownView.frame.origin.x, self.middleScrollShoeSizeView.frame.origin.y + self.middleScrollShoeSizeView.frame.size.height + 1, self.middleScrollDownView.frame.size.width, self.middleScrollDownView.frame.size.height);
            
            self.downScrollBackView.frame = CGRectMake(self.downScrollBackView.frame.origin.x, self.middleScrollDownView.frame.origin.y + self.middleScrollDownView.frame.size.height + 1, self.downScrollBackView.frame.size.width, self.downScrollBackView.frame.size.height);
        }
        else
        {
            [self.middleScrollShoeSizeView removeFromSuperview];
            self.middleScrollTopView.frame = CGRectMake(self.middleScrollTopView.frame.origin.x, self.middleScrollTopView.frame.origin.y, self.middleScrollTopView.frame.size.width,self.descriptionTextView.frame.origin.y + self.descriptionTextView.frame.size.height + 1);
            
            self.middleScrollDownView.frame = CGRectMake(self.middleScrollDownView.frame.origin.x, self.middleScrollTopView.frame.origin.y + self.middleScrollTopView.frame.size.height + 1, self.middleScrollDownView.frame.size.width, self.middleScrollDownView.frame.size.height);
            
            self.downScrollBackView.frame = CGRectMake(self.downScrollBackView.frame.origin.x, self.middleScrollDownView.frame.origin.y + self.middleScrollDownView.frame.size.height + 1, self.downScrollBackView.frame.size.width, self.downScrollBackView.frame.size.height);
        }
    }
    else
    {
        
    }
    
    self.middleScrollView.contentSize = CGSizeMake(self.downScrollBackView.frame.size.width, self.downScrollBackView.frame.origin.y + self.downScrollBackView.frame.size.height);
    
    self.middleScrollView.frame = CGRectMake(self.middleScrollView.frame.origin.x, self.middleScrollView.frame.origin.y, self.middleScrollView.frame.size.width, self.middleScrollView.contentSize.height);
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.middleScrollView.frame.origin.y + self.middleScrollView.contentSize.height);
}

- (IBAction)moreInfoBttnActn:(UIButton *)sender
{
    if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"More_Info"]])
    {
        [ApplicationDelegate showAlertWithMessage:[self.dealsDetailDictionary objectForKey:@"More_Info"] title:nil];
    }
}

- (IBAction)addToBasketBtnActn:(UIButton *)sender
{
    if (sender.tag == 1)
    {
        [ApplicationDelegate showAlertWithMessage:@"Stock not available" title:nil];
    }
    else
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableDictionary *addToBasketData = [[NSMutableDictionary alloc] initWithDictionary:[self getAddToBasketData]];
        
        NSLog(@"%@",addToBasketData);
        
        [ApplicationDelegate.engine postAddToBasketWithDictionary:addToBasketData CompletionHandler:^(NSMutableDictionary *responseDictionary)
        {
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            if ([ApplicationDelegate isValid:responseDictionary])
            {
                if (responseDictionary.count>0)
                {
                    if ([responseDictionary objectForKey:@"Success"])
                    {
                        [[HomeViewController sharedViewController] globalLabel:[responseDictionary objectForKey:@"BasketCount"]];
                        
                        [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                        
                        [ApplicationDelegate removeProgressHUD];
                    }
                    else
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                        [ApplicationDelegate removeProgressHUD];
                    }
                }
            }
        } errorHandler:^(NSError *error)
         {
            [ApplicationDelegate removeProgressHUD];
            [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
        }];
    }
}

-(NSMutableDictionary *)getAddToBasketData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]])
    {
        [postDic setObject:self.productId forKey:@"Deal_id"];
        [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [postDic setObject:kARABIC forKey:@"lang_key"];
        }
        else
        {
            [postDic setObject:kENGLISH forKey:@"lang_key"];
        }
        [postDic setObject:[NSString stringWithFormat:@"%d",globalShoeSizeVariable] forKey:@"size"];
    }
    return postDic;
}

- (IBAction)addToFavoriteBtnActn:(UIButton *)sender
{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableDictionary *addToFavoriteData = [[NSMutableDictionary alloc] initWithDictionary:[self getAddToFavoriteData]];
    
    NSLog(@"addToFavoriteData%@",addToFavoriteData);
    
    [ApplicationDelegate.engine postAddToFavoriteWithDictionary:addToFavoriteData CompletionHandler:^(NSMutableDictionary *responseDictionary)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        if ([ApplicationDelegate isValid:responseDictionary])
        {
            if (responseDictionary.count>0)
            {
                NSLog(@"%@",responseDictionary);
                if ([responseDictionary objectForKey:@"Success"])
                {
                    self.addToFavoriteBtn.userInteractionEnabled = NO;
                    [self.addToFavoriteBtn setImage:[UIImage imageNamed:@"hearticonred.png"] forState:UIControlStateNormal];
                    
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                }
                else
                {
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Message"] title:nil];
                    [ApplicationDelegate removeProgressHUD];
                }
            }
        }
    } errorHandler:^(NSError *error)
    {
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }];
}

-(NSMutableDictionary *)getAddToFavoriteData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]])
    {
        [postDic setObject:self.productId forKey:@"Deal_id"];
        [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [postDic setObject:kARABIC forKey:@"lang_key"];
        }
        else
        {
            [postDic setObject:kENGLISH forKey:@"lang_key"];
        }
        [postDic setObject:[NSString stringWithFormat:@"%d",globalShoeSizeVariable] forKey:@"size"];
    }
    
    return postDic;
}

- (IBAction)currencyChangeAction:(UIButton *)sender
{
    if (sender.tag == 1)
    {
        self.poundBtn.selected = YES;
        self.dolarBtn.selected = NO;
        self.euroBtn.selected = NO;
        self.rupeeBtn.selected = NO;
        if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Currency_conversion"]]) {
            if ([ApplicationDelegate isValid:[self.currencyConvertionDictionary objectForKey:kTo_Currency_GBP]]) {
                NSLog(@"%@",[self.currencyConvertionDictionary objectForKey:kTo_Currency_GBP]);
                self.priceBasedOnCurrency.text = [self.currencyConvertionDictionary objectForKey:kTo_Currency_GBP];
            }
        }
    }
    else if(sender.tag == 2){
        //kTo_Currency_USD
        self.poundBtn.selected = NO;
        self.dolarBtn.selected = YES;
        self.euroBtn.selected = NO;
        self.rupeeBtn.selected = NO;
        if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Currency_conversion"]])
        {
            if ([ApplicationDelegate isValid:[self.currencyConvertionDictionary objectForKey:kTo_Currency_USD]])
            {
                self.priceBasedOnCurrency.text = [self.currencyConvertionDictionary objectForKey:kTo_Currency_USD];
            }
        }
    }
    else if(sender.tag == 3)
    {
        self.poundBtn.selected = NO;
        self.dolarBtn.selected = NO;
        self.euroBtn.selected = YES;
        self.rupeeBtn.selected = NO;
        if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Currency_conversion"]])
        {
            if ([ApplicationDelegate isValid:[self.currencyConvertionDictionary objectForKey:kTo_Currency_EUR]])
            {
                self.priceBasedOnCurrency.text = [self.currencyConvertionDictionary objectForKey:kTo_Currency_EUR];
            }
        }
    }
    else if(sender.tag == 4)
    {
        self.poundBtn.selected = NO;
        self.dolarBtn.selected = NO;
        self.euroBtn.selected = NO;
        self.rupeeBtn.selected = YES;
        if ([ApplicationDelegate isValid:[self.dealsDetailDictionary objectForKey:@"Currency_conversion"]])
        {
            if ([ApplicationDelegate isValid:[self.currencyConvertionDictionary objectForKey:kTo_Currency_SAR]])
            {
                self.priceBasedOnCurrency.text = [self.currencyConvertionDictionary objectForKey:kTo_Currency_SAR];
            }
        }
    }
    
}
- (IBAction)rightArrowBtnActn:(UIButton *)sender
{
    NSLog(@"%lu",(unsigned long)self.imageArry.count);
    if (self.imageArry.count>0)
    {
        bannerCount++;
        NSLog(@"%lu",(unsigned long)bannerCount);
        UIImageView * tempimg = [[UIImageView alloc]init];
        if (bannerCount == (self.imageArry.count-1))
        {
            self.rightArrowBtn.hidden = YES;
            self.leftArrowBtn.hidden = NO;
            tempimg = [self.imageArry objectAtIndex:bannerCount];
            self.bannerView.image = tempimg.image;
        }
        else
        {
            self.leftArrowBtn.hidden = NO;
            NSLog(@"%d",bannerCount);
            tempimg = [self.imageArry objectAtIndex:bannerCount];
            self.bannerView.image = tempimg.image;
        }
    }
}

- (IBAction)leftArrowBtnActn:(UIButton *)sender
{
    if (self.imageArry.count>0)
    {
        bannerCount--;
        UIImageView * tempimg = [[UIImageView alloc]init];
        if (bannerCount == 0)
        {
            self.leftArrowBtn.hidden = YES;
            self.rightArrowBtn.hidden = NO;
            tempimg = [self.imageArry objectAtIndex:bannerCount];
            self.bannerView.image = tempimg.image;
        }
        else
        {
            self.rightArrowBtn.hidden = NO;
            tempimg = [self.imageArry objectAtIndex:bannerCount];
            self.bannerView.image = tempimg.image;
        }
    }
}

- (IBAction)shareBtnActn:(UIButton *)sender
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    [sharingItems addObject:self.descriptionTextView.text];
    
    [sharingItems addObject:[[[self.dealsDetailDictionary objectForKey:@"deals_image_list"]objectAtIndex:0]objectForKey:@"Image"]];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    
    activityController.modalInPopover = YES;
    
    [self.view.window.rootViewController presentViewController:activityController animated:YES completion:NULL];
}

- (IBAction)sizeChangeAction:(UIButton *)sender
{
    NSLog(@"%ld",(long)sender.tag);
    for (int i=0; i < sizeBttnArray.count; i++)
    {
        UIButton * tempBtn = [sizeBttnArray objectAtIndex:i];
        if (tempBtn.tag == sender.tag)
        {
            tempBtn.selected = YES;
            globalShoeSizeVariable = (int)sender.tag;
        } else
        {
            tempBtn.selected = NO;
        }
    }
}

- (IBAction)zoomCloseBtnActn:(UIButton *)sender
{
    zoomCount = bannerCount;
    self.zoomScrollView.zoomScale = 1;
    [self.zoomBackView removeFromSuperview];
}

- (IBAction)zoomBtnActn:(UIButton *)sender
{
    if (self.imageArry.count>0)
    {
        UIImageView * tempimg = [[UIImageView alloc]init];
        tempimg = [self.imageArry objectAtIndex:bannerCount];
        self.zoomImageView.image = tempimg.image;
        zoomCount = bannerCount;
    }
 
   self.zoomImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.view addSubview:self.zoomBackView];
    
    [self.zoomBackView setFrame:self.zoomBackView.superview.bounds];
    
    self.zoomBackView.center = [self.view convertPoint:self.view.center fromView:self.view.superview];
    
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.zoomImageView;
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"left");
    NSLog(@"%d",zoomCount);
    if (self.imageArry.count>0)
    {
        zoomCount++;
        UIImageView * tempimg = [[UIImageView alloc]init];
        if (zoomCount == (self.imageArry.count-1))
        {
            tempimg = [self.imageArry objectAtIndex:zoomCount];
            self.zoomImageView.image = tempimg.image;
        }
        else if (zoomCount <(self.imageArry.count-1))
        {
            tempimg = [self.imageArry objectAtIndex:zoomCount];
            self.zoomImageView.image = tempimg.image;
        }
        else if(zoomCount >(self.imageArry.count-1))
        {
            zoomCount = self.imageArry.count - 1;
        }
    }
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (self.imageArry.count>0)
    {
        zoomCount--;
        UIImageView * tempimg = [[UIImageView alloc]init];
        if (zoomCount == 0)
        {
            tempimg = [self.imageArry objectAtIndex:zoomCount];
            self.zoomImageView.image = tempimg.image;
        }
        else if (zoomCount < 0)
        {
            zoomCount = 0;
        }
        else
        {
            tempimg = [self.imageArry objectAtIndex:zoomCount];
            self.zoomImageView.image = tempimg.image;
        }
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (velocity.x>0)
    {
        NSLog(@"plus");
        if (velocity.x>1.5)
        {
            self.zoomScrollView.zoomScale = 1;
            
            if (self.imageArry.count>0)
            {
                zoomCount++;
                UIImageView * tempimg = [[UIImageView alloc]init];
                if (zoomCount == (self.imageArry.count-1))
                {
                    tempimg = [self.imageArry objectAtIndex:zoomCount];
                    self.zoomImageView.image = tempimg.image;
                }
                else if (zoomCount <(self.imageArry.count-1))
                {
                    tempimg = [self.imageArry objectAtIndex:zoomCount];
                    self.zoomImageView.image = tempimg.image;
                }
                else if(zoomCount >(self.imageArry.count-1))
                {
                    zoomCount = self.imageArry.count - 1;
                }
            }
        }
    }
    else if (velocity.x<0)
    {
        if (velocity.x<-1.5)
        {
            self.zoomScrollView.zoomScale = 1;
            if (self.imageArry.count>0)
            {
                zoomCount--;
                UIImageView * tempimg = [[UIImageView alloc]init];
                if (zoomCount == 0)
                {
                    tempimg = [self.imageArry objectAtIndex:zoomCount];
                    self.zoomImageView.image = tempimg.image;
                }
                else if (zoomCount < 0)
                {
                    zoomCount = 0;
                }
                else
                {
                    tempimg = [self.imageArry objectAtIndex:zoomCount];
                    self.zoomImageView.image = tempimg.image;
                }
            }
        }
    }
}


-(void)xibLoading
{
    NSString *nibName = @"NewDealsDetailsViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

-(void) fontIntilization
{
    [self.brandLbl setFont:EBRI_NORMAL_FONT(17)];
    
    [self.descriptionTextView setFont:EBRI_NORMAL_FONT(12)];
    
    [self.itemName setFont:EBRI_BOLD_FONT(14)];
    
    [self.statusLbl setFont:EBRI_BOLD_FONT(14)];
    
    [self.priceInKd setFont:EBRI_BOLD_FONT(14)];
    
    [self.chooseYourSizeLbl setFont:EBRI_BOLD_FONT(14)];
    
     [self.showInCurrenciesLbl setFont:EBRI_BOLD_FONT(14)];
    
    [self.youMightBeLabel setFont:EBRI_BOLD_FONT(13)];
    
    [self.priceBasedOnCurrency setFont:EBRI_BOLD_FONT(13)];
    
    self.showInCurrenciesLbl .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    
     self.priceBasedOnCurrency .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    
     self.chooseYourSizeLbl .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    
    self.statusLbl .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    
    self.itemName .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
    
    self.priceInKd .textColor = [UIColor colorWithHex:kUNIVERSAL_VIOLET_COLOR];
}

@end
