//
//  AppEngine.h
//  TabDeal
//
//  Created by Sreeraj VR on 21/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface AppEngine : MKNetworkEngine

typedef void (^ResponseDictionaryBlock)(NSMutableDictionary* responseDictionary);

typedef void (^ResponseArrayBlock)(NSMutableArray* responseArray);

@property(nonatomic,retain)MKNetworkOperation *imageOperation;

-(void)loadUpdatePurchaseOnCompleteWithDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)UpdatePersonalInfo:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)UpdateDeliveryAddress:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)EditStatusWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadUsersnotificationDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getSaleEditableParametersDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getSaleParametersWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)EditPasswordWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getBankDetailsWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)EditBillingAddressWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getBrandDetailsWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getUserDetailsWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getPageDetailsWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadUpdatePurchaseWithDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getCountryDetailsWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)EditUserAddressWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)postAddToFavoriteWithDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadUsersBasketLanguageDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)postAddToBasketWithDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadCurrencyConversionWithCurrencyCode:(NSMutableDictionary *)proDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadNewDealsDetailsWithProductId:(NSMutableDictionary *)proDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadNewDealsWithLanguageDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loginUserWithLoginDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)RegisterUserWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)forgetPasswordDataDictionary:(NSMutableDictionary *)forDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loginwithFacebookDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loginwithTwitterDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)DeleteBasketItemWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)EditBasketItemWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadUsersFavoriteDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)DeleteFavouriteItemWithPostDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadUsersPreviousOrderDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)loadPreviousOrderDetailsDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getPushNotification:(NSDictionary *)postDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)updateUserAccount:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)search:(NSDictionary *)postData CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)SellmoreInfo:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)SellTermsAndC:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)SellImageUpload:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)SubmittedItemList:(NSDictionary *)postData CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)SellUpdate:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)InsertPaymentHistory:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;


@end
