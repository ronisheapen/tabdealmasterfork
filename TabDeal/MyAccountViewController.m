//
//  MyAccountViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "MyAccountViewController.h"
#import "MyBankDetailsViewController.h"
#import "HomeViewController.h"
#import "Userinformation.h"

@interface MyAccountViewController ()

@end

@implementation MyAccountViewController

-(void)viewWillAppear:(BOOL)animated{
    //[self xibLoading];
    
    ApplicationDelegate.innerViewCount = [NSNumber numberWithInt:0];
    if (ApplicationDelegate.innerViewCount.intValue==0) {
        [HomeViewController sharedViewController].backArrowView.hidden = YES;
        [HomeViewController sharedViewController].backBttn.hidden = YES;
    }
    else{
        [HomeViewController sharedViewController].backArrowView.hidden = NO;
        [HomeViewController sharedViewController].backBttn.hidden = NO;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([ApplicationDelegate isValid:[prefs objectForKey:@"User Id"]]) {
        [self fetchCountryDataformSever];
        [self fetchUserDataformSever];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    //[self xibLoading];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        
        
        ApplicationDelegate.language=kARABIC;
        [[Localization sharedInstance] setPreferred:@"ar" fallback:@"en"];
        
    }
    else
    {
        
        ApplicationDelegate.language=kENGLISH;
        [[Localization sharedInstance] setPreferred:@"en" fallback:@"ar"];
        
    }
    
    NSLog(@"%@",ApplicationDelegate.language);
    
    self.headingLbl.text = localize(@"MY ACCOUNT");
    
   [self.headingLbl setFont:EBRI_BOLD_FONT(17)];
    
    self.personalinfoTxt.text = localize(@"Personal Information:");
    self.firstNameTxt.placeholder = localize(@"First Name");
    self.lastNameTxt.placeholder = localize(@"Last Name");
    self.mobileNumberTxt.placeholder = localize(@"(Mobile Number)");
    self.codeTxt.placeholder = localize(@"(Code)");
    self.emailAddressTxt.placeholder = localize(@"Email Address");
    
    self.firstNameEditTxt.placeholder = localize(@"First Name");
    self.lastNameEditTxt.placeholder = localize(@"Last Name");
    self.mobileNumEditTxt.placeholder = localize(@"(Mobile Number)");
    self.codeEditTxt.placeholder = localize(@"(Code)");
    self.emailEditTxt.placeholder = localize(@"Email Address");
    
    [self.changePasswordBtn setTitle:localize(@"Change Password") forState:UIControlStateNormal];
    [self.addDeliveryAddressBtn setTitle:localize(@"Add Delivery Address") forState:UIControlStateNormal];
    
    self.enterDeliveryAddressTxt.text = localize(@"Enter Delivery Address:");
    self.nameTxtField.placeholder = localize(@"Receiver full name");
    self.addressLine1Txt.placeholder = localize(@"Address line 1");
    self.addressLine2Txt.placeholder = localize(@"Address line 2");
    self.cityTxt.placeholder = localize(@"City");
    self.countryTextView.placeholder = localize(@"Country");
    self.postalCodeTxt.placeholder = localize(@"Postal Code (only for international users)");
    self.mobileNumberTextField.placeholder = localize(@"(Mobile Number)");
    self.codeTextField.placeholder = localize(@"(Code)");
    self.codeTextField.placeholder = localize(@"(Code)");
    
    self.deliveryAddressHeading.text = localize(@"DELIVERY ADDRESS");
    self.personalInfomationHeading.text = localize(@"PERSONAL INFORMATION");
    [self.saveBtn setTitle:localize(@"SAVE") forState:UIControlStateNormal];
    self.nameEditTxtField.placeholder = localize(@"Receiver full name");
    self.addressLine1EditTxt.placeholder = localize(@"Address line 1");
    self.addressLine2EditTxt.placeholder = localize(@"Address line 2");
    self.cityEditTxt.placeholder = localize(@"City");
    self.countryEditTxt.placeholder = localize(@"Country");
    self.postalCodeEditTxt.placeholder = localize(@"Postal Code (only for international users)");
    self.codeDelEditTxt.placeholder = localize(@"(Code)");
    self.mobileDelEditTxt.placeholder = localize(@"(Mobile Number)");
    
    [self.saveChangesBtn setTitle:localize(@"SAVE CHANGES") forState:UIControlStateNormal];
    self.conditionLbl.text = localize(@"If you are selling items with us , please make sure you update your bank details");
    [self.bankDetailsBtn setTitle:localize(@"BANK DETAILS") forState:UIControlStateNormal];
    self.changePassHeaddingLbl.text = localize(@"CHANGE PASSWORD");
    self.oldPasswordTxt.placeholder = localize(@"Old Password");
    self.passwordNewTxt.placeholder = localize(@"New Password");
    self.confirmPasswordTxt.placeholder = localize(@"Confirm Password");
    
    [self.submitBtn setTitle:localize(@"Submit") forState:UIControlStateNormal];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xibLoading];
    
    dropDownTag = 0;
    
    
    self.myAccountBackView.hidden = YES;
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.backWhiteView.frame.size.height);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileNumberTextField];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileNumberTxt];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.codeTextField];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.codeTxt];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.postalCodeTxt];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileDelEditTxt];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.codeDelEditTxt];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.postalCodeEditTxt];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.codeEditTxt];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileNumEditTxt];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.mobileNumberTextField.inputAccessoryView = numberToolbar;
    
    self.mobileNumEditTxt.inputAccessoryView = numberToolbar;
    
    UIToolbar* numberToolbarDel = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarDel.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarDel.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadDel)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadDel)]];
    [numberToolbarDel sizeToFit];
    
    self.mobileDelEditTxt.inputAccessoryView = numberToolbarDel;
    
    UIToolbar* numberToolbarCode = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarCode.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarCode.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadCode)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadCode)]];
    [numberToolbarCode sizeToFit];
    self.codeTextField.inputAccessoryView = numberToolbarCode;
    
    self.codeEditTxt.inputAccessoryView = numberToolbarCode;
    
    UIToolbar* numberToolbarCodeDel = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarCodeDel.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarCodeDel.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadCodeDel)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadCodeDel)]];
    [numberToolbarCodeDel sizeToFit];
    self.codeDelEditTxt.inputAccessoryView = numberToolbarCodeDel;
    
    UIToolbar* PostalCodeToolbarCode = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    PostalCodeToolbarCode.barStyle = UIBarStyleBlackTranslucent;
    PostalCodeToolbarCode.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPostalCode)],
                                    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                    [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithPostalCode)]];
    [PostalCodeToolbarCode sizeToFit];
    self.postalCodeTxt.inputAccessoryView = PostalCodeToolbarCode;
    self.postalCodeEditTxt.inputAccessoryView = PostalCodeToolbarCode;
    
    UIToolbar* numberToolbarPersonal = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarPersonal.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarPersonal.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadPersonal)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadPersonal)]];
    [numberToolbarPersonal sizeToFit];
    self.mobileNumberTxt.inputAccessoryView = numberToolbarPersonal;
    
    
    UIToolbar* numberToolbarCodePersonal = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarCodePersonal.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarCodePersonal.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadCodePersonal)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadCodePersonal)]];
    [numberToolbarCodePersonal sizeToFit];
    self.codeTxt.inputAccessoryView = numberToolbarCodePersonal;
    
    //[self registerForKeyboardNotifications];
    //[self registerForKeyboardNotifications2];
    //[self registerForKeyboardNotifications3];
    self.editScroll.contentSize = CGSizeMake(self.editScroll.frame.size.width, self.editScroll.frame.size.height + 110);
    self.editScrollPersonal.contentSize = CGSizeMake(self.editScrollPersonal.frame.size.width, self.editScrollPersonal.frame.size.height + 30);
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)fetchUserDataformSever{
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    NSLog(@"%@",userData);
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getUserDetailsWithDataDictionary:userData CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         
         if ([ApplicationDelegate isValid:responseDictionary])
         {
             if (responseDictionary.count>0)
             {
                 
                 Userinformation *userObj = [ApplicationDelegate.mapper getUserinformationFromDictionary:responseDictionary];
                 NSLog(@"%@",userObj);
                 NSLog(@"%@",responseDictionary);
                 
                 if ([userObj.isSeller isEqualToNumber:[NSNumber numberWithInt:0]]) {
                     self.bankDetailsBtn.enabled = NO;
                     self.bankDetailsBtn.userInteractionEnabled = NO;
                     self.bankDetailsBtn.alpha = .5;
                 }
                 
                 //NSLog(@"%@",userObj.firstName);
                 self.myAccountBackView.hidden = NO;
                 NSLog(@"%@",[[responseDictionary objectForKey:@"Pinfo"] objectForKey:@"FirstName"]);
                 self.firstNameTxt.text = userObj.firstName;
                 self.lastNameTxt.text = userObj.lastName;
                 self.codeTxt.text = userObj.personalMobCode;
                 self.mobileNumberTxt.text = userObj.personalMobileNumber;
                 self.emailAddressTxt.text = userObj.email;
                 
                 self.firstNameEditTxt.text = userObj.firstName;
                 self.lastNameEditTxt.text = userObj.lastName;
                 self.codeEditTxt.text = userObj.personalMobCode;
                 personalCodeBuffer = [[NSString alloc]initWithString:userObj.personalMobCode];
                 self.mobileNumEditTxt.text = userObj.personalMobileNumber;
                 personalMobileBuffer = [[NSString alloc]initWithString:userObj.personalMobileNumber];
                 self.emailEditTxt.text = userObj.email;
                 
                 self.nameTxtField.text = userObj.recieverName;
                 self.addressLine1Txt.text = userObj.addressLine1;
                 self.addressLine2Txt.text = userObj.addressLine2;
                 self.cityTxt.text = userObj.city;
                 self.countryTextView.text = userObj.country;
                 self.postalCodeTxt.text = userObj.postalCode;
                 self.codeTextField.text = userObj.mobCode;
                 self.mobileNumberTextField.text = userObj.mobileNumber;
                 
                 NSLog(@"%@",userObj.firstName);
                 self.nameEditTxtField.text = userObj.recieverName;
                 self.addressLine1EditTxt.text = userObj.addressLine1;
                 self.addressLine2EditTxt.text = userObj.addressLine2;
                 self.cityEditTxt.text = userObj.city;
                 self.countryEditTxt.text = userObj.country;
                 self.postalCodeEditTxt.text = userObj.postalCode;
                 deliveryPostalCodeBuffer = [[NSString alloc]initWithString:userObj.postalCode];
                 self.codeDelEditTxt.text = userObj.mobCode;
                 deliveryCodeBuffer = [[NSString alloc]initWithString:userObj.mobCode];
                 self.mobileDelEditTxt.text = userObj.mobileNumber;
                 deliveryMobileBuffer = [[NSString alloc]initWithString:userObj.mobileNumber];
                 
                 if ((self.addressLine1Txt.text.length == 0) && (self.cityTxt.text.length == 0) && (self.codeTextField.text.length == 0) && (self.mobileNumberTextField.text.length == 0))
                 {
                     self.nameTxtField.hidden = YES;
                     self.editDeliveryAddressBtn.hidden = YES;
                     self.enterDeliveryAddressTxt.hidden = YES;
                     self.addressLine1Txt.hidden = YES;
                     self.addressLine2Txt.hidden = YES;
                     self.cityTxt.hidden = YES;
                     self.countryTextView.hidden = YES;
                     self.dropDownUpArrow.hidden = YES;
                     self.dropDownDwnArrow.hidden = YES;
                     self.dropBtn.hidden = YES;
                     self.postalCodeTxt.hidden = YES;
                     self.seperatorTxtView.hidden = YES;
                     self.plusLbl.hidden = YES;
                     self.mobileNumberTextField.hidden = YES;
                     self.codeTextField.hidden = YES;
                     
                     self.conditionLbl.frame = CGRectMake(8, self.addDeliveryAddressBtn.frame.origin.y + self.addDeliveryAddressBtn.frame.size.height + 13, 288, 34);
                     self.bankDetailsBtn.frame = CGRectMake(67, self.conditionLbl.frame.origin.y + self.conditionLbl.frame.size.height + 12, 171, 35);
                     
                     self.backWhiteView.frame = CGRectMake(self.backWhiteView.frame.origin.x, self.backWhiteView.frame.origin.y, self.backWhiteView.frame.size.width, self.bankDetailsBtn.frame.origin.y + self.bankDetailsBtn.frame.size.height + 45);
                     
                     self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.backWhiteView.frame.size.height);
                 }
                 else{
                     self.addDeliveryAddressBtn.hidden = YES;
                 }
                 
             }
         }
         
         [ApplicationDelegate removeProgressHUD];
     } errorHandler:^(NSError *error) {
         
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}

-(NSMutableDictionary *)getUserPostData{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

-(NSMutableDictionary*)getDeliveryInfo
{
    
    NSMutableDictionary *DetailsDictionary = [NSMutableDictionary dictionary];
    [DetailsDictionary setObject:self.addressLine1EditTxt.text forKey:@"AddressLine1"];
    [DetailsDictionary setObject:self.addressLine2EditTxt.text forKey:@"AddressLine2"];
    [DetailsDictionary setObject:self.cityEditTxt.text forKey:@"City"];
    [DetailsDictionary setObject:self.countryEditTxt.text forKey:@"Country"];
    [DetailsDictionary setObject:self.codeDelEditTxt.text forKey:@"MobCode"];
    [DetailsDictionary setObject:self.mobileDelEditTxt.text forKey:@"MobileNumber"];
    [DetailsDictionary setObject:self.postalCodeEditTxt.text forKey:@"PostalCode"];
    [DetailsDictionary setObject:self.nameEditTxtField.text forKey:@"RecieverName"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [DetailsDictionary setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [DetailsDictionary setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [DetailsDictionary setObject:kENGLISH forKey:@"lang_key"];
    }
    return DetailsDictionary;
}

//-(NSMutableDictionary*)getDeliveryInfo
//{
//    
//    NSMutableDictionary *DetailsDictionary = [NSMutableDictionary dictionary];
//    [DetailsDictionary setObject:self.addressLine1Txt.text forKey:@"AddressLine1"];
//    [DetailsDictionary setObject:self.addressLine2Txt.text forKey:@"AddressLine2"];
//    [DetailsDictionary setObject:self.cityTxt.text forKey:@"City"];
//    [DetailsDictionary setObject:self.countryTextView.text forKey:@"Country"];
//    [DetailsDictionary setObject:self.codeTextField.text forKey:@"MobCode"];
//    [DetailsDictionary setObject:self.mobileNumberTextField.text forKey:@"MobileNumber"];
//    [DetailsDictionary setObject:self.postalCodeTxt.text forKey:@"PostalCode"];
//    [DetailsDictionary setObject:self.firstNameTxt.text forKey:@"RecieverName"];
//    
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    //NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
//    [DetailsDictionary setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
//    //[postDic setObject:kENGLISH forKey:@"lang_key"];
//    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
//    {
//        [DetailsDictionary setObject:kARABIC forKey:@"lang_key"];
//    }
//    else{
//        [DetailsDictionary setObject:kENGLISH forKey:@"lang_key"];
//    }
//    return DetailsDictionary;
//}


-(NSMutableDictionary*)getpersonalInfo
{
    NSMutableDictionary *PersonalDictionary = [NSMutableDictionary dictionary];
    [PersonalDictionary setObject:self.emailEditTxt.text forKey:@"Email"];
    [PersonalDictionary setObject:self.firstNameEditTxt.text forKey:@"FirstName"];
    [PersonalDictionary setObject:self.lastNameEditTxt.text forKey:@"LastName"];
    [PersonalDictionary setObject:self.codeEditTxt.text forKey:@"MobCode"];
    [PersonalDictionary setObject:self.mobileNumEditTxt.text forKey:@"MobileNumber"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [PersonalDictionary setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [PersonalDictionary setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [PersonalDictionary setObject:kENGLISH forKey:@"lang_key"];
    }
    return PersonalDictionary;
}

//-(NSMutableDictionary*)getpersonalInfo
//{
//    
//    NSMutableDictionary *PersonalDictionary = [NSMutableDictionary dictionary];
//    [PersonalDictionary setObject:self.emailAddressTxt.text forKey:@"Email"];
//    [PersonalDictionary setObject:self.firstNameTxt.text forKey:@"FirstName"];
//    [PersonalDictionary setObject:self.lastNameTxt.text forKey:@"LastName"];
//    [PersonalDictionary setObject:self.codeTxt.text forKey:@"MobCode"];
//    [PersonalDictionary setObject:self.mobileNumberTxt.text forKey:@"MobileNumber"];
//    return PersonalDictionary;
//}



-(void)fetchCountryDataformSever{
    
    NSMutableDictionary *countryData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserCountryPostData]];
    NSLog(@"%@",countryData);
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine getCountryDetailsWithDataDictionary:countryData CompletionHandler:^(NSMutableArray *responseArray)
     {
         
         if ([ApplicationDelegate isValid:responseArray])
         {
             //NSLog(@"%@",responseArray);
             if (responseArray.count>0)
             {
                 self.countryCodeAndName = [[NSMutableArray alloc]init];
                 countryNameArray = [[NSMutableArray alloc]init];
                 countryCodeArray = [[NSMutableArray alloc]init];
                 for (int i=0; i<responseArray.count; i++)
                 {
                     [self.countryCodeAndName addObject:[responseArray objectAtIndex:i]];
                     [countryNameArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"CountryName"]];
                     [countryCodeArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"CountryCode"]];
                     
                     NSLog(@"%@",[[responseArray objectAtIndex:i]objectForKey:@"CountryName"]);
                     NSLog(@"%@",[[responseArray objectAtIndex:i]objectForKey:@"CountryCode"]);
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
         }
         
     } errorHandler:^(NSError *error) {
         
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
}


-(NSMutableDictionary *)getUserCountryPostData{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"lang_key"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"lang_key"];
    }
    return postDic;
}

- (IBAction)dropDownActn:(UIButton *)sender {
    
    //countryNameArray = [[NSMutableArray alloc] initWithObjects:@"India",@"Pakistan",@"India1",@"Pakistan1",@"India2",@"Pakistan2", nil];
    dropDownTag = 0;
    self.dropBtn.selected = !self.dropBtn.selected;
    if (self.dropBtn.selected)
    {
        self.dropDownUpArrow.hidden = NO;
        self.dropDownDwnArrow.hidden = YES;
        if (self.countryDropDownObj.view.superview) {
            [self.countryDropDownObj.view removeFromSuperview];
        }
        
        self.countryDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.countryDropDownObj.cellHeight = 40.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.countryDropDownObj.view.frame = CGRectMake((self.countryTextView.frame.origin.x),(self.countryTextView.frame.origin.y+self.countryTextView.frame.size.height), (self.countryTextView.frame.size.width), 0);
        
        self.countryDropDownObj.dropDownDelegate = self;
        self.countryDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:countryNameArray];
        
        self.countryDropDownObj.view.layer.borderWidth = 0.1;
        self.countryDropDownObj.view.layer.shadowOpacity = 1.0;
        self.countryDropDownObj.view.layer.shadowRadius = 5.0;
        self.countryDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.countryDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.countryDropDownObj.textLabelColor = [UIColor blackColor];
        self.countryDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (countryNameArray.count>0)
        {
            [self.dropBtn.superview addSubview:self.countryDropDownObj.view];
        }
        else
        {
            self.dropBtn.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)+18.0f;
        }
        //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.countryDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.countryDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x+2,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.dropDownUpArrow.hidden = YES;
        self.dropDownDwnArrow.hidden = NO;
        self.countryDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.countryDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.countryDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
}

-(void)selectList:(int)selectedIndex
{
    if (dropDownTag == 0) {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.countryDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.dropBtn.selected = NO;
        self.dropDownUpArrow.hidden = YES;
        self.dropDownDwnArrow.hidden = NO;
        self.countryTextView.text = [countryNameArray objectAtIndex:selectedIndex];
    } else {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.countryDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.dropBtnEdit.selected = NO;
        self.dropDownUpArrowEdit.hidden = YES;
        self.dropDownDwnArrowEdit.hidden = NO;
        self.countryEditTxt.text = [countryNameArray objectAtIndex:selectedIndex];
    }
}

- (IBAction)bankDetailsBtnActn:(UIButton *)sender {
    
//    if ((self.addressLine1Txt.text.length == 0) && (self.cityTxt.text.length == 0) && (self.countryTextView.text.length == 0) && (self.codeTextField.text.length == 0) && (self.mobileNumberTextField.text.length == 0))
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address details") title:localize(@"Sorry..!")];
//        
//        
//    } else if(self.addressLine1Txt.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.cityTxt.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your city") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.countryTextView.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.codeTextField.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter country code") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.mobileNumberTextField.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.firstNameTxt.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your first name") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.lastNameTxt.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your last name") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.codeTxt.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country code") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.mobileNumberTxt.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
//        
//    }
//    else if(self.emailAddressTxt.text.length == 0)
//    {
//        
//        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your email address") title:localize(@"Sorry..!")];
//        
//    }
//    
//    else{
//        if (![self NSStringIsValidEmail:self.emailAddressTxt.text])
//        {
//            
//            [ApplicationDelegate showAlertWithMessage:localize(@"Please enter a valid email id") title:localize(@"Sorry..!")];
//            
//            
//        }
//        else if(!([self.countryTextView.text isEqualToString:@"Kuwait"]||[self.countryTextView.text isEqualToString:@"الكويت"])){
//            //NSLog(@"%@",self.countryTextView.text);
//            if(self.postalCodeTxt.text.length == 0)
//            {
//                
//                [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your postal code") title:localize(@"Sorry..!")];
//                
//            }
//        }
//        else {
//            if (self.editPersonalInfoBtn.selected || self.editDeliveryAddressBtn.selected) {
//                NSLog(@"Selected");
//                [self postAddressDetails];
//            } else {
//                MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
//                bankDetailsVC.view.backgroundColor = [UIColor clearColor];
//                
//                if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
//                {
//                    [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
//                }
//            }
//        }
//    }

     
    MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
    bankDetailsVC.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
    }
     

}

-(void)postAddressDetails{
    NSDictionary *requestDictionary = @{@"AccParam": self.getUserPostData,@"DInfo": self.getDeliveryInfo,@"PInfo": self.getpersonalInfo};
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    [ApplicationDelegate.engine updateUserAccount:requestDictionary CompletionHandler:^(NSMutableDictionary *responseDic) {
        
        if ([[responseDic objectForKey:@"Success"] integerValue])
        {
            [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
            MyBankDetailsViewController * bankDetailsVC = [[MyBankDetailsViewController alloc]initWithNibName:@"MyBankDetailsViewController" bundle:nil];
            bankDetailsVC.view.backgroundColor = [UIColor clearColor];
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[MyBankDetailsViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:bankDetailsVC animated:NO];
            }
            
            [ApplicationDelegate removeProgressHUD];
        }
        
        else
        {
            [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
            
            [ApplicationDelegate removeProgressHUD];
            //self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
            
        }
        
    } errorHandler:^(NSError *error) {
        
        
        [ApplicationDelegate removeProgressHUD];
    }
     ];
}

- (IBAction)changePasswordBtnActn:(UIButton *)sender {
    [self.view addSubview:self.changePasswordBackView];
    [self.changePasswordBackView setFrame:self.changePasswordBackView.superview.bounds];
}
- (IBAction)saveChangesBtnActn:(UIButton *)sender {
    
    if ((self.addressLine1Txt.text.length == 0) && (self.cityTxt.text.length == 0) && (self.countryTextView.text.length == 0) && (self.codeTextField.text.length == 0) && (self.mobileNumberTextField.text.length == 0))
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address details") title:localize(@"Sorry..!")];
        
        
    } else if(self.addressLine1Txt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address") title:localize(@"Sorry..!")];
        
    }
    else if(self.cityTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your city") title:localize(@"Sorry..!")];
        
    }
    else if(self.countryTextView.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country") title:localize(@"Sorry..!")];
        
    }
    else if(self.codeTextField.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter country code") title:localize(@"Sorry..!")];
        
    }
    else if(self.mobileNumberTextField.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
        
    }
    else if(self.firstNameTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your first name") title:localize(@"Sorry..!")];
        
    }
    else if(self.lastNameTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your last name") title:localize(@"Sorry..!")];
        
    }
    else if(self.codeTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country code") title:localize(@"Sorry..!")];
        
    }
    else if(self.mobileNumberTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
        
    }
    else if(self.emailAddressTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your email address") title:localize(@"Sorry..!")];
        
    }
    
    else{
        if (![self NSStringIsValidEmail:self.emailAddressTxt.text])
        {
            
            [ApplicationDelegate showAlertWithMessage:localize(@"Please enter a valid email id") title:localize(@"Sorry..!")];
            
            
        }
        else if(!([self.countryTextView.text isEqualToString:@"Kuwait"]||[self.countryTextView.text isEqualToString:@"الكويت"])){
            //NSLog(@"%@",self.countryTextView.text);
            if(self.postalCodeTxt.text.length == 0)
            {
                
                [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your postal code") title:localize(@"Sorry..!")];
                
            }
        }
        else{
            NSDictionary *requestDictionary = @{@"AccParam": self.getUserPostData,@"DInfo": self.getDeliveryInfo,@"PInfo": self.getpersonalInfo};
            
            NSLog(@"%@",requestDictionary);
            
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            [ApplicationDelegate.engine updateUserAccount:requestDictionary CompletionHandler:^(NSMutableDictionary *responseDic) {
                
                [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                
                [ApplicationDelegate removeProgressHUD];
                
            } errorHandler:^(NSError *error) {
                
                
                [ApplicationDelegate removeProgressHUD];
            }
             ];
        }
    }
    /*
     
    NSDictionary *requestDictionary = @{@"AccParam": self.getUserPostData,@"DInfo": self.getDeliveryInfo,@"PInfo": self.getpersonalInfo};
     
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    [ApplicationDelegate.engine updateUserAccount:requestDictionary CompletionHandler:^(NSMutableDictionary *responseDic) {
       
        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
        
        [ApplicationDelegate removeProgressHUD];
        
    } errorHandler:^(NSError *error) {
        
        
        [ApplicationDelegate removeProgressHUD];        
    }
     ];
    */
}

- (IBAction)editPersonalInfoBtnActn:(UIButton *)sender {
//    self.editPersonalInfoBtn.selected = YES;
//    self.firstNameTxt.enabled = YES;
//    self.lastNameTxt.enabled = YES;
//    self.codeTxt.enabled = YES;
//    self.mobileNumberTxt.enabled = YES;
//    self.emailAddressTxt.enabled = YES;
    
    self.firstNameEditTxt.text = self.firstNameTxt.text;
    self.lastNameEditTxt.text = self.lastNameTxt.text;
    
    self.emailEditTxt.text = self.emailAddressTxt.text;
    
    self.codeEditTxt.text = personalCodeBuffer;
    self.mobileNumEditTxt.text = personalMobileBuffer;
    
    [self.view addSubview:self.editPersonalInfoBackView];
    [self.editPersonalInfoBackView setFrame:self.editPersonalInfoBackView.superview.bounds];
}
- (IBAction)editDeliveryAdderssBtnActn:(UIButton *)sender {
//    self.editDeliveryAddressBtn.selected = YES;
//    self.addressLine1Txt.enabled = YES;
//    self.addressLine2Txt.enabled = YES;
//    self.cityTxt.enabled = YES;
//    self.dropDownDwnArrow.hidden = NO;
//    self.dropDownUpArrow.hidden = YES;
//    self.dropBtn.enabled = YES;
//    self.postalCodeTxt.enabled = YES;
//    self.codeTextField.enabled = YES;
//    self.mobileNumberTextField.enabled = YES;
    
    self.nameEditTxtField.text = self.nameTxtField.text;
    self.addressLine1EditTxt.text = self.addressLine1Txt.text;
    self.addressLine2EditTxt.text = self.addressLine2Txt.text;
    self.cityEditTxt.text = self.cityTxt.text;
    self.countryEditTxt.text = self.countryTextView.text;
    
    self.postalCodeEditTxt.text = deliveryPostalCodeBuffer;
    self.codeDelEditTxt.text = deliveryCodeBuffer;
    self.mobileDelEditTxt.text = deliveryMobileBuffer;
    
    [self.view addSubview:self.editDeliveryAddressBackView];
    [self.editDeliveryAddressBackView setFrame:self.editDeliveryAddressBackView.superview.bounds];
}

- (void)limitTextField:(NSNotification *)note {
    int limit = 10;
    int codeLimit = 3;
    int postcodeLimit=6;
    if (self.mobileNumberTextField.text.length > limit) {
        [self.mobileNumberTextField setText:[self.mobileNumberTextField.text substringToIndex:limit]];
    }
    else if(self.mobileNumberTxt.text.length > limit){
        [self.mobileNumberTxt setText:[self.mobileNumberTxt.text substringToIndex:limit]];
    }
    else if(self.codeTextField.text.length > codeLimit){
        [self.codeTextField setText:[self.codeTextField.text substringToIndex:codeLimit]];
    }
    else if(self.codeTxt.text.length > codeLimit){
        [self.codeTxt setText:[self.codeTxt.text substringToIndex:codeLimit]];
    }
    else if(self.postalCodeTxt.text.length > postcodeLimit){
        [self.postalCodeTxt setText:[self.postalCodeTxt.text substringToIndex:postcodeLimit]];
    }
    
    if (self.mobileDelEditTxt.text.length > limit) {
        [self.mobileDelEditTxt setText:[self.mobileDelEditTxt.text substringToIndex:limit]];
    }
    else if(self.codeDelEditTxt.text.length > codeLimit){
        [self.codeDelEditTxt setText:[self.codeDelEditTxt.text substringToIndex:codeLimit]];
    }
    else if(self.postalCodeEditTxt.text.length > postcodeLimit){
        [self.postalCodeEditTxt setText:[self.postalCodeEditTxt.text substringToIndex:postcodeLimit]];
    }
    else if(self.codeEditTxt.text.length > codeLimit){
        [self.codeEditTxt setText:[self.codeEditTxt.text substringToIndex:codeLimit]];
    }
    else if(self.mobileNumEditTxt.text.length > limit){
        [self.mobileNumEditTxt setText:[self.mobileNumEditTxt.text substringToIndex:limit]];
    }
}

- (IBAction)closeBtnAction:(UIButton *)sender {
    [self.changePasswordBackView removeFromSuperview];
    [self.editPersonalInfoBackView removeFromSuperview];
    [self.editDeliveryAddressBackView removeFromSuperview];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)submitBtnActn:(UIButton *)sender {
    if (self.oldPasswordTxt.text.length == 0) {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your old password") title:localize(@"Sorry..!")];
    }
    else if(self.passwordNewTxt.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your new password") title:localize(@"Sorry..!")];
    }
    else if(self.confirmPasswordTxt.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:localize(@"Please confirm your new password") title:localize(@"Sorry..!")];
    }
    else if (![self.passwordNewTxt.text isEqualToString:self.confirmPasswordTxt.text]){
        [ApplicationDelegate showAlertWithMessage:localize(@"Confirmation password is wrong") title:localize(@"Sorry..!")];
    }
    else{
        [self postPasswordDetails];
    }
}

-(void)postPasswordDetails{
    NSMutableDictionary *passwordData = [[NSMutableDictionary alloc] initWithDictionary:[self getPasswordPostData]];
    NSLog(@"%@",passwordData);
    if (passwordData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine EditPasswordWithDataDictionary:passwordData CompletionHandler:^(NSMutableDictionary *responseDic)
         
         {
             if ([ApplicationDelegate isValid:responseDic])
             {
                 
                 if (responseDic.count>0)
                 {
                     NSLog(@"%@",responseDic);
                     if ([[responseDic objectForKey:@"Success"] integerValue])
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                         [self.changePasswordBackView removeFromSuperview];
                         
                         [ApplicationDelegate removeProgressHUD];
                         self.passwordNewTxt.text = @"";
                         self.oldPasswordTxt.text = @"";
                         self.confirmPasswordTxt.text = @"";
                     }
                     
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                         self.passwordNewTxt.text = @"";
                         self.oldPasswordTxt.text = @"";
                         self.confirmPasswordTxt.text = @"";
                         
                         [ApplicationDelegate removeProgressHUD];
                         //self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
                         
                     }
                     
                     
                     
                 }
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
             
         }];
        
    }
}

-(NSMutableDictionary *)getPasswordPostData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.passwordNewTxt.text forKey:@"NewPassword"];
    [postDic setObject:self.oldPasswordTxt.text forKey:@"OldPassword"];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    //[postDic setObject:kENGLISH forKey:@"lang_key"];
    //[postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    return postDic;
}

-(void)cancelNumberPad{
    [ self.mobileNumberTextField resignFirstResponder];
    //self.mobileNumberTextField.text = @"";
    [ self.mobileNumEditTxt resignFirstResponder];
    self.mobileNumEditTxt.text = @"";
}

-(void)cancelNumberPadDel{
    [ self.mobileNumberTextField resignFirstResponder];
    //self.mobileNumberTextField.text = @"";
    [ self.mobileDelEditTxt resignFirstResponder];
    self.mobileDelEditTxt.text = @"";
}

-(void)doneWithNumberPad{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.mobileNumberTextField resignFirstResponder];
    [self.mobileNumEditTxt resignFirstResponder];
}
-(void)doneWithNumberPadDel{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.mobileNumberTextField resignFirstResponder];
    [self.mobileDelEditTxt resignFirstResponder];
}

-(void)cancelNumberPadCode{
    [ self.codeTextField resignFirstResponder];
    //self.codeTextField.text = @"";
    [ self.codeEditTxt resignFirstResponder];
    self.codeEditTxt.text = @"";
}

-(void)cancelNumberPadCodeDel{
    [ self.codeTextField resignFirstResponder];
    //self.codeTextField.text = @"";
    [ self.codeDelEditTxt resignFirstResponder];
    self.codeDelEditTxt.text = @"";
}

-(void)doneWithNumberPadCode{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.codeTextField resignFirstResponder];
    //[self.codeDelEditTxt resignFirstResponder];
    [self.codeEditTxt resignFirstResponder];
}

-(void)doneWithNumberPadCodeDel{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.codeTextField resignFirstResponder];
    [self.codeDelEditTxt resignFirstResponder];
    //[self.codeEditTxt resignFirstResponder];
}
-(void)cancelNumberPadPersonal{
    [ self.mobileNumberTxt resignFirstResponder];
    self.mobileNumberTxt.text = @"";
}

-(void)doneWithNumberPadPersonal{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.mobileNumberTxt resignFirstResponder];
}


-(void)cancelNumberPadCodePersonal{
    [ self.codeTxt resignFirstResponder];
    self.codeTxt.text = @"";
}

-(void)doneWithNumberPadCodePersonal{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.codeTxt resignFirstResponder];
}

-(void)cancelPostalCode{
    [ self.postalCodeTxt resignFirstResponder];
    //self.postalCodeTxt.text = @"";
    [ self.postalCodeEditTxt resignFirstResponder];
    self.postalCodeEditTxt.text = @"";
}

-(void)doneWithPostalCode{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.postalCodeTxt resignFirstResponder];
    [self.postalCodeEditTxt resignFirstResponder];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.myAccountBackView.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.firstNameTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.firstNameTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.lastNameTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.lastNameTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.codeTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.codeTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.mobileNumberTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.mobileNumberTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.emailAddressTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.emailAddressTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.addressLine1Txt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.addressLine1Txt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.addressLine2Txt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.addressLine2Txt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.cityTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.cityTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.codeTextField.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.codeTextField.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.mobileNumberTextField.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.mobileNumberTextField.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.postalCodeTxt.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:self.postalCodeTxt.frame animated:YES];
    }
    
    /* NSDictionary* info = [aNotification userInfo];
     CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
     CGRect bkgndRect = self.mTextField.superview.frame;
     bkgndRect.size.height += kbSize.height;
     [self.mTextField.superview setFrame:bkgndRect];
     [self.mScrollView setContentOffset:CGPointMake(0.0, self.mTextField.frame.origin.y-kbSize.height) animated:YES];*/
    
    
    
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.backWhiteView.frame.size.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)xibLoading
{
    NSString *nibName = @"MyAccountViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
    
    
}

- (IBAction)dropDownEditActn:(UIButton *)sender {
    dropDownTag = 1;
    self.dropBtnEdit.selected = !self.dropBtnEdit.selected;
    if (self.dropBtnEdit.selected)
    {
        self.dropDownUpArrowEdit.hidden = NO;
        self.dropDownDwnArrowEdit.hidden = YES;
        if (self.countryDropDownObj.view.superview) {
            [self.countryDropDownObj.view removeFromSuperview];
        }
        
        self.countryDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.countryDropDownObj.cellHeight = 40.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.countryDropDownObj.view.frame = CGRectMake((self.countryEditTxt.frame.origin.x),(self.countryEditTxt.frame.origin.y+self.countryEditTxt.frame.size.height), (self.countryEditTxt.frame.size.width), 0);
        
        self.countryDropDownObj.dropDownDelegate = self;
        self.countryDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:countryNameArray];
        
        self.countryDropDownObj.view.layer.borderWidth = 0.1;
        self.countryDropDownObj.view.layer.shadowOpacity = 1.0;
        self.countryDropDownObj.view.layer.shadowRadius = 5.0;
        self.countryDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.countryDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.countryDropDownObj.textLabelColor = [UIColor blackColor];
        self.countryDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (countryNameArray.count>0)
        {
            [self.dropBtnEdit.superview addSubview:self.countryDropDownObj.view];
        }
        else
        {
            self.dropBtnEdit.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)+18.0f;
        }
        //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.countryDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.countryDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x+2,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.dropDownUpArrowEdit.hidden = YES;
        self.dropDownDwnArrowEdit.hidden = NO;
        self.countryDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.countryDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.countryDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
}
- (IBAction)addDeliveryAddressBtnActn:(UIButton *)sender {
    [self.view addSubview:self.editDeliveryAddressBackView];
    [self.editDeliveryAddressBackView setFrame:self.editDeliveryAddressBackView.superview.bounds];
}
- (IBAction)saveDeliveryAddressBtnActn:(UIButton *)sender {
    if ((self.addressLine1EditTxt.text.length == 0) && (self.cityEditTxt.text.length == 0) && (self.countryEditTxt.text.length == 0) && (self.codeDelEditTxt.text.length == 0) && (self.mobileDelEditTxt.text.length == 0))
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address details") title:localize(@"Sorry..!")];
        
        
    } else if(self.addressLine1EditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your address") title:localize(@"Sorry..!")];
        
    }
    else if(self.cityEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your city") title:localize(@"Sorry..!")];
        
    }
    else if(self.countryEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country") title:localize(@"Sorry..!")];
        
    }
    else if(self.codeDelEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter country code") title:localize(@"Sorry..!")];
        
    }
    else if(self.mobileDelEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
        
    }
    
    else{
        
        if(!([self.countryEditTxt.text isEqualToString:@"Kuwait"]||[self.countryEditTxt.text isEqualToString:@"الكويت"])){
            //NSLog(@"%@",self.countryTextView.text);
            if(self.postalCodeEditTxt.text.length == 0)
            {
                
                [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your postal code") title:localize(@"Sorry..!")];
                
            }
            else{
                //            NSDictionary *requestDictionary = @{@"AccParam": self.getUserPostData,@"DInfo": self.getDeliveryInfo,@"PInfo": self.getpersonalInfo};
                
                NSDictionary *requestDictionary = self.getDeliveryInfo;
                
                NSLog(@"%@",requestDictionary);
                
                [ApplicationDelegate addProgressHUDToView:self.view];
                
                [ApplicationDelegate.engine UpdateDeliveryAddress:requestDictionary CompletionHandler:^(NSMutableDictionary *responseDic) {
                    
                    if ([[responseDic objectForKey:@"Success"] integerValue])
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                        
                        [ApplicationDelegate removeProgressHUD];
                        [self viewWillAppear:YES];
                        [self viewDidLoad];
                    }
                    else
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                        
                        [ApplicationDelegate removeProgressHUD];
                        //self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
                        
                    }
                    
                } errorHandler:^(NSError *error) {
                    
                    
                    [ApplicationDelegate removeProgressHUD];
                    [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
                }
                 ];
            }
        }
        else{
//            NSDictionary *requestDictionary = @{@"AccParam": self.getUserPostData,@"DInfo": self.getDeliveryInfo,@"PInfo": self.getpersonalInfo};
            
            NSDictionary *requestDictionary = self.getDeliveryInfo;
            
            NSLog(@"%@",requestDictionary);
            
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            [ApplicationDelegate.engine UpdateDeliveryAddress:requestDictionary CompletionHandler:^(NSMutableDictionary *responseDic) {
                
                if ([[responseDic objectForKey:@"Success"] integerValue])
                {
                    [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    [self viewWillAppear:YES];
                    [self viewDidLoad];
                }
                else
                {
                    [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    //self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
                    
                }
                
            } errorHandler:^(NSError *error) {
                
                
                [ApplicationDelegate removeProgressHUD];
                [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
            }
             ];
        }
    }
}
- (IBAction)savePersonalInfoBtnActn:(UIButton *)sender {
    if ((self.firstNameEditTxt.text.length == 0) && (self.lastNameEditTxt.text.length == 0) && (self.mobileNumEditTxt.text.length == 0) && (self.codeEditTxt.text.length == 0) && (self.emailEditTxt.text.length == 0))
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your details") title:localize(@"Sorry..!")];
        
        
    }     else if(self.firstNameEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your first name") title:localize(@"Sorry..!")];
        
    }
    else if(self.lastNameEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your last name") title:localize(@"Sorry..!")];
        
    }
    else if(self.codeEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your country code") title:localize(@"Sorry..!")];
        
    }
    else if(self.mobileNumEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your mobile number") title:localize(@"Sorry..!")];
        
    }
    else if(self.emailEditTxt.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your email address") title:localize(@"Sorry..!")];
        
    }
    
    else{
        if (![self NSStringIsValidEmail:self.emailEditTxt.text])
        {
            
            [ApplicationDelegate showAlertWithMessage:localize(@"Please enter a valid email id") title:localize(@"Sorry..!")];
            
            
        }
        else{
            //NSDictionary *requestDictionary = @{@"AccParam": self.getUserPostData,@"DInfo": self.getDeliveryInfo,@"PInfo": self.getpersonalInfo};
            
            NSDictionary *requestDictionary = self.getpersonalInfo;
            
            NSLog(@"%@",requestDictionary);
            
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            [ApplicationDelegate.engine UpdatePersonalInfo:requestDictionary CompletionHandler:^(NSMutableDictionary *responseDic) {
                
                if ([[responseDic objectForKey:@"Success"] integerValue])
                {
                    [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    [self viewWillAppear:YES];
                    [self viewDidLoad];
                }
                else
                {
                    [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                    
                    [ApplicationDelegate removeProgressHUD];
                    //self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
                    
                }
                
            } errorHandler:^(NSError *error) {
                
                
                [ApplicationDelegate removeProgressHUD];
                [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
            }
             ];
        }
    }
}


- (void)registerForKeyboardNotifications2
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown2:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden2:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown2:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.editScroll.contentInset = contentInsets;
    self.editScroll.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.editBackView.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.nameEditTxtField.frame.origin) ) {
        [self.editScroll scrollRectToVisible:self.nameEditTxtField.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.addressLine1EditTxt.frame.origin) ) {
        [self.editScroll scrollRectToVisible:self.addressLine1EditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.addressLine2EditTxt.frame.origin) ) {
        [self.editScroll scrollRectToVisible:self.addressLine2EditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.cityEditTxt.frame.origin) ) {
        [self.editScroll scrollRectToVisible:self.cityEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.postalCodeEditTxt.frame.origin) ) {
        [self.editScroll scrollRectToVisible:self.postalCodeEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.codeDelEditTxt.frame.origin) ) {
        [self.editScroll scrollRectToVisible:self.codeDelEditTxt.frame animated:YES];
    }
    
    else if (!CGRectContainsPoint(aRect, self.mobileDelEditTxt.frame.origin) ) {
        [self.editScroll scrollRectToVisible:self.mobileDelEditTxt.frame animated:YES];
    }
    
    /* NSDictionary* info = [aNotification userInfo];
     CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
     CGRect bkgndRect = self.mTextField.superview.frame;
     bkgndRect.size.height += kbSize.height;
     [self.mTextField.superview setFrame:bkgndRect];
     [self.mScrollView setContentOffset:CGPointMake(0.0, self.mTextField.frame.origin.y-kbSize.height) animated:YES];*/
}

- (void)keyboardWillBeHidden2:(NSNotification*)aNotification
{
    self.editScroll.contentSize = CGSizeMake(self.editScroll.frame.size.width, self.editBackView.frame.size.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.editScroll.contentInset = contentInsets;
    self.editScroll.scrollIndicatorInsets = contentInsets;
}

- (void)registerForKeyboardNotifications3
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown3:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden3:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown3:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.editScrollPersonal.contentInset = contentInsets;
    self.editScrollPersonal.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.editPersonalBackView.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.firstNameEditTxt.frame.origin) ) {
        [self.editScrollPersonal scrollRectToVisible:self.firstNameEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.lastNameEditTxt.frame.origin) ) {
        [self.editScrollPersonal scrollRectToVisible:self.lastNameEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.codeEditTxt.frame.origin) ) {
        [self.editScrollPersonal scrollRectToVisible:self.codeEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.mobileNumEditTxt.frame.origin) ) {
        [self.editScrollPersonal scrollRectToVisible:self.mobileNumEditTxt.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.emailEditTxt.frame.origin) ) {
        [self.editScrollPersonal scrollRectToVisible:self.emailEditTxt.frame animated:YES];
    }
    
    
    /* NSDictionary* info = [aNotification userInfo];
     CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
     CGRect bkgndRect = self.mTextField.superview.frame;
     bkgndRect.size.height += kbSize.height;
     [self.mTextField.superview setFrame:bkgndRect];
     [self.mScrollView setContentOffset:CGPointMake(0.0, self.mTextField.frame.origin.y-kbSize.height) animated:YES];*/
}

- (void)keyboardWillBeHidden3:(NSNotification*)aNotification
{
    self.editScroll.contentSize = CGSizeMake(self.editScrollPersonal.frame.size.width, self.editPersonalBackView.frame.size.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.editScrollPersonal.contentInset = contentInsets;
    self.editScrollPersonal.scrollIndicatorInsets = contentInsets;
}
@end
