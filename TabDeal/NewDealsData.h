//
//  NewDealsData.h
//  TabDeal
//
//  Created by satheesh on 9/29/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewDealsData : NSObject
@property(nonatomic,retain)NSString *Description;
@property(nonatomic,retain)NSString *Icon;
@property(nonatomic,retain)NSString *Id;
@property(nonatomic,retain)NSString *Image;
@property(nonatomic,retain)NSString *Name;
@property(nonatomic, retain)NSNumber * isSoldOut;
@property(nonatomic, retain)NSNumber * isOnSale;
@end
