//
//  SearchResultCell.m
//  TabDeal
//
//  Created by satheesh on 10/27/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "SearchResultCell.h"

@implementation SearchResultCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"SearchResultCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (SearchResultCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"SearchResultCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (SearchResultCell*)[nibViews objectAtIndex: 0];
        }
    }
    return self;
}

- (IBAction)Buttonclicked:(UIButton *)sender {
    
    [self.delegateSearchResultItem searchResultItemDidClicked:self];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
