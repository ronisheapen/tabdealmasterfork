//
//  SubmittedItemsViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 23/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmittedItemsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    //NSUserDefaults * actionStatus;
    NSUserDefaults * actionTracking;
}

@property (strong, nonatomic) IBOutlet UITableView *SubmittedTable;
@property (strong, nonatomic) NSMutableArray * SubmittedDataArray;
@property (weak, nonatomic) IBOutlet UIImageView *warningBackView;
@property (weak, nonatomic) IBOutlet UILabel *warningLbl;

@property (weak, nonatomic) IBOutlet UILabel *headingLbl;
@end
