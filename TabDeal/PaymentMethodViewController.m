//
//  PaymentMethodViewController.m
//  TabDeal
//
//  Created by satheesh on 10/7/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "PaymentMethodViewController.h"
#import "BillingAddressViewController.h"
#import "HomeViewController.h"

@interface PaymentMethodViewController ()

@end

@implementation PaymentMethodViewController

-(void) viewWillAppear:(BOOL)animated
{
    [self xibLoading];
    [self fontIntilization];
    actionTracking = [NSUserDefaults standardUserDefaults];
    self.SubTotal.text = [actionTracking objectForKey:@"SubTotal"];
    self.DeliveryCharge.text = [actionTracking objectForKey:@"DeliveryCharge"];
    self.TotalCharge.text = [actionTracking objectForKey:@"TotalCharge"];
    [actionTracking synchronize];
    
    self.paymentMethod = [[NSString alloc]init];
    if ([self.cashOnDelFlag isEqualToString:@"0"])
    {
        self.cashOnDelBtn.userInteractionEnabled = NO;
        self.cashOnDelBtn.enabled = NO;
        self.cashOnDeliveryLbl.enabled = NO;
    }
    else
    {
        self.cashOnDelBtn.selected = YES;
    }
    self.paymentMethod = @"COD";
}

-(void)viewDidAppear:(BOOL)animated
{
    if ([self.cashOnDelFlag isEqualToString:@"0"])
    {
        self.cashOnDelBtn.userInteractionEnabled = NO;
        self.cashOnDelBtn.selected = NO;
    }
    else
    {
        self.cashOnDelBtn.selected = YES;
    }
    self.subTotalLbl.text = localize(@"Sub Total");
    self.deliveryChargesLbl.text = localize(@"Delivery Charges");
    self.totalLbl.text = localize(@"Total");
    self.headdingLbl.text = localize(@"PLEASE CHOOSE YOUR PAYMENT METHOD :");
    self.cashOnDeliveryLbl.text = localize(@"CASH ON DELIVERY (only for kuwait users):");
    self.conditionLbl.text = localize(@"Delivery charges must be paid whether selected items have or have not been purchased");
    [self.backToHomeBtn setTitle:localize(@"BACK TO HOME") forState:UIControlStateNormal];
    [self.proceedBtn setTitle:localize(@"PROCEED") forState:UIControlStateNormal];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)proceedBtnActn:(UIButton *)sender
{
    if (self.knetBtn.selected || self.visaBtn.selected || self.masterBtn.selected || self.americanBtn.selected || self.cashOnDelBtn.selected )
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableDictionary *updatePurchaseData = [[NSMutableDictionary alloc] initWithDictionary:[self getUpdatePurchaseData]];
        
        NSLog(@"%@",updatePurchaseData);
        
        if (self.cashOnDelBtn.selected) {
            
            [ApplicationDelegate.engine loadUpdatePurchaseWithDictionary:updatePurchaseData CompletionHandler:^(NSMutableDictionary *responseDict)
             {
                 {
                     if ([ApplicationDelegate isValid:responseDict])
                     {
                         if (responseDict.count>0)
                         {
                             if ([[responseDict objectForKey:@"Success"] integerValue])
                             {
                                 BillingAddressViewController * billingAddressVC = [[BillingAddressViewController alloc]initWithNibName:@"BillingAddressViewController" bundle:nil];
                                 billingAddressVC.view.backgroundColor = [UIColor clearColor];
                                 billingAddressVC.SubTotal.text=self.SubTotal.text;
                                 billingAddressVC.DeliveryCharge.text=self.DeliveryCharge.text;
                                 billingAddressVC.TotalCharge.text=self.TotalCharge.text;
                                 billingAddressVC.orderId = self.orderId;
                                 if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[BillingAddressViewController class]])
                                 {
                                     
                                 }
                                 self.responseBackView.frame = self.view.frame;
                                 [self.view addSubview:self.responseBackView];
                                 self.topBackView.hidden = YES;
                                 self.seperatorView.hidden = YES;
                                 self.downBackView.hidden = YES;
                                 
                                 [HomeViewController sharedViewController].backArrowView.hidden = YES;
                                 [HomeViewController sharedViewController].backBttn.hidden = YES;
                                 
                                 NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.responseWebView.frame.size.width-10))];
                                 
                                 [self.responseWebView loadHTMLString:[responseDict objectForKey:@"Message"] baseURL:nil];
                                 
                                 [[HomeViewController sharedViewController] globalLabel:@"0"];
                                 
                                 [ApplicationDelegate removeProgressHUD];
                                 
                             }
                             else
                             {
                                 [ApplicationDelegate showAlertWithMessage:[responseDict objectForKey:@"Message"] title:nil];
                                 
                                 [ApplicationDelegate removeProgressHUD];
                             }
                             
                             [ApplicationDelegate removeProgressHUD];
                         }
                     }
                 }
             } errorHandler:^(NSError *error)
             {
                 [ApplicationDelegate removeProgressHUD];
                 [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
             }];
        }
        else if (self.knetBtn.selected) {
            
            BillingAddressViewController * billingAddressVC = [[BillingAddressViewController alloc]initWithNibName:@"BillingAddressViewController" bundle:nil];
            billingAddressVC.view.backgroundColor = [UIColor clearColor];
            billingAddressVC.SubTotal.text=self.SubTotal.text;
            billingAddressVC.DeliveryCharge.text=self.DeliveryCharge.text;
            billingAddressVC.TotalCharge.text=self.TotalCharge.text;
            billingAddressVC.orderId = self.orderId;
            billingAddressVC.paymentMethod = self.paymentMethod;
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            billingAddressVC.UserId = [prefs objectForKey:@"User Id"];
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[BillingAddressViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:billingAddressVC animated:NO];
            }
        }
        else if (self.visaBtn.selected) {
            BillingAddressViewController * billingAddressVC = [[BillingAddressViewController alloc]initWithNibName:@"BillingAddressViewController" bundle:nil];
            billingAddressVC.view.backgroundColor = [UIColor clearColor];
            billingAddressVC.SubTotal.text=self.SubTotal.text;
            billingAddressVC.DeliveryCharge.text=self.DeliveryCharge.text;
            billingAddressVC.TotalCharge.text=self.TotalCharge.text;
            billingAddressVC.orderId = self.orderId;
            billingAddressVC.paymentMethod = self.paymentMethod;
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            billingAddressVC.UserId = [prefs objectForKey:@"User Id"];
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[BillingAddressViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:billingAddressVC animated:NO];
            }
        }
        else if (self.masterBtn.selected) {
            BillingAddressViewController * billingAddressVC = [[BillingAddressViewController alloc]initWithNibName:@"BillingAddressViewController" bundle:nil];
            billingAddressVC.view.backgroundColor = [UIColor clearColor];
            billingAddressVC.SubTotal.text=self.SubTotal.text;
            billingAddressVC.DeliveryCharge.text=self.DeliveryCharge.text;
            billingAddressVC.TotalCharge.text=self.TotalCharge.text;
            billingAddressVC.orderId = self.orderId;
            billingAddressVC.paymentMethod = self.paymentMethod;
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            billingAddressVC.UserId = [prefs objectForKey:@"User Id"];
            
            if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[BillingAddressViewController class]])
            {
                [ApplicationDelegate.homeTabNav pushViewController:billingAddressVC animated:NO];
            }
        }
        else if (self.americanBtn.selected) {
            
        }
        
    } else
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please select a payment method") title:nil];
    }
}

-(NSMutableDictionary *)getUpdatePurchaseData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * sampDelChar = self.DeliveryCharge.text;
    sampDelChar = [sampDelChar stringByReplacingOccurrencesOfString:@" " withString:@""];
    sampDelChar = [sampDelChar stringByReplacingOccurrencesOfString:@"KD" withString:@""];
    [postDic setObject:sampDelChar forKey:@"DeliveryCharges"];
    [postDic setObject:self.orderId forKey:@"OrderId"];
    [postDic setObject:self.paymentMethod forKey:@"PaymentMethod"];
    NSString * sampSubTot = self.SubTotal.text;
    sampSubTot = [sampSubTot stringByReplacingOccurrencesOfString:@" " withString:@""];
    sampSubTot = [sampSubTot stringByReplacingOccurrencesOfString:@"KD" withString:@""];
    [postDic setObject:sampSubTot forKey:@"SubTotal"];
    NSString * sampTotal = self.TotalCharge.text;
    sampTotal = [sampTotal stringByReplacingOccurrencesOfString:@" " withString:@""];
    sampTotal = [sampTotal stringByReplacingOccurrencesOfString:@"KD" withString:@""];
    [postDic setObject:sampTotal forKey:@"Total"];
    [postDic setObject:[prefs objectForKey:@"User Id"] forKey:@"UserId"];
    return postDic;
}

- (IBAction)backToHomeBtnActn:(UIButton *)sender
{
    [ApplicationDelegate.homeTabNav popToRootViewControllerAnimated:NO];
}

- (IBAction)selectPaymentMethodActn:(UIButton *)sender
{
    if (sender.tag == 1)
    {
        self.paymentMethod = @"KNET";
        self.knetBtn.selected = YES;
        self.visaBtn.selected = NO;
        self.masterBtn.selected = NO;
        self.americanBtn.selected = NO;
        self.cashOnDelBtn.selected = NO;
    }
    else if (sender.tag == 2)
    {
        self.paymentMethod = @"VISA";
        self.knetBtn.selected = NO;
        self.visaBtn.selected = YES;
        self.masterBtn.selected = NO;
        self.americanBtn.selected = NO;
        self.cashOnDelBtn.selected = NO;
    }
    else if (sender.tag == 3)
    {
        self.paymentMethod = @"MC";
        self.knetBtn.selected = NO;
        self.visaBtn.selected = NO;
        self.masterBtn.selected = YES;
        self.americanBtn.selected = NO;
        self.cashOnDelBtn.selected = NO;
    }
    else if (sender.tag == 4)
    {
        self.paymentMethod = @"AEC";
        self.knetBtn.selected = NO;
        self.visaBtn.selected = NO;
        self.masterBtn.selected = NO;
        self.americanBtn.selected = YES;
        self.cashOnDelBtn.selected = NO;
    }
    else if (sender.tag == 5)
    {
        self.paymentMethod = @"COD";
        self.knetBtn.selected = NO;
        self.visaBtn.selected = NO;
        self.masterBtn.selected = NO;
        self.americanBtn.selected = NO;
        self.cashOnDelBtn.selected = YES;
    }
}

-(void)xibLoading
{
    NSString *nibName = @"PaymentMethodViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

-(void) fontIntilization
{
    [self.totalLbl setFont:EBRI_BOLD_FONT(17)];
    
    [self.TotalCharge setFont:EBRI_BOLD_FONT(17)];
    
    [self.subTotalLbl setFont:EBRI_NORMAL_FONT(16)];
    
    [self.SubTotal setFont:EBRI_NORMAL_FONT(16)];
    
    [self.deliveryChargesLbl setFont:EBRI_NORMAL_FONT(16)];
    
    [self.DeliveryCharge setFont:EBRI_NORMAL_FONT(16)];
}

@end
