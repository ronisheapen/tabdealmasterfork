//
//  BuyViewController.h
//  TabDeal
//
//  Created by satheesh on 9/22/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserBasketData.h"
#import "MyBasketCell.h"

@interface BuyViewController : UIViewController<BasketItemDelegate>
{
    CGRect scrollFrame;
    NSUserDefaults * actionTracking;
}
@property (weak, nonatomic) IBOutlet UIButton *proceedToPayBtn;
@property (strong, nonatomic) NSMutableArray * buyViewDataArray;
- (IBAction)proceedToPayBtnActn:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *BasketScrollView;
@property (strong, nonatomic) IBOutlet UIView *proceedView;
@property (strong, nonatomic) IBOutlet UILabel *SubTotal;
@property (strong, nonatomic) IBOutlet UILabel *DeliveryCharge;
@property (strong, nonatomic) IBOutlet UILabel *TotalCharge;
@property (strong, nonatomic) IBOutlet UIView *tableBackView;

@property (strong, nonatomic)NSString * orderId;

- (IBAction)tableCloseBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *tableBackScrollView;
@property (weak, nonatomic) IBOutlet UITableView *sizeTableView;
@property (weak, nonatomic) IBOutlet UIView *tenPlusBackView;
@property (weak, nonatomic) IBOutlet UITextField *tenPlusSizeTxtField;
@property (weak, nonatomic) IBOutlet UIButton *tenPlusSubmitBtn;
- (IBAction)tenPlusSubmitBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;
@property (weak, nonatomic) IBOutlet UIView *downView;
@property (weak, nonatomic) IBOutlet UIImageView *warningBackView;
@property (weak, nonatomic) IBOutlet UILabel *warningLbl;
@property (weak, nonatomic) IBOutlet UILabel *headdingLbl;

@property (weak, nonatomic) IBOutlet UILabel *subTotalLbl;
@property (weak, nonatomic) IBOutlet UILabel *deliveryChargesLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalLbl;

@property (weak, nonatomic) IBOutlet UILabel *selectQuantityLbl;

@end
