//
//  CreateNewAccountViewController.m
//  TabDeal
//
//  Created by satheesh on 9/17/15.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "CreateNewAccountViewController.h"


@interface CreateNewAccountViewController (){
    LoginViewController * loginViewObj;
}

@end

@implementation CreateNewAccountViewController

-(void)viewWillAppear:(BOOL)animated {
    
    self.createNewAccountHeadingLbl.text = localize(@"CREATE NEW ACCOUNT");
    self.signUpLbl.text = localize(@"SIGN UP");
    self.firstNameTextView.placeholder = localize(@"First Name");
    self.lastNameTextView.placeholder = localize(@"Last Name");
    self.emailTextView.placeholder = localize(@"Enter Email");
    self.passwordTextView.placeholder = localize(@"Enter Password");
    self.confirmPassTextView.placeholder = localize(@"Confirm Password");
    self.codeTextView.placeholder = localize(@"(Code)");
    self.mobileNumberTextView.placeholder = localize(@"(Mobile Number)");
    [self.SignUpBttn setTitle:localize(@"SIGN UP") forState:UIControlStateNormal];
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    self.createNewAccountHeadingLbl.text = localize(@"CREATE NEW ACCOUNT");
    self.signUpLbl.text = localize(@"SIGN UP");
    self.firstNameTextView.placeholder = localize(@"First Name");
    self.lastNameTextView.placeholder = localize(@"Last Name");
    self.emailTextView.placeholder = localize(@"Enter Email");
    self.passwordTextView.placeholder = localize(@"Enter Password");
    self.confirmPassTextView.placeholder = localize(@"Confirm Password");
    self.codeTextView.placeholder = localize(@"(Code)");
    self.mobileNumberTextView.placeholder = localize(@"(Mobile Number)");
    [self.SignUpBttn setTitle:localize(@"SIGN UP") forState:UIControlStateNormal];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xibLoading];
    
    self.createNewAccountHeadingLbl.text = localize(@"CREATE NEW ACCOUNT");
    self.signUpLbl.text = localize(@"SIGN UP");
    self.firstNameTextView.placeholder = localize(@"First Name");
    self.lastNameTextView.placeholder = localize(@"Last Name");
    self.emailTextView.placeholder = localize(@"Enter Email");
    self.passwordTextView.placeholder = localize(@"Enter Password");
    self.confirmPassTextView.placeholder = localize(@"Confirm Password");
    self.codeTextView.placeholder = localize(@"(Code)");
    self.mobileNumberTextView.placeholder = localize(@"(Mobile Number)");
    [self.SignUpBttn setTitle:localize(@"SIGN UP") forState:UIControlStateNormal];
    
    scrollFrame = self.createNewBackView.frame;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.mobileNumberTextView.inputAccessoryView = numberToolbar;
    
    [self.mainScroll setContentOffset:CGPointZero];
    self.mainScroll.contentSize = CGSizeMake(self.mainScroll.frame.size.width,575);
    
    UIToolbar* numberToolbarCode = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarCode.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarCode.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadCode)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadCode)]];
    [numberToolbarCode sizeToFit];
    self.codeTextView.inputAccessoryView = numberToolbarCode;
    
    self.createNewBackView.layer.cornerRadius = 4;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.mobileNumberTextView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.codeTextView];
    
    self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    self.navigationController.navigationBarHidden = YES;
    self.createNewBackView.layer.cornerRadius = 4;
    self.firstBackView.layer.cornerRadius = 3;
    self.lastBackView.layer.cornerRadius = 3;
    self.emailBackView.layer.cornerRadius = 3;
    self.passwordBackView.layer.cornerRadius = 3;
    self.confirmPasswordBackView.layer.cornerRadius = 3;
    self.mobileNumBackView.layer.cornerRadius = 3;
    // Do any additional setup after loading the view from its nib.
    
    [self registerForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)xibLoading
{
    NSString *nibName = @"CreateNewAccountViewController";
    
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    self.view = aView;
}

- (IBAction)signUpBtnAction:(UIButton *)sender {
    
    if ((self.firstNameTextView.text.length == 0) && (self.lastNameTextView.text.length == 0) && (self.emailTextView.text.length == 0) && (self.passwordTextView.text.length == 0) && (self.confirmPassTextView.text.length == 0) &&(self.mobileNumberTextView.text.length == 0))
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your details") title:localize(@"Sorry..!")];
        
    } else if(self.firstNameTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your First name") title:localize(@"Sorry..!")];
    }
    else if(self.emailTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter your email id") title:localize(@"Sorry..!")];
    }
    else if(self.passwordTextView.text.length == 0)
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Please enter a password") title:localize(@"Sorry..!")];
    }
    else
    {
        if (![self NSStringIsValidEmail:self.emailTextView.text])
        {
            [ApplicationDelegate showAlertWithMessage:localize(@"Please enter a valid email id") title:localize(@"Sorry..!")];
        }else if(![self.passwordTextView.text isEqualToString:self.confirmPassTextView.text])
        {
            [ApplicationDelegate showAlertWithMessage:localize(@"Confirmation password is wrong") title:localize(@"Sorry..!")];
        }
        else
        {
            [self registerUser];
        }
    }
}

- (IBAction)backBttnActn:(UIButton *)sender
{
    if (!(self.navigationController))
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

-(NSMutableDictionary *)getUserSignUpPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.firstNameTextView.text forKey:@"FirstName"];
    [postDic setObject:self.lastNameTextView.text forKey:@"LastName"];
    [postDic setObject:self.emailTextView.text forKey:@"Email"];
    [postDic setObject:self.passwordTextView.text forKey:@"Password"];
    [postDic setObject:self.codeTextView.text forKey:@"MobCode"];
    [postDic setObject:self.mobileNumberTextView.text forKey:@"MobileNumber"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:kARABIC forKey:@"LanguageKey"];
    }
    else{
        [postDic setObject:kENGLISH forKey:@"LanguageKey"];
    }
    return postDic;
}

#pragma NSURLConnection Delegates

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

- (IBAction)textViewDidSelActn:(UITextField *)sender
{
    if (sender.tag == 1)
    {
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 2)
    {
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 3)
    {
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 4)
    {
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 5)
    {
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    }
    else if (sender.tag == 6)
    {
        self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
        self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.firstNameTextView.text = nil;
    self.lastNameTextView.text = nil;
    self.emailTextView.text = nil;
    self.passwordTextView.text = nil;
    self.confirmPassTextView.text = nil;
    self.mobileNumberTextView.text = nil;
    if (buttonIndex == 0)
    {
        //Code for OK button
    }
    if (buttonIndex == 1)
    {
        //Code for download button
    }
    alrtView.tag = 0;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)registerUser
{
    NSMutableDictionary *signUpData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserSignUpPostData]];
    
    self.firstBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.lastBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.passwordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.confirmPasswordBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    self.mobileNumBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_UNSELECTED_C0lOR];
    
    if (signUpData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine RegisterUserWithPostDataDictionary:signUpData CompletionHandler:^(NSMutableDictionary *responseDic)
         {
             if ([ApplicationDelegate isValid:responseDic])
             {
                 if (responseDic.count>0)
                 {
                     if ([[responseDic objectForKey:@"Success"] integerValue])
                     {
                         self.firstNameTextView.text=@"";
                         self.lastNameTextView.text=@"";
                         self.emailTextView.text=@"";
                         self.passwordTextView.text=@"";
                         self.confirmPassTextView.text=@"";
                         self.mobileNumberTextView.text=@"";
                         
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:@"Message"];
                         
                         loginViewObj = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
                         if (!(self.navigationController))
                         {
                             [self dismissViewControllerAnimated:NO completion:nil];
                         }
                         else
                         {
                             [self.navigationController pushViewController:loginViewObj animated:NO];
                         }
                         [ApplicationDelegate removeProgressHUD];
                     }
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDic objectForKey:@"Message"] title:nil];
                         
                         [ApplicationDelegate removeProgressHUD];
                         self.emailBackView.backgroundColor = [UIColor colorWithHex:kUNIVERSAL_SELECTED_C0lOR];
                     }
                 }
             }
             
         } errorHandler:^(NSError *error) {
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
             
         }];
    }
}

- (void)limitTextField:(NSNotification *)note
{
    int limit = 10;
    int codeLimit = 3;
    if (self.mobileNumberTextView.text.length > limit)
    {
        [self.mobileNumberTextView setText:[self.mobileNumberTextView.text substringToIndex:limit]];
    }
    else if(self.codeTextView.text.length > codeLimit)
    {
        [self.codeTextView setText:[self.codeTextView.text substringToIndex:codeLimit]];
    }
}

-(void)cancelNumberPad
{
    [ self.mobileNumberTextView resignFirstResponder];
    self.mobileNumberTextView.text = @"";
}

-(void)doneWithNumberPad
{
    [self.mobileNumberTextView resignFirstResponder];
}

-(void)cancelNumberPadCode
{
    [ self.codeTextView resignFirstResponder];
    self.codeTextView.text = @"";
}

-(void)doneWithNumberPadCode
{
    [self.codeTextView resignFirstResponder];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.mainScroll.contentInset = contentInsets;
    self.mainScroll.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.createNewBackView.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.firstNameTextView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.firstNameTextView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.lastNameTextView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.lastNameTextView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.emailTextView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.emailTextView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.passwordTextView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.passwordTextView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.confirmPassTextView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.confirmPassTextView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.codeTextView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.codeTextView.frame animated:YES];
    }
    else if (!CGRectContainsPoint(aRect, self.mobileNumberTextView.frame.origin) )
    {
        [self.mainScroll scrollRectToVisible:self.mobileNumberTextView.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.mainScroll.contentSize = CGSizeMake(self.mainScroll.frame.size.width, scrollFrame.size.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainScroll.contentInset = contentInsets;
    self.mainScroll.scrollIndicatorInsets = contentInsets;
}

- (IBAction)languageSelection:(id)sender
{
    ApplicationDelegate.changeLanguage;
    
    [self viewDidLoad];
}


@end
