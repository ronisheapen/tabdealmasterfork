//
//  TD_Constants.h
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#ifndef TabDeal_TD_Constants_h
#define TabDeal_TD_Constants_h
#define EBRI_BOLD_FONT(_size_) [UIFont fontWithName:@"Ebrima-Bold" size:_size_]
#define EBRI_NORMAL_FONT(_size_) [UIFont fontWithName:@"Ebrima" size:_size_]
#define kUNIVERSAL_THEME_COLOR 0xc88f22
#define kUNIVERSAL_SELECTED_C0lOR 0xB87BBF
#define kUNIVERSAL_UNSELECTED_C0lOR 0xDDE0E1
#define kUNIVERSAL_Imagebadge_C0lOR 0x850793
#define kUNIVERSAL_BORDER_C0lOR 0xd5a24e
#define kUNIVERSAL_VIOLET_COLOR 0x6b064e
#define kUNIVERSAL_BROWN_COLOR 0x270c32
#define kUNIVERSAL_Button_COLOR 0x750581
#define setpushtoken(str) ([[NSUserDefaults standardUserDefaults] setObject:(str) forKey:@"pushtoken"])
#define getpushtoken ([[NSUserDefaults standardUserDefaults] stringForKey:@"pushtoken"])
#define setuser(str) ([[NSUserDefaults standardUserDefaults] setObject:(str) forKey:@"userID"])
#define getuser ([[NSUserDefaults standardUserDefaults] stringForKey:@"userID"])
//#define kUNIVERSAL_RELATED_BORDER 0xA5875E

#define kENGLISH @"EN"
#define kARABIC @"AR"

#define kFrom_Currency_Code @"KWD"
#define kTo_Currency_USD @"USD"
#define kTo_Currency_SAR @"SAR"
#define kTo_Currency_EUR @"EUR"
#define kTo_Currency_GBP @"GBP"

//http://service.tabdeal.net/API.svc/

#define kHOST_URL @"service.tabdeal.net/API.svc"

//#define kHOST_URL @"tabdeal.mawaqaademo.com/service/API.svc"
#define kSignUp_URL @"SignUp"
#define kSignIn_URL @"SignIn"
#define kForgetPassword_URL @"FogotPassword"
#define kFacebookSignIn_URL @"FacebookSignIn"
#define kTwitterSignIn_URL @"TwitterSignInNew"
#define kCurrencyConvertor_URL @"CurrencyConvertor"
#define kNewDeals_URL @"NewDeals"
#define kUpdatePurchase_URL @"UpdatePurchase"
#define kUpdatePurchaseOnComplete_URL @"UpdatePurchaseOnComplete"
#define kAboutTabdeal_URL @"AboutTabdeal"
#define kDealsDetails_URL @"DealsDetail"
#define kAddToBasket_URL @"AddToBasket"
#define kAddToFavourate_URL @"AddToFavourate"
#define kDeleteBasketItem_URL @"DeleteBasketItem"
#define kUsersBasket_URL @"UsersBasket"
#define kEditAddress_URL @"EditAddress"
#define kUpdate_bank_details_URL @"Update_bank_details"
#define kUpdateNotificationStatus_URL @"UpdateNotificationStatus"
#define kChangePassword_URL @"ChangePassword"
#define kEditBasketItem_URL @"EditBasketItem"
#define kGetCountry_URL @"GetCountry"
#define kGetBrand_URL @"Brand"
#define kMyAccount_URL @"MyAccount"
#define kget_bank_details_URL @"get_bank_details"
#define kGetSaleParameters_URL @"GetSaleParameters"
#define kGetAllCurrencies_URL @"GetAllCurrencies"
#define kEditSale_URL @"EditSale"
#define kDeleteFavItem_URL @"DeleteFavouriteItem"
#define  kPushNotificaitonURL @"InsertDeviceToken"
#define kUsersFavourite_URL @"GetFavourateItem"
#define kSERVER_ERROR_MSG @"Server error occurred. Please try later."
#define kERROR_MSG @"Something went wrong. Please try later."
#define kFAILED_ERROR_MSG @"Request submission failed. Please try later."
#define kUsersPreviousOrder_URL @"PreviousOrder"
#define kGetPushNotificationList_URL @"GetPushNotificationList"
#define kUsersPreviousOrderDetails_URL @"DealsByOrder"
#define kUpdateUserAccountURL @"Update_user_account"
#define kUpdatePersonalInfoURL @"UpdatePersonalInfo"
#define kUpdateDeliveryAddressURL @"UpdateDeliveryAddress"
#define kSearchURL @"Search"
#define kSellMoreInfoURL @"MoreInfo"
#define kSellTCURL @"TermsAndCondition"
#define kSellImageUploadURL @"Sale"
#define kSubmittedItemsURL @"SubmittedItems"
#define kSellUpdateURL @"UpdateSale"
#define kInsertPaymentHistoryURL @"InsertPaymentHistory"
#endif
