//
//  LaunchingViewController.h
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
@interface LaunchingViewController : UIViewController{
    NSTimer * progressTimer;
}
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property(strong , nonatomic)NSTimer * progressTimer;
@end
