//
//  LaunchingViewController.m
//  TabDeal
//
//  Created by Sreeraj VR on 17/09/2015.
//  Copyright (c) 2015 mawaqaa. All rights reserved.
//

#import "LaunchingViewController.h"
#import "LoginViewController.h"


@interface LaunchingViewController (){
    LoginViewController * loginViewObj;
    HomeViewController * homePageViewObj;
}

@end

@implementation LaunchingViewController
@synthesize progressTimer;
@synthesize progressBar;

-(void)viewWillAppear:(BOOL)animated{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    [self.progressBar setTransform:CGAffineTransformMakeScale(1.0, 3.3)];
    self.progressBar.layer.cornerRadius = 6.0f;
    
    self.progressTimer = [NSTimer scheduledTimerWithTimeInterval: 0.03f
                                             target: self
                                           selector: @selector(updateTimer)
                                           userInfo: nil
                                            repeats: YES];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateTimer
{
    if (self.progressBar.progress >= 1.0) {
        [self.progressTimer invalidate];
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"loginflag"] isEqualToString:@"login"]) {
            
            homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
            [self.navigationController pushViewController:homePageViewObj animated:NO];
            
        }
        
        else{
            loginViewObj = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
            [self.navigationController pushViewController:loginViewObj animated:NO];
        }
        
       }
    [UIView animateWithDuration:1 animations:^{
        float newProgress = [self.progressBar progress] + 0.005;
        [self.progressBar setProgress:newProgress animated:YES];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
